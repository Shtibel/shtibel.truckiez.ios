//
//  LoginSettings.h
//  truckiez
//
//  Created by Menachem Mizrachi on 26/01/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginSettings : NSObject
-(int)loginSetting:(NSString *)phone :(NSString *)password;
-(BOOL)sendPhone:(NSString*)phoneNumber;
@end

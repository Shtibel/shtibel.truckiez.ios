//
//  Wellcome.h
//  truckies
//
//  Created by Menachem Mizrachi on 08/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Wellcome : UIViewController<UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *splashScreen;
@property (strong, nonatomic) IBOutlet UIImageView *truckiesLogo;
- (IBAction)capture:(id)sender;
- (IBAction)skip:(id)sender;
@property NSString *imgName;
@property (strong, nonatomic) IBOutlet UIButton *captureButton;

@end

//
//  PushNotificationCell.h
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MGSwipeTableCell;
@interface PushNotificationCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UILabel *cellContent;
@property (strong, nonatomic) IBOutlet UILabel *cellDate;

@property (strong, nonatomic) IBOutlet UIView *top;
@property (strong, nonatomic) IBOutlet UIView *top2;

@end

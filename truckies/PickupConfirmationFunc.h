//
//  PickupConfirmationFunc.h
//  truckies
//
//  Created by Menachem Mizrachi on 28/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "typeObject.h"
#import "JSON.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface PickupConfirmationFunc : NSObject

-(NSMutableArray *)loadTypeFromServer;
-(NSString*)arrString:(NSMutableArray *)typeArr :(NSMutableArray *)quantityArr :(NSMutableArray *)typeArrFromServer;
-(BOOL)checkQuantity:(NSMutableArray *)typeArr :(NSMutableArray *)quantityArr;
@end

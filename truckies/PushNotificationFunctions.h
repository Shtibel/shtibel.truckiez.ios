//
//  PushNotificationFunctions.h
//  truckies
//
//  Created by Menachem Mizrachi on 23/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "offerObject.h"
#import "shipmentObject.h"

@interface PushNotificationFunctions : UIViewController

@property (strong, nonatomic) UIWindow *window;

-(NSDictionary*)returnOfferAnswer:(NSInteger)offerID;
-(offerObject *)returnOffer:(NSDictionary *)key;

-(NSDictionary*)returnJobAnswer:(NSInteger)jobID;
-(shipmentObject *)returnJob:(NSDictionary *)key;
-(void)openPush:(NSString*)type :(NSInteger)messageID;

@end

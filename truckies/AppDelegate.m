;//
//  AppDelegate.m
//  location
//
//  Created by Menachem Mizrachi on 01/05/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//
// Task: Move keys to constants file
// Task: Clear the work with NSUserDefaults except 1 place
// Task: support IPhone6Plus-IPhone7 plus

// Task: Move external APIs (like GMSServices, GMSPlacesClient, FIRApp)
// Ques: Is JSON is Network Class?
// Task: Move images to assets
// Ques: Does Launch Screen change the image every time
// TASK: ADD DEVELOPER SCREN
// Task: Renmae classes   (like UserAccount-> UserAccount(view) UserAccount(VC) UserAccount(Data holder)
//                          (like Offers-> Offers(view) Offers(VC) Offers(Data holder)
// Terms,welcome,login,forgot password
// Task: 'Location mananger' or 'Location' - who is the manager?
// Task: Separate all the logic from UI (work with server to seperate classes)
// Task: manage the location state, network state
// Task Battery issues
// Task: startapp time
// Task: unused libs

#import "AppDelegate.h"
@import Firebase;
@import FirebaseMessaging;
@import FirebaseInstanceID;
@import UserNotifications;
@import GoogleMaps;
@import GooglePlaces;
@import CocoaLumberjack;
@import Crashlytics;
@import Fabric;

#import "JSON.h"
#import "UIDeviceHardware.h"
#import <sys/utsname.h>
#import "mapsControllerFunc.h"
#import "dbFunctions.h"
#import "StrippingHTML.h"
#import "PushNotification.h"
#import "PushNotificationFunctions.h"
#import "StatusArray.h"
#import "InternetConnection.h"
#import "LocationObject.h"
#import "DesktopFunc.h"
#import "Jobs.h"
#import "Login.h"
#import "Location.h"


@interface AppDelegate ()
{
    JSON *_json;
    mapsControllerFunc *_mapFunc;
    dbFunctions *_dbFunctions;
    StrippingHTML *_html;
    PushNotificationFunctions *_pushFunc;
    StatusArray *_status;
    InternetConnection *_InternetConnection;
    LocationObject *_location;
    DesktopFunc *_desktopFunc;
    
    NSDictionary *userInfoToAlert;
    
    //CLLocationManager *locationManager;
    CLGeocoder *geocoder;
}

@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;

@end


NSString *const SubscriptionTopic = @"/topics/global";
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //debugger
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    //    fileLogger.fi
    [DDLog addLogger:fileLogger];
    
    [Fabric with:@[[Crashlytics class]]];
    
    [self logUser];
    
    [GMSServices provideAPIKey:@"AIzaSyDUDC8ocPMMeUhXmyGQ8hYfVntz3l9ZcnY"];
    //googlle places
    [GMSPlacesClient provideAPIKey:@"AIzaSyDUDC8ocPMMeUhXmyGQ8hYfVntz3l9ZcnY"];
    
    //////FCM
    [FIRApp configure];
    
    //NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    //[self setIdToServer:refreshedToken];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreashCallback:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        
        //Bary: Move all the logic code to seperate class
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] != nil) {
                
                DDLogVerbose(@"get can see price from server");
                //get can see price
                _json = [[JSON alloc] init];
                NSData *myData = [[NSData alloc] init];
                myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateUserDetails&user_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"]]];
                
                if (myData) {
                    
                    _json = [[JSON alloc] init];
                    NSDictionary *dic = [[NSDictionary alloc] init];
                    dic = [_json FromJson:myData];
                    
                    NSString *str;
                    if ([dic objectForKey:@"can_see_price"]== (id)[NSNull null]) {
                        str = @"0";
                    }else{
                        str = [dic objectForKey:@"can_see_price"];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"can_see_price"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    DDLogVerbose(@"save can see price on default");
                }
                
                
                //get rating
                
                DDLogVerbose(@"get rating from server");
                _json = [[JSON alloc] init];
                myData = [[NSData alloc] init];
                myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getRating&user_id=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
                
                if (myData) {
                    
                    _json = [[JSON alloc] init];
                    NSDictionary *dic = [[NSDictionary alloc] init];
                    dic = [_json FromJson:myData];
                    
                    NSString *avg_rating;
                    if([dic objectForKey:@"avg_rating"] == (id)[NSNull null]){
                        
                        avg_rating = @"0.0";
                        [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"avg_rating"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }else{
                        avg_rating = [dic objectForKey:@"avg_rating"];
                        [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"avg_rating"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                    NSString *number_of_voters;
                    if([dic objectForKey:@"number_of_voters"] == (id)[NSNull null]){
                        
                        number_of_voters = @"0";
                        [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"number_of_voters"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }else{
                        number_of_voters = [dic objectForKey:@"number_of_voters"];
                        [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"number_of_voters"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    DDLogVerbose(@"save rating on default");
                    
                }
                
                
            }
            
            //get last version
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:@"tr-api/?action=getLastVersion&version_id=2"];
            
            if (myData) {
                
                _json = [[JSON alloc] init];
                NSDictionary *dic = [[NSDictionary alloc] init];
                dic = [_json FromJson:myData];
                
                NSString *str = @"";
                if ([dic objectForKey:@"lastVersion"]!= (id)[NSNull null]) {
                    str = [dic objectForKey:@"lastVersion"];
                }
                NSString *str2 = @"";
                if ([dic objectForKey:@"lastTestingVersion"]!= (id)[NSNull null]) {
                    str2 = [dic objectForKey:@"lastTestingVersion"];
                }
                
                
                bool ver;
                NSString *currentVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
                if ([str isEqualToString:currentVersion] || [str2 isEqualToString:currentVersion]) {
                    ver = YES;
                }else{
                    ver = NO;
                }
                
                
                [[NSUserDefaults standardUserDefaults] setBool:ver forKey:@"right_version"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        });
    }
    
    //check if first time
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkFirstTime"] == nil)
    {
        DDLogVerbose(@"check First Time");
        application.applicationIconBadgeNumber = 0;
        NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
        [badgeDefault setInteger:0 forKey:@"badge"];
        
        DDLogVerbose(@"create tables");
        _dbFunctions = [[dbFunctions alloc] init];
        [_dbFunctions createTables];
        DDLogVerbose(@"end create tables");
        NSString *firstTime = @"second";
        [badgeDefault setObject:firstTime forKey:@"checkFirstTime"];
        
        [badgeDefault setObject:@"0" forKey:@"latitudeLabel"];
        [badgeDefault setObject:@"0" forKey:@"longitudLabel"];
        [badgeDefault synchronize];
        
    }
    
    //get version of app on itunes
    //BOOL checkVersion = [self needsUpdate];
    
    
    NSMutableArray *arrStatus = [[NSMutableArray alloc] init];
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            DDLogVerbose(@"get Status Data from server");
            _json = [[JSON alloc] init];
            
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:@"tr-api/?action=getStatusData"];
            
            NSDictionary *Dic = [[NSDictionary alloc] init];
            if (myData) {
                _json = [[JSON alloc] init];
                Dic = [_json FromJson:myData];
            }
            DDLogVerbose(@"end get Status Data from server");
            dispatch_async(dispatch_get_main_queue(), ^{
                if (myData) {
                    
                    for (NSDictionary *key in Dic) {
                        int status = [[key objectForKey:@"id"] intValue];
                        switch (status) {
                            case 5:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 7:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 11:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 12:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 15:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 16:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 17:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                                break;
                            case 18:
                                [arrStatus addObject:[key objectForKey:@"status_name"]];
                            default:
                                break;
                        }
                        
                    }
                    
                }else{
                    [arrStatus addObject:@"Waiting for pickup"];
                    [arrStatus addObject:@"En route to pickup"];
                    [arrStatus addObject:@"Shipment picked up"];
                    [arrStatus addObject:@"En route to drop off"];
                    [arrStatus addObject:@"Shipment delivered"];
                    [arrStatus addObject:@"Signature received"];
                    [arrStatus addObject:@"BOL received"];
                    [arrStatus addObject:@"Delivered & pending payment"];
                    
                    
                }
            });
        });
    }
    
    StatusArray *globals = [StatusArray sharedInstance];
    globals.statusArr = [[NSMutableArray alloc] init];
    globals.statusArr = arrStatus;
    
    LocationObject *location = [LocationObject sharedInstance];
    location.availableStatus = [[[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"driverStatus"] intValue];
    
    location.getMyLocation = 1;
    
    
    DDLogVerbose(@"timer to check change status all 10 minutes");
    double delayInSeconds = 120.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self checkChanges];
    });
    //check if user data is change
    
    UIBackgroundTaskIdentifier bgTask1;
    UIApplication  *app1 = [UIApplication sharedApplication];
    bgTask1 = [app1 beginBackgroundTaskWithExpirationHandler:^{
        
        [app1 endBackgroundTask:bgTask1];
    }];
    
    // [NSTimer scheduledTimerWithTimeInterval:10*60.0f
    //                                  target:self selector:@selector(checkChanges) userInfo:nil repeats:YES];
    
    
    return YES;
}
- (void) logUser {
    // TODO: Use the current user's information
    // You can call any combination of these three methods
    
    [CrashlyticsKit setUserIdentifier:@"12345"];
    [CrashlyticsKit setUserEmail:@"user@fabric.io"];
    [CrashlyticsKit setUserName:@"Test User"];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //[NSException raise:@"Invalid foo value" format:@"foo of %d is invalid", 88888];
    //    [CrashlyticsKit crash];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    //    [NSException raise:@"Invalid foo value" format:@"foo of %d is invalid", 88888];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
}

//google cloude messaging
-(void)setIdToServer:(NSString *)theID{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            DDLogVerbose(@"set id of google cloude messaging to sever");
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            
            NSString *versionApp = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
            NSString *platform = [UIDeviceHardware platform];
            NSString *platformString = [UIDeviceHardware platformString];
            NSString *iosVersion = [[UIDevice currentDevice] systemVersion];
            
            
            NSString *device = [NSString stringWithFormat:@"device:%@ ios version:%@", platformString, iosVersion];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateDeviceData&user_id=%@&app_gcm_id=%@&app_device_type=%@&app_version=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"], theID, device, versionApp]];
            
        });
    }
}

// [START connect_gcm_service]
- (void)applicationDidBecomeActive:(UIApplication *)application {
    ///FCM
    /*  NSString *refreashToken = [[FIRInstanceID instanceID] token];
     NSLog(@"instance id token: %@",refreashToken);
     if(refreashToken){
     [self connectToFireBase];
     [self setIdToServer:refreashToken];
     }else{
     DDLogVerbose(@"unable to connect to fcm");
     }*/
    
    [self connectToFireBase];
    
    
}

// [END connect_gcm_service]

// [START disconnect_gcm_service]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    ////FCM
    [[FIRMessaging messaging] disconnect];
    DDLogVerbose(@"disconnect from FCM");
}
// [END disconnect_gcm_service]

// [START receive_apns_token]
- (NSString *)stringWithDeviceToken:(NSData *)deviceToken {
    const char *data = [deviceToken bytes];
    NSMutableString *token = [NSMutableString string];
    
    for (NSUInteger i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhX", data[i]];
    }
    
    return [token copy];
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString* deviceTokenStr = [self stringWithDeviceToken:deviceToken];
    
    
#ifdef DEBUG
    DDLogVerbose(@"APN TOKEN ID DEBUG: %@", deviceTokenStr);
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeSandbox];
#else
    DDLogVerbose(@"APN TOKEN ID RELEASE: %@", deviceTokenStr);
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken
                                        type:FIRInstanceIDAPNSTokenTypeProd];
#endif
    
    /*NSString *refreashToken = [[FIRInstanceID instanceID] token];
     NSLog(@"instance id token: %@",refreashToken);
     if(refreashToken){
     [self connectToFireBase];
     [self setIdToServer:refreashToken];
     }else{
     DDLogVerbose(@"unable to connect to fcm");
     }
     */
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if( notificationSettings.types != UIUserNotificationTypeNone) {
        [application registerForRemoteNotifications];
    }
}
// [START receive_apns_token_error]
- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DDLogVerbose(@"Registration for remote notification failed with error: %@", error);
    // [END receive_apns_token_error]
    NSDictionary *userInfo = @{@"error" :error.localizedDescription};
    [[NSNotificationCenter defaultCenter] postNotificationName:_registrationKey
                                                        object:nil
                                                      userInfo:userInfo];
}

// [START ack_message_reception]
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    if (userInfo[@"gcm.message_id"]) {
        DDLogVerbose(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    }
    
    // Print full message.
    DDLogVerbose(@"%@", userInfo);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    DDLogVerbose(@"Notification received: %@", userInfo);
    // Print message ID.
    if (userInfo[@"gcm.message_id"]) {
        DDLogVerbose(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    }
    
    // Print full message.
    DDLogVerbose(@"%@", userInfo);
    
    //    completionHandler(UIBackgroundFetchResultNewData);
    //////*//
    
    //////*//
    ///////////////
    NSString *pushType =  [userInfo objectForKey:@"gcm.notification.type"];
    NSInteger pushID =  [[userInfo objectForKey:@"gcm.notification.id"] integerValue];
    NSString *alertText = [userInfo objectForKey:@"gcm.notification.message"];
    
    NSDictionary *apsDic = [[NSDictionary alloc] init];
    apsDic = [userInfo objectForKey:@"aps"];
    NSDictionary *alertDic = [[NSDictionary alloc] init];
    alertDic = [apsDic objectForKey:@"alert"];
    NSString *alertTitle = [alertDic objectForKey:@"title"];
    
    if ([alertTitle rangeOfString:@"Rating"].location == NSNotFound) {
        
        if ([pushType isEqualToString:@"job"]) {
            
            _dbFunctions = [[dbFunctions alloc] init];
            int checkPush = [_dbFunctions checkIfPushSend:pushID :pushType];
            if (checkPush == 0) {
                
                
                //insert push to table
                _dbFunctions = [[dbFunctions alloc] init];
                [_dbFunctions insertPush:pushID :alertTitle :alertText :pushType];
                
                //if application is open and push send first time
                if ( application.applicationState == UIApplicationStateActive ){
                    
                    userInfoToAlert = userInfo;
                    [self alertTRunningApp:userInfoToAlert];
                    
                }
            }else{
                _dbFunctions = [[dbFunctions alloc] init];
                [_dbFunctions updateJobPush:pushID :alertTitle :alertText :pushType];
                
            }
        }else{
            //if the first time
            _dbFunctions = [[dbFunctions alloc] init];
            int checkPush = [_dbFunctions checkIfPushSend:pushID :pushType];
            if (checkPush == 0) {
                
                
                //insert push to table
                _dbFunctions = [[dbFunctions alloc] init];
                [_dbFunctions insertPush:pushID :alertTitle :alertText :pushType];
                
                
                //if application is open and push send first time
                if ( application.applicationState == UIApplicationStateActive ){
                    
                    userInfoToAlert = userInfo;
                    [self alertTRunningApp:userInfoToAlert];
                    
                }
            }else{
                //if second time
                NSString *pushType =  [userInfo objectForKey:@"gcm.notification.type"];
                NSInteger pushID =  [[userInfo objectForKey:@"gcm.notification.id"] integerValue];
                
                //update read on db
                _dbFunctions = [[dbFunctions alloc] init];
                [_dbFunctions updatePush:pushID:pushType];
                
                //open push
                _pushFunc = [[PushNotificationFunctions alloc] init];
                [_pushFunc openPush:pushType :pushID];
                
            }
            
        }
    }else{
        _dbFunctions = [[dbFunctions alloc] init];
        int checkPush = [_dbFunctions checkIfPushSend:pushID :@"rate"];
        if (checkPush == 0) {
            //insert push to table
            _dbFunctions = [[dbFunctions alloc] init];
            [_dbFunctions insertPush:pushID :alertTitle :alertText :@"rate"];
            
            //if application is open and push send first time
            if ( application.applicationState == UIApplicationStateActive ){
                
                userInfoToAlert = userInfo;
                [self alertTRunningApp:userInfoToAlert];
                
            }
        }else{
            //if second time
            NSInteger pushID =  [[userInfo objectForKey:@"gcm.notification.id"] integerValue];
            
            //update read on db
            _dbFunctions = [[dbFunctions alloc] init];
            [_dbFunctions updatePush:pushID:@"rate"];
            
            //open push
            _pushFunc = [[PushNotificationFunctions alloc] init];
            [_pushFunc openPush:@"rate" :pushID];
            
        }
        
    }
    
    handler(UIBackgroundFetchResultNoData);
}

- (void)handleBackgroundNotification:(NSDictionary *)notification

{
    NSString *pushType =  [notification objectForKey:@"gcm.notification.type"];
    NSInteger pushID =  [[notification objectForKey:@"gcm.notification.id"] integerValue];
    
    //update read on db
    _dbFunctions = [[dbFunctions alloc] init];
    [_dbFunctions updatePush:pushID :pushType];
    
    //open push
    _pushFunc = [[PushNotificationFunctions alloc] init];
    [_pushFunc openPush:pushType :pushID];
}

- (void)alertTRunningApp:(NSDictionary *)notification

{
    NSString *pushType =  [notification objectForKey:@"gcm.notification.type"];
    
    NSDictionary *apsDic = [[NSDictionary alloc] init];
    apsDic = [notification objectForKey:@"aps"];
    NSDictionary *alertDic = [[NSDictionary alloc] init];
    alertDic = [apsDic objectForKey:@"alert"];
    NSString *alertTitle = [alertDic objectForKey:@"title"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:pushType message:alertTitle delegate:self cancelButtonTitle:@"CLOSE" otherButtonTitles:@"OPEN", nil];
    
    
    [alert setTag:1];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 1)
        {
            //if open the push
            [self handleBackgroundNotification:userInfoToAlert];
        }
    }
    
}


// [START upstream_callbacks]
- (void)willSendDataMessageWithID:(NSString *)messageID error:(NSError *)error {
    if (error) {
        // Failed to send the message.
    } else {
        // Will send message, you can save the messageID to track the message
    }
}

- (void)didSendDataMessageWithID:(NSString *)messageID {
    // Did successfully send message identified by messageID
}
// [END upstream_callbacks]

- (void)didDeleteMessagesOnServer {
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
}

//////////FCM
-(void)tokenRefreashCallback:(NSNotification *)notification{
    
    NSString *refreashToken = notification.object;//[[FIRInstanceID instanceID] token];
    NSLog(@"tokenRefreashCallback instance id token: %@",refreashToken);
    if(refreashToken){
        [self connectToFireBase];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] != nil) {
            [self setIdToServer:refreashToken];
        }
    }else{
        DDLogVerbose(@"unable to connect to fcm");
    }
    
    
    
}
-(void)connectToFireBase{
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if(error != nil){
            DDLogVerbose(@"unable to connect to fcm. %@",error);
            DDLogError(@"Error :%@",error);
        }else{
            DDLogVerbose(@"connect to fcm");
        }
    }];
}


-(void)checkChanges{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] != nil) {
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkLogin"] != nil)
            {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    DDLogVerbose(@"check changes");
                    UIDevice *uniqeDevice = [UIDevice currentDevice];
                    
                    NSString  *currentDeviceId = [[uniqeDevice identifierForVendor]UUIDString];
                    NSString *url = [NSString stringWithFormat:@"tr-api/?action=checkChanges&user_id=%@&carrier_id=%@&password=%@&status_id=%@&can_accept_offers=%@&can_see_price=%@&can_see_offers=%@&unique_device_number=%@",
                                     [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], [[NSUserDefaults standardUserDefaults] stringForKey:@"password"], [[NSUserDefaults standardUserDefaults] stringForKey:@"user_status"], [[NSUserDefaults standardUserDefaults] stringForKey:@"can_accept_offers"], [[NSUserDefaults standardUserDefaults] stringForKey:@"can_see_price"], [[NSUserDefaults standardUserDefaults] stringForKey:@"can_see_offers"], currentDeviceId];
                    
                    DDLogVerbose(@"set data to server to check changes");
                    
                    NSString* prevURL = [[NSUserDefaults standardUserDefaults] stringForKey:@"prevURL"];
                    if (prevURL != nil)
                    {
                        if ([prevURL isEqualToString:url] == false)
                        {
                            NSLog(@"URL WAS CHANGES FROM ANY REASON!!! CHECK it");
                            NSLog(@"prev url: \n%@",prevURL);
                            NSLog(@"New  url: \n%@",url);
                            
                        }
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"prevURL" ];
                    
                    
                    
                    
                    _json = [[JSON alloc] init];
                    NSData *myData = [[NSData alloc] init];
                    myData = [_json getDataFrom:url];
                    NSDictionary *dic = [[NSDictionary alloc] init];
                    
                    if (myData) {
                        _json = [[JSON alloc] init];
                        
                        dic = [_json FromJson:myData];
                    }
                    //        dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (myData) {
                        
                        if ([[dic objectForKey:@"err"] boolValue] == YES) {
                            //stop set location
                            LocationObject *location = [LocationObject sharedInstance];
                            [location.locationManager stopUpdatingLocation];
                            
                            //delete all notification
                            _dbFunctions = [[dbFunctions alloc] init];
                            [_dbFunctions deleteAllPush];
                            
                            //set badge 0
                            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                            NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
                            [badgeDefault setInteger:0 forKey:@"badge"];
                            
                            [badgeDefault synchronize];
                            
                            
                            //save nil on default checkLogin
                            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"checkLogin"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"driverStatus"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            //back to login
                            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            
                            Login *login = [storyboard instantiateViewControllerWithIdentifier:@"Login"];
                            
                            self.window.rootViewController = login;
                            [self.window makeKeyAndVisible];
                            
                            UIApplicationState state = [[UIApplication sharedApplication] applicationState];
                            if (state == UIApplicationStateBackground)
                            {
                                //Do checking here.
                                DDLogVerbose(@"in background!!!!!!!!!!!!!!");
                                //set local notification
                                UILocalNotification *notification = [[UILocalNotification alloc] init];
                                notification.fireDate = [[NSDate date] dateByAddingTimeInterval:1];
                                notification.alertBody = @"Your account settings were changed. Please try to login again or contact support";
                                [[UIApplication sharedApplication] scheduleLocalNotification:notification];
                            }else{
                                
                                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Notice" message:@"Your account settings were changed. Please try to login again or contact support" preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction * action) {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                      }];
                                [alert addAction:defaultAction];
                                UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                                alertWindow.rootViewController = [[UIViewController alloc] init];
                                alertWindow.windowLevel = UIWindowLevelAlert + 1;
                                [alertWindow makeKeyAndVisible];
                                [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
                                
                            }
                            
                        }
                    }
                    //        });
                    
                });
            }
        }
    }
}

-(BOOL)needsUpdate{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if ([lookup[@"resultCount"] integerValue] == 1){
        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
        if (![appStoreVersion isEqualToString:currentVersion]){
            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
            return YES;
        }
    }
    return NO;
}

@end

//
//  UIDeviceHardware.h
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDeviceHardware : NSObject
    + (NSString *) platform;
    + (NSString *) platformString;
@end

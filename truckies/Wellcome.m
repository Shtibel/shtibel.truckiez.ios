//
//  Wellcome.m
//  truckies
//
//  Created by Menachem Mizrachi on 08/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//
#import <CoreText/CoreText.h>

#import "Wellcome.h"
#import "AFNetworking.h"
#import "ImagesFunctions.h"
#import "LoaderImg.h"
#import "CustomAlert.h"
#import "StaticVars.h"

@interface Wellcome ()
{
    ImagesFunctions *_imgFunc;
    LoaderImg *_loaderIMG;
    CustomAlert *_alert;
    
    NSLayoutConstraint *truckiesLogoHeigh;
    UIImage *userImage;
}

@end

@implementation Wellcome

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)loadPage{
    [_captureButton.layer setCornerRadius:5.0f];
    
    _splashScreen.image = [UIImage imageNamed:_imgName];
    _splashScreen.clipsToBounds = YES;
    _splashScreen.contentMode = UIViewContentModeScaleAspectFill;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    truckiesLogoHeigh = [NSLayoutConstraint constraintWithItem:_truckiesLogo
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                      constant:(width - 180)/2];
    [self.view addConstraint:truckiesLogoHeigh];
    
}

- (IBAction)capture:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet setTag:1];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickerController.editing = YES;
            imagePickerController.delegate = (id)self;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        if (buttonIndex == 1) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    userImage = [[UIImage alloc] init];
    userImage = image;
    [picker dismissViewControllerAnimated:NO completion:nil];
    [self postImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}

-(void)postImage{
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    UIImage *mainImageView = [[UIImage alloc] init];
    UIImage *test = [[UIImage alloc] init];
    test = userImage;
    
    int rotate = userImage.imageOrientation;
    
    if (rotate == 3 || rotate == 2) {
        _imgFunc = [[ImagesFunctions alloc] init];
        mainImageView = [_imgFunc imageRotatedByDegrees:test deg:90];
    }
    if (rotate == 2) {
        _imgFunc = [[ImagesFunctions alloc] init];
        mainImageView = [_imgFunc imageRotatedByDegrees:test deg:270];
    }
    else{
        mainImageView = test;
    }
    
    NSData *imageToUpload;
    imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.5);
    
    NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=uploadImageDriver&user_id=%@",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]];
    NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:encodedUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageToUpload name:@"file" fileName:@"userImg.jpeg" mimeType:@"image/jpeg.jpeg"];
    } error:nil];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //                          [progressView setProgress:uploadProgress.fractionCompleted];
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [backgroundView removeFromSuperview];
                      if (response) {
                          //save url on
                          NSDictionary *dic= [[NSDictionary alloc] init];
                          dic = responseObject;
                          
                          NSString *str = [dic objectForKey:@"url"];
                          [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"user_image"];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          
                          [self performSegueWithIdentifier:@"openDesktop" sender:self];
                          
                      }else{
                          _alert = [[CustomAlert alloc] init];
                          [_alert alertView:nil :nil :@"Notice" :@"Image was not saved, please try again or skip this step" :@"" :@"OK" :@"" :nil :self.view :0];
                      
                      }
                      
                      
                      
                  }];
    
    [uploadTask resume];
    
}

- (IBAction)skip:(id)sender {
    //show loader
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    [self performSegueWithIdentifier:@"openDesktop" sender:self];
}
@end

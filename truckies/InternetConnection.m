//
//  InternetConnection.m
//  NirApp
//
//  Created by Dan Dushinsky on 17/12/15.
//  Copyright (c) 2015 Dan Dushinsky. All rights reserved.
//

#import "InternetConnection.h"
#import "Reachability.h"
#import "CustomAlert.h"
@import CocoaLumberjack;

@interface InternetConnection ()
{
    CustomAlert *_alert;
    UIView *alert;
    CGFloat widthAlert;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation InternetConnection

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)connected
{
    DDLogVerbose(@"check internet connection");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    DDLogVerbose(@"end check internet connection");
    return networkStatus != NotReachable;
}


-(void)alertConnected:(UIView *)viewController{
    _alert = [[CustomAlert alloc] init];
    [_alert alertView:nil :nil :@"ERROR!" :@"There is a problem connecting to the server. Please check that you are connected to the Internet, or try again later" :@"" :@"CLOSE" :@"" :nil :self.view :0];
}

-(void)closeAlert{
    [alert removeFromSuperview];
}

@end

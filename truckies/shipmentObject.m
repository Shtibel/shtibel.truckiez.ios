//
//  shipmentObject.m
//  location
//
//  Created by Menachem Mizrachi on 04/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "shipmentObject.h"
#import "JSON.h"

@implementation shipmentObject

-(shipmentObject*)returnJob:(NSDictionary *)key{

    shipmentObject *sh = [[shipmentObject alloc]init];
    
    
    sh.shipmentsId = [[key objectForKey:@"id"] integerValue];
    
    if ([key objectForKey:@"status_id"] != (id)[NSNull null]) {
        sh.status_id = [[key objectForKey:@"status_id"] integerValue];
    }else{
        sh.status_id = 0;
    }
    
    if ([key objectForKey:@"driver_notes"] != (id)[NSNull null]) {
        sh.driver_notes = [key objectForKey:@"driver_notes"];
    }else{
        sh.driver_notes = @"";
    }
    
    if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
        sh.load_weight_type = [key objectForKey:@"load_weight_type"];
    }else{
        sh.load_weight_type = @"";
    }
    
    if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
        sh.status_name = [key objectForKey:@"status_name"];
    }else{
        sh.status_name = @"";
    }
    
    if ([key objectForKey:@"status_name_circle_color"] != (id)[NSNull null]) {
        sh.status_name_circle_color = [key objectForKey:@"status_name_circle_color"];
    }else{
        sh.status_name_circle_color = @"";
    }
    
    if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
        sh.origin_lat = [key objectForKey:@"origin_lat"];
    }else{
        sh.origin_lat = @"";
    }
    
    if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
        sh.origin_lng = [key objectForKey:@"origin_lng"];
    }else{
        sh.origin_lng = @"";
    }
    
    if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
        sh.destination_lat = [key objectForKey:@"destination_lat"];
    }else{
        sh.destination_lat = @"";
    }
    
    if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
        sh.destination_lng = [key objectForKey:@"destination_lng"];
    }else{
        sh.destination_lng = @"";
    }
    
    if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
        sh.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
    }else{
        sh.shipment_carrier_payout_text = @"";
    }
    
    if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
        sh.box_num = [[key objectForKey:@"box_num"] integerValue];
    }else{
        sh.box_num = 0;
    }
    
    if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
        sh.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
    }else{
        sh.pallet_num = 0;
    }
    
    if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
        sh.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
    }else{
        sh.truckload_num = 0;
    }
    
    if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
        sh.shipment_special_request = [key objectForKey:@"shipment_special_request"];
    }else{
        sh.shipment_special_request = @"";
    }
    
    if ([key objectForKey:@"shipment_special_request_with_document"]!= (id)[NSNull null]) {
        sh.shipment_special_request_with_document = [key objectForKey:@"shipment_special_request_with_document"];
    }else{
        sh.shipment_special_request_with_document = @"";
    }
    
    
    if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
        sh.pickup_date = [key objectForKey:@"pickup_date"];
    }else{
        sh.pickup_date = @"";
    }
    
    if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
        sh.dropoff_date = [key objectForKey:@"dropoff_date"];
    }else{
        sh.dropoff_date = @"";
    }
    
    if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
        sh.pickup_from_time = [key objectForKey:@"pickup_from_time"];
    }else{
        sh.pickup_from_time = @"";
    }
    
    if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
        sh.pickup_till_time = [key objectForKey:@"pickup_till_time"];
    }else{
        sh.pickup_till_time = @"";
    }
    
    if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
        sh.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
    }else{
        sh.dropoff_from_time = @"";
    }
    
    if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
        sh.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
    }else{
        sh.dropoff_till_time = @"";
    }
    
    if ([key objectForKey:@"recycle"] != (id)[NSNull null]) {
        sh.recycle = [[key objectForKey:@"recycle"] integerValue];
    }else{
        sh.recycle = 0;
    }
    
    if ([key objectForKey:@"driver_id"] != (id)[NSNull null]) {
        sh.driver_id = [[key objectForKey:@"driver_id"] integerValue];
    }else{
        sh.driver_id = 0;
    }
    
    if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
        sh.total_load_weight = [key objectForKey:@"total_load_weight"];
    }else{
        sh.total_load_weight = @"";
    }
    
    if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
        sh.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
    }else{
        sh.total_driving_distance = 0;
    }
    
    
    if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
        sh.origin_address = [key objectForKey:@"origin_address"];
    }else{
        sh.origin_address = @"";
    }
    
    if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
        sh.destination_address = [key objectForKey:@"destination_address"];
    }else{
        sh.destination_address = @"";
    }
    
    if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
        sh.shipment_load = [key objectForKey:@"shipment_load"];
    }else{
        sh.shipment_load = @"";
    }
    
    if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
        sh.comments = [key objectForKey:@"comments"];
    }else{
        sh.comments = @"";
    }
    
    if ([key objectForKey:@"origin_contact_name"]!= (id)[NSNull null]) {
        sh.origin_contact_name = [key objectForKey:@"origin_contact_name"];
    }else{
        sh.origin_contact_name = @"";
    }
    
    if ([key objectForKey:@"origin_contact_phone"]!= (id)[NSNull null]) {
        sh.origin_contact_phone = [key objectForKey:@"origin_contact_phone"];
    }else{
        sh.origin_contact_phone = @"";
    }
    
    if ([key objectForKey:@"destination_contact_name"]!= (id)[NSNull null]) {
        sh.destination_contact_name = [key objectForKey:@"destination_contact_name"];
    }else{
        sh.destination_contact_name = @"";
    }
    
    if ([key objectForKey:@"destination_contact_phone"]!= (id)[NSNull null]) {
        sh.destination_contact_phone = [key objectForKey:@"destination_contact_phone"];
    }else{
        sh.destination_contact_phone = @"";
    }
    
    if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
        sh.pickup_in_time = [key objectForKey:@"pickup_in_time"];
    }else{
        sh.pickup_in_time = @"";
    }
    
    if ([key objectForKey:@"dropoff_in_time"]!= (id)[NSNull null]) {
        sh.dropoff_in_time = [key objectForKey:@"dropoff_in_time"];
    }else{
        sh.dropoff_in_time = @"";
    }
    
    if ([key objectForKey:@"signature_url"]!= (id)[NSNull null]) {
        sh.signature_url = [key objectForKey:@"signature_url"];
    }else{
        sh.signature_url = @"";
    }
    
    if ([key objectForKey:@"signature_url_pickup"]!= (id)[NSNull null]) {
        sh.signature_url_pickup = [key objectForKey:@"signature_url_pickup"];
    }else{
        sh.signature_url_pickup = @"";
    }
    
    if ([key objectForKey:@"signature_receiver_name"]!= (id)[NSNull null]) {
        sh.signature_receiver_name = [key objectForKey:@"signature_receiver_name"];
    }else{
        sh.signature_receiver_name = @"";
    }
    
    if ([key objectForKey:@"bol_url"]!= (id)[NSNull null]) {
        sh.bol_url = [key objectForKey:@"bol_url"];
    }else{
        sh.bol_url = @"";
    }
    
    if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
        sh.shipment_google_root = [key objectForKey:@"shipment_google_root"];
    }else{
        sh.shipment_google_root = @"";
    }
    if ([key objectForKey:@"shipment_complete_date"]!= (id)[NSNull null]) {
        sh.completed_at = [key objectForKey:@"shipment_complete_date"];
    }else{
        sh.completed_at = @"";
    }
    
    if ([key objectForKey:@"truck_type_name"]!= (id)[NSNull null]) {
        sh.truck_type_name = [key objectForKey:@"truck_type_name"];
    }else{
        sh.truck_type_name = @"";
    }
    
    if ([key objectForKey:@"shipper_name"]!= (id)[NSNull null]) {
        sh.shipper_name = [key objectForKey:@"shipper_name"];
    }else{
        sh.shipper_name = @"";
    }
    
    if ([key objectForKey:@"shipper_phone"]!= (id)[NSNull null]) {
        sh.shipper_phone = [key objectForKey:@"shipper_phone"];
    }else{
        sh.shipper_phone = @"";
    }
    
    if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
        sh.original_pickup_date = [key objectForKey:@"original_pickup_date"];
    }else{
        sh.original_pickup_date = @"";
    }
    
    if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
        sh.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
    }else{
        sh.origin_special_site_instructions = @"";
    }
    
    if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
        sh.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
    }else{
        sh.destination_special_site_instructions = @"";
    }
    
    if ([key objectForKey:@"truck_max_weight"]!= (id)[NSNull null]) {
        sh.truck_max_weight = [[key objectForKey:@"truck_max_weight"] intValue];
    }else{
        sh.truck_max_weight = 0;
    }
    
    if ([key objectForKey:@"signature_receiver_name_pickup"]!= (id)[NSNull null]) {
        sh.signature_receiver_name_pickup = [key objectForKey:@"signature_receiver_name_pickup"];
    }else{
        sh.signature_receiver_name_pickup = @"";
    }
    
    if ([key objectForKey:@"signature_receiver_name"]!= (id)[NSNull null]) {
        sh.signature_receiver_name = [key objectForKey:@"signature_receiver_name"];
    }else{
        sh.signature_receiver_name = @"";
    }
    
    if ([key objectForKey:@"confirmation_phone_pickup"]!= (id)[NSNull null]) {
        sh.confirmation_phone_pickup = [key objectForKey:@"confirmation_phone_pickup"];
    }else{
        sh.confirmation_phone_pickup = @"";
    }
    
    if ([key objectForKey:@"confirmation_phone_dropoff"]!= (id)[NSNull null]) {
        sh.confirmation_phone_dropoff = [key objectForKey:@"confirmation_phone_dropoff"];
    }else{
        sh.confirmation_phone_dropoff = @"";
    }
    
    if ([key objectForKey:@"confirmation_user_pickup"]!= (id)[NSNull null]) {
        sh.confirmation_user_pickup = [key objectForKey:@"confirmation_user_pickup"];
    }else{
        sh.confirmation_user_pickup = @"";
    }
    
    if ([key objectForKey:@"confirmation_user_dropoff"]!= (id)[NSNull null]) {
        sh.confirmation_user_dropoff = [key objectForKey:@"confirmation_user_dropoff"];
    }else{
        sh.confirmation_user_dropoff = @"";
    }
    
    if ([key objectForKey:@"confirmation_date_pickup"]!= (id)[NSNull null]) {
        sh.confirmation_date_pickup = [key objectForKey:@"confirmation_date_pickup"];
    }else{
        sh.confirmation_date_pickup = @"";
    }
    
    if ([key objectForKey:@"confirmation_date_dropoff"]!= (id)[NSNull null]) {
        sh.confirmation_date_dropoff = [key objectForKey:@"confirmation_date_dropoff"];
    }else{
        sh.confirmation_date_dropoff = @"";
    }
    
    NSString *str;
    if ([key objectForKey:@"load_types_pickup_str"]!= (id)[NSNull null]) {
        str = [key objectForKey:@"load_types_pickup_str"];
    }else{
        str = @"";
    }
    sh.load_types_pickup_str = [[NSArray alloc] init];
    sh.load_types_pickup_str = [str componentsSeparatedByString:@","];
    
    if ([key objectForKey:@"load_types_pickup_quantity_str"]!= (id)[NSNull null]) {
        str = [key objectForKey:@"load_types_pickup_quantity_str"];
    }else{
        str = @"";
    }
    sh.load_types_pickup_quantity_str = [[NSArray alloc] init];
    sh.load_types_pickup_quantity_str = [str componentsSeparatedByString:@","];
    
    
    if ([key objectForKey:@"load_types_dropoff_str"]!= (id)[NSNull null]) {
        str = [key objectForKey:@"load_types_dropoff_str"];
    }else{
        str = @"";
    }
    sh.load_types_dropoff_str = [[NSArray alloc] init];
    sh.load_types_dropoff_str = [str componentsSeparatedByString:@","];
    
    
    if ([key objectForKey:@"load_types_dropoff_quantity_str"]!= (id)[NSNull null]) {
        str = [key objectForKey:@"load_types_dropoff_quantity_str"];
    }else{
        str = @"";
    }
    sh.load_types_dropoff_quantity_str = [[NSArray alloc] init];
    sh.load_types_dropoff_quantity_str = [str componentsSeparatedByString:@","];
    
    if ([key objectForKey:@"total_weight_pickup"]!= (id)[NSNull null]) {
        sh.total_weight_pickup = [key objectForKey:@"total_weight_pickup"];
    }else{
        sh.total_weight_pickup = @"";
    }
    
    if ([key objectForKey:@"total_weight_dropoff"]!= (id)[NSNull null]) {
        sh.total_weight_dropoff = [key objectForKey:@"total_weight_dropoff"];
    }else{
        sh.total_weight_dropoff = @"";
    }
    
    if ([key objectForKey:@"rating"]!= (id)[NSNull null]) {
        sh.rating = [[key objectForKey:@"rating"] intValue];
    }else{
        sh.rating = 0;
    }
    
    if ([key objectForKey:@"feedback"]!= (id)[NSNull null]) {
        sh.feedback = [key objectForKey:@"feedback"];
    }else{
        sh.feedback = @"";
    }
    
    
    return sh;
}

@end

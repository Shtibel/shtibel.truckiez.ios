//
//  LocationObject.h
//  truckies
//
//  Created by Menachem Mizrachi on 13/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"

@interface LocationObject : NSObject<CLLocationManagerDelegate>

@property NSString *latitude;
@property NSString *longitude;
@property int availableStatus;
@property NSMutableArray *jobsArray;
@property int getMyLocation;

@property CLLocationManager *locationManager;
@property float meters;

+ (LocationObject *)sharedInstance;

@end

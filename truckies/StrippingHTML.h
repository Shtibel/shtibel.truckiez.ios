//
//  StrippingHTML.h
//  NirApp
//
//  Created by Menachem Mizrachi on 28/01/2016.
//  Copyright © 2016 Dan Dushinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrippingHTML : UIViewController

-(NSString *) stringByStrippingHTML:(NSString *)htmlStr;


@end

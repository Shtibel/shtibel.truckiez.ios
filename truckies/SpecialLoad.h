//
//  SpecialLoad.h
//  truckies
//
//  Created by Menachem Mizrachi on 18/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialLoad : UIViewController

@property NSString *imgName;
@property (strong, nonatomic) IBOutlet UIImageView *specialLoadImage;
- (IBAction)back:(id)sender;


@end

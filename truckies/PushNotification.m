//
//  PushNotification.m
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "PushNotification.h"
#import "MGSwipeButton.h"
#import "PushNotificationCell.h"
#import "dbFunctions.h"
#import "PushObject.h"
#import "StrippingHTML.h"
#import "offerObject.h"
#import "PushNotificationFunctions.h"
#import "Offers.h"
#import "Jobs.h"
#import "shipmentObject.h"
#import "CustomAlert.h"
#import "InternetConnection.h"
#import "JSON.h"
#import "LoaderImg.h"

@interface PushNotification ()
{
    dbFunctions *_dbFunctions;
    StrippingHTML *_html;
    NSMutableArray *pushArray;
    offerObject *offer;
    PushNotificationFunctions *_func;
    shipmentObject *job;
    CustomAlert *_alert;
    InternetConnection *_InternetConnection;
    JSON *_json;// Bary: save network as memeber? should work as singelton
    LoaderImg *_loaderIMG;
    
    UIView *alert;
    CGFloat widthAlert;
    
    int checkOpenDrop;
}

@end

@implementation PushNotification

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)loadPage{
    _noPushLabel.hidden = YES;
    _dropDownView.hidden = YES;
    checkOpenDrop = 0;
    _dropDownView.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _dropDownView.layer.borderWidth = 1.0f;
    
    _dbFunctions = [[dbFunctions alloc] init];
    pushArray = [[NSMutableArray alloc] init];
    pushArray = [_dbFunctions selectPush];
    
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"pushDateFormat"
                                        ascending:NO];
    NSMutableArray *sortDescriptors = [[NSMutableArray alloc] init];
    sortDescriptors = [NSMutableArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [pushArray
                                 sortedArrayUsingDescriptors:sortDescriptors];
    pushArray = [NSMutableArray arrayWithArray:sortedEventArray];
    
    widthAlert = [UIScreen mainScreen].bounds.size.width - 60;
    
    if (pushArray.count == 0) {
        _noPushLabel.hidden = NO;
    }
}

//table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return pushArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PushCell";
    
    PushNotificationCell *cell = (PushNotificationCell *)[_tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    PushObject *push = [[PushObject alloc] init];
    push = [pushArray objectAtIndex:indexPath.row];
    
    CGFloat borderWidth;
    
    cell.viewCell.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    cell.viewCell.frame = CGRectInset(cell.viewCell.frame, -borderWidth, -borderWidth);
    cell.viewCell.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    cell.viewCell.layer.borderWidth = borderWidth;
    
    cell.top.layer.cornerRadius = 5.0f;
    
    if (push.isRead == 1) {
        cell.top.backgroundColor = [UIColor colorWithRed:0.88 green:0.89 blue:0.89 alpha:1.0];
        cell.top2.backgroundColor = [UIColor colorWithRed:0.88 green:0.89 blue:0.89 alpha:1.0];
        cell.cellTitle.textColor = [UIColor blackColor];
        cell.cellDate.textColor = [UIColor blackColor];
    }else{
        cell.top.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        cell.top2.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        cell.cellTitle.textColor = [UIColor whiteColor];
        cell.cellDate.textColor = [UIColor whiteColor];
    }
    
    _html = [[StrippingHTML alloc] init];
    NSString *str = [_html stringByStrippingHTML:push.pushText];
    cell.cellContent.text = str;
    cell.cellContent.lineBreakMode = NSLineBreakByWordWrapping;
    cell.cellContent.numberOfLines = 0;
    
    if ([push.messageType isEqualToString:@"message"]) {
        if (push.isRead == 1) {
            cell.icon.image = [UIImage imageNamed:@"push_black_message.png"];
        }else{
            cell.icon.image = [UIImage imageNamed:@"push_white_message.png"];
        }
        cell.cellTitle.text = @"Message";
    }else if ([push.messageType isEqualToString:@"offer"]){
        if (push.isRead == 1) {
            cell.icon.image = [UIImage imageNamed:@"push_black_offer.png"];
        }else{
            cell.icon.image = [UIImage imageNamed:@"push_white_offer.png"];
        }
        cell.cellTitle.text = [NSString stringWithFormat:@"New offer#%d",push.messageID];
    }else{
        if (push.isRead == 1) {
            cell.icon.image = [UIImage imageNamed:@"push_black_job.png"];
        }else{
            cell.icon.image = [UIImage imageNamed:@"push_white_job.png"];
        }
        cell.cellTitle.text = [NSString stringWithFormat:@"Job#%d",push.messageID];
    }
    
    cell.cellDate.text = push.pushDate;
    
    cell.delegate = self;
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"delete_notification.png"] backgroundColor:[UIColor clearColor] padding:10]];
    cell.leftButtons = @[[MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"delete_notification.png"] backgroundColor:[UIColor clearColor]padding:10]];
    
//    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag; // MICHAL CHECK
//    cell.leftSwipeSettings.transition = MGSwipeTransition3D;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PushObject *push = [[PushObject alloc] init];
    push = [pushArray objectAtIndex:indexPath.row];
    
    float height = 0;
    float widthScreen = [[UIScreen mainScreen] bounds].size.width;
    CGSize constraint = CGSizeMake(widthScreen - 30, 400);
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
    CGRect textsize = [push.pushText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    height = textsize.size.height;
    
    return height + 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
    PushObject *push = [[PushObject alloc] init];
    push = [pushArray objectAtIndex:indexPath.row];
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        if ([push.messageType isEqualToString:@"offer"]) {
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getSpecificOffer&user_id=%@&carrier_id=%@&offer_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"],(long)push.messageID]];
        }else if([push.messageType isEqualToString:@"job"]){
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getSpecificJob&user_id=%@&carrier_id=%@&job_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"],(long)push.messageID]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [backgroundView removeFromSuperview];
            _InternetConnection = [[InternetConnection alloc] init];
            if([_InternetConnection connected] && myData)
            {
                
                if ([push.messageType isEqualToString:@"offer"]) {
                    if([CLLocationManager locationServicesEnabled] &&
                       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
                        //update read message
                        _dbFunctions = [[dbFunctions alloc] init];
                        [_dbFunctions updatePush:push.messageID :push.messageType];
                        
                        //open Offer
                        NSDictionary *dic= [[NSDictionary alloc] init];
                        _func = [[PushNotificationFunctions alloc] init];
                        dic = [_func returnOfferAnswer:push.messageID];
                        if ([[dic objectForKey:@"err"] intValue] == 0 && [dic objectForKey:@"offer"] != (id)[NSNull null]) {
                            
                            NSDictionary *offerDic = [[NSDictionary alloc] init];
                            offerDic = [dic objectForKey:@"offer"];
                            
                            _func = [[PushNotificationFunctions alloc] init];
                            offer = [[offerObject alloc] init];
                            offer = [_func returnOffer:offerDic];
                            [backgroundView removeFromSuperview];
                            
                            [self performSegueWithIdentifier:@"openOffer" sender:self];
                        }else{
                            [backgroundView removeFromSuperview];
                            _alert = [[CustomAlert alloc] init];
                            [_alert alertView:nil :nil :@"Notice" :@"This offer is no longer assigned to you. If you need help with this offer please contact your carrier or Truckiez" :@"" :@"OK" :@"" :nil :self.view :0];
                        }
                    }else{
                        [backgroundView removeFromSuperview];
                        
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(mute:)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        alert = [[UIView alloc] init];
                        _alert = [[CustomAlert alloc] init];
                        
                        alert = [_alert alertView:doSomething :nil :@"Location settings" :@"Your location is not enabled. Please go to settings and enable location" :@"Settings" :@"Cancel" :@"" :nil :self.view :0];
                    }
                }
                else if ([push.messageType isEqualToString:@"job"] || [push.messageType isEqualToString:@"rate"]){
                    if([CLLocationManager locationServicesEnabled] &&
                       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
                        //update read message
                        _dbFunctions = [[dbFunctions alloc] init];
                        [_dbFunctions updatePush:push.messageID :push.messageType];
                        
                        //open Offer
                        NSDictionary *dic= [[NSDictionary alloc] init];
                        _func = [[PushNotificationFunctions alloc] init];
                        dic = [_func returnJobAnswer:push.messageID];
                        if ([[dic objectForKey:@"err"] intValue] == 0 && [dic objectForKey:@"job"] != (id)[NSNull null]) {
                            
                            NSDictionary *jobDic = [[NSDictionary alloc] init];
                            jobDic = [dic objectForKey:@"job"];
                            
                            _func = [[PushNotificationFunctions alloc] init];
                            job = [[shipmentObject alloc] init];
                            job = [_func returnJob:jobDic];
                            
                            [backgroundView removeFromSuperview];
                            [self performSegueWithIdentifier:@"openJob" sender:self];
                        }else{
                            
                            [backgroundView removeFromSuperview];
                            
                            _alert = [[CustomAlert alloc] init];
                            [_alert alertView:nil :nil :@"Notice" :@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez" :@"" :@"OK" :@"" :nil :self.view :0];
                        }
                    }else{
                        
                        [backgroundView removeFromSuperview];
                        
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(mute:)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        alert = [[UIView alloc] init];
                        _alert = [[CustomAlert alloc] init];
                        
                        alert = [_alert alertView:doSomething :nil :@"Location settings" :@"Your location is not enabled. Please go to settings and enable location" :@"Settings" :@"Cancel" :@"" :nil :self.view :0];
                    }
                }else{
                    _dbFunctions = [[dbFunctions alloc] init];
                    [_dbFunctions updatePush:push.messageID :push.messageType];
                }
                push.isRead = 1;
                [_tableView reloadData];
            }else{
                
                [backgroundView removeFromSuperview];
                
                _alert = [[CustomAlert alloc] init];
                [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
            }
        });
        
    });
    }else{
        
        _alert = [[CustomAlert alloc] init];
        
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        
        
    }
    

    
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSIndexPath *cellIndexPath = [_tableView indexPathForCell:cell];
    
    PushObject *push = [[PushObject alloc] init];
    push = [pushArray objectAtIndex:cellIndexPath.row];
    
    if (direction == MGSwipeDirectionRightToLeft && index == 0)
    {
        //delete push
        _dbFunctions = [[dbFunctions alloc] init];
        //[dbFunctions deletePush:push.pushID];
        [_dbFunctions deletePush:push.pushID];
        
        [pushArray removeObjectAtIndex:cellIndexPath.row];
        [_tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
    }
    if (direction == MGSwipeDirectionLeftToRight && index == 0)
    {
        //delete push
        _dbFunctions = [[dbFunctions alloc] init];
        //[dbFunctions deletePush:push.pushID];
        [_dbFunctions deletePush:push.pushID];
        
        [pushArray removeObjectAtIndex:cellIndexPath.row];
        [_tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
    }
    return YES;
}

- (IBAction)back:(id)sender {
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    [self performSegueWithIdentifier:@"backToDesktop" sender:self];
    //    [self dismissViewControllerAnimated:YES completion:^{
    //        [self.navigationController popToRootViewControllerAnimated:YES];
    //    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openOffer"]) {
        Offers *offersController = [segue destinationViewController];
        offersController.offer = [[offerObject alloc] init];
        offersController.offer = offer;
        
    }
    if([segue.identifier isEqualToString:@"openJob"]) {
        Jobs *jobController = [segue destinationViewController];
        jobController.job = [[shipmentObject alloc] init];
        jobController.job = job;
        
    }
}

- (IBAction)openMenu:(id)sender {
    if (checkOpenDrop == 0) {
        _dropDownView.hidden = NO;
        checkOpenDrop = 1;
    }else{
        _dropDownView.hidden = YES;
        checkOpenDrop = 0;
    }
}
- (IBAction)clearAll:(id)sender {
    
    _dropDownView.hidden = YES;
    
    UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
    [doSomething addTarget:self
                    action:@selector(clearAllNotifications)
          forControlEvents:UIControlEventTouchUpInside];
    
    alert = [[UIView alloc] init];
    _alert = [[CustomAlert alloc] init];
    
    alert = [_alert alertView:doSomething :nil :@"Clear all" :@"Are you sure you want to clear all notifications?" :@"Yes" :@"Cancel" :@"" :nil :self.view :0];
}

-(void)clearAllNotifications{
    [alert removeFromSuperview];
    
    _dbFunctions = [[dbFunctions alloc] init];
    bool success = [_dbFunctions clearPushTable];
    if (success == true) {
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Access" :@"All notifications are delete" :@"" :@"OK" :@"" :nil :self.view :0];
        
        pushArray = [[NSMutableArray alloc] init];
        [_tableView reloadData];
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Error!" :@"There was a problem deleting notifications" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}


- (IBAction)mute:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    
}

- (IBAction)callSupport:(id)sender {
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        NSLog(@"cannnot call");
    }
    
}
@end

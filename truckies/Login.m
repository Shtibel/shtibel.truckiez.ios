//
//  Login.m
//  location
//
//  Created by Menachem Mizrachi on 23/06/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <CoreText/CoreText.h>

#import "Login.h"
#import "JSON.h"
#import "InternetConnection.h"
#import "ForgotPassword.h"
#import "CustomAlert.h"
#import "dbFunctions.h"
#import "Wellcome.h"
#import "Terms.h"
#import "LoaderImg.h"
#import "Location.h"
@import CocoaLumberjack;
#import "LoginSettings.h"
#import "StaticVars.h"

@interface Login ()
{
    JSON *_json;
    InternetConnection *_InternetConnection;
    dbFunctions *_dbFunctions;
    CustomAlert *_alert;
    LoaderImg *_loaderIMG;
    Location *_location;
    LoginSettings *_ls;
    
    NSString *image;
    
    NSLayoutConstraint *truckiesLogoHeigh;
    NSLayoutConstraint *truckiesLogoWidth;
    NSLayoutConstraint *truckiesLoginView;
    
    int checkbox;
    
    int nextLength;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation Login

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //validate
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    DDLogVerbose(@"loginVC");
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] != nil) {
    DDLogVerbose(@"start with location");
    _location = [[Location alloc] init];
    [_location sendMyLocationAtFirstTime];
    _location = [[Location alloc] init];
    [_location startLocation];
    }
    
    [self loadPage];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkLogin"] == nil)
    {
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkAgree"] != nil)
        {
            _agreeView.hidden = YES;
            truckiesLoginView = [NSLayoutConstraint constraintWithItem:_loginView
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1.0
                                                              constant:100];
            [self.view addConstraint:truckiesLoginView];
            checkbox = 1;
        }
        
        
        _InternetConnection = [[InternetConnection alloc] init];
        
        if([_InternetConnection connected])
            
        {
            

        //get phone support and terms use text
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            DDLogVerbose(@"get general settings from server");
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:@"tr-api/?action=generalSettings"];
            
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            NSString *str;
            if ([dic objectForKey:@"app_terms"]== (id)[NSNull null]) {
                str = @"";
            }else{
                str = [dic objectForKey:@"app_terms"];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"app_terms"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([dic objectForKey:@"app_support_phone"]== (id)[NSNull null]) {
                str = @"";
            }else{
                str = [dic objectForKey:@"app_support_phone"];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"app_support_phone"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //time_show_shipper_info_origin_destination_contact_info
            if ([dic objectForKey:@"time_show_shipper_info_origin_destination_contact_info"]== (id)[NSNull null]) {
                str = @"";
            }else{
                str = [dic objectForKey:@"time_show_shipper_info_origin_destination_contact_info"];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"time_show_job"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        });
        }else{
            
            _alert = [[CustomAlert alloc] init];
            
            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
            
            
        }
        

    }else{
        
        [self performSelector:@selector(openDesktop) withObject:self.view afterDelay:1.0];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    DDLogVerbose(@"loginVC");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkboxReload) name:@"checkboxReload" object:nil];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



-(void)openDesktop{
    [self performSegueWithIdentifier:@"openDesktop" sender:self];
}

-(void)loadPage{
    
    [_email.layer setCornerRadius:5.0f];
    [_password.layer setCornerRadius:5.0f];
    [_loginButton.layer setCornerRadius:5.0f];
    [_phoneView.layer setCornerRadius:5.0f];
    
    NSArray *arr = [[NSArray alloc] init];
    arr = @[@"pic1.jpg",@"pic2.jpg",@"pic3.jpg"];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkLogin"] == nil)
    {
        _truckiesLogo2.hidden = YES;
        NSUInteger randomIndex = arc4random() % [arr count];
        image = arr[randomIndex];
        _splashScreen.image = [UIImage imageNamed:arr[randomIndex]];
        _splashScreen.clipsToBounds = YES;
        _splashScreen.contentMode = UIViewContentModeScaleAspectFill;
        UITapGestureRecognizer *Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
        Tap.numberOfTapsRequired = 1;
        [_splashScreen setUserInteractionEnabled:YES];
        [_splashScreen addGestureRecognizer:Tap];
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        truckiesLogoHeigh = [NSLayoutConstraint constraintWithItem:_truckiesLogo
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:(width - 180)/2];
        [self.view addConstraint:truckiesLogoHeigh];
        
    }else{
        _truckiesLogo.hidden = YES;
        _loginView.hidden = YES;
        NSUInteger randomIndex = arc4random() % [arr count];
        image = arr[randomIndex];
        _splashScreen.image = [UIImage imageNamed:arr[randomIndex]];
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        truckiesLogoHeigh = [NSLayoutConstraint constraintWithItem:_truckiesLogo2
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:width/2 + 5];
        [self.view addConstraint:truckiesLogoHeigh];
        
        truckiesLogoWidth = [NSLayoutConstraint constraintWithItem:_truckiesLogo2
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:width/2 + 20];
        [self.view addConstraint:truckiesLogoWidth];
        
    }
    
    UITapGestureRecognizer *checkboxTerm = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkboxClick)];
    checkboxTerm.numberOfTapsRequired = 1;
    [_checkboxView setUserInteractionEnabled:YES];
    [_checkboxView addGestureRecognizer:checkboxTerm];
    checkbox = 0;
    
    _checkBox.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _checkBox.layer.borderWidth = 1.0f;
}

-(void)checkboxClick{
    if (checkbox == 0) {
        _checkBox.image = [UIImage imageNamed:@"full_checkbox.png"];
        checkbox = 1;
    }else{
        _checkBox.image = [UIImage imageNamed:@""];
        checkbox = 0;
    }
}

- (IBAction)login:(id)sender {
    DDLogVerbose(@"close keyboard");
    [self.view endEditing:YES];
    //check internet connection
    DDLogVerbose(@"check internet connection");
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        DDLogVerbose(@"check if fill textfields");
        if(![_email.text isEqualToString:@""] && ![_email.text isEqualToString:@"Phone"])
        {
            if (checkbox == 1) {
            DDLogVerbose(@"show loader");
            //show loader
            UIView *backgroundView;
            _loaderIMG = [[LoaderImg alloc] init];
            backgroundView = [_loaderIMG loader:backgroundView];
            [self.view addSubview:backgroundView];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                _ls = [[LoginSettings alloc] init];
                BOOL check = [_ls sendPhone:_email.text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backgroundView removeFromSuperview];
                    if (check == YES) {
                        [self performSegueWithIdentifier:@"openForgotPassword" sender:self];
                    }else{
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertView:nil :nil :@"Notice" :@"Phone number is not correct" :@"" :@"OK" :@"" :nil :self.view :0];
                    }
                });
            });
            }else{
                _alert = [[CustomAlert alloc] init];
                [_alert alertView:nil :nil :@"Notice" :@"You must agree to the terms" :@"" :@"OK" :@"" :nil :self.view :0];
            }
            
        }else{
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Please fill phone textfield" :@"" :@"OK" :@"" :nil :self.view :0];
        }
        
    }else{
        DDLogVerbose(@"if have problem with internet connection");
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }

}

- (IBAction)forgotPassword:(id)sender {
    [self performSegueWithIdentifier:@"openForgotPassword" sender:self];
}

- (IBAction)needSignUp:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/",[StaticVars url]]]];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _email) {
    int length = (int)[self getLength:textField.text];
    
    if(length == 10)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 4 && nextLength < length)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"%@ ",num];
        
        //if(range.length > 0)
        //    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:4]];
    }
    else if(length == 7 && nextLength < length)
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"%@ %@ ",[num  substringToIndex:4],[num substringFromIndex:4]];
        
        //if(range.length > 0)
        //    textField.text = [NSString stringWithFormat:@"%@ %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    nextLength = length;
    }
    return YES;
    
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}


- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    textField.text = @"";
    if (textField == _password) {
        textField.secureTextEntry = YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text isEqualToString:@""]) {
        if (textField == _email) {
            textField.text = @"Phone";
        }else{
            textField.text = @"Password";
            textField.secureTextEntry = NO;
        }
    }
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,- keyboardSize.height,width,height)];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    [self.view setFrame:CGRectMake(0,0,width,height)];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openForgotPassword"]) {
        ForgotPassword *forgotController = [segue destinationViewController];
        forgotController.imgName = image;
        forgotController.phone = [_email.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    if([segue.identifier isEqualToString:@"openWellcome"]) {
        Wellcome *Wellcome = [segue destinationViewController];
        Wellcome.imgName = image;
    }
    if([segue.identifier isEqualToString:@"openTerms"]) {
        Terms *Terms = [segue destinationViewController];
        Terms.imgName = image;
    }
}

- (IBAction)terms:(id)sender {
    [self performSegueWithIdentifier:@"openTerms" sender:self];
}

-(void)checkboxReload{
    _checkBox.image = [UIImage imageNamed:@"full_checkbox.png"];
    checkbox = 1;
}

-(void)closeKeyboard{
    [self.view endEditing:YES];
}
@end

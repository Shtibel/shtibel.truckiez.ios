//
//  PickupConfirmationCell.m
//  truckies
//
//  Created by Menachem Mizrachi on 27/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "PickupConfirmationCell.h"

@implementation PickupConfirmationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

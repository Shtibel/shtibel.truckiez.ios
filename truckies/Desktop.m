//
//  Desktop.m
//  truckies
//
//  Created by Menachem Mizrachi on 02/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "Desktop.h"
#import "DesktopCell.h"
#import "shipmentObject.h"
#import "DesktopFunc.h"
#import "ShipmentsControllerFunctions.h"
#import "UIImageView+WebCache.h"
#import "ConvertColor.h"
#import "StrippingHTML.h"
#import "DesktopOfferCell.h"
#import "offerObject.h"
#import "Jobs.h"
#import "Offers.h"
#import "JSON.h"
#import "dbFunctions.h"
#import "InternetConnection.h"
#import "CustomAlert.h"
#import "LocationObject.h"
#import "LoaderImg.h"
#import "navigationController.h"
#import "StaticVars.h"
@import CocoaLumberjack;

@interface Desktop ()
{
    DesktopFunc *_desktopFunc;
    ShipmentsControllerFunctions *_SCFunc;
    ConvertColor *_ConvertColor;
    StrippingHTML *_html;
    JSON *_json;
    dbFunctions *_dbFunctions;
    InternetConnection *_InternetConnection;
    CustomAlert *_alert;
    LoaderImg *_loaderIMG;
    
    int checkLeftSwipe;
    int checkRightSwipe;
    
    NSMutableArray *today;
    NSMutableArray *schedule;
    NSMutableArray *complete;
    NSMutableArray *currentArray;
    
    NSMutableArray *currentOffersArray;
    NSMutableArray *offersTodayArr;
    NSMutableArray *offersSchedule;
    
    NSLayoutConstraint *jobsContentViewHeight;
    int selectJobTab;
    int checkScrollJobs;
    
    NSLayoutConstraint *offersContentViewHeight;
    int selectOffersTab;
    int checkScrollOffers;
    
    shipmentObject *shipmentSelect;
    offerObject *offerSelect;
    
    int tabType;
    int tabSelect;
    int checkBack;
    int offerNum;
    
    //search offer
    NSString *searchCountry;
    NSString *searchCity;
    NSString *searchStreet;
    
    NSString *offerPallet;
    NSString *offerBox;
    NSString *offerTruckload;
    int offerCheckPallet;
    int offerCheckBox;
    int offerCheckTruckload;
    int checkDatePickerSelect;
    NSString *fromDateStr;
    NSString *toDateStr;
    int checkOfferSearchView;
    //search job
    NSString *jobPallet;
    NSString *jobBox;
    NSString *jobTruckload;
    int jobCheckPallet;
    int jobCheckBox;
    int jobCheckTruckload;
    int checkDatePickerJobSelect;
    NSString *fromDateJobStr;
    NSString *toDateJobStr;
    CGFloat widthAlert;
    UIView *alert;
    
    BOOL isFirstTime;
    
    int jContentViewHeight;
    
    NSLayoutConstraint *jobsRight;
    
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation Desktop


- (void)viewDidLoad {
    [super viewDidLoad];
    DDLogVerbose(@"desktopVC");
    isFirstTime=YES;
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        [self.item setTarget:self.revealViewController];
        [self.item setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [_badgeButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    [self jobs];
    [self loadPage];
    [self loadGesture];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            DDLogVerbose(@"load content from server");
            [self loadContent];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self jobs];
                [self offers];
                [self loadPage3];
                [self loadPage2];
                [_tableView reloadData];
                [_offerTable reloadData];
                [backgroundView removeFromSuperview];
            });
            
        });
    }else{
        [backgroundView removeFromSuperview];
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    DDLogVerbose(@"desktopVC");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadOfferTable) name:@"reloadOfferTable" object:nil];
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        //created by batya
        if (isFirstTime) {
            isFirstTime=NO;
        }
        else if(selectJobTab!=4){
            //checkScrollJobs = 1;
            UIView *backgroundView;
            _loaderIMG = [[LoaderImg alloc] init];
            backgroundView = [_loaderIMG loader:backgroundView];
            [self.view addSubview:backgroundView];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                //load search
                NSString *keywordSearch = _keywordAdvanceJob.text;
                if ([keywordSearch isEqualToString:@"Key Word"]) {
                    keywordSearch = @"";
                }
                NSString *searchURl = [NSString stringWithFormat:@"tr-api/?action=searchJob&user_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], jobPallet, jobBox, jobTruckload, searchStreet, searchCity, searchCountry, fromDateJobStr, toDateJobStr, keywordSearch];
                
                NSMutableArray *arr = [[NSMutableArray alloc] init];
                currentArray = [[NSMutableArray alloc] init];
                _desktopFunc = [[DesktopFunc alloc] init];
                arr = [_desktopFunc loadContentWhenScroll:selectJobTab :searchURl];
                
                if (selectJobTab != 4) {
                    currentArray = arr[0];
                    today = arr[1];
                    schedule = arr[2];
                    complete = arr[3];
                }else{
                    currentArray = arr;
                }
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_tableView reloadData];
                    [backgroundView removeFromSuperview];
                    checkScrollJobs = 0;
                    if (currentArray.count == 0) {
                        if (selectOffersTab == 4) {
                            _noJobsLabel.text = @"Does not jobs in this search";
                        }else{
                            _noJobsLabel.text = @"There are no job";
                        }
                        _noJobsLabel.hidden = NO;
                    }else{
                        _noJobsLabel.hidden = YES;
                    }
                });
                
            });
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)loadGesture{
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [_container addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [_container addGestureRecognizer:swiperight];
    
    UITapGestureRecognizer *jobsNavTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swiperight:)];
    jobsNavTap.numberOfTapsRequired = 1;
    [_jobsNavView setUserInteractionEnabled:YES];
    [_jobsNavView addGestureRecognizer:jobsNavTap];
    
    if (![[[NSUserDefaults standardUserDefaults]
           stringForKey:@"can_see_offers"] isEqualToString:@"0"])
    {
        UITapGestureRecognizer *offersNavTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swipeleft:)];
        offersNavTap.numberOfTapsRequired = 1;
        [_offersNavView setUserInteractionEnabled:YES];
        [_offersNavView addGestureRecognizer:offersNavTap];
    }
    
    //jobs
    UITapGestureRecognizer *jobsTodayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsTodayClick)];
    jobsTodayTap.numberOfTapsRequired = 1;
    [_jobsToday setUserInteractionEnabled:YES];
    [_jobsToday addGestureRecognizer:jobsTodayTap];
    
    UITapGestureRecognizer *jobsSchedualedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSchedualedClick)];
    jobsSchedualedTap.numberOfTapsRequired = 1;
    [_jobsSchedualed setUserInteractionEnabled:YES];
    [_jobsSchedualed addGestureRecognizer:jobsSchedualedTap];
    
    UITapGestureRecognizer *jobsCompleteTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsCompleteClick)];
    jobsSchedualedTap.numberOfTapsRequired = 1;
    [_jobsComplete setUserInteractionEnabled:YES];
    [_jobsComplete addGestureRecognizer:jobsCompleteTap];
    
    UITapGestureRecognizer *jobsSearchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSearchClick)];
    jobsSearchTap.numberOfTapsRequired = 1;
    [_jobsSearch setUserInteractionEnabled:YES];
    [_jobsSearch addGestureRecognizer:jobsSearchTap];
    
    UITapGestureRecognizer *jobsSearchClickView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSearchClickView)];
    jobsSearchClickView.numberOfTapsRequired = 1;
    [_jobSearchClickView setUserInteractionEnabled:YES];
    [_jobSearchClickView addGestureRecognizer:jobsSearchClickView];
    
    UITapGestureRecognizer *jobsSearchBox = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSearchBoxClick)];
    jobsSearchBox.numberOfTapsRequired = 1;
    [_jobSearchBoxView setUserInteractionEnabled:YES];
    [_jobSearchBoxView addGestureRecognizer:jobsSearchBox];
    
    UITapGestureRecognizer *jobsSearchPallet = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSearchPalletClick)];
    jobsSearchPallet.numberOfTapsRequired = 1;
    [_jobSearchPalletView setUserInteractionEnabled:YES];
    [_jobSearchPalletView addGestureRecognizer:jobsSearchPallet];
    
    UITapGestureRecognizer *jobsSearchTruckload = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobsSearchTruckloadClick)];
    jobsSearchTruckload.numberOfTapsRequired = 1;
    [_jobSearchTruckloadView setUserInteractionEnabled:YES];
    [_jobSearchTruckloadView addGestureRecognizer:jobsSearchTruckload];
    
    
    //offers
    UITapGestureRecognizer *offersTodayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersTodayClick)];
    offersTodayTap.numberOfTapsRequired = 1;
    [_offersToday setUserInteractionEnabled:YES];
    [_offersToday addGestureRecognizer:offersTodayTap];
    
    UITapGestureRecognizer *offersSchedualedTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSchedualedClick)];
    offersSchedualedTap.numberOfTapsRequired = 1;
    [_offersSchedualed setUserInteractionEnabled:YES];
    [_offersSchedualed addGestureRecognizer:offersSchedualedTap];
    
    UITapGestureRecognizer *offersSearchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSearchClick)];
    offersSearchTap.numberOfTapsRequired = 1;
    [_offersSearch setUserInteractionEnabled:YES];
    [_offersSearch addGestureRecognizer:offersSearchTap];
    
    UITapGestureRecognizer *offersSearchBox = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSearchBoxClick)];
    offersSearchBox.numberOfTapsRequired = 1;
    [_offerSearchBoxView setUserInteractionEnabled:YES];
    [_offerSearchBoxView addGestureRecognizer:offersSearchBox];
    
    UITapGestureRecognizer *offersSearchPallet = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSearchPalletClick)];
    offersSearchPallet.numberOfTapsRequired = 1;
    [_offerSearchPalletView setUserInteractionEnabled:YES];
    [_offerSearchPalletView addGestureRecognizer:offersSearchPallet];
    
    UITapGestureRecognizer *offersSearchTruckload = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSearchTruckloadClick)];
    offersSearchTruckload.numberOfTapsRequired = 1;
    [_offerSearchTruckloadView setUserInteractionEnabled:YES];
    [_offerSearchTruckloadView addGestureRecognizer:offersSearchTruckload];
    
    UITapGestureRecognizer *offersSearchClickView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offersSearchClickView)];
    offersSearchClickView.numberOfTapsRequired = 1;
    [_offerSearchClickView setUserInteractionEnabled:YES];
    [_offerSearchClickView addGestureRecognizer:offersSearchClickView];
    
    UITapGestureRecognizer *offerClearLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offerClearLocation)];
    offerClearLocation.numberOfTapsRequired = 1;
    [_clearLocationOffer setUserInteractionEnabled:YES];
    [_clearLocationOffer addGestureRecognizer:offerClearLocation];
    
    UITapGestureRecognizer *jobClearLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jobClearLocation)];
    jobClearLocation.numberOfTapsRequired = 1;
    [_clearLocation setUserInteractionEnabled:YES];
    [_clearLocation addGestureRecognizer:jobClearLocation];
    
    UITapGestureRecognizer *badgeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(badgeClick:)];
    badgeTap.numberOfTapsRequired = 1;
    [_badgeView setUserInteractionEnabled:YES];
    [_badgeView addGestureRecognizer:badgeTap];
}

-(void)loadPage{
    
    widthAlert = [UIScreen mainScreen].bounds.size.width - 60;
    
    checkLeftSwipe = 1;
    checkRightSwipe = 0;
    //UI
    _jobsNavMarker.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersNavMarker.backgroundColor = [UIColor clearColor];
    _jobsNavTitle.textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersNavTitle.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    [self checkCanGetOffers];
    [self firstHidden];
    [self badge];
    [self showSegment];
    
    [self jobsTodayEmptyUI];
    _noJobsLabel.hidden = YES;
}

-(void)loadPage3{
    
    //jobs
    
    [self offerSearch];
    [self searchJob];
    
    //all hidden
    _offerDatePickerView.hidden = YES;
    _jobDatePickerView.hidden = YES;
    _swipeNavigation.hidden = NO;
    _container.hidden = NO;
    
    
    
    
}

-(void)loadPage2{
    currentArray = [[NSMutableArray alloc] init];
    
    //jobs
    if (today.count == 0) {
        if (schedule.count == 0) {
            if (complete.count == 0) {
                [self jobsTodayEmptyUI];
            }else{
                [self jobsCompleteUI];
            }
        }else{
            [self jobsScheduleUI];
        }
    }else{
        [self jobsTodayUI];
    }
    
    //offers
    currentOffersArray = [[NSMutableArray alloc] init];
    if (offersTodayArr.count == 0) {
        if (offersSchedule.count == 0) {
            [self offersTodayEmptyUI];
        }else{
            [self offerScheduleUI];
        }
    }else{
        [self offersTodayUI];
    }
    if (offerNum != 0) {
        [_offerNumView.layer setCornerRadius:8.0f];
        _offerNumLabel.text = [NSString stringWithFormat:@"%d",offerNum];
        _offerNumView.hidden = NO;
    }
}

-(void)loadContent{
    _desktopFunc = [[DesktopFunc alloc] init];
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    arr = [_desktopFunc loadContent];
    
    
    today = [[NSMutableArray alloc] init];
    today = arr[0];
    
    schedule = [[NSMutableArray alloc] init];
    schedule = arr[1];
    
    complete = [[NSMutableArray alloc] init];
    complete = arr[2];
    
    LocationObject *location = [LocationObject sharedInstance];
    location.jobsArray = [[NSMutableArray alloc] init];
    location.jobsArray = arr[3];
    
    _desktopFunc = [[DesktopFunc alloc] init];
    
    NSMutableArray *arr2 = [[NSMutableArray alloc] init];
    arr2 = [_desktopFunc loadContentOffers];
    
    offersTodayArr = [[NSMutableArray alloc] init];
    offersTodayArr = arr2[0];
    
    offersSchedule = [[NSMutableArray alloc] init];
    offersSchedule = arr2[1];
    
    offerNum = (int)offersTodayArr.count + (int)offersSchedule.count;
    
    
    
    //search offer
    searchCountry = @"";
    searchCity = @"";
    searchStreet = @"";
    
    offerPallet = @"0";
    offerBox = @"0";
    offerTruckload = @"0";
    offerCheckPallet = 0;
    offerCheckBox = 0;
    offerCheckTruckload = 0;
    fromDateStr = @"";
    toDateStr = @"";
    
    //search job
    jobPallet = @"0";
    jobBox = @"0";
    jobTruckload = @"0";
    jobCheckPallet = 0;
    jobCheckBox = 0;
    jobCheckTruckload = 0;
    fromDateJobStr = @"";
    toDateJobStr = @"";
    
    jContentViewHeight = 0;
    
    
    for (int shipment = 0; shipment < currentArray.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = currentArray[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    

    //menu
    NSString *str = [NSString stringWithFormat:@"%@%@",[StaticVars url],[[NSUserDefaults standardUserDefaults]
                                                                                 stringForKey:@"user_image"]];
    
    NSURL *urlForImageShipments = [NSURL URLWithString:str];
    NSData *dataImageShipments = [NSData dataWithContentsOfURL:urlForImageShipments];
    
    _imageDriver = [UIImage imageWithData:dataImageShipments];
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(_imageDriver) forKey:@"userImageObject"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

///////////////////
//swipe

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (![[[NSUserDefaults standardUserDefaults]
           stringForKey:@"can_see_offers"] isEqualToString:@"0"])
    {
        if(checkLeftSwipe == 1){
            [self swipeLeftUI];
        }
    }
    
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if (![[[NSUserDefaults standardUserDefaults]
           stringForKey:@"can_see_offers"] isEqualToString:@"0"])
    {
        if (checkRightSwipe == 1) {
            [self swipeRightUI];
            
        }
    }
}

////////////////////
//tabs click
-(void)jobsTodayClick{
    _jobsToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsTodayTitle.textColor = [UIColor whiteColor];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    currentArray = [[NSMutableArray alloc] init];
    currentArray = today;
    int heightScroll = 0;
    if (currentArray.count == 0) {
        _noJobsLabel.hidden = NO;
        _noJobsLabel.text = @"There are no jobs today";
    }else{
        _noJobsLabel.hidden = YES;
        for (int i = 0; i < currentArray.count; i++) {
            shipmentObject *sh = [[shipmentObject alloc] init];
            sh = [currentArray objectAtIndex:i];
            if (sh.status_id < 11) {
                heightScroll += 40;
            }
        }
    }
    
    [self.view removeConstraint: jobsContentViewHeight];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height5 = [UIScreen mainScreen].bounds.size.height-100;
    if (height5 < (today.count*(width + 100) + 50)) {
        height5 = today.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < today.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = today[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height5) {
        jContentViewHeight = height5;
    }
    
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    [_tableView reloadData];
    
    selectJobTab = 1;
    
}

-(void)jobsSchedualedClick{
    _jobsSchedualed.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsToday.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsSchedualedTitle.textColor = [UIColor whiteColor];
    _jobsTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    currentArray = [[NSMutableArray alloc] init];
    currentArray = schedule;
    int heightScroll = 0;
    if (currentArray.count == 0) {
        _noJobsLabel.hidden = NO;
        _noJobsLabel.text = @"There are no jobs scheduled";
    }else{
        _noJobsLabel.hidden = YES;
        for (int i = 0; i < currentArray.count; i++) {
            shipmentObject *sh = [[shipmentObject alloc] init];
            sh = [currentArray objectAtIndex:i];
            if (sh.status_id < 11) {
                heightScroll += 40;
            }
        }
    }
    
    [self.view removeConstraint: jobsContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height6 = [UIScreen mainScreen].bounds.size.height - 100;
    if (height6 < (schedule.count*(width + 100) + 50)) {
        height6 = schedule.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < schedule.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = schedule[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height6) {
        jContentViewHeight = height6;
    }
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    [_tableView reloadData];
    [backgroundView removeFromSuperview];
    
    
    selectJobTab = 2;
}

-(void)jobsCompleteClick{
    _jobsComplete.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsToday.backgroundColor = [UIColor clearColor];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsCompleteTitle.textColor = [UIColor whiteColor];
    _jobsTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    currentArray = [[NSMutableArray alloc] init];
    currentArray = complete;
    
    if (currentArray.count == 0) {
        _noJobsLabel.hidden = NO;
        _noJobsLabel.text = @"There are no completed";
    }else{
        _noJobsLabel.hidden = YES;
    }
    
    [self.view removeConstraint: jobsContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height7 = [UIScreen mainScreen].bounds.size.height - 100;
    if (height7 < (complete.count*(width + 100) + 50)) {
        height7 = complete.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < complete.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = complete[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height7) {
        jContentViewHeight = height7;
    }
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    
    [_tableView reloadData];
    [backgroundView removeFromSuperview];
    
    
    
    selectJobTab = 3;
}

-(void)jobsSearchClick{
    
    _jobsSearch.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobSearchImage.image = [UIImage imageNamed:@"search_white.png"];
    _jobsToday.backgroundColor = [UIColor clearColor];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    
    _jobsTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    // _searchViewJob.hidden = NO;
    _searchJobAdvance.hidden = NO;
    _noJobsLabel.hidden = YES;
    selectJobTab = 4;
    currentArray = [[NSMutableArray alloc] init];
    [_tableView reloadData];
}

-(void)offersTodayClick{
    _offersToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersSchedualed.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    _offerSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _offersTodayTitle.textColor = [UIColor whiteColor];
    _offersSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    currentOffersArray = [[NSMutableArray alloc] init];
    currentOffersArray = offersTodayArr;
    if (currentOffersArray.count == 0) {
        _noOfferLabel.hidden = NO;
        _noOfferLabel.text = @"There are no offers";
    }else{
        _noOfferLabel.hidden = YES;
    }
    
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    
    CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
    if (heightOffer2 < (offersTodayArr.count*(width + 140) + 50)) {
        heightOffer2 = offersTodayArr.count*(width + 140) + 50;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightOffer2];
    [self.view addConstraint:offersContentViewHeight];
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    
    [_offerTable reloadData];
    [backgroundView removeFromSuperview];
    
    
    selectOffersTab = 1;
}
-(void)offersSchedualedClick{
    _offersSchedualed.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersToday.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    _offerSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _offersSchedualedTitle.textColor = [UIColor whiteColor];
    _offersTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    currentOffersArray = [[NSMutableArray alloc] init];
    currentOffersArray = offersSchedule;
    if (currentOffersArray.count == 0) {
        _noOfferLabel.hidden = NO;
        _noOfferLabel.text = @"There are no offers";
    }else{
        _noOfferLabel.hidden = YES;
    }
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    
    CGFloat heightOffer3 = [UIScreen mainScreen].bounds.size.height;
    if (heightOffer3 < (offersSchedule.count*(width + 140) + 50)) {
        heightOffer3 = offersSchedule.count*(width + 140) + 50;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightOffer3];
    [self.view addConstraint:offersContentViewHeight];
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    
    [_offerTable reloadData];
    [backgroundView removeFromSuperview];
    
    
    selectOffersTab = 2;
}

-(void)offersSearchClick{
    _offersSearch.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offerSearchImage.image = [UIImage imageNamed:@"search_white.png"];
    _offersToday.backgroundColor = [UIColor clearColor];
    _offersSchedualed.backgroundColor = [UIColor clearColor];
    
    _offersSchedualedTitle.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _offersTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    //_searchViewOffer.hidden = NO;
    _searchOfferAdvance.hidden = NO;
    _noOfferLabel.hidden = YES;
    selectOffersTab = 3;
    currentOffersArray = [[NSMutableArray alloc] init];
    [_offerTable reloadData];
}

//table
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == _tableView) {
        return currentArray.count;
    }else{
        return currentOffersArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DDLogVerbose(@"cell");
    if (![NSThread isMainThread])
    {
        NSLog(@"Not Run On main Thread!! check if needed");
    }
    
    if (tableView == _tableView) {//Bary - why you need it?
        
        
        static NSString *cellIdentifier = @"Cell";
        DesktopCell *cell = (DesktopCell *)[_tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = currentArray[indexPath.row];
        
        UIBezierPath *maskPath;
        CGFloat borderWidth;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.viewCell.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        cell.viewCell.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        cell.viewCell.frame = CGRectInset(cell.viewCell.frame, -borderWidth, -borderWidth);
        cell.viewCell.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        cell.viewCell.layer.borderWidth = borderWidth;
        
        cell.codeView.layer.cornerRadius = 5.0f;
        cell.codeLabel.text = [NSString stringWithFormat:@"JOB#%ld",(long)sh.shipmentsId];
        //created by batya
        cell.pickupView.hidden = NO;
        //top cell
        //created by batya
        if (sh.status_id>=18) {
            cell.pickupView.hidden = YES;
            
            _html = [[StrippingHTML alloc] init];
            NSString *str = [_html stringByStrippingHTML:[NSString stringWithFormat:@"Completed %@",sh.completed_at]];
            cell.statusTitle.text = str;
            [cell.statusColor.layer setCornerRadius:7.0f];
            _ConvertColor = [[ConvertColor alloc] init];
            cell.statusColor.backgroundColor =[UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0];
            
        }
        else
            //created by batya
            if (sh.status_id > 14) {
                cell.pickupView.hidden = YES;
                
                _html = [[StrippingHTML alloc] init];
                NSString *str = [_html stringByStrippingHTML:sh.status_name];
                cell.statusTitle.text = str;
                [cell.statusColor.layer setCornerRadius:7.0f];
                _ConvertColor = [[ConvertColor alloc] init];
                cell.statusColor.backgroundColor = [_ConvertColor colorWithHexString:sh.status_name_circle_color alpha:1];
            }else{
                if (sh.status_id < 11) {
                    cell.pickupView.hidden = NO;
                    cell.pickupImage.image = [UIImage imageNamed:@"pickup_blue.png"];
                    cell.pickupText.text = [NSString stringWithFormat:@"Pickup %@ %@-%@",sh.pickup_date, sh.pickup_from_time, sh.pickup_till_time];
                    
                    if (!([sh.pickup_in_time rangeOfString:@"late"].location == NSNotFound))
                    {
                        cell.pickupText.textColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
                    }else if (!([sh.pickup_in_time rangeOfString:@"in 1 hour"].location == NSNotFound))
                    {
                        
                        cell.pickupText.textColor = [UIColor colorWithRed:1.00 green:0.56 blue:0.17 alpha:1.0];
                    }else{
                        cell.pickupText.textColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
                    }
                    
                }else{
                    cell.pickupImage.image = [UIImage imageNamed:@"pickup_orange.png"];
                    cell.pickupText.text = [NSString stringWithFormat:@"Dropoff %@ %@-%@",sh.dropoff_date, sh.dropoff_from_time, sh.dropoff_till_time];
                    
                    if (!([sh.dropoff_in_time rangeOfString:@"late"].location == NSNotFound))
                    {
                        cell.pickupText.textColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
                    }else if (!([sh.dropoff_in_time rangeOfString:@"in 1 hour"].location == NSNotFound))
                    {
                        
                        cell.pickupText.textColor = [UIColor colorWithRed:1.00 green:0.56 blue:0.17 alpha:1.0];
                    }else{
                        cell.pickupText.textColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
                    }
                }
            }
        
        //map cell
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.mapCell
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0
                                                                      constant:width]];
        
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                _desktopFunc = [[DesktopFunc alloc] init];
                NSURL *mapURL = [_desktopFunc GetGoogleStaticMap:sh.origin_lat :sh.origin_lng :sh.destination_lat :sh.destination_lng :sh.shipment_google_root];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.mapImage sd_setImageWithURL:mapURL
                                     placeholderImage:[UIImage imageNamed:@"icons_07.png"]];
                });
            });
        }
        
        if ([[[NSUserDefaults standardUserDefaults]
              stringForKey:@"can_see_price"] isEqualToString:@"0"])
        {
            cell.priceView.hidden = YES;
        }else{
            [cell.priceView.layer setCornerRadius:5.0f];
            cell.priceLabel.text = sh.shipment_carrier_payout_text;
            
        }
        
        //dropoff view
        if (sh.status_id < 11) {
            cell.dropoffText.text = [NSString stringWithFormat:@"Dropoff %@ %@-%@",sh.dropoff_date, sh.dropoff_from_time, sh.dropoff_till_time];
            cell.dropoffView.hidden = NO;
        }else{
            cell.dropoffView.hidden = YES;
            
        }
        
        //content job cell
        [[cell.jobContentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat getCellWidth = self.view.bounds.size.width - 20;
        float widthCell = 0;
        
        NSString *text;
        text = sh.truck_type_name;
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,getCellWidth,30)];
        
        widthCell = getCellWidth;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        label.text = sh.truck_type_name;
        
        [label setFont:[UIFont systemFontOfSize:14]];
        
        [cell.jobContentView addSubview:boxes];
        //            [boxes addSubview:img];
        [boxes addSubview:label];
        
        widthCell = 0;
        //special request in jobContentView
        NSArray *arraySpecialRequest = [sh.shipment_special_request componentsSeparatedByString:@","];
        UIView *specialReq = [[UIView alloc]initWithFrame:CGRectMake(10,25,arraySpecialRequest.count * 25,40)];
        
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //            for (int i=0; i<arraySpecialRequest.count; i++) {
        //                UIImageView *imageHolder;
        //                imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*20 + 5, 5, 20, 20)];
        //
        //                UIImage *image1;
        //                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://truckies.shtibel.com%@",arraySpecialRequest[i]]]];
        //
        //                image1 = [UIImage imageWithData: imageData];
        //                imageHolder.image = image1;
        //                // Bary: BUG it is not allowed to add subView in BGThread!!! hav to be on main thraed
        //                dispatch_async(dispatch_get_main_queue(),
        //                               ^{
        //                                   [specialReq addSubview:imageHolder];
        //                               });
        //            }
        //        });
        
        //weight
        int widthCellToWeight = widthCell  + 20;
        if (![sh.total_load_weight isEqualToString:@""]) {
            NSString *text;
            
            if ([sh.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                text = [NSString stringWithFormat:@"%@T",sh.total_load_weight];
            }else{
                text = [NSString stringWithFormat:@"%@KG",sh.total_load_weight];
            }
            CGSize frameSize = CGSizeMake(300, 30);
            UIFont *font = [UIFont systemFontOfSize:14];
            
            CGRect idealFrame = [text boundingRectWithSize:frameSize
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{ NSFontAttributeName:font }
                                                   context:nil];
            
            if ((getCellWidth - widthCellToWeight) >= 30 + idealFrame.size.width) {
                
                UIView *weight = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,25,30 + idealFrame.size.width,30)];
                
                widthCell = widthCell + 40 + idealFrame.size.width;
                
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
                
                if ([sh.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                    img.image = [UIImage imageNamed:@"T1.png"];
                }else{
                    img.image = [UIImage imageNamed:@"KG1.png"];
                }
                
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
                label.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
                label.text = text;
                [label setFont:[UIFont systemFontOfSize:14]];
                
                [cell.jobContentView addSubview:weight];
                [weight addSubview:img];
                [weight addSubview:label];
                
                //req view
                CGRect btFrame;
                btFrame = specialReq.frame;
                btFrame.origin.x = widthCell;
                specialReq.frame = btFrame;
                
            }
            
        }
        
        //[cell.jobContentView addSubview:specialReq];
        
        return cell;
    }else{
        static NSString *cellIdentifier = @"offerCell";
        DesktopOfferCell *cell = (DesktopOfferCell *)[_offerTable dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        offerObject *offer = [[offerObject alloc] init];
        offer = currentOffersArray[indexPath.row];
        
        UIBezierPath *maskPath;
        CGFloat borderWidth;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.viewCell.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        cell.viewCell.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        cell.viewCell.frame = CGRectInset(cell.viewCell.frame, -borderWidth, -borderWidth);
        cell.viewCell.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        cell.viewCell.layer.borderWidth = borderWidth;
        
        cell.codeView.layer.cornerRadius = 5.0f;
        cell.codeLabel.text = [NSString stringWithFormat:@"OFFER#%d",offer.offerId];
        
        //top
        cell.pickupDate.text = offer.pickup_date;
        cell.pickupHour.text = [NSString stringWithFormat:@"%@-%@",offer.pickup_from_time, offer.pickup_till_time];
        
        cell.dropoffDate.text = offer.dropoff_date;
        cell.dropoffHour.text = [NSString stringWithFormat:@"%@-%@",offer.dropoff_from_time, offer.dropoff_till_time];
        
        //map
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cell.mapCell
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0
                                                                      constant:width]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            _desktopFunc = [[DesktopFunc alloc] init];
            NSURL *mapURL = [_desktopFunc GetGoogleStaticMap:offer.origin_lat :offer.origin_lng :offer.destination_lat :offer.destination_lng :offer.shipment_google_root];
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell.mapImage sd_setImageWithURL:mapURL
                                 placeholderImage:[UIImage imageNamed:@"icons_07.png"]];
            });
        });
        
        
        
        
        if ([[[NSUserDefaults standardUserDefaults]
              stringForKey:@"can_see_price"] isEqualToString:@"0"])
        {
            cell.priceView.hidden = YES;
        }else{
            [cell.priceView.layer setCornerRadius:5.0f];
            cell.priceLabel.text = offer.shipment_carrier_payout_text;
        }
        
        //content
        [[cell.offerContentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat getCellWidth = self.view.bounds.size.width - 20;
        float widthCell = 0;
        
        //if (offer.truckload_num != 0) {
        
        NSString *text;
        text = offer.truck_type_name;
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
        
        widthCell = getCellWidth;
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        img.image = [UIImage imageNamed:@"truckload1.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        label.text = offer.truck_type_name;
        
        [label setFont:[UIFont systemFontOfSize:14]];
        
        [cell.offerContentView addSubview:boxes];
        //[boxes addSubview:img];
        [boxes addSubview:label];
        
        // }
        
        //special request in jobContentView
        //        NSArray *arraySpecialRequest = [offer.shipment_special_request componentsSeparatedByString:@","];
        UIView *specialReq = [[UIView alloc]initWithFrame:CGRectMake(10,25,0,40)];
        //
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //            for (int i=0; i<arraySpecialRequest.count; i++) {
        //                UIImageView *imageHolder;
        //                imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*20 + 5, 5, 20, 20)];
        //
        //                UIImage *image1;
        //                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://truckies.shtibel.com%@",arraySpecialRequest[i]]]];
        //
        //                image1 = [UIImage imageWithData: imageData];
        //                imageHolder.image = image1;
        //
        //                dispatch_async(dispatch_get_main_queue(),
        //                               ^{
        //                                   [specialReq addSubview:imageHolder];
        //                               });
        //            }
        //        });
        
        //weight
        int widthCellToWeight = widthCell + 20;
        if (![offer.total_load_weight isEqualToString:@""]) {
            NSString *text;
            if ([offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                text = [NSString stringWithFormat:@"%@T",offer.total_load_weight];
            }else{
                text = [NSString stringWithFormat:@"%@KG",offer.total_load_weight];
            }
            CGSize frameSize = CGSizeMake(300, 30);
            UIFont *font = [UIFont systemFontOfSize:14];
            
            CGRect idealFrame = [text boundingRectWithSize:frameSize
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:@{ NSFontAttributeName:font }
                                                   context:nil];
            
            //if ((getCellWidth - widthCellToWeight) >= 30 + idealFrame.size.width) {
                
                UIView *weight = [[UIView alloc]initWithFrame:CGRectMake(10,25,30,30)];
                
                widthCell = widthCell + 40 + idealFrame.size.width;
                
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10,30,20,20)];
                if ([offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                    img.image = [UIImage imageNamed:@"T1.png"];
                }else{
                    img.image = [UIImage imageNamed:@"KG1.png"];
                }
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35,32,idealFrame.size.width + 5,20)];
                label.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
                label.text = text;
                [label setFont:[UIFont systemFontOfSize:14]];
                
                //[cell.offerContentView addSubview:weight];
                [cell.offerContentView addSubview:img];
                [cell.offerContentView addSubview:label];
                
            //}
            
        }
        
        NSString *text1;
        if ([offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
            text = [NSString stringWithFormat:@"%@T",offer.total_load_weight];
        }else{
            text = [NSString stringWithFormat:@"%@KG",offer.total_load_weight];
        }
        cell.weight.text = text1;
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tableView) {
        
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = currentArray[indexPath.row];
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        
        return height + width;
    }else{
        
        offerObject *offer = [[offerObject alloc] init];
        offer = currentOffersArray[indexPath.row];
        
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        
        int height = 140;
        
        return height + width;
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //  if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        
        //dispatch_async(dispatch_get_main_queue(), ^{
        
        [_checkOffersTimer invalidate];
        DDLogVerbose(@"didSelectRow");
        if (tableView == _tableView) {
            if([CLLocationManager locationServicesEnabled] &&
               [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
                shipmentSelect = [[shipmentObject alloc] init];
                shipmentSelect = [currentArray objectAtIndex:indexPath.row];
                
                DDLogVerbose(@"openSelectJob");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backgroundView removeFromSuperview];
                    [self performSegueWithIdentifier:@"openSelectJob" sender:self];
                    
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backgroundView removeFromSuperview];
                    UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                    [doSomething addTarget:self
                                    action:@selector(turnOnMylocation)
                          forControlEvents:UIControlEventTouchUpInside];
                    
                    alert = [[UIView alloc] init];
                    _alert = [[CustomAlert alloc] init];
                    
                    alert = [_alert alertView:doSomething :nil :@"Location settings" :@"Your location is not enabled. Please go to settings and enable location" :@"Settings" :@"Cancel" :@"" :nil :self.view :0];
                });
            }
            
            
        }else{
            if([CLLocationManager locationServicesEnabled] &&
               [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
                offerSelect = [[offerObject alloc] init];
                offerSelect = [currentOffersArray objectAtIndex:indexPath.row];
                DDLogVerbose(@"openSelectOffer");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backgroundView removeFromSuperview];
                    [self performSegueWithIdentifier:@"openSelectOffer" sender:self];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [backgroundView removeFromSuperview];
                    UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(20,0,widthAlert/2 - 25,30)];
                    [doSomething addTarget:self
                                    action:@selector(turnOnMylocation)
                          forControlEvents:UIControlEventTouchUpInside];
                    
                    alert = [[UIView alloc] init];
                    _alert = [[CustomAlert alloc] init];
                    
                    alert = [_alert alertView:doSomething :nil :@"Location settings" :@"Your location is not enabled. Please go to settings and enable location" :@"Settings" :@"Cancel" :@"" :nil :self.view :0];
                });
            }
        }
        
    });
    
}

/////////////////
//avialable
- (IBAction)available:(id)sender {
    
    switch (_segment.selectedSegmentIndex)
    {
        case 0:
        {
            _segment.selectedSegmentIndex = 1;
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
                
           dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    //set to service
                    _json = [[JSON alloc] init];
                    NSData *myData = [[NSData alloc] init];
                    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateAvailableStatus&user_id=%@&driver_application_status_id=2",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (! myData) {
                            _segment.selectedSegmentIndex = 1;
                            _alert = [[CustomAlert alloc] init];
                            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                            
                        }else{
                            _segment.selectedSegmentIndex = 0;
                            [_segment setTintColor:[UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0]];
                            [_segment setTitle:@"Available    " forSegmentAtIndex:1];
                            [_segment setWidth:30.0f forSegmentAtIndex:0];
                            [_segment setWidth:80.0f forSegmentAtIndex:1];
                            [_segment setTitle:@"" forSegmentAtIndex:0];
                            
                            NSString *driverStatus = @"2";
                            [[NSUserDefaults standardUserDefaults] setObject:driverStatus forKey:@"driverStatus"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                    });
                    
                });
            }else{
                _segment.selectedSegmentIndex = 1;
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                         message:nil
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                     [self updateApp];
                                                                 }];
                [alertController addAction:action];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * action) {
                                                                   [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                               }];
                [alertController addAction:cancel];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
            
            
        }
            break;
        case 1:
        {
            _segment.selectedSegmentIndex = 0;
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
            
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    //set to service
                    _json = [[JSON alloc] init];
                    NSData *myData = [[NSData alloc] init];
                    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateAvailableStatus&user_id=%@&driver_application_status_id=4",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (! myData) {
                            _segment.selectedSegmentIndex = 0;
                            _alert = [[CustomAlert alloc] init];
                            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                        }else{
                            _segment.selectedSegmentIndex = 1;
                            [_segment setTintColor:[UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0]];
                            [_segment setWidth:30.0f forSegmentAtIndex:1];
                            [_segment setTitle:@"" forSegmentAtIndex:1];
                            [_segment setWidth:80.0f forSegmentAtIndex:0];
                            [_segment setTitle:@"  Unvailable" forSegmentAtIndex:0];
                            
                            NSString *driverStatus = @"4";
                            [[NSUserDefaults standardUserDefaults] setObject:driverStatus forKey:@"driverStatus"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                    });
                    
                });
            }else{
                _segment.selectedSegmentIndex = 0;
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                         message:nil
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                     [self updateApp];
                                                                 }];
                [alertController addAction:action];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * action) {
                                                                   [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                               }];
                [alertController addAction:cancel];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
            break;
        default:
            break;
    }
    
}

////////////////
//scroll
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    DDLogVerbose(@"end drag");
    if (scrollView == _jobScroll) {
        
        if(scrollView.contentOffset.y < -50.0 && checkScrollJobs == 0){
            _InternetConnection = [[InternetConnection alloc] init];
            if([_InternetConnection connected])
            {
                checkScrollJobs = 1;
                UIView *backgroundView;
                _loaderIMG = [[LoaderImg alloc] init];
                backgroundView = [_loaderIMG loader:backgroundView];
                [self.view addSubview:backgroundView];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    //load search
                    NSString *keywordSearch = _keywordAdvanceJob.text;
                    if ([keywordSearch isEqualToString:@"Key Word"]) {
                        keywordSearch = @"";
                    }
                    NSString *searchURl = [NSString stringWithFormat:@"tr-api/?action=searchJob&user_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], jobPallet, jobBox, jobTruckload, searchStreet, searchCity, searchCountry, fromDateJobStr, toDateJobStr, keywordSearch];
                    
                    NSMutableArray *arr = [[NSMutableArray alloc] init];
                    currentArray = [[NSMutableArray alloc] init];
                    _desktopFunc = [[DesktopFunc alloc] init];
                    arr = [_desktopFunc loadContentWhenScroll:selectJobTab :searchURl];
                    
                    if (selectJobTab != 4) {
                        currentArray = arr[0];
                        today = arr[1];
                        schedule = arr[2];
                        complete = arr[3];
                    }else{
                        currentArray = arr;
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_tableView reloadData];
                        
                        CGFloat heightCurrent = [UIScreen mainScreen].bounds.size.height - 100;
                        jContentViewHeight = 0;
                        for (int shipment = 0; shipment < currentArray.count; shipment++) {
                            shipmentObject *sh = [[shipmentObject alloc] init];
                            sh = currentArray[shipment];
                            CGFloat width = [UIScreen mainScreen].bounds.size.width;
                            width = width*0.66;
                            int height = 120;
                            if (sh.status_id < 11) {
                                height = height + 40;
                            }
                            jContentViewHeight += height + width;
                        }
                        jContentViewHeight +=70;
                        if (jContentViewHeight < heightCurrent) {
                            jContentViewHeight = heightCurrent;
                        }
                        [self.view removeConstraint:jobsContentViewHeight];
                        jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                                             attribute:NSLayoutAttributeHeight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:nil
                                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                                            multiplier:1.0
                                                                              constant:jContentViewHeight];
                        [self.view addConstraint:jobsContentViewHeight];
                        
                        
                        [backgroundView removeFromSuperview];
                        checkScrollJobs = 0;
                        if (currentArray.count == 0) {
                            if (selectOffersTab == 4) {
                                _noJobsLabel.text = @"Does not jobs in this search";
                            }else{
                                _noJobsLabel.text = @"There are no job";
                            }
                            _noJobsLabel.hidden = NO;
                        }else{
                            _noJobsLabel.hidden = YES;
                        }
                    });
                    
                });
            }
        }
        
    }else{
        if(scrollView.contentOffset.y < -50.0 && checkScrollOffers == 0){
            _InternetConnection = [[InternetConnection alloc] init];
            if([_InternetConnection connected])
            {
                checkScrollOffers = 1;
                UIView *backgroundView;
                _loaderIMG = [[LoaderImg alloc] init];
                backgroundView = [_loaderIMG loader:backgroundView];
                [self.view addSubview:backgroundView];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    //load search
                    NSString *keywordSearch = _keywordAdvanceOffer.text;
                    if ([keywordSearch isEqualToString:@"Key Word"]) {
                        keywordSearch = @"";
                    }
                    NSString *searchURl = [NSString stringWithFormat:@"tr-api/?action=searchOffer&carrier_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], offerPallet, offerBox, offerTruckload, searchStreet, searchCity, searchCountry, fromDateStr, toDateStr, @""];
                    
                    NSMutableArray *arr = [[NSMutableArray alloc] init];
                    currentOffersArray = [[NSMutableArray alloc] init];
                    _desktopFunc = [[DesktopFunc alloc] init];
                    arr = [_desktopFunc loadContentOffersWhenScroll:selectOffersTab :searchURl];
                    if (selectOffersTab == 3) {
                        currentOffersArray = arr;
                    }else{
                        currentOffersArray = arr[0];
                        offersTodayArr = arr[1];
                        offersSchedule = arr[2];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view removeConstraint: offersContentViewHeight];
                        CGFloat width = [UIScreen mainScreen].bounds.size.width;
                        width = width*0.66;
                        CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
                        if (heightOffer2 < (currentOffersArray.count*(width + 140) + 50)) {
                            heightOffer2 = currentOffersArray.count*(width + 140) + 50;
                        }
                        offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                                               attribute:NSLayoutAttributeHeight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil
                                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0
                                                                                constant:heightOffer2];
                        [self.view addConstraint:offersContentViewHeight];
                        [_offerTable reloadData];
                        [backgroundView removeFromSuperview];
                        checkScrollOffers = 0;
                        if (currentOffersArray.count == 0) {
                            if (selectOffersTab == 3) {
                                _noOfferLabel.text = @"Does not offers in this search";
                            }else{
                                _noOfferLabel.text = @"There are no offers";
                            }
                            _noOfferLabel.hidden = NO;
                            _offerNumView.hidden = YES;
                        }else{
                            _noOfferLabel.hidden = YES;
                            [_offerNumView.layer setCornerRadius:8.0f];
                            _offerNumLabel.text = [NSString stringWithFormat:@"%d",offerNum];
                            _offerNumView.hidden = NO;
                        }
                    });
                    
                });
            }
        }
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openSelectJob"]) {
        Jobs *jobsController = [segue destinationViewController];
        jobsController.job = [[shipmentObject alloc] init];
        jobsController.job = shipmentSelect;
    }
    if([segue.identifier isEqualToString:@"openSelectOffer"]) {
        Offers *offersController = [segue destinationViewController];
        offersController.offer = [[offerObject alloc] init];
        offersController.offer = offerSelect;
    }
}

-(void)getSelectTab:(int)type :(int)tab{
    checkBack = 1;
    tabType = type;
    tabSelect = tab;
}

- (IBAction)closeSearchOffer:(id)sender {
    _searchViewOffer.hidden = YES;
    _keywordOffer.text = @"Key Word";
}

- (IBAction)offerSearchAdvance:(id)sender {
    _searchOfferAdvance.hidden = NO;
    _noOfferLabel.hidden = YES;
    
    _offerTable.hidden = YES;
}
- (IBAction)closeSearchAdvance:(id)sender {
    _searchOfferAdvance.hidden = YES;
    
    _searchLocationOffer.text = @"Location";
    searchCountry = @"";
    searchCity = @"";
    searchStreet = @"";
    _offerSearchBox.image = [UIImage imageNamed:@""];
    _offerSearchPallet.image = [UIImage imageNamed:@""];
    _offerSearchTruckload.image = [UIImage imageNamed:@""];
    _keywordAdvanceOffer.text = @"Key Word";
    [_offerSearchTo setTitle:@" To" forState:UIControlStateNormal];
    [_offerSearchFrom setTitle:@" From" forState:UIControlStateNormal];
    
    NSDate *currentDate = [NSDate date];
    [_offerDatePickerFrom setDate:currentDate];
    [_offerDatePickerTo setDate:currentDate];
    
}

- (IBAction)searchAdvance:(id)sender {
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSString *keywordSearch = _keywordAdvanceOffer.text;
            if ([keywordSearch isEqualToString:@"Key Word"]) {
                keywordSearch = @"";
            }
            _desktopFunc = [[DesktopFunc alloc] init];
            currentOffersArray = [[NSMutableArray alloc] init];
            currentOffersArray = [_desktopFunc searchOffer:searchCountry :searchCity :searchStreet :offerPallet :offerBox :offerTruckload :fromDateStr :toDateStr :@""];
            dispatch_async(dispatch_get_main_queue(), ^{
                _searchOfferAdvance.hidden = YES;
                _offerTable.hidden = NO;
                
                [self.view removeConstraint: offersContentViewHeight];
                CGFloat width = [UIScreen mainScreen].bounds.size.width;
                width = width*0.66;
                
                CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
                if (heightOffer2 < (currentOffersArray.count*(width + 140) + 50)) {
                    heightOffer2 = currentOffersArray.count*(width + 140) + 50;
                }
                offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:heightOffer2];
                [self.view addConstraint:offersContentViewHeight];
                
                [_offerTable reloadData];
                
                if (currentOffersArray.count == 0) {
                    _noOfferLabel.text = @"Does not offers in this search";
                    _noOfferLabel.hidden = NO;
                }
                
                
                _searchLocationOffer.text = @"Location";
                searchCountry = @"";
                searchCity = @"";
                searchStreet = @"";
                _offerSearchBox.image = [UIImage imageNamed:@""];
                _offerSearchPallet.image = [UIImage imageNamed:@""];
                _offerSearchTruckload.image = [UIImage imageNamed:@""];
                _keywordAdvanceOffer.text = @"Key Word";
                [_offerSearchTo setTitle:@" To" forState:UIControlStateNormal];
                [_offerSearchFrom setTitle:@" From" forState:UIControlStateNormal];
                
                NSDate *currentDate = [NSDate date];
                [_offerDatePickerFrom setDate:currentDate];
                [_offerDatePickerTo setDate:currentDate];
                [backgroundView removeFromSuperview];
            });
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)offersSearchClickView{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSString *keywordSearch = _keywordOffer.text;
            if ([keywordSearch isEqualToString:@"Key Word"]) {
                keywordSearch = @"";
            }
            _desktopFunc = [[DesktopFunc alloc] init];
            currentOffersArray = [[NSMutableArray alloc] init];
            currentOffersArray = [_desktopFunc searchOffer:@"" :@"" :@"" :@"0" :@"0" :@"0" :@"" :@"" :keywordSearch];
            dispatch_async(dispatch_get_main_queue(), ^{
                _searchOfferAdvance.hidden = YES;
                _offerTable.hidden = NO;
                
                [self.view removeConstraint: offersContentViewHeight];
                CGFloat width = [UIScreen mainScreen].bounds.size.width;
                width = width*0.66;
                
                CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
                if (heightOffer2 < (currentOffersArray.count*(width + 140) + 50)) {
                    heightOffer2 = currentOffersArray.count*(width + 140) + 50;
                }
                offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:heightOffer2];
                [self.view addConstraint:offersContentViewHeight];
                
                [_offerTable reloadData];
                
                [backgroundView removeFromSuperview];
            });
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)jobsSearchClickView{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSString *keywordSearch = _keywordJob.text;
            if ([keywordSearch isEqualToString:@"Key Word"]) {
                keywordSearch = @"";
            }
            _desktopFunc = [[DesktopFunc alloc] init];
            currentArray = [[NSMutableArray alloc] init];
            currentArray = [_desktopFunc searchJob:@"" :@"" :@"" :@"0" :@"0" :@"0" :@"" :@"" :keywordSearch];
            dispatch_async(dispatch_get_main_queue(), ^{
                _searchJobAdvance.hidden = YES;
                _tableView.hidden = NO;
                
                
                int heightScroll = 0;
                if (currentArray.count == 0) {
                    _noJobsLabel.hidden = NO;
                    _noJobsLabel.text = @"Does not jobs in this search";
                }else{
                    _noJobsLabel.hidden = YES;
                    for (int i = 0; i < currentArray.count; i++) {
                        shipmentObject *sh = [[shipmentObject alloc] init];
                        sh = [currentArray objectAtIndex:i];
                        if (sh.status_id < 11) {
                            heightScroll += 40;
                        }
                    }
                }
                [self.view removeConstraint: jobsContentViewHeight];
                CGFloat width = [UIScreen mainScreen].bounds.size.width;
                width = width*0.66;
                
                CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height - 100;
                if (heightOffer2 < (currentArray.count*(width + 100) + 50)) {
                    heightOffer2 = currentArray.count*(width + 100) + 50;
                }
                
                jContentViewHeight = 0;
                for (int shipment = 0; shipment < currentArray.count; shipment++) {
                    shipmentObject *sh = [[shipmentObject alloc] init];
                    sh = currentArray[shipment];
                    CGFloat width = [UIScreen mainScreen].bounds.size.width;
                    width = width*0.66;
                    int height = 120;
                    if (sh.status_id < 11) {
                        height = height + 40;
                    }
                    jContentViewHeight += height + width;
                }
                jContentViewHeight +=70;
                if (jContentViewHeight < heightOffer2) {
                    jContentViewHeight = heightOffer2;
                }
                jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0
                                                                      constant:jContentViewHeight];
                [self.view addConstraint:jobsContentViewHeight];
                
                [_tableView reloadData];
                
                [backgroundView removeFromSuperview];
            });
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

- (IBAction)searchjobAdvance:(id)sender {
    DDLogVerbose(@"search job advance");
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSString *keywordSearch = _keywordAdvanceJob.text;
            if ([keywordSearch isEqualToString:@"Key Word"]) {
                keywordSearch = @"";
            }
            _desktopFunc = [[DesktopFunc alloc] init];
            currentArray = [[NSMutableArray alloc] init];
            currentArray = [_desktopFunc searchJob:searchCountry :searchCity :searchStreet :jobPallet :jobBox :jobTruckload :fromDateJobStr : toDateJobStr :keywordSearch];
            dispatch_async(dispatch_get_main_queue(), ^{
                _searchJobAdvance.hidden = YES;
                _tableView.hidden = NO;
                
                int heightScroll = 0;
                if (currentArray.count == 0) {
                    _noJobsLabel.hidden = NO;
                    _noJobsLabel.text = @"Does not jobs in this search";
                }else{
                    _noJobsLabel.hidden = YES;
                    for (int i = 0; i < currentArray.count; i++) {
                        shipmentObject *sh = [[shipmentObject alloc] init];
                        sh = [currentArray objectAtIndex:i];
                        if (sh.status_id < 11) {
                            heightScroll += 40;
                        }
                    }
                }
                [self.view removeConstraint: jobsContentViewHeight];
                CGFloat width = [UIScreen mainScreen].bounds.size.width;
                width = width*0.66;
                
                CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height - 100;
                if (heightOffer2 < (currentArray.count*(width + 100) + 50)) {
                    heightOffer2 = currentArray.count*(width + 100) + 50;
                }
                
                jContentViewHeight = 0;
                for (int shipment = 0; shipment < currentArray.count; shipment++) {
                    shipmentObject *sh = [[shipmentObject alloc] init];
                    sh = currentArray[shipment];
                    CGFloat width = [UIScreen mainScreen].bounds.size.width;
                    width = width*0.66;
                    int height = 120;
                    if (sh.status_id < 11) {
                        height = height + 40;
                    }
                    jContentViewHeight += height + width;
                }
                jContentViewHeight +=70;
                if (jContentViewHeight < heightOffer2) {
                    jContentViewHeight = heightOffer2;
                }
                jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0
                                                                      constant:jContentViewHeight];
                [self.view addConstraint:jobsContentViewHeight];
                
                [_tableView reloadData];
                
                [backgroundView removeFromSuperview];
            });
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == _searchLocationOffer || textField == _searchLocationJob) {
        //textField.text = @"";
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        acController.delegate = self;
        [self presentViewController:acController animated:YES completion:nil];
    }
    else{
        textField.text = @"";
    }
    
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    DDLogVerbose(@"Place name %@", place.name);
    DDLogVerbose(@"Place address %@", place.formattedAddress);
    _searchLocationOffer.text = place.formattedAddress;
    _searchLocationJob.text = place.formattedAddress;
    DDLogVerbose(@"Place attributions %@", place.attributions.string);
    NSArray *arr = [[NSArray alloc] init];
    arr = [place.formattedAddress componentsSeparatedByString:@","];
    if (arr.count == 1) {
        searchCountry = arr[0];
    }else if (arr.count == 2){
        searchCity = arr[0];
        searchCountry = [arr[1] substringWithRange:NSMakeRange(1, [arr[1] length]-1)];
    }else if (arr.count == 3){
        searchStreet = arr[0];
        searchCity = [arr[1] substringWithRange:NSMakeRange(1, [arr[1] length]-1)];
        searchCountry = [arr[2] substringWithRange:NSMakeRange(1, [arr[2] length]-1)];
        
        
    }
    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    DDLogVerbose(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (CGFloat)widthOfString:(NSString *)string withFont:(NSFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

-(void)offersSearchBoxClick{
    if (offerCheckBox == 0) {
        offerCheckBox = 1;
        offerBox = @"1";
        _offerSearchBox.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        offerCheckBox = 0;
        offerBox = @"0";
        _offerSearchBox.image = [UIImage imageNamed:@""];
    }
}

-(void)offersSearchPalletClick{
    if (offerCheckPallet == 0) {
        offerCheckPallet = 1;
        offerPallet = @"1";
        _offerSearchPallet.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        offerCheckPallet = 0;
        offerPallet = @"0";
        _offerSearchPallet.image = [UIImage imageNamed:@""];
    }
}

-(void)offersSearchTruckloadClick{
    if (offerCheckTruckload == 0) {
        offerCheckTruckload = 1;
        offerTruckload = @"1";
        _offerSearchTruckload.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        offerCheckTruckload = 0;
        offerTruckload = @"0";
        _offerSearchTruckload.image = [UIImage imageNamed:@""];
    }
}

-(void)jobsSearchBoxClick{
    if (jobCheckBox == 0) {
        jobCheckBox = 1;
        jobBox = @"1";
        _jobSearchBox.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        jobCheckBox = 0;
        jobBox = @"0";
        _jobSearchBox.image = [UIImage imageNamed:@""];
    }
}

-(void)jobsSearchPalletClick{
    if (jobCheckPallet == 0) {
        jobCheckPallet = 1;
        jobPallet = @"1";
        _jobSearchpallet.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        jobCheckPallet = 0;
        jobPallet = @"0";
        _jobSearchpallet.image = [UIImage imageNamed:@""];
    }
}

-(void)jobsSearchTruckloadClick{
    if (jobCheckTruckload == 0) {
        jobCheckTruckload = 1;
        jobTruckload = @"1";
        _jobSearchTruclkoad.image = [UIImage imageNamed:@"check_box.png"];
        
    }else{
        jobCheckTruckload = 0;
        jobTruckload = @"0";
        _jobSearchTruclkoad.image = [UIImage imageNamed:@""];
    }
}


//textfield
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //validate
    
    [textField resignFirstResponder];
    
    
    return YES;
}


- (IBAction)chooseOfferPicker:(id)sender {
    _offerDatePickerView.hidden = YES;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if (checkDatePickerSelect == 1) {
        NSString *str = [dateFormatter stringFromDate:_offerDatePickerFrom.date];
        [_offerSearchFrom setTitle:[NSString stringWithFormat:@"  %@",str] forState:UIControlStateNormal];
        fromDateStr = str;
    }
    if (checkDatePickerSelect == 2) {
        NSString *str = [dateFormatter stringFromDate:_offerDatePickerTo.date];
        [_offerSearchTo setTitle:[NSString stringWithFormat:@"  %@",str] forState:UIControlStateNormal];
        toDateStr = str;
        
    }
}
- (IBAction)offerSearchFromClick:(id)sender {
    _offerDatePickerView.hidden = NO;
    _offerDatePickerTo.hidden = YES;
    _offerDatePickerFrom.hidden = NO;
    checkDatePickerSelect = 1;
}
- (IBAction)offerSearchToClick:(id)sender {
    _offerDatePickerView.hidden = NO;
    _offerDatePickerTo.hidden = NO;
    _offerDatePickerFrom.hidden = YES;
    checkDatePickerSelect = 2;
}

- (IBAction)closeSearchJob:(id)sender {
    _searchViewJob.hidden = YES;
    _keywordJob.text = @"Key Word";
}

- (IBAction)jobSearchAdvance:(id)sender {
    _searchJobAdvance.hidden = NO;
    
}
- (IBAction)closeSearchJobAdvance:(id)sender {
    _searchJobAdvance.hidden = YES;
    
    _searchLocationJob.text = @"Location";
    searchCountry = @"";
    searchCity = @"";
    searchStreet = @"";
    _jobSearchBox.image = [UIImage imageNamed:@""];
    _jobSearchpallet.image = [UIImage imageNamed:@""];
    _jobSearchTruclkoad.image = [UIImage imageNamed:@""];
    _keywordAdvanceJob.text = @"Key Word";
    [_jobSearchTo setTitle:@" To" forState:UIControlStateNormal];
    [_jobSearchFrom setTitle:@" From" forState:UIControlStateNormal];
    
    NSDate *currentDate = [NSDate date];
    [_offerDatePickerFrom setDate:currentDate];
    [_offerDatePickerTo setDate:currentDate];
}

- (IBAction)chooseJobPicker:(id)sender {
    _jobDatePickerView.hidden = YES;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if (checkDatePickerJobSelect == 1) {
        NSString *str = [dateFormatter stringFromDate:_jobDatePickerFrom.date];
        [_jobSearchFrom setTitle:[NSString stringWithFormat:@"  %@",str] forState:UIControlStateNormal];
        fromDateJobStr = str;
    }
    if (checkDatePickerJobSelect == 2) {
        NSString *str = [dateFormatter stringFromDate:_jobDatePickerTo.date];
        [_jobSearchTo setTitle:[NSString stringWithFormat:@"  %@",str] forState:UIControlStateNormal];
        toDateJobStr = str;
        
    }
    
}

- (IBAction)jobSearchFromClick:(id)sender {
    _jobDatePickerView.hidden = NO;
    _jobDatePickerTo.hidden = YES;
    _jobDatePickerFrom.hidden = NO;
    checkDatePickerJobSelect = 1;
}

- (IBAction)jobSearchToClick:(id)sender {
    _jobDatePickerView.hidden = NO;
    _jobDatePickerTo.hidden = NO;
    _jobDatePickerFrom.hidden = YES;
    checkDatePickerJobSelect = 2;
}

-(void)reloadOfferTable{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        DDLogVerbose(@"reloadOfferTable");
        checkScrollOffers = 1;
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //load search
            NSString *keywordSearch = _keywordAdvanceOffer.text;
            if ([keywordSearch isEqualToString:@"Key Word"]) {
                keywordSearch = @"";
            }
            NSString *searchURl = [NSString stringWithFormat:@"tr-api/?action=searchOffer&carrier_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], offerPallet, offerBox, offerTruckload, searchStreet, searchCity, searchCountry, fromDateStr, toDateStr, @""];
            
            NSMutableArray *arr = [[NSMutableArray alloc] init];
            currentOffersArray = [[NSMutableArray alloc] init];
            _desktopFunc = [[DesktopFunc alloc] init];
            arr = [_desktopFunc loadContentOffersWhenScroll:selectOffersTab :searchURl];
            if (selectOffersTab == 3) {
                currentOffersArray = arr;
            }else{
                currentOffersArray = arr[0];
                offersTodayArr = arr[1];
                offersSchedule = arr[2];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view removeConstraint: offersContentViewHeight];
                CGFloat width = [UIScreen mainScreen].bounds.size.width;
                width = width*0.66;
                CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
                if (heightOffer2 < (currentOffersArray.count*(width + 140) + 50)) {
                    heightOffer2 = currentOffersArray.count*(width + 140) + 50;
                }
                offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:heightOffer2];
                [self.view addConstraint:offersContentViewHeight];
                [_offerTable reloadData];
                [backgroundView removeFromSuperview];
                checkScrollOffers = 0;
                if (currentOffersArray.count == 0) {
                    if (selectOffersTab == 3) {
                        _noOfferLabel.text = @"Does not offers in this search";
                    }else{
                        _noOfferLabel.text = @"There are no offers";
                    }
                    _noOfferLabel.hidden = NO;
                }else{
                    _noOfferLabel.hidden = YES;
                }
            });
            
        });
    }
}

-(void)jobClearLocation{
    _searchLocationJob.text = @"Location";
}

-(void)offerClearLocation{
    _searchLocationOffer.text = @"Location";
}

-(void)turnOnMylocation{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
}

//created by batya


- (IBAction)supportCall:(id)sender {
    DDLogVerbose(@"supportCall");
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
    
}

//function
-(void)badge{
    _dbFunctions =[[dbFunctions alloc] init];
    int badge = [_dbFunctions returnBadgeNum];
    [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
    NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
    [badgeDefault setInteger:badge forKey:@"badge"];
    
    [badgeDefault synchronize];
    if (badge == 0) {
        _badgeView.hidden = YES;
        _badgeButton.hidden = YES;
    }else{
        [_badgeView.layer setCornerRadius:10.0f];
        _badgeLabel.text = [NSString stringWithFormat:@"%d",badge];
         [_badgeButton.layer setCornerRadius:10.0f];
        [_badgeButton setTitle:[NSString stringWithFormat:@"%d",badge] forState:UIControlStateNormal];
    }
}

-(void)showSegment{
    //segment
    [_segment setWidth:30.0f forSegmentAtIndex:0];
    
    _segment.layer.cornerRadius = 15.0;
    _segment.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _segment.layer.borderWidth = 1.0f;
    _segment.layer.masksToBounds = YES;
    LocationObject *location = [LocationObject sharedInstance];
    if([[[NSUserDefaults standardUserDefaults]
         stringForKey:@"driverStatus"] intValue] == 4 || location.getMyLocation == 0){
        //unavailable
        [_segment setTintColor:[UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0]];
        [_segment setSelectedSegmentIndex:1];
        [_segment setWidth:30.0f forSegmentAtIndex:1];
        [_segment setTitle:@"" forSegmentAtIndex:1];
        [_segment setWidth:80.0f forSegmentAtIndex:0];
        [_segment setTitle:@"  Unvailable" forSegmentAtIndex:0];
        
        
    }else{
        
        [_segment setTintColor:[UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0]];
        [_segment setSelectedSegmentIndex:0];
        
        [_segment setWidth:80.0f forSegmentAtIndex:1];
        [_segment setTitle:@"Available    " forSegmentAtIndex:1];
        [_segment setWidth:30.0f forSegmentAtIndex:0];
        [_segment setTitle:@"" forSegmentAtIndex:0];
        [_segment setTintColor:[UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0]];
        
    }
}

-(void)checkCanGetOffers{
    //check if can get offers
    if ([[[NSUserDefaults standardUserDefaults]
          stringForKey:@"can_see_offers"] isEqualToString:@"0"])
    {
        _offersNavView.hidden = YES;
        _offersView.hidden = YES;
        
        jobsRight = [NSLayoutConstraint constraintWithItem:_jobsNavView
                                                 attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:self.view
                                                 attribute:NSLayoutAttributeRight
                                                multiplier:1.0
                                                  constant:0];
        [self.view addConstraint:jobsRight];
        
    }
}

-(void)firstHidden{
    _offerNumView.hidden = YES;
    _searchOfferAdvance.hidden = YES;
    _searchViewOffer.hidden = YES;
    _searchViewJob.hidden = YES;
    _searchJobAdvance.hidden = YES;
    //_swipeNavigation.hidden = YES;
    //_container.hidden = YES;
    _noJobsLabel.hidden = YES;
}

-(void)jobs{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_jobsToday.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _jobsToday.layer.mask = maskLayer;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_jobsComplete.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.view.bounds;
    maskLayer2.path  = maskPath2.CGPath;
    _jobsComplete.layer.mask = maskLayer2;
    
    _jobsNavigation.layer.cornerRadius = 20.0f;
    CGFloat borderWidth = 1.0f;
    
    _jobsNavigation.frame = CGRectInset(_jobsNavigation.frame, -borderWidth, -borderWidth);
    _jobsNavigation.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _jobsNavigation.layer.borderWidth = borderWidth;
    
    _jobsToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    
    _jobsTodayTitle.textColor = [UIColor whiteColor];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _noJobsLabel.hidden = YES;
}

-(void)offers{
    UIBezierPath *maskPathOffers = [UIBezierPath bezierPathWithRoundedRect:_offersToday.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayerOffers = [[CAShapeLayer alloc] init];
    maskLayerOffers.frame = self.view.bounds;
    maskLayerOffers.path  = maskPathOffers.CGPath;
    _offersToday.layer.mask = maskLayerOffers;
    
    maskPathOffers = [UIBezierPath bezierPathWithRoundedRect:_offersSchedualed.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayerOffers = [[CAShapeLayer alloc] init];
    maskLayerOffers.frame = self.view.bounds;
    maskLayerOffers.path  = maskPathOffers.CGPath;
    _offersSchedualed.layer.mask = maskLayerOffers;
    
    _offersNavigation.layer.cornerRadius = 20.0f;
    CGFloat borderWidthOffers = 1.0f;
    
    _offersNavigation.frame = CGRectInset(_offersNavigation.frame, -borderWidthOffers, -borderWidthOffers);
    _offersNavigation.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _offersNavigation.layer.borderWidth = borderWidthOffers;
    
    _offersToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersSchedualed.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    
    _offersTodayTitle.textColor = [UIColor whiteColor];
    _offersSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    
    _searchOfferAdvance.frame = CGRectInset(_offersNavigation.frame, -borderWidthOffers, -borderWidthOffers);
    _searchOfferAdvance.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _searchOfferAdvance.layer.borderWidth = borderWidthOffers;
    _searchOfferAdvance.layer.cornerRadius = 10.0f;
    
    _topSearchAdvance.layer.cornerRadius = 10.0f;
    _searchButtonSearchAdvance.layer.cornerRadius = 5.0f;
    
    _offerSearchBox.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _offerSearchBox.layer.borderWidth = 1.0f;
    _offerSearchPallet.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _offerSearchPallet.layer.borderWidth = 1.0f;
    _offerSearchTruckload.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _offerSearchTruckload.layer.borderWidth = 1.0f;
    
    
    
}

-(void)offerSearch{
    _offerSearchTo.layer.cornerRadius = 5.0f;
    _offerSearchTo.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _offerSearchTo.layer.borderWidth = 1.0f;
    
    _offerSearchFrom.layer.cornerRadius = 5.0f;
    _offerSearchFrom.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _offerSearchFrom.layer.borderWidth = 1.0f;
    
    _searchViewOffer.layer.cornerRadius = 20.0f;
    
    UIBezierPath *maskPathOffers;
    CAShapeLayer *maskLayerOffers;
    maskPathOffers = [UIBezierPath bezierPathWithRoundedRect:_offerSearchClickView.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayerOffers = [[CAShapeLayer alloc] init];
    maskLayerOffers.frame = self.view.bounds;
    maskLayerOffers.path  = maskPathOffers.CGPath;
    _offerSearchClickView.layer.mask = maskLayerOffers;
    
}

-(void)searchJob{
    _searchViewJob.layer.cornerRadius = 20.0f;
    UIBezierPath *maskPathOffers;
    CAShapeLayer *maskLayerOffers;
    CGFloat borderWidthOffers = 1.0f;
    maskPathOffers = [UIBezierPath bezierPathWithRoundedRect:_jobSearchClickView.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayerOffers = [[CAShapeLayer alloc] init];
    maskLayerOffers.frame = self.view.bounds;
    maskLayerOffers.path  = maskPathOffers.CGPath;
    _jobSearchClickView.layer.mask = maskLayerOffers;
    
    _searchJobAdvance.frame = CGRectInset(_offersNavigation.frame, -borderWidthOffers, -borderWidthOffers);
    _searchJobAdvance.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _searchJobAdvance.layer.borderWidth = borderWidthOffers;
    _searchJobAdvance.layer.cornerRadius = 10.0f;
    
    _topSearchJobAdvance.layer.cornerRadius = 10.0f;
    _searchButtonJobAdvance.layer.cornerRadius = 5.0f;
    
    _jobSearchBox.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _jobSearchBox.layer.borderWidth = 1.0f;
    _jobSearchpallet.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _jobSearchpallet.layer.borderWidth = 1.0f;
    _jobSearchTruclkoad.layer.borderColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0].CGColor;
    _jobSearchTruclkoad.layer.borderWidth = 1.0f;
    
    _jobSearchTo.layer.cornerRadius = 5.0f;
    _jobSearchTo.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _jobSearchTo.layer.borderWidth = 1.0f;
    
    _jobSearchFrom.layer.cornerRadius = 5.0f;
    _jobSearchFrom.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _jobSearchFrom.layer.borderWidth = 1.0f;
    
    _offerSearchDateButton.layer.cornerRadius = 5.0f;
    _jobSearchDateButton.layer.cornerRadius = 5.0f;
}

-(void)jobsTodayEmptyUI{
    currentArray = today;
    
    _jobsToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsTodayTitle.textColor = [UIColor whiteColor];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noJobsLabel.hidden = NO;
    _noJobsLabel.text = @"There are no jobs today";
    
    
    [self.view removeConstraint: jobsContentViewHeight];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height2 = [UIScreen mainScreen].bounds.size.height - 100;
    if (height2 < (today.count*(width + 100) + 50)) {
        height2 = today.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < today.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = today[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height2) {
        jContentViewHeight = height2;
    }
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    [_tableView reloadData];
    
    selectJobTab = 1;
}

-(void)jobsCompleteUI{
    currentArray = complete;
    
    _jobsComplete.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsToday.backgroundColor = [UIColor clearColor];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsCompleteTitle.textColor = [UIColor whiteColor];
    _jobsTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noJobsLabel.hidden = YES;
    
    
    [self.view removeConstraint: jobsContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height7 = [UIScreen mainScreen].bounds.size.width - 100;
    if (height7 < (complete.count*(width + 100) + 50)) {
        height7 = complete.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < complete.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = complete[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height7) {
        jContentViewHeight = height7;
    }
    
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    [_tableView reloadData];
    
    selectJobTab = 3;
}

-(void)jobsScheduleUI{
    currentArray = schedule;
    _jobsSchedualed.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsToday.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsSchedualedTitle.textColor = [UIColor whiteColor];
    _jobsTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noJobsLabel.hidden = YES;
    
    int heightScroll = 0;
    
    for (int i = 0; i < currentArray.count; i++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = [currentArray objectAtIndex:i];
        if (sh.status_id < 11) {
            heightScroll += 40;
        }
    }
    
    [self.view removeConstraint: jobsContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height3 = [UIScreen mainScreen].bounds.size.height - 100;
    if (height3 < (schedule.count*(width + 100) + 50)) {
        height3 = schedule.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < schedule.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = schedule[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height3) {
        jContentViewHeight = height3;
    }
    
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    [_tableView reloadData];
    
    selectJobTab = 2;
}

-(void)jobsTodayUI{
    currentArray = today;
    
    _jobsToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsSchedualed.backgroundColor = [UIColor clearColor];
    _jobsComplete.backgroundColor = [UIColor clearColor];
    _jobsSearch.backgroundColor = [UIColor clearColor];
    _jobSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _jobsTodayTitle.textColor = [UIColor whiteColor];
    _jobsSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    _jobsCompleteTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noJobsLabel.hidden = YES;
    int heightScroll = 0;
    
    for (int i = 0; i < currentArray.count; i++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = [currentArray objectAtIndex:i];
        if (sh.status_id < 11) {
            heightScroll += 40;
        }
    }
    
    [self.view removeConstraint: jobsContentViewHeight];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    CGFloat height2 = [UIScreen mainScreen].bounds.size.height - 100;
    if (height2 < (today.count*(width + 100) + 50)) {
        height2 = today.count*(width + 100) + 50;
    }
    
    jContentViewHeight = 0;
    for (int shipment = 0; shipment < today.count; shipment++) {
        shipmentObject *sh = [[shipmentObject alloc] init];
        sh = today[shipment];
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width*0.66;
        int height = 120;
        if (sh.status_id < 11) {
            height = height + 40;
        }
        jContentViewHeight += height + width;
    }
    jContentViewHeight +=70;
    if (jContentViewHeight < height2) {
        jContentViewHeight = height2;
    }
    jobsContentViewHeight = [NSLayoutConstraint constraintWithItem:_jobsContentView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:jContentViewHeight];
    [self.view addConstraint:jobsContentViewHeight];
    
    [_tableView reloadData];
    
    selectJobTab = 1;
    
}

-(void)offersTodayEmptyUI{
    currentOffersArray = offersTodayArr;
    _noOfferLabel.hidden = NO;
    _noOfferLabel.text = @"There are no offers";
    
    currentOffersArray = offersTodayArr;
    
    _offersToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersSchedualed.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    _offerSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _offersTodayTitle.textColor = [UIColor whiteColor];
    _offersSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    
    CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
    if (heightOffer2 < (offersTodayArr.count*(width + 140) + 50)) {
        heightOffer2 = offersTodayArr.count*(width + 140) + 50;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightOffer2];
    [self.view addConstraint:offersContentViewHeight];
    
    [_offerTable reloadData];
    
    selectOffersTab = 1;
    
}

-(void)offerScheduleUI{
    currentOffersArray = offersSchedule;
    _noOfferLabel.hidden = YES;
    
    _offersSchedualed.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersToday.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    _offerSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _offersSchedualedTitle.textColor = [UIColor whiteColor];
    _offersTodayTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noOfferLabel.hidden = YES;
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    
    CGFloat heightOffer3 = [UIScreen mainScreen].bounds.size.height;
    if (heightOffer3 < (offersSchedule.count*(width + 140) + 50)) {
        heightOffer3 = offersSchedule.count*(width + 140) + 50;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightOffer3];
    [self.view addConstraint:offersContentViewHeight];
    
    [_offerTable reloadData];
    
    selectOffersTab = 2;
}

-(void)offersTodayUI{
    currentOffersArray = offersTodayArr;
    
    _offersToday.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersSchedualed.backgroundColor = [UIColor clearColor];
    _offersSearch.backgroundColor = [UIColor clearColor];
    _offerSearchImage.image = [UIImage imageNamed:@"search.png"];
    
    _offersTodayTitle.textColor = [UIColor whiteColor];
    _offersSchedualedTitle.textColor  = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    _noOfferLabel.hidden = YES;
    
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    width = width*0.66;
    
    CGFloat heightOffer2 = [UIScreen mainScreen].bounds.size.height;
    if (heightOffer2 < (offersTodayArr.count*(width + 140) + 50)) {
        heightOffer2 = offersTodayArr.count*(width + 140) + 50;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_offersContentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightOffer2];
    [self.view addConstraint:offersContentViewHeight];
    
    [_offerTable reloadData];
    
    selectOffersTab = 1;
    
}

-(void)swipeLeftUI{
    _offersNavMarker.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsNavMarker.backgroundColor = [UIColor clearColor];
    _offersNavTitle.textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _jobsNavTitle.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_container.layer addAnimation:transition forKey:nil];
    
    [_container addSubview:_offersView];
    
    
    
    CATransition *transition2 = [CATransition animation];
    transition2.duration = 0.5;
    transition2.type = kCATransitionPush;
    transition2.subtype = kCATransitionFromLeft;
    [transition2 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_offersNavMarker.layer addAnimation:transition2 forKey:nil];
    
    checkRightSwipe = 1;
    checkLeftSwipe = 0;
    
}

-(void)swipeRightUI{
    _jobsNavMarker.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersNavMarker.backgroundColor = [UIColor clearColor];
    
    _jobsNavTitle.textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    _offersNavTitle.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_container.layer addAnimation:transition forKey:nil];
    
    [_container addSubview:_jobsView];
    
    CATransition *transition2 = [CATransition animation];
    transition2.duration = 0.5;
    transition2.type = kCATransitionPush;
    transition2.subtype = kCATransitionFromRight;
    [transition2 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_jobsNavMarker.layer addAnimation:transition2 forKey:nil];
    
    checkRightSwipe = 0;
    checkLeftSwipe = 1;
    
}

-(void)updateApp{
    NSString *iTunesLink = @"https://itunes.apple.com/il/app/truckiez/id1191308814?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

@end

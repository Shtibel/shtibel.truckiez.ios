//
//  PushNotificationCell.m
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "PushNotificationCell.h"

@implementation PushNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

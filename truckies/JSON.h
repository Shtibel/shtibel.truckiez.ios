//
//  JSON.h
//  location
//
//  Created by Menachem Mizrachi on 23/06/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSON : UIViewController

-(NSDictionary *)CreateAndSendJSON:(NSDictionary *)inputData :(NSString *)urlToSend;
-(NSDictionary*)getJsonDic :(NSString *)par1 :(NSString *)par2;
-(void)test:(NSString *)url :(NSString *)post;
- (NSData *) getDataFrom:(NSString *)url;
-(NSDictionary *) FromJson:(NSData *)returnedData;
- (NSData *) getDataFrom2:(NSString *)url2;
@end

//
//  PickupConfirmationCell.h
//  truckies
//
//  Created by Menachem Mizrachi on 27/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupConfirmationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *type;
@property (strong, nonatomic) IBOutlet UITextField *quantity;
@property (strong, nonatomic) IBOutlet UIButton *remove;
@property (strong, nonatomic) IBOutlet UITextField *quantity2;
@property (strong, nonatomic) IBOutlet UIImageView *arrow;


@end

//
//  JSON.m
//  location
//
//  Created by Menachem Mizrachi on 23/06/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "JSON.h"
#import "StaticVars.h"
@import CocoaLumberjack;

@interface JSON ()

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation JSON

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSData *) getDataFrom:(NSString *)url{
    
    if ([NSThread isMainThread])
    {
        NSLog(@"Run On main Thread!! check if needed");
    }
    NSString *url2 = [NSString stringWithFormat:@"%@/%@",[StaticVars url],url];
    NSString *encodedUrl = [url2 stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:encodedUrl]];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *responseCode = nil;
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    NSTimeInterval newTimeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval duration = newTimeInSeconds - timeInSeconds;
    DDLogVerbose(@"number seconds to get data from server: %f",duration);
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %li", url2, (long)[responseCode statusCode]);
        return nil;
    }
    return oResponseData;//[[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
}


- (NSData *) getDataFrom2:(NSString *)url2{
    
    if ([NSThread isMainThread])
    {
        NSLog(@"Run On main Thread!! check if needed");
    }
    NSString *encodedUrl = [url2 stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:encodedUrl]];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *responseCode = nil;
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    NSTimeInterval newTimeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval duration = newTimeInSeconds - timeInSeconds;
    DDLogVerbose(@"number seconds to get data from server: %f",duration);
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %li", url2, (long)[responseCode statusCode]);
        return nil;
    }
    return oResponseData;//[[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
}

-(NSDictionary *) FromJson:(NSData *)returnedData {
    if(NSClassFromString(@"NSJSONSerialization"))
    {
        NSError *error = nil;
        
        NSDictionary *object = [[NSDictionary alloc] init];
        if (returnedData) {
            object = [NSJSONSerialization JSONObjectWithData:returnedData options:NSJSONReadingAllowFragments error:&error];
            
        }
        
        if(error) {
            NSLog(@"%@",error);
        }
        
        NSDictionary *results = object;
        return results;
    }
    else{}
    return nil;
}
-(void)test:(NSString *)url :(NSString *)post{
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",(int)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[post dataUsingEncoding:NSUTF8StringEncoding]];
    ///////
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
}


-(NSDictionary *)CreateAndSendJSON:(NSDictionary *)inputData :(NSString *)urlToSend{
    if ([NSThread isMainThread])
    {
        NSLog(@"Run On main Thread!! check if needed");
    }
    NSError *error = nil;
    NSData *jsonInputData = [NSJSONSerialization dataWithJSONObject:inputData options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonInputString = [[NSString alloc] initWithData:jsonInputData encoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlToSend];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonInputString dataUsingEncoding:NSUTF8StringEncoding]];
    ///////
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *jsonString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    
    // Parse response
    id jsonResponseData = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
    
    NSDictionary *jsonResponseDict;
    
    jsonResponseDict = jsonResponseData;
    
    
    if ([jsonResponseData isKindOfClass:[NSDictionary class]]) {
        jsonResponseDict = jsonResponseData;
    } else {
        // Error-handling code
    }
    if (jsonResponseData == nil) {
        // Server may have returned a response containing an error
        // The "ExceptionType" value is returned from my .NET server used in sample
        id jsonExceptioTypeData = [jsonResponseDict objectForKey:@"ExceptionType"];
        if (jsonExceptioTypeData != nil) {
            NSLog(@"%s ERROR : Server returned an exception", __func__);
            NSLog(@"%s ERROR : Server error details = %@", __func__, jsonResponseDict);
        }
    }
    NSLog(@"aaa: %@",jsonResponseDict);
    return jsonResponseDict;
}


-(NSDictionary*)getJsonDic :(NSString *)par1 :(NSString *)par2{
    NSData *myData = [[NSData alloc] init];
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    myData = [self getDataFrom:[NSString stringWithFormat:@"http://app.shtibelapps.co.il/test.php?device_id=%@&lat=%@&lng=%@", uniqueIdentifier, par1, par2]];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [self FromJson:myData];
    
    return dic;
}


@end

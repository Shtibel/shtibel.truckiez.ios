//
//  Jobs.m
//  truckies
//
//  Created by Menachem Mizrachi on 07/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "Jobs.h"
#import "DesktopFunc.h"
#import "mapsControllerFunc.h"
#import "ConvertColor.h"
#import "ShipmentsControllerFunctions.h"
#import "Desktop.h"
#import "JSON.h"
#import "AFNetworking.h"
#import "CustomAlert.h"
#import "InternetConnection.h"
#import "LocationObject.h"
#import "StrippingHTML.h"
#import "SpecialLoad.h"
#import "LoaderImg.h"
#import "AwesomeMenu.h"
#import "PickupConfirmation.h"
@import CocoaLumberjack;
#import "QuartzCore/QuartzCore.h"
#import "StaticVars.h"

@interface Jobs ()
{
    DesktopFunc *_desktopFunc;
    mapsControllerFunc *_mapFunc;
    ConvertColor *_ConvertColor;
    ShipmentsControllerFunctions *_SFunc;
    JSON *_json;
    CustomAlert *_alert;
    InternetConnection *_InternetConnection;
    StrippingHTML *_html;
    LoaderImg *_loaderIMG;
    
    UIView *alert;
    CGFloat widthAlert;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
    GMSMarker *marker;
    GMSCameraPosition *camera;
    GMSMapView *mapView_;
    
    NSLayoutConstraint *offersContentViewHeight;
    NSLayoutConstraint *openOfferHeight;
    NSLayoutConstraint *heightOriginText;
    NSLayoutConstraint *heightOriginBottomText;
    NSLayoutConstraint *heightOriginTop;
    NSLayoutConstraint *heightOriginBottom;
    NSLayoutConstraint *heightOriginContent;
    NSLayoutConstraint *heightDestinationText;
    NSLayoutConstraint *heightDestinationTop;
    NSLayoutConstraint *heightRouteView;
    NSLayoutConstraint *heightSpecialRequest;
    NSLayoutConstraint *topSpecialRequest;
    NSLayoutConstraint *heightLoadView;
    NSLayoutConstraint *heightLoadbottom;
    NSLayoutConstraint *heightCommentBottom;
    NSLayoutConstraint *heightCommentView;
    NSLayoutConstraint *heightCommentsView;
    NSLayoutConstraint *heightDetails;
    NSLayoutConstraint *bottomChangeStatus;
    NSLayoutConstraint *heightChangeStatus;
    NSLayoutConstraint *routeTop;
    NSLayoutConstraint *heightShipperDetails;
    NSLayoutConstraint *topShipperDetails;
    NSLayoutConstraint *showDetailsTop;
    NSLayoutConstraint *shareHeight;
    NSLayoutConstraint *shareBottom;
    NSLayoutConstraint *rateTop;
    
    int checkOpen;
    int checkGes;
    bool mouseSwiped;
    CGPoint lastPoint;
    UIImage *imageBol;
    int heightCon;
    
    NSString *specialLoad;
    
    int checkCamera;
    UIView *backgroundView1;
    UITextView *noteField;
    
    AwesomeMenu *menu;
    
    NSString *latitudeLabel;
    NSString *longitudeLabel;
    
    NSTimer *checkLocationTimer;
    int statusConfirmation;
    
    //ui
    UIBezierPath *maskPath;
    CAShapeLayer *maskLayer;
    CGRect textsize;
    CGFloat originTextWidth;
    CGSize constraint;
    NSDictionary *attributes;
    CGFloat borderWidth;
    float widthCell;
    CGFloat getCellWidth;
    CGFloat heightScreen;
    float originTop;
    float originBottom;
    float destinationTop;
    float destinationBottom;
    
    UIView *popUp;
    int checkMapView;
}


@end
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation Jobs

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //validate
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    DDLogVerbose(@"stop location");
    //    LocationObject *location = [LocationObject sharedInstance];
    //    [location.locationManager stopUpdatingLocation];
    
    [self loadGesture];
    [self loadPage];
    [self loadPage2];
    [self awsomeMenu];
    
    if (_checkLastController == 2) {
        
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Congratulations! you have a new job. Please review the details and make sure the job will be completed successfully" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    if (_job.status_id == 16) {
        _bolView.hidden = NO;
        [menu removeFromSuperview];
        _bolImage.image = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)floatRatingView:(TPFloatRatingView *)ratingView ratingDidChange:(CGFloat)rating
{
    //self.ratingLabel.text = [NSString stringWithFormat:@"%.2f", rating];
}

- (void)floatRatingView:(TPFloatRatingView *)ratingView continuousRating:(CGFloat)rating
{
    // self.liveLabel.text = [NSString stringWithFormat:@"%.2f", rating];
}


-(void)loadGesture{
    UITapGestureRecognizer *showDeatilsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDeatils)];
    showDeatilsTap.numberOfTapsRequired = 1;
    [_detailsTap setUserInteractionEnabled:YES];
    [_detailsTap addGestureRecognizer:showDeatilsTap];
    
    UITapGestureRecognizer *changeStatusTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeStatusTap)];
    changeStatusTap.numberOfTapsRequired = 1;
    [_changeStatus setUserInteractionEnabled:YES];
    [_changeStatus addGestureRecognizer:changeStatusTap];
    
    UITapGestureRecognizer *myLocationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myLocation)];
    myLocationTap.numberOfTapsRequired = 1;
    [_myLoactionButton setUserInteractionEnabled:YES];
    [_myLoactionButton addGestureRecognizer:myLocationTap];
    
    UITapGestureRecognizer *routeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(route)];
    routeTap.numberOfTapsRequired = 1;
    [_routeButton setUserInteractionEnabled:YES];
    [_routeButton addGestureRecognizer:routeTap];
    
    UITapGestureRecognizer *shareTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(share)];
    routeTap.numberOfTapsRequired = 1;
    [_shareButton setUserInteractionEnabled:YES];
    [_shareButton addGestureRecognizer:shareTap];
    
    UITapGestureRecognizer *cameraTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraClick)];
    cameraTap.numberOfTapsRequired = 1;
    [_camera setUserInteractionEnabled:YES];
    [_camera addGestureRecognizer:cameraTap];
    
    UITapGestureRecognizer *noteTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noteClick)];
    noteTap.numberOfTapsRequired = 1;
    [_notes setUserInteractionEnabled:YES];
    [_notes addGestureRecognizer:noteTap];
    
    UITapGestureRecognizer *captureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureClick)];
    captureTap.numberOfTapsRequired = 1;
    [_captureClickView setUserInteractionEnabled:YES];
    [_captureClickView addGestureRecognizer:captureTap];
}


-(void)loadPage{
    longitudeLabel = @"0";
    latitudeLabel = @"0";
    
    widthAlert = [UIScreen mainScreen].bounds.size.width - 60;
    getCellWidth = self.view.bounds.size.width - 20;
    widthCell = 0;
    heightCon = 0;
    checkGes = 0;
    checkOpen = 0;
    originTextWidth = [UIScreen mainScreen].bounds.size.width;
    
    _truckName.text = _job.truck_type_name;
    
    
    _cancelPODView.hidden = YES;
    [_con1pickup.layer setCornerRadius:20.0f];
    [_con2pickup.layer setCornerRadius:20.0f];
    [_con2dropoff.layer setCornerRadius:20.0f];
    [_con3pickup.layer setCornerRadius:20.0f];
    [_con3pod.layer setCornerRadius:20.0f];
    [_bolCloseButton2.layer setCornerRadius:5.0f];
    [_captureClickView.layer setCornerRadius:5.0f];
    _changeStatus.layer.cornerRadius = 5.0f;
    
    
    _con1pickup.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _con1pickup.titleLabel.numberOfLines = 2;
    _con2pickup.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _con2pickup.titleLabel.numberOfLines = 2;
    _con2dropoff.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _con2dropoff.titleLabel.numberOfLines = 2;
    _con3pickup.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _con3pickup.titleLabel.numberOfLines = 2;
    _con3dropoff.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _con3dropoff.titleLabel.numberOfLines = 2;
    if ([[[NSUserDefaults standardUserDefaults]
          stringForKey:@"can_see_price"] isEqualToString:@"0"])
    {
        _priceView.hidden = YES;
    }else{
        _priceView.layer.cornerRadius = 15.0f;
        borderWidth = 1.0f;
        _priceView.frame = CGRectInset(_priceView.frame, -borderWidth, -borderWidth);
        _priceView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _priceView.layer.borderWidth = borderWidth;
    }
    
    _jobCode.text = [NSString stringWithFormat:@"JOB#%ld",_job.shipmentsId];
    
    [self showDetailsUI];
    [self addShipperDetails];
    [self loadUI];
    [self truckNumUI];
    [self specialRequestUI];
    [self weightUI];
    [self checkStatusNumUI];
    [self loadRouteUI];
    [self specialRequestUI2];
    [self commentsUI];
    [self loadRateUI];
    openOfferHeight = [NSLayoutConstraint constraintWithItem:_contentView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:heightCon];
    [self.view addConstraint:openOfferHeight];
    
    [self.view removeConstraint: offersContentViewHeight];
    heightScreen = [UIScreen mainScreen].bounds.size.height;
    heightScreen = heightScreen - 124;
    if ((heightCon + 86) < heightScreen) {
        heightScreen = heightCon + 86;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_showDetails
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightScreen];
    
    [self.view addConstraint:offersContentViewHeight];
    
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-heightScreen + 156];
    [self.view addConstraint:showDetailsTop];
    
}

-(void)loadPage2{
    
    checkLocationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                          target:self selector:@selector(checkLocation) userInfo:nil repeats:YES];
    
    [self performSelector:@selector(openMap:) withObject:_map afterDelay:0.5];
    
    
    //signature
    self.drawImageView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    self.drawImageView.layer.borderWidth = 1.0f;
    self.drawImageView.layer.cornerRadius = 5.0f;
    _signatureView.hidden = YES;
    
    _signatureSaveButton.layer.cornerRadius = 5.0f;
    _signatureClearButton.layer.cornerRadius = 5.0f;
    _signatureCloseButton.layer.cornerRadius = 5.0f;
    
    
    //BOL
    self.bolImageView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    self.bolImageView.layer.borderWidth = 1.0f;
    self.bolImageView.layer.cornerRadius = 5.0f;
    
    
    _bolView.hidden = YES;
    
    self.bol.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    self.bol.layer.borderWidth = 1.0f;
    self.bol.layer.cornerRadius = 5.0f;
    
    self.signature.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    self.signature.layer.borderWidth = 1.0f;
    self.signature.layer.cornerRadius = 5.0f;
    
    _bolSaveButton.layer.cornerRadius = 5.0f;
    _bolCloseButton.layer.cornerRadius = 5.0f;
    _bolCaptureButton.layer.cornerRadius = 5.0f;
    
    //load next status
    if (_job.status_id < 18) {
        if (_job.status_id == 15) {
            //_signatureView.hidden = NO;
            _changeStatusText.text = @"Driver confirmation";
        }else if (_job.status_id == 16) {
            //_bolView.hidden = NO;
            _changeStatusText.text = @"POD";
        }
        else{
            _InternetConnection = [[InternetConnection alloc] init];
            if([_InternetConnection connected])
            {
                _SFunc = [[ShipmentsControllerFunctions alloc] init];
                NSString *status = [_SFunc nextStatus:_job.status_id];
                _changeStatusText.text = status;
            }else{
                _changeStatusText.text = @"";
            }
        }
    }
}

-(void)checkDrag{
    if (checkGes == 0) {
        [mapView_ animateToBearing:0];
        [mapView_ animateToLocation:CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue])];
        marker.position = CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue]);
        
        checkGes = 1;
    }
}

- (IBAction)openMap:(id)sender {
    DDLogVerbose(@"open map");
    _map.hidden = NO;
    
    camera = [GMSCameraPosition cameraWithLatitude:[[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue]
                                         longitude:[[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue]
                                              zoom:16];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, _map.frame.size.width, _map.frame.size.height) camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = NO;
    mapView_.settings.myLocationButton = NO;
    mapView_.settings.consumesGesturesInView = YES;
    mapView_.trafficEnabled = YES;
    
    for (UIGestureRecognizer *gestureRecognizer in mapView_.gestureRecognizers) {
        [gestureRecognizer addTarget:self action:@selector(handlePan:)];
    }
    
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue]);
    marker.title = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userName"]];
    marker.snippet = @"";
    marker.map = mapView_;
    _mapFunc = [[mapsControllerFunc alloc] init];
    marker.icon = [_mapFunc image:[UIImage imageNamed:@"truck_icon.png"] scaledToSize:CGSizeMake(50.0f, 50.0f)];
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    
    _mapFunc = [[mapsControllerFunc alloc] init];
    [_mapFunc addJobsMarkers:_job :mapView_ :[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"]:[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"]];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        
        _mapFunc = [[mapsControllerFunc alloc] init];
        if (_job.status_id < 11) {
            [_mapFunc addRoute:[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] :[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] :_job.origin_lat :_job.origin_lng :mapView_];
        }else{
            [_mapFunc addRoute:[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] :[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] :_job.destination_lat :_job.destination_lng :mapView_];
        }
    }
    
    [_map addSubview:mapView_];
    
    CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue]);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
    
    CLLocationCoordinate2D originLocation = CLLocationCoordinate2DMake([_job.origin_lat floatValue], [_job.origin_lng floatValue]);
    bounds = [bounds includingCoordinate:originLocation];
    
    CLLocationCoordinate2D destinationLocation = CLLocationCoordinate2DMake([_job.destination_lat floatValue], [_job.destination_lng floatValue]);
    bounds = [bounds includingCoordinate:destinationLocation];
    
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
    checkGes = 3;
    checkMapView = 1;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading{
    //for ios8
    double heading = newHeading.magneticHeading;
    //for more than ios8
    //double heading = newHeading.trueHeading;
    
    // double headingDegrees = (heading*M_PI/180);
    if (checkGes == 2) {
        
    }else{
        [mapView_ animateToBearing:heading];
        [mapView_ animateToViewingAngle:90];
        //marker.rotation = heading;
    }
}

-(IBAction) handlePan:(UIGestureRecognizer*)sender {
    
    if (sender.state == UIGestureRecognizerStateChanged) {
        checkGes = 2;
        DDLogVerbose(@"drag");
    }
}

- (IBAction)back:(id)sender {
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    [checkLocationTimer invalidate];
    [self performSegueWithIdentifier:@"backFromJobToDesktop" sender:self];
    
}

- (BOOL)isModal {
    if([self presentingViewController])
        return YES;
    //    if([[self presentingViewController] presentedViewController] == self)
    //        return YES;
    //    if([[[self navigationController] presentingViewController] presentedViewController] == [self navigationController])
    //        return YES;
    //    if([[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
    //        return YES;
    
    return NO;
}

-(void)showDeatils{
    if (checkOpen == 0) {
        
        [self.view removeConstraint: showDetailsTop];
        showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                       constant:64];
        [self.view addConstraint:showDetailsTop];
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
            checkOpen = 1;
        }];
    }else{
        
        
        [self.view removeConstraint: showDetailsTop];
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        height = height - 124;
        if ((heightCon + 86) < height) {
            height = heightCon + 86;
        }
        showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                       constant:-height + 156];
        [self.view addConstraint:showDetailsTop];
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
            checkOpen = 0;
        }];
    }
    
}

-(void)changeStatusTap{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            int test = (int)_job.status_id;
            switch (test) {
                case 5:
                {
                    UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                    [doSomething addTarget:self
                                    action:@selector(changeStatusClick)
                          forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    alert = [[UIView alloc] init];
                    _alert = [[CustomAlert alloc] init];
                    
                    alert = [_alert alertView:doSomething :nil :@"Confirm" :@"Are you sure you are on the way to pickup?" :@"Yes" :@"Cancel" :@"" :nil :self.view :0];
                }
                    break;
                case 7:
                case 8:
                case 9:
                {
                    statusConfirmation = 1;
                    [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
                }
                    break;
                case 11:
                {
                    
                    
                    UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                    [doSomething addTarget:self
                                    action:@selector(changeStatusClick)
                          forControlEvents:UIControlEventTouchUpInside];
                    
                    alert = [[UIView alloc] init];
                    _alert = [[CustomAlert alloc] init];
                    
                    alert = [_alert alertView:doSomething :nil :@"Confirm" :@"Are you sure you are on the way to dropoff?" :@"Yes" :@"Cancel" :@"" :nil :self.view :0];
                }
                    
                    break;
                case 12:
                case 13:
                case 14:
                {
                    {
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(changeStatusClick)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        alert = [[UIView alloc] init];
                        _alert = [[CustomAlert alloc] init];
                        
                        alert = [_alert alertView:doSomething :nil :@"Confirm" :@"Are you sure shipment was deliverd?" :@"Yes" :@"Cancel" :@"" :nil :self.view :0];
                        
                    }
                }
                    break;
                case 15:
                    statusConfirmation = 2;
                    [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
                    break;
                case 16:
                {
                    [self.view removeConstraint: showDetailsTop];
                    CGFloat height = [UIScreen mainScreen].bounds.size.height;
                    height = height - 124;
                    if ((heightCon + 86) < height) {
                        height = heightCon + 86;
                    }
                    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                                  attribute:NSLayoutAttributeTop
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.view
                                                                  attribute:NSLayoutAttributeTop
                                                                 multiplier:1
                                                                   constant:-height + 156];
                    [self.view addConstraint:showDetailsTop];
                    [self.view setNeedsUpdateConstraints];
                    [UIView animateWithDuration:0.3f animations:^{
                        [self.view layoutIfNeeded];
                        checkOpen = 0;
                    }];
                    
                    _bolView.hidden = NO;
                    [menu removeFromSuperview];
                    _bolImage.image = nil;
                }break;
                default:
                    break;
            }
        }else{
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        }
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [self updateApp];
                                                         }];
        [alertController addAction:action];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                           [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                       }];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 2){
        if (buttonIndex == 1) {
            //take a picture
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickerController.editing = YES;
            imagePickerController.delegate = (id)self;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        else if (buttonIndex == 2){
            //upload image
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
            
        }
    }
}

-(void)myLocation{
    if (checkMapView == 1) {
        [locationManager startUpdatingHeading];
        [mapView_ animateToBearing:0];
        [mapView_ animateToZoom:17];
        
        [mapView_ animateToLocation:CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue])];
        marker.position = CLLocationCoordinate2DMake([[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue], [[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"] floatValue]);
        
        checkGes = 0;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        [locationManager startUpdatingHeading];
        checkMapView = 2;
    }else{
        [locationManager stopUpdatingHeading];
        [mapView_ animateToBearing:0];
        [mapView_ animateToViewingAngle:0];
        CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake([latitudeLabel floatValue], [longitudeLabel floatValue]);
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
        
        CLLocationCoordinate2D originLocation = CLLocationCoordinate2DMake([_job.origin_lat floatValue], [_job.origin_lng floatValue]);
        bounds = [bounds includingCoordinate:originLocation];
        
        CLLocationCoordinate2D destinationLocation = CLLocationCoordinate2DMake([_job.destination_lat floatValue], [_job.destination_lng floatValue]);
        bounds = [bounds includingCoordinate:destinationLocation];
        
        [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        checkMapView = 1;
    }
    
}

-(void)route{
    DDLogVerbose(@"click on route");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please select navigation app"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Maps", @"Google maps", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet setTag:1];
    
    DDLogVerbose(@"end click on route");
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            if (_job.status_id < 11) {
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"],[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"],_job.origin_lat,_job.origin_lng]];
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            }else{
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"],[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"],_job.destination_lat,_job.destination_lng]];
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            }
        }
        if (buttonIndex == 1) {
            BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps:"]];
            
            DDLogVerbose(@"if have google maps app-open");
            if (canHandle) {
                // Google maps installed
                
                if (_job.status_id < 11) {
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@,%@&daddr=%@,%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"],[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"],_job.origin_lat,_job.origin_lng]];
                    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                }else{
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@,%@&daddr=%@,%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"],[[NSUserDefaults standardUserDefaults] stringForKey:@"longitudLabel"],_job.destination_lat,_job.destination_lng]];
                    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                }
            }else{
                _alert = [[CustomAlert alloc] init];
                [_alert alertView:nil :nil :@"Notice" :@"Google Maps not installed on your device!" :@"" :@"OK" :@"" :nil :self.view :0];
            }
        }
    }
}

-(void)share{
    DDLogVerbose(@"click on share");
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        [self shareFunction];
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)shareFunction{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        DDLogVerbose(@"share func");
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString *name = [[NSUserDefaults standardUserDefaults] stringForKey:@"userName"];
            DDLogVerbose(@"get link from server");
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getShareLink&user_id=%@&shipment_id=%d", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (int)_job.shipmentsId]];
            
            NSDictionary *dic;
            
            if (myData) {
                _json = [[JSON alloc] init];
                dic = [[NSDictionary alloc] init];
                dic = [_json FromJson:myData];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [backgroundView  removeFromSuperview];
                if (myData) {
                    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"<html><body>Hi there,</br>%@ has shared with you this shipment data.</br>You can track and see the shipment data in this link</br><a href='%@'>%@</a></body></html>", name, [dic objectForKey:@"share_url"], [dic objectForKey:@"share_url"]]] applicationActivities:nil];
                    
                    [self presentViewController:controller animated:YES completion:nil];
                }else{
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                }
                DDLogVerbose(@"end share click");
            });
        });
        
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    
}

- (IBAction)signatureClick:(id)sender {
    
    statusConfirmation = 2;
    [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
}

- (IBAction)bolClick:(id)sender {
    [self.view removeConstraint: showDetailsTop];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightCon + 86) < height) {
        height = heightCon + 86;
    }
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-height + 156];
    [self.view addConstraint:showDetailsTop];
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        checkOpen = 0;
    }];
    _bolView.hidden = NO;
    [menu removeFromSuperview];
    _bolImage.image = nil;
    
    if (![_job.bol_url isEqualToString:@""]) {
        NSString *str = [NSString stringWithFormat:@"%@/%@",[StaticVars url],_job.bol_url];
        NSURL *urlForImageShipments = [NSURL URLWithString:str];
        NSData *dataImageShipments = [NSData dataWithContentsOfURL:urlForImageShipments];
        
        UIImage *imageShipments = [UIImage imageWithData:dataImageShipments];
        
        _bolImage.image = imageShipments;
        _bolCaptureButton.titleLabel.text = @"RE-CAPTURE";
        _captureClickLabel.text = @"RE-CAPTURE";
    }
    
}

- (IBAction)saveSignature:(id)sender {
    //save image
    if (![_nameRecived.text isEqualToString:@""] && _drawImage.image != nil) {
        [self saveSignatureClick];
        
        
    }else if (_drawImage.image == nil){
        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake((widthAlert - 70)/2,0,70,35)];
        [doSomething addTarget:self
                        action:@selector(missSignature)
              forControlEvents:UIControlEventTouchUpInside];
        alert = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        
        alert = [_alert alertView:doSomething :nil :@"Notice" :@"Please ask the reciver to sign" :@"OK" :nil :@"" :nil :self.view :0];
    }else{
        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake((widthAlert - 70)/2,0,70,35)];
        [doSomething addTarget:self
                        action:@selector(missReciver)
              forControlEvents:UIControlEventTouchUpInside];
        
        alert = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        
        alert = [_alert alertView:doSomething :nil :@"Notice" :@"Please enter the reciver name" :@"OK" :nil :@"" :nil :self.view :0];
    }
}

- (IBAction)clearSignature:(id)sender {
    
    _drawImage.image = nil;
}

- (IBAction)closeSignature:(id)sender {
    _nameRecived.text = @"";
    _signatureView.hidden = YES;
    [self awsomeMenu];
}

- (IBAction)saveBOL:(id)sender {
    if (_bolImage.image == nil){
        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake((widthAlert - 70)/2,0,70,35)];
        [doSomething addTarget:self
                        action:@selector(missSignature)
              forControlEvents:UIControlEventTouchUpInside];
        alert = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        
        alert = [_alert alertView:doSomething :nil :@"Notice" :@"Please take a picture of the POD" :@"OK" :nil :@"" :nil :self.view :0];
    }else{
        [self saveBOLClick];
    }
    
}

- (IBAction)captureBOL:(id)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.editing = YES;
    imagePickerController.delegate = (id)self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (IBAction)closeBOL:(id)sender {
    _bolView.hidden = YES;
    _cancelPODView.hidden = YES;
    _bolCaptureButton.titleLabel.text = @"CAPTURE";
    _captureClickLabel.text = @"CAPTURE";
    [self awsomeMenu];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    
    lastPoint = [touch locationInView:_drawImage];
    lastPoint.y -= 20;
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    _signHere.hidden = YES;
    mouseSwiped = YES;
    
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:_drawImage];
    currentPoint.y -= 20;
    
    /*
     UIGraphicsBeginImageContext(_drawImage.frame.size);
     [_drawImage.image drawInRect:CGRectMake(0, 0, _drawImage.frame.size.width, _drawImage.frame.size.height)];
     CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
     CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 3.0);
     CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
     CGContextBeginPath(UIGraphicsGetCurrentContext());
     CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
     CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
     CGContextStrokePath(UIGraphicsGetCurrentContext());
     _drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     lastPoint = currentPoint;*/
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /*
     if(!mouseSwiped) {
     UIGraphicsBeginImageContext(_drawImage.frame.size);
     [_drawImage.image drawInRect:CGRectMake(0, 0, _drawImage.frame.size.width, _drawImage.frame.size.height)];
     CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
     CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 3.0);
     CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
     CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
     CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
     CGContextStrokePath(UIGraphicsGetCurrentContext());
     CGContextFlush(UIGraphicsGetCurrentContext());
     _drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     }*/
}

//take a picture
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    // Request to save the image to camera roll
    [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
        if (error) {
            DDLogVerbose(@"error to take a picture");
        } else {
            
            imageBol = image;
            _bolImage.image = imageBol;
            _bolCaptureButton.titleLabel.text = @"RE-CAPTURE";
            _captureClickLabel.text = @"RE-CAPTURE";
        }
        
    }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}
//upload image
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if (checkCamera == 2) {
        [self jobImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
        checkCamera = 1;
    }else{
        _bolImage.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _captureClickLabel.text = @"RE-CAPTURE";
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)tapPhoneShipper{
    NSString *phNo = _job.shipper_phone;
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",phNo] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
    
}

- (void)tapOnPhone
{
    NSString *phNo = _job.origin_contact_phone;
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",phNo] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
    
}

- (void)tapPhoneDest
{
    NSString *phNo = _job.destination_contact_phone;
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",phNo] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
    
}


-(void)changeStatusClick{
    [alert removeFromSuperview];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            _SFunc = [[ShipmentsControllerFunctions alloc] init];
            int updateStatus;
            updateStatus = [_SFunc updateJobStatus:_job.shipmentsId :_job.status_id];
            
            NSString *urlToSend = [NSString stringWithFormat:@"tr-api/?action=updateShipmentSatus&user_id=%@&carrier_id=%@&shipment_id=%ld&status_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], (long)_job.shipmentsId, (long)updateStatus];
            
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:urlToSend];
            
            DDLogVerbose(@"test 1: %@",urlToSend);
            
            
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            DDLogVerbose(@"test 1: %@",dic);
            
            if ([[dic objectForKey:@"err"] boolValue] == false) {
                
                
                //check if update right
                NSDictionary *jobDic = [[NSDictionary alloc] init];
                jobDic = [dic objectForKey:@"shipment"];
                
                if ([jobDic objectForKey:@"status_id"] != 0 && [jobDic objectForKey:@"status_id"] != (id)[NSNull null]) {
                    _job.status_id = [[jobDic objectForKey:@"status_id"] intValue];
                    
                    //change on status array
                    if (updateStatus == 7 || updateStatus == 11 || updateStatus == 12 || updateStatus == 15) {
                        LocationObject *location = [LocationObject sharedInstance];
                        _desktopFunc = [[DesktopFunc alloc] init];
                        location.jobsArray = [[NSMutableArray alloc] init];
                        location.jobsArray = [_desktopFunc loadJobsToChngeStatus];
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (_job.status_id == 15) {
                            _changeStatusText.text = @"Driver confirmation";
                            statusConfirmation = 2;
                            [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
                            
                        }else if (_job.status_id == 16){
                            
                        }else{
                            
                            _SFunc = [[ShipmentsControllerFunctions alloc] init];
                            NSString *status = [_SFunc nextStatus:_job.status_id];
                            _changeStatusText.text = status;
                        }
                        
                        [backgroundView  removeFromSuperview];
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [backgroundView  removeFromSuperview];
                    });
                }
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([[dic objectForKey:@"errType"] isEqualToString:@"update shipment"]) {
                        
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(outToDesktop)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertview2:doSomething :@"Notice" :@"The job has changed. Please reopen it" :@"OK" :self.view];
                    }else{
                        
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(outToDesktop)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertview2:doSomething :@"Notice" :@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez" :@"OK" :self.view];
                    }
                });
                
            }
            
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)saveSignatureClick{
    [alert removeFromSuperview];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        
        NSString *imageType = @"signature";
        UIImage *mainImageView;
        mainImageView = _drawImage.image;
        NSData *imageToUpload;
        imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.25);
        
        NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=uploadImage&user_id=%@&shipment_id=%ld&image_type=%@&receiver_name=%@",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (long)_job.shipmentsId, imageType, _nameRecived.text];
        NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:encodedUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageToUpload name:@"file" fileName:[NSString stringWithFormat:@"%ld_signature.jpg",(long)_job.shipmentsId] mimeType:@"image/jpeg"];
        } error:nil];
        
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          
                          _SFunc = [[ShipmentsControllerFunctions alloc] init];
                          int updateStatus;
                          updateStatus = [_SFunc updateJobStatus:_job.shipmentsId :_job.status_id];
                          
                          NSString *urlToSend = [NSString stringWithFormat:@"tr-api/?action=updateShipmentSatus&user_id=%@&carrier_id=%@&shipment_id=%ld&status_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], _job.shipmentsId, (long)updateStatus];
                          _json = [[JSON alloc] init];
                          NSData *myData = [[NSData alloc] init];
                          myData = [_json getDataFrom:urlToSend];
                          
                          DDLogVerbose(@"test 2: %@",urlToSend);
                          
                          _json = [[JSON alloc] init];
                          NSDictionary *dic = [[NSDictionary alloc] init];
                          dic = [_json FromJson:myData];
                          
                          DDLogVerbose(@"test 2: %@",dic);
                          if ([[dic objectForKey:@"err"] boolValue] == false) {
                              //check if update right
                              NSDictionary *jobDic = [[NSDictionary alloc] init];
                              jobDic = [dic objectForKey:@"shipment"];
                              
                              if ([jobDic objectForKey:@"status_id"] != 0 && [jobDic objectForKey:@"status_id"] != (id)[NSNull null]) {
                                  _job.status_id = [[jobDic objectForKey:@"status_id"] intValue];
                                  
                                  if (_job.status_id < 17) {
                                      
                                      
                                      _bolView.hidden = NO;
                                      _changeStatusText.text = @"POD";
                                      _bolImage.image = nil;
                                  }
                                  _signatureView.hidden = YES;
                              }
                          }else{
                              UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                              [doSomething addTarget:self
                                              action:@selector(outToDesktop)
                                    forControlEvents:UIControlEventTouchUpInside];
                              
                              
                              _alert = [[CustomAlert alloc] init];
                              [_alert alertview2:doSomething :@"Notice" :@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez" :@"OK" :self.view];
                              
                          }
                          [backgroundView  removeFromSuperview];
                          
                      }];
        
        [uploadTask resume];
        
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)saveBOLClick{
    DDLogVerbose(@"saveBOL");
    [alert removeFromSuperview];
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        NSString *imageType = @"bol";
        UIImage *mainImageView;
        mainImageView = _bolImage.image;
        NSData *imageToUpload;
        imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.25);
        
        NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=uploadImage&user_id=%@&shipment_id=%ld&image_type=%@",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (long)_job.shipmentsId, imageType];
        NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:encodedUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageToUpload name:@"file" fileName:[NSString stringWithFormat:@"%ld_bol.jpg",(long)_job.shipmentsId] mimeType:@"image/jpeg"];
        } error:nil];
        
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          
                          
                          if (_job.status_id < 18) {
                              
                              //change status to 17
                              _SFunc = [[ShipmentsControllerFunctions alloc] init];
                              int updateStatus;
                              updateStatus = [_SFunc updateJobStatus:_job.shipmentsId :_job.status_id];
                              
                              NSString *urlToSend = [NSString stringWithFormat:@"tr-api/?action=updateShipmentSatus&user_id=%@&carrier_id=%@&shipment_id=%ld&status_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], (long)_job.shipmentsId, (long)updateStatus];
                              _json = [[JSON alloc] init];
                              NSData *myData = [[NSData alloc] init];
                              myData = [_json getDataFrom:urlToSend];
                              
                              DDLogVerbose(@"test 3: %@",urlToSend);
                              _json = [[JSON alloc] init];
                              NSDictionary *dic = [[NSDictionary alloc] init];
                              dic = [_json FromJson:myData];
                              DDLogVerbose(@"test 3: %@",dic);
                              
                              if ([[dic objectForKey:@"err"] boolValue] == false) {
                                  
                                  //check if update right
                                  NSDictionary *jobDic = [[NSDictionary alloc] init];
                                  jobDic = [dic objectForKey:@"shipment"];
                                  
                                  if ([jobDic objectForKey:@"status_id"] != 0 && [jobDic objectForKey:@"status_id"] != (id)[NSNull null]) {
                                      _job.status_id = [[jobDic objectForKey:@"status_id"] intValue];
                                      
                                      //show alert complete
                                      UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                                      [doSomething addTarget:self
                                                      action:@selector(outToDesktop)
                                            forControlEvents:UIControlEventTouchUpInside];
                                      
                                      
                                      _alert = [[CustomAlert alloc] init];
                                      [_alert alertview2:doSomething :@"Notice" :@"Well done! The job was completed successfully." :@"OK" :self.view];
                                  }
                              }else{
                                  if ([[dic objectForKey:@"errType"] isEqualToString:@"update shipment"]) {
                                      UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                                      [doSomething addTarget:self
                                                      action:@selector(outToDesktop)
                                            forControlEvents:UIControlEventTouchUpInside];
                                      
                                      
                                      _alert = [[CustomAlert alloc] init];
                                      [_alert alertview2:doSomething :@"Notice" :@"The job has changed. Please reopen it" :@"OK" :self.view];
                                  }else{
                                      UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                                      [doSomething addTarget:self
                                                      action:@selector(outToDesktop)
                                            forControlEvents:UIControlEventTouchUpInside];
                                      
                                      
                                      _alert = [[CustomAlert alloc] init];
                                      [_alert alertview2:doSomething :@"Notice" :@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez" :@"OK" :self.view];
                                  }
                              }
                              [backgroundView removeFromSuperview];
                              
                          }
                          
                      }];
        [uploadTask resume];
        
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)closeAlert{
    [alert removeFromSuperview];
}

-(void)CallUs{
    
    [alert removeFromSuperview];
    
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
}


-(UIView*)loadTextAndPhone:(NSString*)str :(NSString *)phoneNumber :(CGRect)sizeView :(NSString *)actionString{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0,0,sizeView.size.width,sizeView.size.height)];
    
    UILabel *viewText;
    UIButton *phone;
    CGRect textsizeS;
    if (![str isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsizeS = [str boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        viewText = [[UILabel alloc] initWithFrame:CGRectMake(5,0,textsizeS.size.width,sizeView.size.height)];
        
        viewText.text = str;
        viewText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        viewText.text = str;
        [viewText setFont:[UIFont systemFontOfSize:15]];
        
        viewText.lineBreakMode = NSLineBreakByWordWrapping;
        viewText.numberOfLines = 0;
        
    }
    
    CGRect textsize2;
    if (![phoneNumber isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize2 = [phoneNumber boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        phone = [[UIButton alloc] initWithFrame:CGRectMake(textsizeS.size.width,0,textsize2.size.width + 20,sizeView.size.height)];
        [phone setTitle:phoneNumber forState:UIControlStateNormal];
        phone.titleLabel.font = [UIFont systemFontOfSize:15];
        [phone setTitleColor:[UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0] forState:UIControlStateNormal];
        
        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:phoneNumber];
        [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
        UIColor* textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
        [phone setAttributedTitle:commentString forState:UIControlStateNormal];
    }
    [phone addTarget:self
              action:@selector(tapPhoneDest)
    forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [view addSubview:viewText];
    [view addSubview:phone];
    return view;
}

-(UIView*)loadOriginTextAndPhone:(NSString*)str :(NSString *)phoneNumber :(CGRect)sizeView :(NSString *)actionString{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0,0,sizeView.size.width,sizeView.size.height)];
    
    UILabel *viewText;
    UIButton *phone;
    CGRect textsizeS;
    if (![str isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsizeS = [str boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        viewText = [[UILabel alloc] initWithFrame:CGRectMake(5,0,textsizeS.size.width,sizeView.size.height)];
        
        viewText.text = str;
        viewText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        viewText.text = str;
        [viewText setFont:[UIFont systemFontOfSize:15]];
        
        viewText.lineBreakMode = NSLineBreakByWordWrapping;
        viewText.numberOfLines = 0;
        
    }
    
    CGRect textsize2;
    if (![phoneNumber isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize2 = [phoneNumber boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        phone = [[UIButton alloc] initWithFrame:CGRectMake(textsizeS.size.width,0,textsize2.size.width + 20,sizeView.size.height)];
        [phone setTitle:phoneNumber forState:UIControlStateNormal];
        phone.titleLabel.font = [UIFont systemFontOfSize:15];
        [phone setTitleColor:[UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0] forState:UIControlStateNormal];
        
        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:phoneNumber];
        [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
        UIColor* textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
        [phone setAttributedTitle:commentString forState:UIControlStateNormal];
    }
    [phone addTarget:self
              action:@selector(tapOnPhone)
    forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [view addSubview:viewText];
    [view addSubview:phone];
    return view;
}

-(UIView*)loadShipperTextAndPhone:(NSString*)str :(NSString *)phoneNumber :(CGRect)sizeView :(NSString *)actionString{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0,0,sizeView.size.width,sizeView.size.height)];
    
    UILabel *viewText;
    UIButton *phone;
    CGRect textsizeS;
    if (![str isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsizeS = [str boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        viewText = [[UILabel alloc] initWithFrame:CGRectMake(5,0,textsizeS.size.width,sizeView.size.height)];
        
        viewText.text = str;
        viewText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        viewText.text = str;
        [viewText setFont:[UIFont systemFontOfSize:15]];
        
        viewText.lineBreakMode = NSLineBreakByWordWrapping;
        viewText.numberOfLines = 0;
        
    }
    
    CGRect textsize2;
    if (![phoneNumber isEqualToString:@""]) {
        
        CGSize constraintS = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributesS = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize2 = [phoneNumber boundingRectWithSize:constraintS options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesS context:nil];
        
        phone = [[UIButton alloc] initWithFrame:CGRectMake(textsizeS.size.width,0,textsize2.size.width + 20,sizeView.size.height)];
        [phone setTitle:phoneNumber forState:UIControlStateNormal];
        phone.titleLabel.font = [UIFont systemFontOfSize:15];
        [phone setTitleColor:[UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0] forState:UIControlStateNormal];
        
        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:phoneNumber];
        [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
        UIColor* textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
        [phone setAttributedTitle:commentString forState:UIControlStateNormal];
    }
    [phone addTarget:self
              action:@selector(tapPhoneShipper)
    forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [view addSubview:viewText];
    [view addSubview:phone];
    return view;
}

-(void)missReciver{
    [alert removeFromSuperview];
    [_nameRecived becomeFirstResponder];
}
-(void)missSignature{
    [alert removeFromSuperview];
}
-(void)openFile:(UIButton*)sender{
    int tag = (int)sender.tag;
    DDLogVerbose(@"aa %d",tag);
    
    NSArray *arraySpecialRequest = [[NSArray alloc] init];
    arraySpecialRequest = [_job.shipment_special_request_with_document componentsSeparatedByString:@","];
    
    NSArray *single = [[NSArray alloc] init];
    single = [arraySpecialRequest[tag] componentsSeparatedByString:@"["];
    
    specialLoad = single[0];
    [self performSegueWithIdentifier:@"openSpecialLoad" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openSpecialLoad"]) {
        SpecialLoad *SpecialLoad = [segue destinationViewController];
        SpecialLoad.imgName = specialLoad;
    }
    if([segue.identifier isEqualToString:@"openPickupConfirmation"]) {
        PickupConfirmation *p = [segue destinationViewController];
        p.job = _job;
        p.con = statusConfirmation;
    }
}

- (IBAction)handlePanShowDetails:(UIPanGestureRecognizer *)recognizer {
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightCon + 86) < height) {
        height = heightCon + 86;
    }
    
    
    CGPoint translation = [recognizer translationInView:_showDetails];
    CGPoint velocity = [recognizer velocityInView:_showDetails];
    if (velocity.y > 0) {
        checkOpen = 1;
    }else{
        checkOpen = 0;
    }
    
    if (recognizer.view.center.y <= (self.view.bounds.size.height-height/2 - 64)) {
        
        if (recognizer.view.center.y >= -64) {
            recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                 recognizer.view.center.y + translation.y);
            [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetails];
        }else{
            if (velocity.y > 0) {
                recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                     -63);
                [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetails];
            }
        }
        
        
    }else{
        if (velocity.y < 0) {
            recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                 self.view.bounds.size.height-height/2 - 65);
            [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetails];
        }
    }
    
    
}

-(BOOL)checkCanShipperDetails{
    DDLogVerbose(@"checkCanShipperDetails");
    NSString *strCurrentDate;
    NSDate *date = [NSDate date];
    NSDateFormatter *df =[[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    strCurrentDate = [df stringFromDate:date];
    DDLogVerbose(@"Current Date and Time: %@",strCurrentDate);
    int hoursToAdd = 3;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setHour:hoursToAdd];
    NSDate *newDate= [calendar dateByAddingComponents:components toDate:date options:0];
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *strDate = [NSString stringWithFormat:@"%@ %@",_job.original_pickup_date, _job.pickup_from_time];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date2 = [dateFormat dateFromString:strDate];
    
    if ([date2 compare:newDate]==NSOrderedAscending) {
        return YES;
    }
    else if ([newDate compare:date2]==NSOrderedSame){
        return YES;
    }else{
        return NO;
    }
    
    
}
- (IBAction)callSupport:(id)sender {
    DDLogVerbose(@"supportCall");
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        DDLogVerbose(@"cannnot call");
    }
    
}

-(void)cameraClick{
    DDLogVerbose(@"cameraClick");
    checkCamera = 2;
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.editing = YES;
    imagePickerController.delegate = (id)self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
}

-(void)popUp:(NSString *)title{
    backgroundView1.hidden = NO;
    backgroundView1 = [[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    backgroundView1.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.5];
    
    
    popUp= [[UIView alloc]initWithFrame:CGRectMake(20,120,[UIScreen mainScreen].bounds.size.width - 40,[UIScreen mainScreen].bounds.size.height- 240)];
    popUp.backgroundColor = [UIColor colorWithRed:0.96 green:0.97 blue:0.98 alpha:1.0];
    
    UIBezierPath *maskPathP;
    CAShapeLayer *maskLayerP;
    
    maskPathP= [UIBezierPath bezierPathWithRoundedRect:popUp.bounds byRoundingCorners:(UIRectCornerTopRight|UIRectCornerTopLeft|UIRectCornerBottomLeft|UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    maskLayerP = [[CAShapeLayer alloc] init];
    maskLayerP.frame = self.view.bounds;
    maskLayerP.path  = maskPathP.CGPath;
    popUp.layer.mask = maskLayerP;
    
    [backgroundView1 addSubview:popUp];
    
    UIView *titleView =  [[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width - 40,50)];
    titleView.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    
    [popUp addSubview:titleView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,5,[UIScreen mainScreen].bounds.size.width - 60,40)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = title;
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleView addSubview:titleLabel];
    
    //buttons
    UIButton *save = [[UIButton alloc]initWithFrame:CGRectMake(popUp.bounds.size.width/2-75,popUp.bounds.size.height- 45,70,35)];
    //save.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    save.backgroundColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
    [save setTitle:@"Save" forState:UIControlStateNormal];
    [save.layer setCornerRadius:5.0f];
    save.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [save addTarget:self
             action:@selector(saveNote)
   forControlEvents:UIControlEventTouchUpInside];
    
    [popUp addSubview:save];
    
    
    UIButton *close = [[UIButton alloc]initWithFrame:CGRectMake(popUp.bounds.size.width/2+5,popUp.bounds.size.height- 45,70,35)];
    close.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    [close setTitle:@"Close" forState:UIControlStateNormal];
    [close.layer setCornerRadius:5.0f];
    close.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [close addTarget:self
              action:@selector(closeNote)
    forControlEvents:UIControlEventTouchUpInside];
    
    [popUp addSubview:close];
    
    
    noteField = [[UITextView alloc] initWithFrame:CGRectMake(10, 80, popUp.bounds.size.width - 20, popUp.bounds.size.height - 130)];
    [noteField setFont:[UIFont boldSystemFontOfSize:16]];
    noteField.text = _job.driver_notes;
    noteField.delegate = self;
    
    [popUp addSubview:noteField];
    backgroundView1.layer.zPosition = 4;
    [self.view addSubview:backgroundView1];
    
    UITapGestureRecognizer *backgroundTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundClick)];
    backgroundTap.numberOfTapsRequired = 1;
    [backgroundView1 setUserInteractionEnabled:YES];
    [backgroundView1 addGestureRecognizer:backgroundTap];
    
    
}

-(void)backgroundClick{
    [self.view endEditing:YES];
}

-(void)jobImage:(UIImage *)img{
    DDLogVerbose(@"jobImage");
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        UIImage *mainImageView;
        mainImageView = img;
        NSData *imageToUpload;
        imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.25);
        
        NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=uploadJobImages&user_id=%@&shipment_id=%ld&file_type_id=1",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (long)_job.shipmentsId];
        NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:registerUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageToUpload name:@"file" fileName:[NSString stringWithFormat:@"%ld_jobImg.jpg",(long)_job.shipmentsId] mimeType:@"image/jpeg"];
        } error:nil];
        
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (response) {
                              _alert = [[CustomAlert alloc] init];
                              [_alert alertView:nil :nil :@"Notice" :@"Image uploaded successfully" :@"" :@"OK" :@"" :nil :self.view :0];
                          }else{
                              
                              _alert = [[CustomAlert alloc] init];
                              [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                          }
                          
                          DDLogVerbose(@"responseObjec:%@", responseObject);
                          
                          [backgroundView  removeFromSuperview];
                      }];
        
        [uploadTask resume];
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    
}

-(void)noteClick{
    DDLogVerbose(@"noteClick");
    [self popUp:@"Edit notes"];
}

-(void)closeNote{
    [self.view endEditing:YES];
    backgroundView1.hidden = YES;
}

-(void)saveNote{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        DDLogVerbose(@"save note");
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        backgroundView.layer.zPosition = 5;
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateDriverNotes&user_id=%@&shipment_id=%d&notes=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],_job.shipmentsId,noteField.text]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [backgroundView  removeFromSuperview];
                if (myData) {
                    backgroundView1.hidden = YES;
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"Notes saved successfully" :@"" :@"OK" :@"" :nil :self.view :0];
                }else{
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                }
                DDLogVerbose(@"end save note");
            });
        });
    }else{
        
        _alert = [[CustomAlert alloc] init];
        
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        
    }
    
}

-(void)awsomeMenu{
    CGSize size = CGSizeMake(60,60);
    
    UIImage *menuImg=[self imageWithImage:[UIImage imageNamed:@"menu3.png"] convertToSize:size];
    UIImage *storyMenuItemImagePressed = [self imageWithImage:[UIImage imageNamed:@""]  convertToSize:size];
    UIImage *itemImg1=[self imageWithImage:[UIImage imageNamed:@"arrowRoute.png"] convertToSize:size];
    UIImage *itemImg2=[self imageWithImage:[UIImage imageNamed:@"notes.png"] convertToSize:size];
    UIImage *itemImg3=[self imageWithImage:[UIImage imageNamed:@"camera.png"] convertToSize:size];
    UIImage *itemImg4=[self imageWithImage:[UIImage imageNamed:@"share.png"] convertToSize:size];
    
    
    AwesomeMenuItem *starMenuItem1 = [[AwesomeMenuItem alloc] initWithImage:itemImg1
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:itemImg1
                                                    highlightedContentImage:nil];
    AwesomeMenuItem *starMenuItem2 = [[AwesomeMenuItem alloc] initWithImage:itemImg3
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:itemImg3
                                                    highlightedContentImage:nil];
    
    AwesomeMenuItem *starMenuItem3 = [[AwesomeMenuItem alloc] initWithImage:itemImg2
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:itemImg2
                                                    highlightedContentImage:nil];
    AwesomeMenuItem *starMenuItem4 = [[AwesomeMenuItem alloc] initWithImage:itemImg4
                                                           highlightedImage:storyMenuItemImagePressed
                                                               ContentImage:itemImg4
                                                    highlightedContentImage:nil];
    // the start item, similar to "add" button of Path
    AwesomeMenuItem *startItem = [[AwesomeMenuItem alloc] initWithImage:menuImg
                                                       highlightedImage:menuImg
                                                           ContentImage:[UIImage imageNamed:@"close3.png"]
                                                highlightedContentImage:[UIImage imageNamed:@"close3.png"]];
    
    
    if (_job.status_id >= 15) {
        if (_job.status_id >=18) {
            menu= [[AwesomeMenu alloc] initWithFrame:self.view.bounds startItem:startItem optionMenus:[NSArray arrayWithObjects:starMenuItem1, starMenuItem2, nil]];
        }else{
            menu= [[AwesomeMenu alloc] initWithFrame:self.view.bounds startItem:startItem optionMenus:[NSArray arrayWithObjects:starMenuItem1, starMenuItem2, starMenuItem3, nil]];
        }
    }else{
        menu= [[AwesomeMenu alloc] initWithFrame:self.view.bounds startItem:startItem optionMenus:[NSArray arrayWithObjects:starMenuItem1, starMenuItem2, starMenuItem3, starMenuItem4, nil]];
    }
    menu.delegate = self;
    [self.view addSubview:menu];
    
    if (_job.status_id >17) {
        menu.startPoint = CGPointMake([UIScreen mainScreen].bounds.size.width - 46, [UIScreen mainScreen].bounds.size.height - 40);
        menu.rotateAngle =4.7;
    }else{
        menu.startPoint = CGPointMake([UIScreen mainScreen].bounds.size.width - 46, [UIScreen mainScreen].bounds.size.height - 100);
        menu.rotateAngle =4.5;
    }
    
    menu.menuWholeAngle = M_PI/2;
    menu.timeOffset = 0.036f;
    
    menu.farRadius = 200.0f;
    menu.nearRadius = 160.0f;
    menu.endRadius = 120.0f;
    menu.layer.zPosition = 1;
    _showDetails.layer.zPosition = 2;
    _navView.layer.zPosition = 3;
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)awesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    if (idx == 0) {
        //route
        [self route];
    }else if (idx == 1){
        //image
        [self cameraClick];
        
    }else if (idx == 2){
        //note
        [self noteClick];
    }else{
        
        //share
        [self share];
    }
}
- (void)awesomeMenuDidFinishAnimationClose:(AwesomeMenu *)menu {
    NSLog(@"Menu was closed!");
}
- (void)awesomeMenuDidFinishAnimationOpen:(AwesomeMenu *)menu {
    NSLog(@"Menu is open!");
}

-(void)outToDesktop{
    [self performSegueWithIdentifier:@"backFromJobToDesktop" sender:self];
    
}

-(void)checkLocation{
    float a1 = [[[NSUserDefaults standardUserDefaults]
                 stringForKey:@"latitudeLabel"] floatValue];
    float a2 = [latitudeLabel floatValue];
    float b1 = [[[NSUserDefaults standardUserDefaults]
                 stringForKey:@"longitudLabel"] floatValue];
    float b2 = [longitudeLabel floatValue];
    
    if (((a1 - a2) >0.0004) || ((a1 - a2) < -0.0004) || ((b1 - b2) >0.0004) || ((b1 - b2) < -0.0004)) {
        latitudeLabel = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"latitudeLabel"];
        longitudeLabel = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"longitudLabel"];
        
        if (checkGes == 1) {
            [mapView_ animateToBearing:0];
            [mapView_ animateToLocation:CLLocationCoordinate2DMake([latitudeLabel floatValue], [longitudeLabel floatValue])];
            marker.position = CLLocationCoordinate2DMake([latitudeLabel floatValue], [longitudeLabel floatValue]);
        }
        if (checkGes == 0) {
            CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake([latitudeLabel floatValue], [longitudeLabel floatValue]);
            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
            
            CLLocationCoordinate2D originLocation = CLLocationCoordinate2DMake([_job.origin_lat floatValue], [_job.origin_lng floatValue]);
            bounds = [bounds includingCoordinate:originLocation];
            
            CLLocationCoordinate2D destinationLocation = CLLocationCoordinate2DMake([_job.destination_lat floatValue], [_job.destination_lng floatValue]);
            bounds = [bounds includingCoordinate:destinationLocation];
            
            [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        }
        
    }
    
    if (checkGes == 3) {
        CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake([latitudeLabel floatValue], [longitudeLabel floatValue]);
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
        
        CLLocationCoordinate2D originLocation = CLLocationCoordinate2DMake([_job.origin_lat floatValue], [_job.origin_lng floatValue]);
        bounds = [bounds includingCoordinate:originLocation];
        
        CLLocationCoordinate2D destinationLocation = CLLocationCoordinate2DMake([_job.destination_lat floatValue], [_job.destination_lng floatValue]);
        bounds = [bounds includingCoordinate:destinationLocation];
        
        [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        checkGes = 0;
    }
}

-(void)checkStatusNumUI{
    _confirmation1.hidden = YES;
    _confirmation2.hidden = YES;
    _confirmation3.hidden = YES;
    if (_job.status_id >17) {
        _changeStatus.hidden = YES;
        
        bottomChangeStatus = [NSLayoutConstraint
                              constraintWithItem:_changeStatus
                              attribute:NSLayoutAttributeBottom
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.view
                              attribute:NSLayoutAttributeBottom
                              multiplier:1.0f
                              constant:0.f];
        [self.view addConstraint:bottomChangeStatus];
        
        
        heightChangeStatus = [NSLayoutConstraint constraintWithItem:_changeStatus
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:0];
        [self.view addConstraint:heightChangeStatus];
    }
    
    if (_job.status_id >= 11) {
        if (_job.status_id >= 11 && _job.status_id < 16) {
            _confirmation1.hidden = NO;
            _confirmation2.hidden = YES;
            _confirmation3.hidden = YES;
        }else if (_job.status_id >= 16 && _job.status_id <18) {
            _confirmation1.hidden = YES;
            _confirmation2.hidden = NO;
            _confirmation3.hidden = YES;
        }else{
            _confirmation1.hidden = YES;
            _confirmation2.hidden = YES;
            _confirmation3.hidden = NO;
        }
        
    }else{
        
    }
}
- (IBAction)conPickup:(id)sender {
    [self.view removeConstraint: showDetailsTop];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightCon + 86) < height) {
        height = heightCon + 86;
    }
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-height + 156];
    [self.view addConstraint:showDetailsTop];
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        checkOpen = 0;
    }];
    statusConfirmation = 3;
    [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
}

- (IBAction)conDropoff:(id)sender {
    [self.view removeConstraint: showDetailsTop];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightCon + 86) < height) {
        height = heightCon + 86;
    }
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-height + 156];
    [self.view addConstraint:showDetailsTop];
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        checkOpen = 0;
    }];
    statusConfirmation = 4;
    [self performSegueWithIdentifier:@"openPickupConfirmation" sender:self];
    
}

- (IBAction)conPOD:(id)sender {
    [self.view removeConstraint: showDetailsTop];
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightCon + 86) < height) {
        height = heightCon + 86;
    }
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetails
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-height + 156];
    [self.view addConstraint:showDetailsTop];
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
        checkOpen = 0;
    }];
    
    _bolView.hidden = NO;
    _cancelPODView.hidden = NO;
    [menu removeFromSuperview];
    _bolImage.image = nil;
    
    if (![_job.bol_url isEqualToString:@""]) {
        NSString *str = [NSString stringWithFormat:@"%@/%@",[StaticVars url],_job.bol_url];
        NSURL *urlForImageShipments = [NSURL URLWithString:str];
        NSData *dataImageShipments = [NSData dataWithContentsOfURL:urlForImageShipments];
        
        UIImage *imageShipments = [UIImage imageWithData:dataImageShipments];
        
        _bolImage.image = imageShipments;
    }
    
}

-(void)captureClick{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.editing = YES;
    imagePickerController.delegate = (id)self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
}

//load page
-(void)showDetailsUI{
    //show details
    
    NSString *string = _job.shipment_carrier_payout_text;
    NSRange range = [string rangeOfString:@"."];
    NSString *shortString = [string substringToIndex:range.location];
    _price.text = shortString;
    if (![_job.signature_receiver_name isEqualToString:@""]) {
        _nameRecived.text = _job.signature_receiver_name;
    }
    //created by batya
    if (_job.status_id>=18) {
        _pickupView.hidden = YES;
        
        _html = [[StrippingHTML alloc] init];
        NSString *str = [_html stringByStrippingHTML:[NSString stringWithFormat:@"Completed %@",_job.completed_at]];
        _statusName.text = str;
        [_statusCircleColor.layer setCornerRadius:7.0f];
        _ConvertColor = [[ConvertColor alloc] init];
        _statusCircleColor.backgroundColor = [UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0];
    }
    else
        if (_job.status_id > 15) {
            _pickupView.hidden = YES;
            
            _html = [[StrippingHTML alloc] init];
            NSString *str = [_html stringByStrippingHTML:_job.status_name];
            _statusName.text = str;
            [_statusCircleColor.layer setCornerRadius:7.0f];
            _ConvertColor = [[ConvertColor alloc] init];
            _statusCircleColor.backgroundColor = [_ConvertColor colorWithHexString:_job.status_name_circle_color alpha:1];
        }else{
            _statusView.hidden = YES;
            if (_job.status_id < 11) {
                
                _pickupImg.image = [UIImage imageNamed:@"pickup_blue.png"];
                _pickupViewText.text = [NSString stringWithFormat:@"Pickup %@ %@-%@",_job.pickup_date, _job.pickup_from_time, _job.pickup_till_time];
                
                if (!([_job.pickup_in_time rangeOfString:@"late"].location == NSNotFound))
                {
                    _pickupViewText.textColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
                }else if (!([_job.pickup_in_time rangeOfString:@"in 1 hour"].location == NSNotFound))
                {
                    
                    _pickupViewText.textColor = [UIColor colorWithRed:1.00 green:0.56 blue:0.17 alpha:1.0];
                }else{
                    _pickupViewText.textColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
                }
                
            }else{
                _pickupImg.image = [UIImage imageNamed:@"pickup_orange.png"];
                _pickupViewText.text = [NSString stringWithFormat:@"Dropoff %@ %@-%@",_job.dropoff_date, _job.dropoff_from_time, _job.dropoff_till_time];
                
                if (!([_job.dropoff_in_time rangeOfString:@"late"].location == NSNotFound))
                {
                    _pickupViewText.textColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
                }else if (!([_job.dropoff_in_time rangeOfString:@"in 1 hour"].location == NSNotFound))
                {
                    
                    _pickupViewText.textColor = [UIColor colorWithRed:1.00 green:0.56 blue:0.17 alpha:1.0];
                }else{
                    _pickupViewText.textColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
                }
            }
        }
    
}

-(void)addShipperDetails{
    BOOL testDate = [self checkCanShipperDetails];
    if (testDate == YES) {
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:_shipperDeatilsTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        _shipperDeatilsTop.layer.mask = maskLayer;
        
        _shipperDetailsView.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _shipperDetailsView.frame = CGRectInset(_shipperDetailsView.frame, -borderWidth, -borderWidth);
        _shipperDetailsView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _shipperDetailsView.layer.borderWidth = borderWidth;
        
        CGFloat widthView1 = [UIScreen mainScreen].bounds.size.width - 60;
        CGRect aRect1 = CGRectMake(0, 0,widthView1, 25);
        
        UIView *viewPhone = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        viewPhone = [self loadShipperTextAndPhone:_job.shipper_name :_job.shipper_phone :aRect1 :@"tapPhoneShipper"];
        [_shipperDetailsBottom addSubview:viewPhone];
        heightCon +=85;
    }else{
        _shipperDetailsView.hidden = YES;
        heightShipperDetails = [NSLayoutConstraint constraintWithItem:_shipperDetailsView
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:0];
        [self.view addConstraint:heightShipperDetails];
        
        topShipperDetails = [NSLayoutConstraint constraintWithItem:_shipperDetailsView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:_routeView
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1
                                                          constant:0];
        [self.view addConstraint:topShipperDetails];
        
    }
}

-(void)loadUI{
    if (_job.box_num != 0) {
        
        NSString *text;
        text = [NSString stringWithFormat:@"%ld Boxes",(long)_job.box_num];
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
        
        widthCell = widthCell + 40 + idealFrame.size.width;
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        img.image = [UIImage imageNamed:@"box2.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        label.text = [NSString stringWithFormat:@"%ld Boxes",(long)_job.box_num];
        
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_jobContentLoad addSubview:boxes];
        [boxes addSubview:img];
        [boxes addSubview:label];
        
    }
    
    if (_job.pallet_num != 0) {
        
        NSString *text;
        text = [NSString stringWithFormat:@"%ld Pallets",(long)_job.pallet_num];
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        UIView *pallets;
        if ((widthCell + 30 + idealFrame.size.width) > getCellWidth)
        {
            pallets = [[UIView alloc]initWithFrame:CGRectMake(5,35,30 + idealFrame.size.width,30)];
            widthCell = idealFrame.size.width + 50;
        }else{
            pallets = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
            widthCell = widthCell + 40 + idealFrame.size.width;
        }
        
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        img.image = [UIImage imageNamed:@"pallet2.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        label.text = [NSString stringWithFormat:@"%ld Pallets",(long)_job.pallet_num];
        
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_jobContentLoad addSubview:pallets];
        [pallets addSubview:img];
        [pallets addSubview:label];
        
    }
    
}

-(void)truckNumUI{
    if (_job.truckload_num != 0) {
        
        NSString *text;
        text = _job.truck_type_name;
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont boldSystemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
        
        widthCell = widthCell + 40 + idealFrame.size.width;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        label.text = _job.truck_type_name;
        
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_jobContentLoad addSubview:boxes];
        [boxes addSubview:label];
        
    }
    
}

-(void)specialRequestUI{
    NSArray *arraySpecialRequest = [_job.shipment_special_request componentsSeparatedByString:@","];
    UIView *specialReq = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,arraySpecialRequest.count * 25,40)];
    
    for (int i=0; i<arraySpecialRequest.count; i++) {
        UIImageView *imageHolder;
        imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*20 + 5, 5, 20, 20)];
        
        UIImage *image1;
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[StaticVars url],arraySpecialRequest[i]]]];
        
        image1 = [UIImage imageWithData: imageData];
        imageHolder.image = image1;
        
        [specialReq addSubview:imageHolder];
    }
    
}

-(void)weightUI{
    NSArray *arraySpecialRequest = [_job.shipment_special_request componentsSeparatedByString:@","];
    UIView *specialReq = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,arraySpecialRequest.count * 25,40)];
    int widthCellToWeight = widthCell + arraySpecialRequest.count * 25 + 20;
    if (![_job.total_load_weight isEqualToString:@""]) {
        NSString *text;
        if ([_job.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
            text = [NSString stringWithFormat:@"%@ T",_job.total_load_weight];
        }else{
            text = [NSString stringWithFormat:@"%@ KG",_job.total_load_weight];
        }
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        if ((getCellWidth - widthCellToWeight) >= 30 + idealFrame.size.width) {
            
            UIView *weight = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
            
            widthCell = widthCell + 40 + idealFrame.size.width;
            
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
            if ([_job.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                img.image = [UIImage imageNamed:@"T2.png"];
            }else{
                img.image = [UIImage imageNamed:@"KG2.png"];
            }
            
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
            label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
            label.text = text;
            [label setFont:[UIFont boldSystemFontOfSize:14]];
            
            [_jobContentLoad addSubview:weight];
            [weight addSubview:img];
            [weight addSubview:label];
            
            //req view
            CGRect btFrame;
            btFrame = specialReq.frame;
            btFrame.origin.x = widthCell;
            specialReq.frame = btFrame;
            
        }
        
    }
    
    [_jobContentLoad addSubview:specialReq];
    
}

-(void)loadRouteUI{
    //////////////////////route
    
    //origin text
    _originText.text = _job.origin_address;
    _originText.lineBreakMode = NSLineBreakByWordWrapping;
    _originText.numberOfLines = 0;
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
    textsize =[_originText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    if (textsize.size.height < 25) {
        originTop = 25;
    }else{
        originTop = textsize.size.height;
    }
    
    
    heightOriginText = [NSLayoutConstraint constraintWithItem:_originText
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:textsize.size.height];
    [self.view addConstraint:heightOriginText];
    
    heightOriginTop = [NSLayoutConstraint constraintWithItem:_originTop
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:originTop + 35];
    [self.view addConstraint:heightOriginTop];
    
    
    
    _pickupText.text = [NSString stringWithFormat:@"%@-%@",_job.pickup_from_time, _job.pickup_till_time];
    
    
    _pickupContactText.hidden = YES;
    _pickupContactText.text = [NSString stringWithFormat:@"%@ %@",_job.origin_contact_name, _job.origin_contact_phone];
    _pickupContactText.lineBreakMode = NSLineBreakByWordWrapping;
    _pickupContactText.numberOfLines = 0;
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
    textsize =[_pickupContactText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    if (textsize.size.height < 25) {
        originBottom = 25;
    }else{
        originBottom = textsize.size.height;
    }
    
    
    heightOriginBottomText = [NSLayoutConstraint constraintWithItem:_pickupContactText
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:textsize.size.height];
    [self.view addConstraint:heightOriginBottomText];
    
    heightOriginBottom = [NSLayoutConstraint constraintWithItem:_originBottom
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil
                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1.0
                                                       constant:originBottom + 5];
    [self.view addConstraint:heightOriginBottom];
    
    ///////
    NSString *str = _job.origin_special_site_instructions;
    CGSize constraint4 = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, 400);
    NSDictionary *attributes4 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
    CGRect textsize4 = [str boundingRectWithSize:constraint4 options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes4 context:nil];
    
    UILabel *origin_intruction;
    origin_intruction = [[UILabel alloc] initWithFrame:CGRectMake(10,originTop + originBottom + 70,textsize4.size.width,textsize4.size.height)];
    
    origin_intruction.text = str;
    origin_intruction.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    [origin_intruction setFont:[UIFont systemFontOfSize:15]];
    
    origin_intruction.lineBreakMode = NSLineBreakByWordWrapping;
    origin_intruction.numberOfLines = 0;
    if (![str isEqualToString:@""]) {
        textsize4.size.height += 10;
    }else{
        textsize4.size.height = 0;
    }
    originBottom += textsize4.size.height;
    
    [_originContentView addSubview:origin_intruction];
    
    heightOriginContent = [NSLayoutConstraint constraintWithItem:_originContentView
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:originTop + originBottom + 70];
    [self.view addConstraint:heightOriginContent];
    //destination text
    _destinationText.text = _job.destination_address;
    _destinationText.lineBreakMode = NSLineBreakByWordWrapping;
    _destinationText.numberOfLines = 0;
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
    textsize =[_destinationText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    if (textsize.size.height < 25) {
        destinationTop = 25;
    }else{
        destinationTop = textsize.size.height;
    }
    
    heightDestinationText = [NSLayoutConstraint constraintWithItem:_destinationText
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:textsize.size.height];
    [self.view addConstraint:heightDestinationText];
    
    heightDestinationTop = [NSLayoutConstraint constraintWithItem:_destinationTop
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0
                                                         constant:destinationTop + 35];
    [self.view addConstraint:heightDestinationTop];
    
    
    
    
    _dropoffText.text = [NSString stringWithFormat:@"%@-%@",_job.dropoff_from_time, _job.dropoff_till_time];
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:17.0] forKey:NSFontAttributeName];
    textsize =[[NSString stringWithFormat:@"%@ %@",_job.destination_contact_name, _job.destination_contact_phone] boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    if (textsize.size.height < 25) {
        destinationBottom = 25;
    }else{
        destinationBottom = textsize.size.height;
    }
    
    str = _job.destination_special_site_instructions;
    constraint4 = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, 400);
    attributes4 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
    textsize4 = [str boundingRectWithSize:constraint4 options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes4 context:nil];
    
    UILabel *dest_intruction;
    dest_intruction = [[UILabel alloc] initWithFrame:CGRectMake(10,destinationTop + destinationBottom + 70,textsize4.size.width,textsize4.size.height)];
    
    dest_intruction.text = str;
    dest_intruction.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    [dest_intruction setFont:[UIFont systemFontOfSize:15]];
    
    dest_intruction.lineBreakMode = NSLineBreakByWordWrapping;
    dest_intruction.numberOfLines = 0;
    /* if (![str isEqualToString:@""]) {
     textsize4.size.height += 10;
     }else{
     textsize4.size.height = 0;
     }*/
    destinationBottom += textsize4.size.height;
    [_destinationContentView addSubview:dest_intruction];
    
    heightRouteView = [NSLayoutConstraint constraintWithItem:_routeView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:originTop + destinationTop + originBottom + destinationBottom + 210];
    [self.view addConstraint:heightRouteView];
    
    
    CGFloat widthView = [UIScreen mainScreen].bounds.size.width - 60;
    CGRect aRect = CGRectMake(0, 0,widthView, 25);
    
    UIView *view = [[UIView alloc] init];
    _alert = [[CustomAlert alloc] init];
    view = [self loadOriginTextAndPhone:_job.origin_contact_name :_job.origin_contact_phone :aRect :@"tapOnPhone"];
    [_contactView addSubview:view];
    
    UIView *view2 = [[UIView alloc] init];
    _alert = [[CustomAlert alloc] init];
    view2 = [self loadTextAndPhone:_job.destination_contact_name :_job.destination_contact_phone :aRect :@"tapPhoneDest"];
    [_dropofContactView addSubview:view2];
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_topRoute.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _topRoute.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_destinationContentView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    
    _routeView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _routeView.frame = CGRectInset(_routeView.frame, -borderWidth, -borderWidth);
    _routeView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _routeView.layer.borderWidth = borderWidth;
    
    CGFloat height = CGRectGetHeight(_routeView.bounds);
    heightCon += originTop + destinationTop + originBottom + destinationBottom + 220;
    if (_job.status_id < 11 && _job.rating == 0) {
        routeTop = [NSLayoutConstraint constraintWithItem:_routeView
                                                attribute:NSLayoutAttributeTop
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:_contentView
                                                attribute:NSLayoutAttributeTop
                                               multiplier:1
                                                 constant:10];
        [self.view addConstraint:routeTop];
    }else{
        if (_job.status_id >= 11 && _job.rating == 0) {
            heightCon += 60;
            routeTop = [NSLayoutConstraint constraintWithItem:_routeView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_contentView
                                                    attribute:NSLayoutAttributeTop
                                                   multiplier:1
                                                     constant:60];
            [self.view addConstraint:routeTop];
        }
        if (_job.status_id < 11 && _job.rating > 0) {
            heightCon += 110;
            routeTop = [NSLayoutConstraint constraintWithItem:_routeView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_contentView
                                                    attribute:NSLayoutAttributeTop
                                                   multiplier:1
                                                     constant:110];
            [self.view addConstraint:routeTop];
        }
        if (_job.status_id >= 11 && _job.rating > 0) {
            heightCon += 160;
            routeTop = [NSLayoutConstraint constraintWithItem:_routeView
                                                    attribute:NSLayoutAttributeTop
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:_contentView
                                                    attribute:NSLayoutAttributeTop
                                                   multiplier:1
                                                     constant:160];
            [self.view addConstraint:routeTop];
        }
        
    }
    
    
    UILabel *loadText = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, originTextWidth - 45, 20)];
    loadText.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    loadText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    if ([_job.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
        loadText.text = [NSString stringWithFormat:@"%@ %@T",_job.truck_type_name,_job.total_load_weight];
    }else{
        loadText.text = [NSString stringWithFormat:@"%@ %@KG",_job.truck_type_name,_job.total_load_weight];
    }
    
    
    [_loadBottom addSubview:loadText];
    
    heightLoadbottom = [NSLayoutConstraint constraintWithItem:_loadBottom
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:30];
    [self.view addConstraint:heightLoadbottom];
    
    
    heightLoadView = [NSLayoutConstraint constraintWithItem:_loadContentView
                                                  attribute:NSLayoutAttributeHeight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1.0
                                                   constant:76];
    [self.view addConstraint:heightLoadView];
    
    heightCon += 86;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_loadTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _loadTop.layer.mask = maskLayer;
    
    _loadContentView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _loadContentView.frame = CGRectInset(_loadContentView.frame, -borderWidth, -borderWidth);
    _loadContentView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _loadContentView.layer.borderWidth = borderWidth;
    
    height = CGRectGetHeight(_loadContentView.bounds);
}

-(void)specialRequestUI2{
    if (![_job.shipment_special_request_with_document isEqualToString:@""]) {
        int checkFiles = 0;
        
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            NSArray *arraySpecialRequest = [[NSArray alloc] init];
            arraySpecialRequest = [_job.shipment_special_request_with_document componentsSeparatedByString:@","];
            
            
            for (int i=0; i<arraySpecialRequest.count; i++) {
                NSArray *single = [[NSArray alloc] init];
                single = [arraySpecialRequest[i] componentsSeparatedByString:@"["];
                
                UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*30, 5, 20, 20)];
                UIImage *image1;
                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[StaticVars url],single[0]]]];
                
                image1 = [UIImage imageWithData: imageData];
                imageHolder.image = image1;
                
                [_SRBottom addSubview:imageHolder];
                
                NSString *file = @"";
                if (single.count > 1) {
                    DDLogVerbose(@"have file");
                    file = [single[1] substringToIndex:[single[1] length] - 1];
                    checkFiles = 1;
                }
                
                if (![file isEqualToString:@""]) {
                    UIButton *fileButton = [[UIButton alloc] initWithFrame:CGRectMake(i*30, 27, 28, 25)];
                    
                    fileButton.tag = i;
                    [fileButton addTarget:self
                                   action:@selector(openFile:)
                         forControlEvents:UIControlEventTouchUpInside];
                    
                    fileButton.titleLabel.font = [UIFont boldSystemFontOfSize:11];
                    [fileButton setTitle:@"view" forState:UIControlStateNormal];
                    [fileButton setTitleColor:[UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0] forState:UIControlStateNormal];
                    
                    [_SRBottom addSubview:fileButton];
                }
            }
        }
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:_SRTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        _SRTop.layer.mask = maskLayer;
        
        _SRview.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _SRview.frame = CGRectInset(_SRview.frame, -borderWidth, -borderWidth);
        _SRview.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _SRview.layer.borderWidth = borderWidth;
        
        // height = CGRectGetHeight(_SRview.bounds);
        
        heightCon +=80;
        
        if (checkFiles == 1) {
            CGRect btFrame;
            btFrame = _SRBottom.frame;
            btFrame.size.height = btFrame.size.height + 30;
            _SRBottom.frame = btFrame;
            heightCon += 30;
            
            heightSpecialRequest = [NSLayoutConstraint constraintWithItem:_SRview
                                                                attribute:NSLayoutAttributeHeight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil
                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:1.0
                                                                 constant:100];
            [self.view addConstraint:heightSpecialRequest];
        }
        
    }else{
        heightSpecialRequest = [NSLayoutConstraint constraintWithItem:_SRview
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:0];
        [self.view addConstraint:heightSpecialRequest];
        
        topSpecialRequest = [NSLayoutConstraint constraintWithItem:_loadContentView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:_SRview
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1
                                                          constant:0];
        [self.view addConstraint:topSpecialRequest];
        
        _SRview.hidden = YES;
    }
}

-(void)commentsUI{
    if (![_job.comments isEqualToString:@""]) {
        _commentText.text = _job.comments;
        _commentText.lineBreakMode = NSLineBreakByWordWrapping;
        _commentText.numberOfLines = 0;
        
        
        constraint = CGSizeMake(originTextWidth - 20, 400);
        attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
        textsize =[_commentText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        originTop = textsize.size.height;
        
        heightCommentBottom = [NSLayoutConstraint constraintWithItem:_commentsBottom
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:textsize.size.height + 10];
        [self.view addConstraint:heightCommentBottom];
        
        heightCommentView = [NSLayoutConstraint constraintWithItem:_commentsView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:textsize.size.height + 50];
        [self.view addConstraint:heightCommentView];
        
        heightCon += textsize.size.height + 60;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:_commentsTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        _commentsTop.layer.mask = maskLayer;
        
        _commentsView.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _commentsView.frame = CGRectInset(_commentsView.frame, -borderWidth, -borderWidth);
        _commentsView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _commentsView.layer.borderWidth = borderWidth;
        //height = CGRectGetHeight(_commentsView.bounds);
    }else{
        heightCommentsView = [NSLayoutConstraint constraintWithItem:_commentsView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:0];
        [self.view addConstraint:heightCommentsView];
        _commentsView.hidden = YES;
    }
    
}

-(void)loadRateUI{
    if (_job.rating > 0) {
        _ratingView.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _ratingView.frame = CGRectInset(_ratingView.frame, -borderWidth, -borderWidth);
        _ratingView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _ratingView.layer.borderWidth = borderWidth;
        
        _ratingLabel.text = _job.feedback;
        
        self.rating.delegate = self;
        self.rating.emptySelectedImage = [UIImage imageNamed:@"star_empty.png"];
        self.rating.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
        self.rating.contentMode = UIViewContentModeScaleAspectFill;
        self.rating.maxRating = 5;
        self.rating.minRating = 1;
        self.rating.rating = (float)_job.rating;
        self.rating.floatRatings = YES;
        
        if (_job.status_id < 11) {
            rateTop = [NSLayoutConstraint constraintWithItem:_ratingView
                                                   attribute:NSLayoutAttributeTop
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:_contentView
                                                   attribute:NSLayoutAttributeTop
                                                  multiplier:1
                                                    constant:10];
            [self.view addConstraint:rateTop];
            
        }
    }else{
        _ratingView.hidden = YES;
    }
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,- keyboardSize.height+120,width,height)];
    
}

-(void)cancelNumberPad
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    [self.view setFrame:CGRectMake(0,0,width,height)];
    [self.view endEditing:YES];
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)]];
    [numberToolbar sizeToFit];
    textView.inputAccessoryView = numberToolbar;
    return YES;
}

-(void)updateApp{
    NSString *iTunesLink = @"https://itunes.apple.com/il/app/truckiez/id1191308814?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

@end

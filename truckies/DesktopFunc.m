//
//  DesktopFunc.m
//  truckies
//
//  Created by Menachem Mizrachi on 03/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "DesktopFunc.h"
#import "JSON.h"
#import "shipmentObject.h"
#import "ShipmentsControllerFunctions.h"
#import "offerObject.h"
#import "LocationObject.h"
#import "InternetConnection.h"
#import "StaticVars.h"

@interface DesktopFunc ()
{
    JSON *_json;
    ShipmentsControllerFunctions *_SCFunc;
    LocationObject *_location;
    shipmentObject *_job;
    InternetConnection *_InternetConnection;
}

@end

@implementation DesktopFunc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSMutableArray *)loadContent{
    
    NSMutableArray *today = [[NSMutableArray alloc] init];
    NSMutableArray *schedule = [[NSMutableArray alloc] init];
    NSMutableArray *complete = [[NSMutableArray alloc] init];
    NSMutableArray *locationArr = [[NSMutableArray alloc] init];
    
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=jobs&user_id=%@&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
    
    if (myData) {
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        for (int i = 0; i < 3; i ++) {
            NSDictionary *innerDic = [[NSDictionary alloc] init];
            if (i == 0) {
                innerDic = [dic objectForKey:@"today"];
                today = [[NSMutableArray alloc] init];
            }else if (i == 1){
                innerDic = [dic objectForKey:@"schedule"];
                schedule = [[NSMutableArray alloc] init];
            }else{
                innerDic = [dic objectForKey:@"complete"];
                complete = [[NSMutableArray alloc] init];
            }
            
            if(innerDic != (id)[NSNull null]) {
                for (NSDictionary *key in innerDic) {
                    
                    shipmentObject *sh;
                    _job = [[shipmentObject alloc] init];
                    sh = [_job returnJob:key];
                    
                    
                    if (i == 0) {
                        [today addObject:sh];
                    }else if (i == 1){
                        [schedule addObject:sh];
                    }else{
                        [complete addObject:sh];
                    }
                    
                    
                    if (sh.status_id >= 7 && sh.status_id <15) {
                        [locationArr addObject:sh];
                    }
                    
                }
            }
        }
        
    }
    NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects:today, schedule, complete, locationArr, nil];
    return arr;
    
}

-(NSMutableArray *)loadContentWhenScroll:(int)select :(NSString *)searchURL{
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (select == 4) {
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:searchURL];
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        if (dic != (id)[NSNull null]) {
            
            for (NSDictionary *key in dic) {
                shipmentObject *sh;
                _job = [[shipmentObject alloc] init];
                sh = [_job returnJob:key];
                
                [arr addObject:sh];
            }
        }
    }else{
        NSMutableArray *currentArray = [[NSMutableArray alloc] init];
        NSMutableArray *today;
        NSMutableArray *schedule;
        NSMutableArray *complete;
        
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=jobs&user_id=%@&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
        
        if (myData) {
            
            
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            for (int i = 0; i < 3; i ++) {
                NSDictionary *innerDic = [[NSDictionary alloc] init];
                if (i == 0) {
                    innerDic = [dic objectForKey:@"today"];
                    today = [[NSMutableArray alloc] init];
                }else if (i == 1){
                    innerDic = [dic objectForKey:@"schedule"];
                    schedule = [[NSMutableArray alloc] init];
                }else{
                    innerDic = [dic objectForKey:@"complete"];
                    complete = [[NSMutableArray alloc] init];
                }
                
                if(innerDic != (id)[NSNull null]) {
                    for (NSDictionary *key in innerDic) {
                        
                        shipmentObject *sh;
                        _job = [[shipmentObject alloc] init];
                        sh = [_job returnJob:key];
                        
                        if (i == 0) {
                            [today addObject:sh];
                        }else if (i == 1){
                            [schedule addObject:sh];
                        }else{
                            [complete addObject:sh];
                        }
                        
                    }
                }
            }
            if (select == 1) {
                currentArray = today;
            }else if (select == 2){
                currentArray = schedule;
            }else{
                currentArray = complete;
            }
        }
        arr = [[NSMutableArray alloc] initWithObjects:currentArray, today, schedule, complete, nil];
    }
    return arr;
    
}

-(NSURL*)GetGoogleStaticMap:(NSString *)latitude :(NSString *)longitude :(NSString *)latitude2 :(NSString *)longitude2  :(NSString *)path
{
    
    NSString* coordinates = [NSString stringWithFormat:@"%f,%f", [latitude doubleValue], [longitude doubleValue]];
    NSString* coordinates2 = [NSString stringWithFormat:@"%f,%f", [latitude2 doubleValue], [longitude2 doubleValue]];
    
    //NSString *str;
    if ([path isEqualToString:@""]) {
        _SCFunc = [[ShipmentsControllerFunctions alloc] init];
        path= [_SCFunc addPolyLineStaticMap:latitude :longitude :latitude2 :longitude2];
    }else{

    }
    
    NSString *stringURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=color:0x378EB9ff|weight:4|enc:%@&maptype=roadmap&markers=icon:%@/template/application/from.png|%@&markers=icon:%@/template/application/to.png|%@&key=AIzaSyAr8t2iKU1zyA2MxIqLAhiSzOKYoO5f9jk",path,[StaticVars url], coordinates,[StaticVars url], coordinates2];
    //NSString *stringURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?size=600x400&%@&maptype=roadmap&markers=icon:http://truckies.shtibel.com/template/application/from.png|%@&markers=icon:http://truckies.shtibel.com/template/application/to.png|%@&key=AIzaSyAr8t2iKU1zyA2MxIqLAhiSzOKYoO5f9jk",path, coordinates, coordinates2];
    NSString *newStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:newStringURL];
    
    return url;
}


-(void)sendToDB:(NSString *)latitudeLabel :(NSString *)longitudeLabel :(NSString *)driverStatus{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
    _json = [[JSON alloc] init];
    // NSData *myData = [[NSData alloc] init];
    [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=locationMove&user_id=%@&lat=%@&lng=%@&driver_application_status_id=%@&carrier_id%@",[[NSUserDefaults standardUserDefaults]
                                                                                                                                                                                    stringForKey:@"userID"], latitudeLabel, longitudeLabel, driverStatus, [[NSUserDefaults standardUserDefaults]
                                                                                                                                                                                                                                                           stringForKey:@"carrier_id"]]];
    
    NSLog(@"send from maps: %@ %@ %@",latitudeLabel, longitudeLabel, driverStatus);
    }
    //    NSDictionary *dic = [[NSDictionary alloc] init];
    //    _json = [[JSON alloc] init];
    //    dic = [_json FromJson:myData];
    //    NSLog(@"%@",dic);
    
}

-(NSString *)dayString:(NSString *)dateObject{
    NSString *stringReturn = @"";
    
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    //today
    NSString *day;
    if ([today day] < 10) {
        day = [NSString stringWithFormat:@"0%ld",(long)[today day]];
    }else{
        day = [NSString stringWithFormat:@"%ld",(long)[today day]];
    }
    
    NSString *month;
    if ([today month] < 10) {
        month = [NSString stringWithFormat:@"0%ld",(long)[today month]];
    }else{
        month = [NSString stringWithFormat:@"%ld",(long)[today month]];
    }
    
    NSString *dayOfToday = [NSString stringWithFormat:@"%@/%@",day, month];
    
    if ([dateObject isEqualToString:dayOfToday]) {
        stringReturn = @"today";
    }
    
    //tommorow
    today.day = today.day + 1;
    if ([today day] < 10) {
        day = [NSString stringWithFormat:@"0%ld",(long)[today day]];
    }else{
        day = [NSString stringWithFormat:@"%ld",(long)[today day]];
    }
    
    if ([today month] < 10) {
        month = [NSString stringWithFormat:@"0%ld",(long)[today month]];
    }else{
        month = [NSString stringWithFormat:@"%ld",(long)[today month]];
    }
    
    dayOfToday = [NSString stringWithFormat:@"%@/%@",day, month];
    
    if ([dateObject isEqualToString:dayOfToday]) {
        stringReturn = @"tomorrow";
    }
    
    return stringReturn;
}

-(NSMutableArray *)loadContentOffers{
    NSMutableArray *offers_today;
    NSMutableArray *offers_scheduald;
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=offers&user_id=%@&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    for (int i = 0; i < 2; i ++) {
        NSDictionary *innerDic = [[NSDictionary alloc] init];
        if (i == 0) {
            innerDic = [dic objectForKey:@"offers_today"];
            offers_today = [[NSMutableArray alloc] init];
        }else{
            innerDic = [dic objectForKey:@"offers_scheduald"];
            offers_scheduald = [[NSMutableArray alloc] init];
        }
        
        if(innerDic != (id)[NSNull null]) {
            for (NSDictionary *key in innerDic) {
                offerObject *offer = [[offerObject alloc]init];
                
                
                offer.offerId = [[key objectForKey:@"id"] integerValue];
                
                if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
                    offer.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
                }else{
                    offer.origin_special_site_instructions = @"";
                }
                
                if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
                    offer.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
                }else{
                    offer.destination_special_site_instructions = @"";
                }

                if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
                    offer.load_weight_type = [key objectForKey:@"load_weight_type"];
                }else{
                    offer.load_weight_type = @"";
                }
                
                if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
                    offer.status_name = [key objectForKey:@"status_name"];
                }else{
                    offer.status_name = @"";
                }
                
                if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                    offer.origin_lat = [key objectForKey:@"origin_lat"];
                }else{
                    offer.origin_lat = @"";
                }
                
                if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                    offer.origin_lng = [key objectForKey:@"origin_lng"];
                }else{
                    offer.origin_lng = @"";
                }
                
                if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                    offer.destination_lat = [key objectForKey:@"destination_lat"];
                }else{
                    offer.destination_lat = @"";
                }
                
                if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                    offer.destination_lng = [key objectForKey:@"destination_lng"];
                }else{
                    offer.destination_lng = @"";
                }
                
                if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
                    offer.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
                }else{
                    offer.shipment_carrier_payout_text = @"";
                }
                
                if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
                    offer.box_num = [[key objectForKey:@"box_num"] integerValue];
                }else{
                    offer.box_num = 0;
                }
                
                if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
                    offer.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
                }else{
                    offer.pallet_num = 0;
                }
                
                if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
                    offer.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
                }else{
                    offer.truckload_num = 0;
                }
                
                if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
                    offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
                }else{
                    offer.shipment_special_request = @"";
                }
                
                if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
                    offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
                }else{
                    offer.shipment_special_request = @"";
                }
                
                if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
                    offer.pickup_date = [key objectForKey:@"pickup_date"];
                }else{
                    offer.pickup_date = @"";
                }
                
                if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
                    offer.dropoff_date = [key objectForKey:@"dropoff_date"];
                }else{
                    offer.dropoff_date = @"";
                }
                
                if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
                    offer.pickup_from_time = [key objectForKey:@"pickup_from_time"];
                }else{
                    offer.pickup_from_time = @"";
                }
                
                if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
                    offer.pickup_till_time = [key objectForKey:@"pickup_till_time"];
                }else{
                    offer.pickup_till_time = @"";
                }
                
                if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
                    offer.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
                }else{
                    offer.dropoff_from_time = @"";
                }
                
                if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
                    offer.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
                }else{
                    offer.dropoff_till_time = @"";
                }
                
                
                if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
                    offer.total_load_weight = [key objectForKey:@"total_load_weight"];
                }else{
                    offer.total_load_weight = @"";
                }
                
                if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
                    offer.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
                }else{
                    offer.total_driving_distance = 0;
                }
                
                if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
                    offer.pickup_in_time = [key objectForKey:@"pickup_in_time"];
                }else{
                    offer.pickup_in_time = @"";
                }
                
                if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
                    offer.origin_address= [key objectForKey:@"origin_address"];
                }else{
                    offer.origin_address = @"";
                }
                
                if ([key objectForKey:@"origin_address_name"]!= (id)[NSNull null]) {
                    offer.origin_address_name = [key objectForKey:@"origin_address_name"];
                }else{
                    offer.origin_address_name = @"";
                }
                
                if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
                    offer.destination_address = [key objectForKey:@"destination_address"];
                }else{
                    offer.destination_address = @"";
                }
                
                if ([key objectForKey:@"destination_address_name"]!= (id)[NSNull null]) {
                    offer.destination_address_name = [key objectForKey:@"destination_address_name"];
                }else{
                    offer.destination_address_name = @"";
                }
                
                if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
                    offer.original_pickup_date = [key objectForKey:@"original_pickup_date"];
                }else{
                    offer.original_pickup_date = @"";
                }
                
                if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
                    offer.shipment_load = [key objectForKey:@"shipment_load"];
                }else{
                    offer.shipment_load = @"";
                }
                
                if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
                    offer.comments = [key objectForKey:@"comments"];
                }else{
                    offer.comments = @"";
                }
                
                if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
                    offer.shipment_google_root = [key objectForKey:@"shipment_google_root"];
                }else{
                    offer.shipment_google_root = @"";
                }
                
                if ([key objectForKey:@"truck_type_name"]!= (id)[NSNull null]) {
                    offer.truck_type_name = [key objectForKey:@"truck_type_name"];
                }else{
                    offer.truck_type_name = @"";
                }
                
                
                
                if (i == 0) {
                    [offers_today addObject:offer];
                }else{
                    [offers_scheduald addObject:offer];
                }
            }
        }
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects:offers_today, offers_scheduald, nil];
    
    return arr;
}

-(NSMutableArray *)loadContentOffersWhenScroll:(int)select :(NSString *)searchURL{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    if (select == 3) {
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:searchURL];
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        if (dic != (id)[NSNull null]) {
            
            for (NSDictionary *key in dic) {
                offerObject *offer = [[offerObject alloc]init];
                
                
                offer.offerId = [[key objectForKey:@"id"] integerValue];
                
                if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
                    offer.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
                }else{
                    offer.origin_special_site_instructions = @"";
                }
                
                if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
                    offer.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
                }else{
                    offer.destination_special_site_instructions = @"";
                }
                
                if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
                    offer.load_weight_type = [key objectForKey:@"load_weight_type"];
                }else{
                    offer.load_weight_type = @"";
                }
                
                if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
                    offer.status_name = [key objectForKey:@"status_name"];
                }else{
                    offer.status_name = @"";
                }
                
                if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                    offer.origin_lat = [key objectForKey:@"origin_lat"];
                }else{
                    offer.origin_lat = @"";
                }
                
                if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                    offer.origin_lng = [key objectForKey:@"origin_lng"];
                }else{
                    offer.origin_lng = @"";
                }
                
                if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                    offer.destination_lat = [key objectForKey:@"destination_lat"];
                }else{
                    offer.destination_lat = @"";
                }
                
                if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                    offer.destination_lng = [key objectForKey:@"destination_lng"];
                }else{
                    offer.destination_lng = @"";
                }
                
                if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
                    offer.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
                }else{
                    offer.shipment_carrier_payout_text = @"";
                }
                
                if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
                    offer.box_num = [[key objectForKey:@"box_num"] integerValue];
                }else{
                    offer.box_num = 0;
                }
                
                if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
                    offer.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
                }else{
                    offer.pallet_num = 0;
                }
                
                if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
                    offer.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
                }else{
                    offer.truckload_num = 0;
                }
                
                if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
                    offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
                }else{
                    offer.shipment_special_request = @"";
                }
                
                if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
                    offer.pickup_date = [key objectForKey:@"pickup_date"];
                }else{
                    offer.pickup_date = @"";
                }
                
                if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
                    offer.dropoff_date = [key objectForKey:@"dropoff_date"];
                }else{
                    offer.dropoff_date = @"";
                }
                
                if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
                    offer.pickup_from_time = [key objectForKey:@"pickup_from_time"];
                }else{
                    offer.pickup_from_time = @"";
                }
                
                if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
                    offer.pickup_till_time = [key objectForKey:@"pickup_till_time"];
                }else{
                    offer.pickup_till_time = @"";
                }
                
                if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
                    offer.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
                }else{
                    offer.dropoff_from_time = @"";
                }
                
                if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
                    offer.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
                }else{
                    offer.dropoff_till_time = @"";
                }
                
                
                if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
                    offer.total_load_weight = [key objectForKey:@"total_load_weight"];
                }else{
                    offer.total_load_weight = @"";
                }
                
                if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
                    offer.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
                }else{
                    offer.total_driving_distance = 0;
                }
                
                if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
                    offer.pickup_in_time = [key objectForKey:@"pickup_in_time"];
                }else{
                    offer.pickup_in_time = @"";
                }
                
                if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
                    offer.origin_address= [key objectForKey:@"origin_address"];
                }else{
                    offer.origin_address = @"";
                }
                
                if ([key objectForKey:@"origin_address_name"]!= (id)[NSNull null]) {
                    offer.origin_address_name = [key objectForKey:@"origin_address_name"];
                }else{
                    offer.origin_address_name = @"";
                }
                
                if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
                    offer.destination_address = [key objectForKey:@"destination_address"];
                }else{
                    offer.destination_address = @"";
                }
                
                if ([key objectForKey:@"destination_address_name"]!= (id)[NSNull null]) {
                    offer.destination_address_name = [key objectForKey:@"destination_address_name"];
                }else{
                    offer.destination_address_name = @"";
                }
                
                if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
                    offer.original_pickup_date = [key objectForKey:@"original_pickup_date"];
                }else{
                    offer.original_pickup_date = @"";
                }
                
                if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
                    offer.shipment_load = [key objectForKey:@"shipment_load"];
                }else{
                    offer.shipment_load = @"";
                }
                
                if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
                    offer.comments = [key objectForKey:@"comments"];
                }else{
                    offer.comments = @"";
                }
                
                if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
                    offer.shipment_google_root = [key objectForKey:@"shipment_google_root"];
                }else{
                    offer.shipment_google_root = @"";
                }
                
                if ([key objectForKey:@"truck_type_name"]!= (id)[NSNull null]) {
                    offer.truck_type_name = [key objectForKey:@"truck_type_name"];
                }else{
                    offer.truck_type_name = @"";
                }
                
                [arr addObject:offer];
            }
        }
        
    }else{
        NSMutableArray *currentArray = [[NSMutableArray alloc] init];
        NSMutableArray *offers_today;
        NSMutableArray *offers_scheduald;
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=offers&user_id=%@&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        for (int i = 0; i < 2; i ++) {
            NSDictionary *innerDic = [[NSDictionary alloc] init];
            if (i == 0) {
                innerDic = [dic objectForKey:@"offers_today"];
                offers_today = [[NSMutableArray alloc] init];
            }else{
                innerDic = [dic objectForKey:@"offers_scheduald"];
                offers_scheduald = [[NSMutableArray alloc] init];
            }
            
            if(innerDic != (id)[NSNull null]) {
                for (NSDictionary *key in innerDic) {
                    offerObject *offer = [[offerObject alloc]init];
                    
                    
                    offer.offerId = [[key objectForKey:@"id"] integerValue];
                    
                    if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
                        offer.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
                    }else{
                        offer.origin_special_site_instructions = @"";
                    }
                    
                    if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
                        offer.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
                    }else{
                        offer.destination_special_site_instructions = @"";
                    }
                    
                    if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
                        offer.load_weight_type = [key objectForKey:@"load_weight_type"];
                    }else{
                        offer.load_weight_type = @"";
                    }
                    
                    if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
                        offer.status_name = [key objectForKey:@"status_name"];
                    }else{
                        offer.status_name = @"";
                    }
                    
                    if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                        offer.origin_lat = [key objectForKey:@"origin_lat"];
                    }else{
                        offer.origin_lat = @"";
                    }
                    
                    if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                        offer.origin_lng = [key objectForKey:@"origin_lng"];
                    }else{
                        offer.origin_lng = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                        offer.destination_lat = [key objectForKey:@"destination_lat"];
                    }else{
                        offer.destination_lat = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                        offer.destination_lng = [key objectForKey:@"destination_lng"];
                    }else{
                        offer.destination_lng = @"";
                    }
                    
                    if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
                        offer.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
                    }else{
                        offer.shipment_carrier_payout_text = @"";
                    }
                    
                    if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
                        offer.box_num = [[key objectForKey:@"box_num"] integerValue];
                    }else{
                        offer.box_num = 0;
                    }
                    
                    if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
                        offer.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
                    }else{
                        offer.pallet_num = 0;
                    }
                    
                    if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
                        offer.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
                    }else{
                        offer.truckload_num = 0;
                    }
                    
                    if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
                        offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
                    }else{
                        offer.shipment_special_request = @"";
                    }
                    
                    if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
                        offer.pickup_date = [key objectForKey:@"pickup_date"];
                    }else{
                        offer.pickup_date = @"";
                    }
                    
                    if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
                        offer.dropoff_date = [key objectForKey:@"dropoff_date"];
                    }else{
                        offer.dropoff_date = @"";
                    }
                    
                    if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
                        offer.pickup_from_time = [key objectForKey:@"pickup_from_time"];
                    }else{
                        offer.pickup_from_time = @"";
                    }
                    
                    if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
                        offer.pickup_till_time = [key objectForKey:@"pickup_till_time"];
                    }else{
                        offer.pickup_till_time = @"";
                    }
                    
                    if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
                        offer.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
                    }else{
                        offer.dropoff_from_time = @"";
                    }
                    
                    if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
                        offer.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
                    }else{
                        offer.dropoff_till_time = @"";
                    }
                    
                    
                    if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
                        offer.total_load_weight = [key objectForKey:@"total_load_weight"];
                    }else{
                        offer.total_load_weight = @"";
                    }
                    
                    if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
                        offer.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
                    }else{
                        offer.total_driving_distance = 0;
                    }
                    
                    if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
                        offer.pickup_in_time = [key objectForKey:@"pickup_in_time"];
                    }else{
                        offer.pickup_in_time = @"";
                    }
                    
                    if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
                        offer.origin_address= [key objectForKey:@"origin_address"];
                    }else{
                        offer.origin_address = @"";
                    }
                    
                    if ([key objectForKey:@"origin_address_name"]!= (id)[NSNull null]) {
                        offer.origin_address_name = [key objectForKey:@"origin_address_name"];
                    }else{
                        offer.origin_address_name = @"";
                    }
                    
                    if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
                        offer.destination_address = [key objectForKey:@"destination_address"];
                    }else{
                        offer.destination_address = @"";
                    }
                    
                    if ([key objectForKey:@"destination_address_name"]!= (id)[NSNull null]) {
                        offer.destination_address_name = [key objectForKey:@"destination_address_name"];
                    }else{
                        offer.destination_address_name = @"";
                    }
                    
                    if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
                        offer.original_pickup_date = [key objectForKey:@"original_pickup_date"];
                    }else{
                        offer.original_pickup_date = @"";
                    }
                    
                    if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
                        offer.shipment_load = [key objectForKey:@"shipment_load"];
                    }else{
                        offer.shipment_load = @"";
                    }
                    
                    if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
                        offer.comments = [key objectForKey:@"comments"];
                    }else{
                        offer.comments = @"";
                    }
                    
                    if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
                        offer.shipment_google_root = [key objectForKey:@"shipment_google_root"];
                    }else{
                        offer.shipment_google_root = @"";
                    }
                    
                    if ([key objectForKey:@"truck_type_name"]!= (id)[NSNull null]) {
                        offer.truck_type_name = [key objectForKey:@"truck_type_name"];
                    }else{
                        offer.truck_type_name = @"";
                    }
                    
                    
                    if (i == 0) {
                        [offers_today addObject:offer];
                    }else{
                        [offers_scheduald addObject:offer];
                    }
                }
            }
        }
        
        if (select == 1) {
            currentArray = offers_today;
        }else{
            currentArray = offers_scheduald;
        }
        
        arr = [[NSMutableArray alloc] initWithObjects:currentArray, offers_today, offers_scheduald, nil];
    }
    return arr;
}

-(NSMutableArray *)searchOffer:(NSString *)country :(NSString *)city :(NSString *)street :(NSString *)pallet :(NSString *)box :(NSString *)truckload :(NSString *)fromDate :(NSString *)toDate :(NSString *)keyword{
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"searchOffer&carrier_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], pallet, box, truckload, street, city, country, fromDate, toDate, keyword];
   // NSString *newStringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *fullURL = [NSString stringWithFormat:@"tr-api/?action=%@",stringURL];
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:fullURL];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    if (dic != (id)[NSNull null]) {
        
        for (NSDictionary *key in dic) {
            offerObject *offer = [[offerObject alloc]init];
            
            
            offer.offerId = [[key objectForKey:@"id"] integerValue];
            
            if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
                offer.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
            }else{
                offer.origin_special_site_instructions = @"";
            }
            
            if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
                offer.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
            }else{
                offer.destination_special_site_instructions = @"";
            }
            
            if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
                offer.load_weight_type = [key objectForKey:@"load_weight_type"];
            }else{
                offer.load_weight_type = @"";
            }
            
            if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
                offer.status_name = [key objectForKey:@"status_name"];
            }else{
                offer.status_name = @"";
            }
            
            if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                offer.origin_lat = [key objectForKey:@"origin_lat"];
            }else{
                offer.origin_lat = @"";
            }
            
            if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                offer.origin_lng = [key objectForKey:@"origin_lng"];
            }else{
                offer.origin_lng = @"";
            }
            
            if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                offer.destination_lat = [key objectForKey:@"destination_lat"];
            }else{
                offer.destination_lat = @"";
            }
            
            if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                offer.destination_lng = [key objectForKey:@"destination_lng"];
            }else{
                offer.destination_lng = @"";
            }
            
            if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
                offer.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
            }else{
                offer.shipment_carrier_payout_text = @"";
            }
            
            if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
                offer.box_num = [[key objectForKey:@"box_num"] integerValue];
            }else{
                offer.box_num = 0;
            }
            
            if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
                offer.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
            }else{
                offer.pallet_num = 0;
            }
            
            if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
                offer.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
            }else{
                offer.truckload_num = 0;
            }
            
            if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
                offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
            }else{
                offer.shipment_special_request = @"";
            }
            
            if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
                offer.pickup_date = [key objectForKey:@"pickup_date"];
            }else{
                offer.pickup_date = @"";
            }
            
            if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
                offer.dropoff_date = [key objectForKey:@"dropoff_date"];
            }else{
                offer.dropoff_date = @"";
            }
            
            if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
                offer.pickup_from_time = [key objectForKey:@"pickup_from_time"];
            }else{
                offer.pickup_from_time = @"";
            }
            
            if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
                offer.pickup_till_time = [key objectForKey:@"pickup_till_time"];
            }else{
                offer.pickup_till_time = @"";
            }
            
            if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
                offer.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
            }else{
                offer.dropoff_from_time = @"";
            }
            
            if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
                offer.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
            }else{
                offer.dropoff_till_time = @"";
            }
            
            
            if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
                offer.total_load_weight = [key objectForKey:@"total_load_weight"];
            }else{
                offer.total_load_weight = @"";
            }
            
            if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
                offer.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
            }else{
                offer.total_driving_distance = 0;
            }
            
            if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
                offer.pickup_in_time = [key objectForKey:@"pickup_in_time"];
            }else{
                offer.pickup_in_time = @"";
            }
            
            if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
                offer.origin_address= [key objectForKey:@"origin_address"];
            }else{
                offer.origin_address = @"";
            }
            
            if ([key objectForKey:@"origin_address_name"]!= (id)[NSNull null]) {
                offer.origin_address_name = [key objectForKey:@"origin_address_name"];
            }else{
                offer.origin_address_name = @"";
            }
            
            if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
                offer.destination_address = [key objectForKey:@"destination_address"];
            }else{
                offer.destination_address = @"";
            }
            
            if ([key objectForKey:@"destination_address_name"]!= (id)[NSNull null]) {
                offer.destination_address_name = [key objectForKey:@"destination_address_name"];
            }else{
                offer.destination_address_name = @"";
            }
            
            if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
                offer.original_pickup_date = [key objectForKey:@"original_pickup_date"];
            }else{
                offer.original_pickup_date = @"";
            }
            
            if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
                offer.shipment_load = [key objectForKey:@"shipment_load"];
            }else{
                offer.shipment_load = @"";
            }
            
            if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
                offer.comments = [key objectForKey:@"comments"];
            }else{
                offer.comments = @"";
            }
            
            if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
                offer.shipment_google_root = [key objectForKey:@"shipment_google_root"];
            }else{
                offer.shipment_google_root = @"";
            }
            
            [arr addObject:offer];
        }
    }
    
    return arr;
}

-(NSMutableArray *)searchJob:(NSString *)country :(NSString *)city :(NSString *)street :(NSString *)pallet :(NSString *)box :(NSString *)truckload :(NSString *)fromDate :(NSString *)toDate :(NSString *)keyword{
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    NSString *stringURL = [NSString stringWithFormat:@"searchJob&user_id=%@&pallets=%@&boxes=%@&truckload=%@&street_name=%@&city=%@&country=%@&from_date=%@&till_date=%@&keyword=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], pallet, box, truckload, street, city, country, fromDate, toDate, keyword];
    
    NSString *fullURL = [NSString stringWithFormat:@"tr-api/?action=%@",stringURL];
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:fullURL];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    if (dic != (id)[NSNull null]) {
        
        for (NSDictionary *key in dic) {
            shipmentObject *sh;
            _job = [[shipmentObject alloc] init];
            sh = [_job returnJob:key];
            
            [arr addObject:sh];
        }
        
    }
    return  arr;
}

-(NSMutableArray *)loadJobsToChngeStatus{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=jobs&user_id=%@&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"],[[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
    if (myData) {
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        NSDictionary *today = [[NSDictionary alloc] init];
        today = [dic objectForKey:@"today"];
        if(today != (id)[NSNull null]) {
            for (NSDictionary *key in today) {
                
                if ((([[key objectForKey:@"status_id"] intValue] >= 7) && ([[key objectForKey:@"status_id"] intValue] < 11)) || (([[key objectForKey:@"status_id"] intValue] >= 12) && ([[key objectForKey:@"status_id"] intValue] < 15))) {
                    
                    shipmentObject *sh = [[shipmentObject alloc]init];
                    
                    sh.shipmentsId = [[key objectForKey:@"id"] integerValue];
                    if ([key objectForKey:@"status_id"] != (id)[NSNull null]) {
                        sh.status_id = [[key objectForKey:@"status_id"] integerValue];
                    }else{
                        sh.status_id = 0;
                    }
                    
                    
                    
                    if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                        sh.origin_lat = [key objectForKey:@"origin_lat"];
                    }else{
                        sh.origin_lat = @"";
                    }
                    
                    if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                        sh.origin_lng = [key objectForKey:@"origin_lng"];
                    }else{
                        sh.origin_lng = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                        sh.destination_lat = [key objectForKey:@"destination_lat"];
                    }else{
                        sh.destination_lat = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                        sh.destination_lng = [key objectForKey:@"destination_lng"];
                    }else{
                        sh.destination_lng = @"";
                    }
                    
                    [arr addObject:sh];
                }
            }
        }
        
        NSDictionary *schedule = [[NSDictionary alloc] init];
        schedule = [dic objectForKey:@"schedule"];
        if(schedule != (id)[NSNull null]) {
            for (NSDictionary *key in schedule) {
                
                if ((([[key objectForKey:@"status_id"] intValue] >= 7) && ([[key objectForKey:@"status_id"] intValue] < 11)) || (([[key objectForKey:@"status_id"] intValue] >= 12) && ([[key objectForKey:@"status_id"] intValue] < 15))) {
                    
                    shipmentObject *sh = [[shipmentObject alloc]init];
                    
                    sh.shipmentsId = [[key objectForKey:@"id"] integerValue];
                    if ([key objectForKey:@"status_id"] != (id)[NSNull null]) {
                        sh.status_id = [[key objectForKey:@"status_id"] integerValue];
                    }else{
                        sh.status_id = 0;
                    }
                    
                    if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
                        sh.origin_lat = [key objectForKey:@"origin_lat"];
                    }else{
                        sh.origin_lat = @"";
                    }
                    
                    if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
                        sh.origin_lng = [key objectForKey:@"origin_lng"];
                    }else{
                        sh.origin_lng = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
                        sh.destination_lat = [key objectForKey:@"destination_lat"];
                    }else{
                        sh.destination_lat = @"";
                    }
                    
                    if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
                        sh.destination_lng = [key objectForKey:@"destination_lng"];
                    }else{
                        sh.destination_lng = @"";
                    }
                    
                    [arr addObject:sh];
                }
            }
        }
    }
    
    return arr;
}

-(NSMutableArray *)changeJobsStatus:(NSMutableArray *)arr{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
    
    LocationObject *location = [LocationObject sharedInstance];
    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:[location.latitude floatValue] longitude:[location.longitude floatValue]];
    int updateStatus;
    
    for (int i = 0; i < arr.count; i ++) {
        shipmentObject *sh = [arr objectAtIndex:i];
        
        CLLocation *endLocation;
        
        if (sh.status_id < 11) {
            endLocation = [[CLLocation alloc] initWithLatitude:[sh.origin_lat floatValue] longitude:[sh.origin_lng floatValue]];
        }else{
            endLocation = [[CLLocation alloc] initWithLatitude:[sh.destination_lat floatValue] longitude:[sh.destination_lng floatValue]];
        }
        
        
        CLLocationDistance distance = [startLocation distanceFromLocation:endLocation];
        
        if (distance <= 3000 && sh.status_id == 7) {
            NSLog(@"change status to: 3 km from pickup");
            _SCFunc = [[ShipmentsControllerFunctions alloc] init];
            updateStatus = [_SCFunc updateJobStatusInLocation:sh.shipmentsId :sh.status_id];
            sh.status_id = updateStatus;
        }
        if (distance <= 100 && sh.status_id == 8) {
            NSLog(@"change status to: arrived to pickup location");
            _SCFunc = [[ShipmentsControllerFunctions alloc] init];
            updateStatus = [_SCFunc updateJobStatusInLocation:sh.shipmentsId :sh.status_id];
            sh.status_id = updateStatus;
        }
        
        if (distance <= 3000 && sh.status_id == 12) {
            NSLog(@"change status to 3 km from dropoff");
            _SCFunc = [[ShipmentsControllerFunctions alloc] init];
            updateStatus = [_SCFunc updateJobStatusInLocation:sh.shipmentsId :sh.status_id];
            sh.status_id = updateStatus;
        }
        if (distance <= 100 && sh.status_id == 13) {
            NSLog(@"change status to: arrived to dropoff location");
            _SCFunc = [[ShipmentsControllerFunctions alloc] init];
            updateStatus = [_SCFunc updateJobStatusInLocation:sh.shipmentsId :sh.status_id];
            sh.status_id = updateStatus;
        }
        
        [array addObject:sh];
        
    }
    }
    
    return array;
}
@end

//
//  Login.h
//  location
//
//  Created by Menachem Mizrachi on 23/06/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface Login : UIViewController<UITextFieldDelegate, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;

@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotButton;
@property (strong, nonatomic) IBOutlet UIButton *needSignButton;

- (IBAction)login:(id)sender;
- (IBAction)forgotPassword:(id)sender;
- (IBAction)needSignUp:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIImageView *splashScreen;

@property (strong, nonatomic) IBOutlet UIImageView *truckiesLogo;
@property (strong, nonatomic) IBOutlet UIImageView *truckiesLogo2;

@property (strong, nonatomic) IBOutlet UIImageView *checkBox;
- (IBAction)terms:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *checkboxView;
@property (strong, nonatomic) IBOutlet UIView *agreeView;
@property (strong, nonatomic) IBOutlet UIView *phoneView;

@end

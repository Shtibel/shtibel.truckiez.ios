//
//  CustomAlert.m
//  truckies
//
//  Created by Menachem Mizrachi on 30/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "CustomAlert.h"

@interface CustomAlert ()
{
    UIView *alert;
    UIView *backgroundView;
}

@end

@implementation CustomAlert

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(UIView*)loadTextAndPhone:(NSString*)str :(NSString *)phoneNumber :(CGRect)sizeView :(NSString *)actionString{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0,0,sizeView.size.width,sizeView.size.height)];
    
    UILabel *viewText;
    UIButton *phone;
    CGRect textsize;
    if (![str isEqualToString:@""]) {
        
        CGSize constraint = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize = [str boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        viewText = [[UILabel alloc] initWithFrame:CGRectMake(5,0,textsize.size.width,sizeView.size.height)];
        
        viewText.text = str;
        viewText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        viewText.text = str;
        [viewText setFont:[UIFont systemFontOfSize:15]];
        
        viewText.lineBreakMode = NSLineBreakByWordWrapping;
        viewText.numberOfLines = 0;
        
    }
    
    CGRect textsize2;
    if (![phoneNumber isEqualToString:@""]) {
        
        CGSize constraint = CGSizeMake(sizeView.size.width, 400);
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize2 = [phoneNumber boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        phone = [[UIButton alloc] initWithFrame:CGRectMake(textsize.size.width,0,textsize2.size.width + 20,sizeView.size.height)];
        [phone setTitle:phoneNumber forState:UIControlStateNormal];
        phone.titleLabel.font = [UIFont systemFontOfSize:15];
        [phone setTitleColor:[UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0] forState:UIControlStateNormal];

        NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:phoneNumber];
        [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
        UIColor* textColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
        [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
        [phone setAttributedTitle:commentString forState:UIControlStateNormal];
    }
    [phone addTarget:self
              action:NSSelectorFromString(actionString)
    forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [view addSubview:viewText];
    [view addSubview:phone];
    return view;
}

-(UIView *)alertView:(UIButton *)doSomething :(UIButton *)moreButton :(NSString *)alertTitle :(NSString *)alertMessage :(NSString *)alertDo :(NSString *)alertClose :(NSString *)alertMoreButton :(UIView *)loadView :(UIView *)viewController :(CGFloat)widthScreen{
    
    CGFloat width;
    if (widthScreen != 0) {
        width = widthScreen;
    }else{
        width = [UIScreen mainScreen].bounds.size.width - 60;
    }
    
    alert = [[UIView alloc]initWithFrame:CGRectMake(30,140,width,200)];
    alert.backgroundColor = [UIColor whiteColor];


    //top
    UIView *topAlert = [[UIView alloc]initWithFrame:CGRectMake(0,0,width,40)];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:topAlert.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    topAlert.layer.mask = maskLayer;
    
    topAlert.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    
    UILabel *topText = [[UILabel alloc] initWithFrame:CGRectMake(10,0,width - 20,40)];
    topText.textColor = [UIColor whiteColor];
    topText.text = alertTitle;
    [topText setFont:[UIFont boldSystemFontOfSize:16]];
    //middle
    float height = 0;
    float heightLoad = 0;
    NSString *alertText;
    CGRect textsize;
    textsize.size.height = 0;
    if (![alertMessage isEqualToString:@""]) {
        alertText = alertMessage;
        CGSize constraint = CGSizeMake(width - 30, 400);
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16.0] forKey:NSFontAttributeName];
        textsize = [alertText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        height = textsize.size.height;
        heightLoad += textsize.size.height + 30;
    }
    
    
    
    if (loadView != nil) {
        height += loadView.bounds.size.height;
    }
    
    UIView *middleAlert = [[UIView alloc]initWithFrame:CGRectMake(0,40,width,height + 30)];
    middleAlert.backgroundColor = [UIColor whiteColor];
    
    UILabel *middleText;
    if (![alertMessage isEqualToString:@""]) {
        middleText = [[UILabel alloc] initWithFrame:CGRectMake(15,15,width - 30,textsize.size.height + 5)];
        middleText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        middleText.text = alertText;
        [middleText setFont:[UIFont systemFontOfSize:16]];
        middleText.lineBreakMode = NSLineBreakByWordWrapping;
        middleText.numberOfLines = 0;
    }
    
    //middle
    CGRect btFrame;
    if (doSomething != nil) {
        
        
        btFrame = doSomething.frame;
        btFrame.origin.y = 80 + height;
        doSomething.frame = btFrame;
        doSomething.backgroundColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
        [doSomething setTitle:alertDo forState:UIControlStateNormal];
        
        [doSomething.layer setCornerRadius:5.0f];
        doSomething.titleLabel.font = [UIFont systemFontOfSize:14];
        
    }
    if (moreButton != nil) {
        btFrame = moreButton.frame;
        btFrame.origin.y = 80 + height;
        moreButton.frame = btFrame;
        moreButton.backgroundColor = [UIColor colorWithRed:0.12 green:0.60 blue:0.23 alpha:1.0];
        [moreButton setTitle:alertMoreButton forState:UIControlStateNormal];
        
        [moreButton.layer setCornerRadius:5.0f];
        moreButton.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    
    //close button
    UIButton *closeview = [[UIButton alloc]initWithFrame:CGRectMake((width - 70)/2,0,70,35)];
    if (doSomething != nil) {
        closeview = [[UIButton alloc]initWithFrame:CGRectMake(width/2 + 5,0,70,35)];
    }
    if (moreButton != nil) {
        closeview = [[UIButton alloc]initWithFrame:CGRectMake(width/2 + 40,0,70,35)];
    }
    [closeview addTarget:self
                  action:@selector(closeAlert)
        forControlEvents:UIControlEventTouchUpInside];
    
    btFrame = closeview.frame;
    btFrame.origin.y = 80 + height;
    closeview.frame = btFrame;
    closeview.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    [closeview setTitle:alertClose forState:UIControlStateNormal];
    [closeview.layer setCornerRadius:5.0f];
    closeview.titleLabel.font = [UIFont systemFontOfSize:14];
    
    
    [topAlert addSubview:topText];
    [alert addSubview:topAlert];
    
    if (![alertMessage isEqualToString:@""]) {
        [middleAlert addSubview:middleText];
    }
    
    if (loadView != nil) {
        btFrame = loadView.frame;
        btFrame.origin.y = heightLoad;
        loadView.frame = btFrame;
        
        [middleAlert addSubview:loadView];
    }
    [alert addSubview:middleAlert];
    
    if (alertClose != nil) {
        [alert addSubview:closeview];
    }
    if (doSomething != nil) {
        [alert addSubview:doSomething];
    }
    if (moreButton != nil) {
        [alert addSubview:moreButton];
    }
    
    CGFloat heightScreen = [UIScreen mainScreen].bounds.size.height;
    CGRect aFrame = alert.frame;
    aFrame.size.height = 130 + height;
    aFrame.origin.y = (heightScreen - height -130)/2;
    alert.frame = aFrame;
    alert.layer.cornerRadius = 5.0f;
    
    backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    backgroundView.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.5];
    [viewController addSubview:backgroundView];
    backgroundView.layer.zPosition = 10;
    [backgroundView addSubview:alert];
    
    return backgroundView;
}


-(void)closeAlert{
    [alert removeFromSuperview];
    [backgroundView removeFromSuperview];
}

-(UIView *)alertview2:(UIButton *)doSomething :(NSString*)alertTitle :(NSString*)alertMessage :(NSString*)buttonTitle :(UIView *)viewController{
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 60;
    
    alert = [[UIView alloc]initWithFrame:CGRectMake(30,140,width,200)];
    alert.backgroundColor = [UIColor whiteColor];
    
    //top
    UIView *topAlert = [[UIView alloc]initWithFrame:CGRectMake(0,0,width,40)];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:topAlert.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    topAlert.layer.mask = maskLayer;
    
    topAlert.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    
    UILabel *topText = [[UILabel alloc] initWithFrame:CGRectMake(10,0,width - 20,40)];
    topText.textColor = [UIColor whiteColor];
    topText.text = alertTitle;
    [topText setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    float height = 0;
    float heightLoad = 0;
    NSString *alertText;
    CGRect textsize;
    textsize.size.height = 0;
    if (![alertMessage isEqualToString:@""]) {
        alertText = alertMessage;
        CGSize constraint = CGSizeMake(width - 30, 400);
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:16.0] forKey:NSFontAttributeName];
        textsize = [alertText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        height = textsize.size.height;
        heightLoad += textsize.size.height + 30;
    }
    
    UIView *middleAlert = [[UIView alloc]initWithFrame:CGRectMake(0,40,width,height + 30)];
    middleAlert.backgroundColor = [UIColor whiteColor];
    
    UILabel *middleText;
    if (![alertMessage isEqualToString:@""]) {
        middleText = [[UILabel alloc] initWithFrame:CGRectMake(15,15,width - 30,textsize.size.height + 5)];
        middleText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        middleText.text = alertText;
        [middleText setFont:[UIFont systemFontOfSize:16]];
        middleText.lineBreakMode = NSLineBreakByWordWrapping;
        middleText.numberOfLines = 0;
    }
    
    //close button
    CGRect btFrame;
    btFrame = doSomething.frame;
    btFrame.origin.y = 80 + height;
    doSomething.frame = btFrame;
    doSomething.backgroundColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
    [doSomething setTitle:buttonTitle forState:UIControlStateNormal];
    [doSomething.layer setCornerRadius:5.0f];
    doSomething.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [topAlert addSubview:topText];
    [alert addSubview:topAlert];
    if (![alertMessage isEqualToString:@""]) {
        [middleAlert addSubview:middleText];
    }
    [alert addSubview:middleAlert];
    [alert addSubview:doSomething];
    
    CGFloat heightScreen = [UIScreen mainScreen].bounds.size.height;
    CGRect aFrame = alert.frame;
    aFrame.size.height = 130 + height;
    aFrame.origin.y = (heightScreen - height -130)/2;
    alert.frame = aFrame;
    alert.layer.cornerRadius = 5.0f;
    
    backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    backgroundView.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.5];
    [viewController addSubview:backgroundView];
    
    [backgroundView addSubview:alert];
    
    return backgroundView;
    
}

@end

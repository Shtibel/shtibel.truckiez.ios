//
//  PickupConfirmation.h
//  truckies
//
//  Created by Menachem Mizrachi on 27/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shipmentObject.h"
#import <CoreText/CoreText.h>
#import <CoreGraphics/CoreGraphics.h>

@interface PickupConfirmation : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *contentScroll;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIView *topScroll;
@property (strong, nonatomic) IBOutlet UIView *selectView;
@property (strong, nonatomic) IBOutlet UIView *weightView;
@property (strong, nonatomic) IBOutlet UITextField *weightText;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@property (strong, nonatomic) IBOutlet UIButton *addButton2;
@property (strong, nonatomic) IBOutlet UITextField *senderName;
@property (strong, nonatomic) IBOutlet UITextField *senderNumber;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;
@property (strong, nonatomic) IBOutlet UILabel *signatureLabel;
@property (strong, nonatomic) IBOutlet UIView *drawImageView;
@property (strong, nonatomic) IBOutlet UIImageView *drawImage;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *driverConfirmationButton;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIView *uneditableView;
@property (strong, nonatomic) IBOutlet UITextField *driverName;
@property (strong, nonatomic) IBOutlet UITextField *confirmationDate;
@property (strong, nonatomic) IBOutlet UIButton *closeUneditable;
@property (strong, nonatomic) IBOutlet UIView *confirmationTop;
@property (strong, nonatomic) IBOutlet UIView *confirmationTextfieldView;


@property shipmentObject *job;
@property int con;


- (IBAction)back:(id)sender;
- (IBAction)close:(id)sender;
- (IBAction)driverConfirmation:(id)sender;
- (IBAction)addType:(id)sender;
- (IBAction)clear:(id)sender;

@end

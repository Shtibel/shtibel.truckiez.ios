//
//  Desktop.h
//  truckies
//
//  Created by Menachem Mizrachi on 02/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
@import GooglePlaces;


@interface Desktop : UIViewController<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) IBOutlet UIView *jobsView;
@property (strong, nonatomic) IBOutlet UIView *offersView;


@property (strong, nonatomic) IBOutlet UIView *swipeNavigation;
@property (strong, nonatomic) IBOutlet UIView *jobsNavView;
@property (strong, nonatomic) IBOutlet UILabel *jobsNavTitle;
@property (strong, nonatomic) IBOutlet UIView *jobsNavMarker;

@property (strong, nonatomic) IBOutlet UIView *offersNavView;
@property (strong, nonatomic) IBOutlet UILabel *offersNavTitle;
@property (strong, nonatomic) IBOutlet UIView *offersNavMarker;

@property (strong, nonatomic) IBOutlet UIView *jobsNavigation;
@property (strong, nonatomic) IBOutlet UIView *jobsToday;
@property (strong, nonatomic) IBOutlet UILabel *jobsTodayTitle;


@property (strong, nonatomic) IBOutlet UIView *jobsSchedualed;
@property (strong, nonatomic) IBOutlet UILabel *jobsSchedualedTitle;


@property (strong, nonatomic) IBOutlet UIView *jobsComplete;
@property (strong, nonatomic) IBOutlet UILabel *jobsCompleteTitle;


@property (strong, nonatomic) IBOutlet UIView *jobsSearch;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;

- (IBAction)available:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButtonNav;

@property (strong, nonatomic) IBOutlet UIView *jobsContentView;

//offers
@property (strong, nonatomic) IBOutlet UIView *offersNavigation;
@property (strong, nonatomic) IBOutlet UIView *offersToday;
@property (strong, nonatomic) IBOutlet UILabel *offersTodayTitle;


@property (strong, nonatomic) IBOutlet UIView *offersSchedualed;
@property (strong, nonatomic) IBOutlet UILabel *offersSchedualedTitle;

@property (strong, nonatomic) IBOutlet UIView *offersSearch;
@property (strong, nonatomic) IBOutlet UIView *offersContentView;
@property (strong, nonatomic) IBOutlet UITableView *offerTable;
@property (strong, nonatomic) IBOutlet UILabel *noJobsLabel;
@property (strong, nonatomic) IBOutlet UILabel *noOfferLabel;
@property (strong, nonatomic) IBOutlet UIImageView *offerSearchImage;
@property (strong, nonatomic) IBOutlet UIImageView *jobSearchImage;
@property (strong, nonatomic) IBOutlet UIScrollView *offerScroll;
@property (strong, nonatomic) IBOutlet UIScrollView *jobScroll;

-(void)getSelectTab:(int)type :(int)tab;

@property (strong, nonatomic) IBOutlet UIView *badgeView;
@property (strong, nonatomic) IBOutlet UILabel *badgeLabel;
//search
//offer
@property (strong, nonatomic) IBOutlet UIView *searchViewOffer;
- (IBAction)closeSearchOffer:(id)sender;
- (IBAction)offerSearchAdvance:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *searchOfferAdvance;
- (IBAction)closeSearchAdvance:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *searchLocationOffer;
- (IBAction)searchAdvance:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *item;
@property (strong, nonatomic) IBOutlet UIView *locationView;
@property (strong, nonatomic) IBOutlet UIView *topSearchAdvance;
@property (strong, nonatomic) IBOutlet UIButton *searchButtonSearchAdvance;
@property (strong, nonatomic) IBOutlet UIView *offerMoreReqView;

@property (strong, nonatomic) IBOutlet UIView *offerSearchBoxView;
@property (strong, nonatomic) IBOutlet UIImageView *offerSearchBox;


@property (strong, nonatomic) IBOutlet UIView *offerSearchPalletView;
@property (strong, nonatomic) IBOutlet UIImageView *offerSearchPallet;

@property (strong, nonatomic) IBOutlet UIImageView *offerSearchTruckload;

@property (strong, nonatomic) IBOutlet UIView *offerSearchTruckloadView;
@property (strong, nonatomic) IBOutlet UIView *offerDatePickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *offerDatePickerFrom;
@property (strong, nonatomic) IBOutlet UIDatePicker *offerDatePickerTo;


- (IBAction)chooseOfferPicker:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *offerSearchFrom;
- (IBAction)offerSearchFromClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *offerSearchTo;
- (IBAction)offerSearchToClick:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *keywordAdvanceOffer;
@property (strong, nonatomic) IBOutlet UITextField *keywordOffer;
@property (strong, nonatomic) IBOutlet UIView *offerSearchClickView;
@property (strong, nonatomic) IBOutlet UIButton *offerSearchDateButton;
@property (strong, nonatomic) IBOutlet UIView *clearLocationOffer;

//job search
@property (strong, nonatomic) IBOutlet UIView *searchViewJob;
- (IBAction)closeSearchJob:(id)sender;
- (IBAction)jobSearchAdvance:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *keywordJob;
@property (strong, nonatomic) IBOutlet UIView *jobSearchClickView;
@property (strong, nonatomic) IBOutlet UIView *searchJobAdvance;
- (IBAction)closeSearchJobAdvance:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *topSearchJobAdvance;
@property (strong, nonatomic) IBOutlet UIButton *searchButtonJobAdvance;
- (IBAction)searchjobAdvance:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *jobSearchBoxView;
@property (strong, nonatomic) IBOutlet UIImageView *jobSearchBox;
@property (strong, nonatomic) IBOutlet UIView *jobSearchPalletView;
@property (strong, nonatomic) IBOutlet UIImageView *jobSearchpallet;
@property (strong, nonatomic) IBOutlet UIView *jobSearchTruckloadView;
@property (strong, nonatomic) IBOutlet UIImageView *jobSearchTruclkoad;
@property (strong, nonatomic) IBOutlet UIButton *jobSearchFrom;
@property (strong, nonatomic) IBOutlet UIButton *jobSearchTo;
@property (strong, nonatomic) IBOutlet UITextField *keywordAdvanceJob;
@property (strong, nonatomic) IBOutlet UITextField *searchLocationJob;
@property (strong, nonatomic) IBOutlet UIView *jobDatePickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *jobDatePickerFrom;
@property (strong, nonatomic) IBOutlet UIDatePicker *jobDatePickerTo;
- (IBAction)chooseJobPicker:(id)sender;
- (IBAction)jobSearchFromClick:(id)sender;
- (IBAction)jobSearchToClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *jobSearchDateButton;
@property (strong, nonatomic) IBOutlet UIView *clearLocation;



//google places

@property GMSAutocompleteResultsViewController *result;


@property (strong, nonatomic) IBOutlet UIView *offerNumView;
@property (strong, nonatomic) IBOutlet UILabel *offerNumLabel;

- (IBAction)supportCall:(id)sender;

//menu
@property NSTimer *checkOffersTimer;
@property UIImage *imageDriver;

@property (strong, nonatomic) IBOutlet UIButton *badgeButton;


@end

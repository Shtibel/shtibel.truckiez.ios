//
//  InternetConnection.h
//  NirApp
//
//  Created by Dan Dushinsky on 17/12/15.
//  Copyright (c) 2015 Dan Dushinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InternetConnection : UIViewController

- (BOOL)connected;
-(void)alertConnected:(UIView *)viewController;
@end

//
//  PushNotification.h
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MGSwipeTableCell;

@interface PushNotification : UIViewController<UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate>


@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *dropDownView;
@property (strong, nonatomic) IBOutlet UILabel *noPushLabel;

- (IBAction)mute:(id)sender;
- (IBAction)clearAll:(id)sender;
- (IBAction)openMenu:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)callSupport:(id)sender;

@end

//
//  StatusArray.h
//  truckies
//
//  Created by Menachem Mizrachi on 13/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatusArray : NSObject

@property NSMutableArray *statusArr;


+ (StatusArray *)sharedInstance;

@end

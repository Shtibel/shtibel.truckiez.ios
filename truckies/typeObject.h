//
//  typeObject.h
//  truckies
//
//  Created by Menachem Mizrachi on 28/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface typeObject : NSObject
@property int typeId;
@property NSString *load_type_name;
@property NSString *create_date;
@property int create_user;
@property NSString *update_date;
@property int update_user;
@property int recycle;
@property int default_load;
@property int quantity;
@end

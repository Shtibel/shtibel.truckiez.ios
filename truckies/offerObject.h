//
//  offerObject.h
//  truckies
//
//  Created by Menachem Mizrachi on 05/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface offerObject : NSObject

@property NSInteger offerId;
@property NSString *status_name;
@property NSString *origin_lat;
@property NSString *origin_lng;
@property NSString *destination_lat;
@property NSString *destination_lng;
@property NSString *shipment_carrier_payout_text;
@property NSInteger box_num;
@property NSInteger pallet_num;
@property NSInteger truckload_num;
@property NSString *shipment_special_request;
@property NSString *pickup_date;
@property NSString *dropoff_date;
@property NSString *pickup_from_time;
@property NSString *pickup_till_time;
@property NSString *dropoff_from_time;
@property NSString *dropoff_till_time;
@property NSString *total_load_weight;
@property NSInteger total_driving_distance;
@property NSString *pickup_in_time;
@property NSString *origin_address;
@property NSString *origin_address_name;
@property NSString *destination_address;
@property NSString *destination_address_name;
@property NSString *original_pickup_date;
@property NSString *comments;
@property NSString *shipment_load;
@property NSString *shipment_google_root;
@property NSString *truck_type_name;
@property NSString *load_weight_type;
@property NSString *origin_special_site_instructions;
@property NSString *destination_special_site_instructions;
@end

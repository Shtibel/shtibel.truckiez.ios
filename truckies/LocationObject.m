//
//  LocationObject.m
//  truckies
//
//  Created by Menachem Mizrachi on 13/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "LocationObject.h"

@implementation LocationObject

+ (LocationObject *)sharedInstance
{
    // the instance of this class is stored here
    static LocationObject *myInstance = nil;
    
    // check to see if an instance already exists
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
        // initialize variables here
    }
    // return the instance of this class
    return myInstance;
}



@end

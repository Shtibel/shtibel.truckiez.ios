//
//  SpecialLoad.m
//  truckies
//
//  Created by Menachem Mizrachi on 18/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "SpecialLoad.h"
#import "StaticVars.h"

@interface SpecialLoad ()

@end

@implementation SpecialLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadPage{
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[StaticVars url],_imgName]]];
    
    _specialLoadImage.image = [UIImage imageWithData: imageData];
    _specialLoadImage.contentMode = UIViewContentModeScaleAspectFit;
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
@end

//
//  PickupConfirmationFunc.m
//  truckies
//
//  Created by Menachem Mizrachi on 28/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "PickupConfirmationFunc.h"


@implementation PickupConfirmationFunc
{
    JSON *_json;
}

-(NSMutableArray *)loadTypeFromServer{
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:@"tr-api/?action=getLoadTypes"];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    NSMutableArray *typesArr = [[NSMutableArray alloc] init];
    if (dic != (id)[NSNull null]) {
        
        for (NSDictionary *key in dic) {
            typeObject *type = [[typeObject alloc] init];
            
            if ([key objectForKey:@"id"]!= (id)[NSNull null]) {
                type.typeId = [[key objectForKey:@"id"] intValue];
            }
            if ([key objectForKey:@"load_type_name"]!= (id)[NSNull null]) {
                type.load_type_name = [key objectForKey:@"load_type_name"];
            }
            if ([key objectForKey:@"create_date"]!= (id)[NSNull null]) {
                type.create_date = [key objectForKey:@"create_date"];
            }
            if ([key objectForKey:@"create_user"]!= (id)[NSNull null]) {
                type.create_user = [[key objectForKey:@"create_user"] intValue];
            }
            if ([key objectForKey:@"update_date"]!= (id)[NSNull null]) {
                type.update_date = [key objectForKey:@"update_date"];
            }
            if ([key objectForKey:@"update_user"]!= (id)[NSNull null]) {
                type.update_user = [[key objectForKey:@"update_user"] intValue];
            }
            if ([key objectForKey:@"recycle"]!= (id)[NSNull null]) {
                type.recycle = [[key objectForKey:@"recycle"] intValue];
            }
            if ([key objectForKey:@"default_load"]!= (id)[NSNull null]) {
                type.default_load = [[key objectForKey:@"default_load"] intValue];
            }
            type.quantity = 0;
            [typesArr addObject:type];
         }
    }
    
    return typesArr;
}

-(NSString*)arrString:(NSMutableArray *)typeArr :(NSMutableArray *)quantityArr :(NSMutableArray *)typeArrFromServer{

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i =0; i < typeArrFromServer.count; i++) {
        
        typeObject *t1 = [[typeObject alloc] init];
        t1 = [typeArrFromServer objectAtIndex:i];
        
        for (int j = 0; j < typeArr.count; j++) {
            typeObject *t2 = [[typeObject alloc] init];
            t2 = [typeArr objectAtIndex:j];
            if (t1.typeId == t2.typeId) {
                int a = [[NSString stringWithFormat:@"%@",[quantityArr objectAtIndex:j]] intValue];
                t1.quantity += a;
            }
        }
        if (t1.quantity > 0) {
            [arr addObject:t1];
        }
    }
    
    NSString *str = @"";
    for (int i = 0; i < arr.count; i++) {
        typeObject *t = [[typeObject alloc] init];
        t = [arr objectAtIndex:i];
        str = [NSString stringWithFormat:@"%@&quantity_%d=%d",str,t.typeId,t.quantity];
    }
    return str;
}



-(BOOL)checkQuantity:(NSMutableArray *)typeArr :(NSMutableArray *)quantityArr{
    BOOL check = YES;
    for (int i = 0; i < typeArr.count; i++) {
        if ([[quantityArr objectAtIndex:i] isEqualToString:@"0"]) {
            check = NO;
        }
    }
    return check;
}

@end

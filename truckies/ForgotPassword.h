//
//  ForgotPassword.h
//  truckies
//
//  Created by Menachem Mizrachi on 25/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *email;

@property (strong, nonatomic) IBOutlet UIButton *backLoginButton;
@property (strong, nonatomic) IBOutlet UIButton *needSignButton;

- (IBAction)backLogin:(id)sender;
- (IBAction)needSignUp:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *splashScreen;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
- (IBAction)sendEmail:(id)sender;

@property NSString *imgName;
@property (strong, nonatomic) IBOutlet UIImageView *truckiesLogo;
@property (strong, nonatomic) IBOutlet UILabel *explainText;

@property NSString *phone;

@end

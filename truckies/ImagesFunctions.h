//
//  ImagesFunctions.h
//  truckies
//
//  Created by Menachem Mizrachi on 24/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagesFunctions : UIViewController

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees;
@end

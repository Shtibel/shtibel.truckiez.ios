//
//  StaticVars.h
//  truckiez
//
//  Created by Menachem Mizrachi on 22/03/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaticVars : NSObject
+ (NSString*)url;
@end

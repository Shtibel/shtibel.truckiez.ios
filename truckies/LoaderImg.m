//
//  LoaderImg.m
//  truckies
//
//  Created by Menachem Mizrachi on 21/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "LoaderImg.h"
#import "UIImage+animatedGIF.h"

@interface LoaderImg ()

@end

@implementation LoaderImg

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UIView *)loader:(UIView *)backgroundView{
    
    NSURL *urlGif = [[NSBundle mainBundle] URLForResource:@"loader" withExtension:@"gif"];
    
    backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    backgroundView.backgroundColor = [UIColor clearColor];
    //backgroundView.backgroundColor = [UIColor colorWithRed:0.00 green:0.00 blue:0.00 alpha:0.5];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width - 40)/2,([UIScreen mainScreen].bounds.size.height - 40)/2,40,40)];
    img.image = [UIImage animatedImageWithAnimatedGIFURL:urlGif];
    [backgroundView addSubview:img];
    
    return backgroundView;
}

@end

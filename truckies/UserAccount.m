//
//  UserAccount.m
//  truckies
//
//  Created by Menachem Mizrachi on 21/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//
#import <CoreText/CoreText.h>

#import "UserAccount.h"
#import "JSON.h"
#import "AFNetworking.h"
#import "CustomAlert.h"
#import "ImagesFunctions.h"
#import "InternetConnection.h"
#import "LoaderImg.h"
#import "StaticVars.h"

@interface UserAccount ()
{
    JSON *_json;
    CustomAlert *_alert;
    ImagesFunctions *_imgFunc;
    InternetConnection *_InternetConnection;
    LoaderImg *_loaderIMG;
    
    NSMutableArray *truckTypes;
    
    int truck_id;
    NSString *truck_name;
    int number_of_voters;
    float avg_rating;
    
    UIView *alert;
    CGFloat widthAlert;
}

@end

@implementation UserAccount

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contentView.hidden = YES;
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self loadContent];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [backgroundView removeFromSuperview];
                
                [self loadPage];
                
                
                _contentView.hidden = NO;
            });
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - TPFloatRatingViewDelegate

- (void)floatRatingView:(TPFloatRatingView *)ratingView ratingDidChange:(CGFloat)rating
{
    //self.ratingLabel.text = [NSString stringWithFormat:@"%.2f", rating];
}

- (void)floatRatingView:(TPFloatRatingView *)ratingView continuousRating:(CGFloat)rating
{
    // self.liveLabel.text = [NSString stringWithFormat:@"%.2f", rating];
}

-(void)loadPage2{
    
}

-(void)loadContent{
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:@"tr-api/?action=truckTypes"];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    truckTypes = [[NSMutableArray alloc] init];
    truck_name = @"truck type";
    for (NSDictionary *key in dic) {
        
        if ([[[NSUserDefaults standardUserDefaults]
              stringForKey:@"truck_type_id"] intValue] == [[key objectForKey:@"id"] intValue]) {
            truck_name = [key objectForKey:@"name"];
            truck_id = [[key objectForKey:@"id"] intValue];
            
        }
        NSArray *arr = [[NSArray alloc] init];
        arr = @[[key objectForKey:@"id"],[key objectForKey:@"name"]];
        [truckTypes addObject:arr];
    }
    
    //rating
    avg_rating = [[[NSUserDefaults standardUserDefaults]
                   stringForKey:@"avg_rating"] floatValue];
}

-(void)loadPage{
    
    [_truckType setTitle:[NSString stringWithFormat:@" %@",truck_name] forState:UIControlStateNormal];
    
    if([[[NSUserDefaults standardUserDefaults]
         stringForKey:@"driverStatus"] intValue] == 4){
        _avilable.text = @"Unvailable";
        _statusCircle.backgroundColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
    }else{
        _avilable.text = @"Available";
        _statusCircle.backgroundColor = [UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0];
    }
    
    [_statusCircle.layer setCornerRadius:6.0f];
    [_userImage.layer setCornerRadius:30.0f];
    
    UIBezierPath *maskPath;
    CAShapeLayer *maskLayer;
    CGFloat borderWidth;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_topUpdateImage.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _topUpdateImage.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_updateImageView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    _updateImageView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _updateImageView.frame = CGRectInset(_updateImageView.frame, -borderWidth, -borderWidth);
    _updateImageView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _updateImageView.layer.borderWidth = borderWidth;
    
    //
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_topUpdateInfo.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _topUpdateInfo.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_updateInfoview.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    _updateInfoview.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _updateInfoview.frame = CGRectInset(_updateInfoview.frame, -borderWidth, -borderWidth);
    _updateInfoview.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _updateInfoview.layer.borderWidth = borderWidth;
    
    _phoneView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _phoneView.layer.borderWidth = borderWidth;
    [_phoneView.layer setCornerRadius:5.0f];
    
    _emailView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _emailView.layer.borderWidth = borderWidth;
    [_emailView.layer setCornerRadius:5.0f];
    
    _truckTypeView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _truckTypeView.layer.borderWidth = borderWidth;
    [_truckTypeView.layer setCornerRadius:5.0f];
    //
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_topUpdatePassword.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _topUpdatePassword.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_updatePasswordView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    _updatePasswordView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _updatePasswordView.frame = CGRectInset(_updatePasswordView.frame, -borderWidth, -borderWidth);
    _updatePasswordView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _updatePasswordView.layer.borderWidth = borderWidth;
    
    
    _passwordView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _passwordView.layer.borderWidth = borderWidth;
    [_passwordView.layer setCornerRadius:5.0f];
    
    _passwordNewView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _passwordNewView.layer.borderWidth = borderWidth;
    [_passwordNewView.layer setCornerRadius:5.0f];
    
    _confirmPasswordView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _confirmPasswordView.layer.borderWidth = borderWidth;
    [_confirmPasswordView.layer setCornerRadius:5.0f];
    //load Text field
    _firstName.text = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"user_first_name"];
    _lastName.text = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"user_last_name"];
    _email.text = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"user_email"];
    _phone.text = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"user_phone"];
    _fullName.text = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"userName"];
    
    NSString *str = [NSString stringWithFormat:@"%@%@",[StaticVars url],[[NSUserDefaults standardUserDefaults]
                                                                                 stringForKey:@"user_image"]];
    NSURL *urlForImageShipments = [NSURL URLWithString:str];
    NSData *dataImageShipments = [NSData dataWithContentsOfURL:urlForImageShipments];
    
    UIImage *imageShipments = [UIImage imageWithData:dataImageShipments];
    _userImage.image = imageShipments;
    _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
    _userImage.clipsToBounds = YES;
    _userImage.contentMode = UIViewContentModeScaleAspectFill;
    _userImage.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _userImage.layer.borderWidth = borderWidth;
    
    [_changePicture.layer setCornerRadius:5.0f];
    [_saveInfoButton.layer setCornerRadius:5.0f];
    [_savePasswordView.layer setCornerRadius:5.0f];
    
    //rating
    
    number_of_voters = [[[NSUserDefaults standardUserDefaults]
                         stringForKey:@"number_of_voters"] floatValue];
    _ratingLabel.text = [NSString stringWithFormat:@"%d rating", number_of_voters];
    self.ratingView.delegate = self;
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"star_empty.png"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 1;
    self.ratingView.rating = avg_rating;
    self.ratingView.floatRatings = YES;
    
    
    
    widthAlert = [UIScreen mainScreen].bounds.size.width - 60;
    
    //required fields
    _reqFirstName.hidden = YES;
    _reqLastName.hidden = YES;
    _reqPhone.hidden = YES;
    _reqEmail.hidden = YES;
    
    //textfield
    _firstName.tag = 1;
    _lastName.tag = 2;
    _phone.tag = 3;
    _email.tag = 4;
    
}

- (IBAction)back:(id)sender{
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    [self performSegueWithIdentifier:@"backToDesktop" sender:self];
    //    [self dismissViewControllerAnimated:YES completion:^{
    //        [self.navigationController popToRootViewControllerAnimated:YES];
    //    }];
}


-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 1) {
        
        if (buttonIndex == 0) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePickerController.editing = YES;
            imagePickerController.delegate = (id)self;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        if (buttonIndex == 1) {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.delegate = self;
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    else if (actionSheet.tag == 2){
        if (buttonIndex != truckTypes.count) {
            
            NSArray *arr = [[NSArray alloc] init];
            arr = [truckTypes objectAtIndex:buttonIndex];
            
            [_truckType setTitle:[NSString stringWithFormat:@" %@",arr[1]] forState:UIControlStateNormal];
            truck_id = [arr[0] intValue];
        }
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *originalImage = image;
    _userImage.image = originalImage;
    [picker dismissViewControllerAnimated:NO completion:nil];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        [self postImage];
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}



- (IBAction)truckType:(id)sender {
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Truck Types"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    for (int i = 0; i<truckTypes.count ; i ++) {
        NSArray *arr = [[NSArray alloc] init];
        arr = [truckTypes objectAtIndex:i];
        [actionSheet addButtonWithTitle:arr[1]];
    }
    [actionSheet addButtonWithTitle:@"Cancel"];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showInView:self.view];
    [actionSheet setTag:2];
}

- (IBAction)changePictureUser:(id)sender {
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
        
        [actionSheet showInView:self.view];
        [actionSheet setTag:1];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [self updateApp];
                                                         }];
        [alertController addAction:action];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                           [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                       }];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
}

- (IBAction)updateInfo:(id)sender {
    [self.view endEditing:YES];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        if ([_firstName.text isEqualToString:@""] || [_lastName.text isEqualToString:@""] || [_email.text isEqualToString:@""] || [_phone.text isEqualToString:@""]) {
            //data missing
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Missing data or incorrect information" :@"" :@"OK" :@"" :nil :self.view :0];
        }
        else{
            [self changeInfo];
            //            UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
            //            [doSomething addTarget:self
            //                            action:@selector(changeInfo)
            //                  forControlEvents:UIControlEventTouchUpInside];
            //
            //            alert = [[UIView alloc] init];
            //            _alert = [[CustomAlert alloc] init];
            //
            //            alert = [_alert alertView:doSomething :nil :@"Confirmation" :@"Are you sure you want to change info?" :@"Yes" :@"OK" :@"" :nil :self.view :0];
            
        }
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

- (IBAction)updatePassword:(id)sender {
    [self.view endEditing:YES];
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        if (![_password.text isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"password"]]) {
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Incorrect password, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
            
        }
        else if (![_passwordNew.text isEqualToString:_confirmPassword.text]){
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Passwords do not match, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
            
        }
        else{
            [self changePassword];
            //            UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
            //            [doSomething addTarget:self
            //                            action:@selector(changePassword)
            //                  forControlEvents:UIControlEventTouchUpInside];
            //
            //            alert = [[UIView alloc] init];
            //            _alert = [[CustomAlert alloc] init];
            //            alert = [_alert alertView:doSomething :nil :@"Change password" :@"Are you sure you want to change password?" :@"Yes" :@"Cancel" :@"" :nil :self.view :0];
        }
    }else{
        _alert = [[CustomAlert alloc] init];
        
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    switch (textField.tag) {
        case 1:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqFirstName.hidden = NO;
            }else{
                _reqFirstName.hidden = YES;
            }
        }
            break;
        case 2:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqLastName.hidden = NO;
            }else{
                _reqLastName.hidden = YES;
            }
        }
            break;
        case 3:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqPhone.hidden = NO;
            }else{
                _reqPhone.hidden = YES;
            }
        }
            break;
        case 4:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqEmail.hidden = NO;
            }else{
                _reqEmail.hidden = YES;
            }
        }
            break;
    }
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,- keyboardSize.height,width,height)];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    [self.view setFrame:CGRectMake(0,0,width,height)];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)saveInfoOnDefault{
    NSString *det1 = _firstName.text;
    [[NSUserDefaults standardUserDefaults] setObject:det1 forKey:@"user_first_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *det2 = _lastName.text;
    [[NSUserDefaults standardUserDefaults] setObject:det2 forKey:@"user_last_name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *det3 = _email.text;
    [[NSUserDefaults standardUserDefaults] setObject:det3 forKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *det4 = [NSString stringWithFormat:@"%d",truck_id];
    [[NSUserDefaults standardUserDefaults] setObject:det4 forKey:@"truck_type_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *det5 = _phone.text;
    [[NSUserDefaults standardUserDefaults] setObject:det5 forKey:@"user_phone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *det6 = [NSString stringWithFormat:@"%@ %@",_firstName.text, _lastName.text];
    [[NSUserDefaults standardUserDefaults] setObject:det6 forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
}

-(void)postImage{
    
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    UIImage *mainImageView = [[UIImage alloc] init];
    UIImage *test = [[UIImage alloc] init];
    test = _userImage.image;
    
    int rotate = _userImage.image.imageOrientation;
    
    if (rotate == 3 || rotate == 2) {
        _imgFunc = [[ImagesFunctions alloc] init];
        mainImageView = [_imgFunc imageRotatedByDegrees:test deg:90];
    }
    if (rotate == 2) {
        _imgFunc = [[ImagesFunctions alloc] init];
        mainImageView = [_imgFunc imageRotatedByDegrees:test deg:270];
    }
    else{
        mainImageView = test;
    }
    
    NSData *imageToUpload;
    imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.5);
    
    NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=uploadImageDriver&user_id=%@",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]];
    NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:encodedUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageToUpload name:@"file" fileName:@"userImg.jpg" mimeType:@"image/jpeg"];
    } error:nil];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      [backgroundView removeFromSuperview];
                      if (response) {
                          //save url on
                          NSDictionary *dic= [[NSDictionary alloc] init];
                          dic = responseObject;
                          
                          NSString *str = [dic objectForKey:@"url"];
                          [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"user_image"];
                          [[NSUserDefaults standardUserDefaults] synchronize];
                          
                          _alert = [[CustomAlert alloc] init];
                          [_alert alertView:nil :nil :@"Notice" :@"Your changes were saved successfully" :@"" :@"OK" :@"" :nil :self.view :0];
                      }else{
                          _alert = [[CustomAlert alloc] init];
                          [_alert alertView:nil :nil :@"Notice" :@"Your changes were not saved, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                      }
                      
                  }];
    
    [uploadTask resume];
    
    
}

-(void)changeInfo{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
        [alert removeFromSuperview];
        //update on server
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateUserAccount&user_id=%@&first_name=%@&last_name=%@&phone=%@&truck_type=%d&email=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], _firstName.text, _lastName.text, _phone.text ,0, _email.text]];
            
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [backgroundView removeFromSuperview];
                if (!dic) {
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"Your changes were not saved, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                }else if (![[dic objectForKey:@"errType"] isEqualToString:@""]) {
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"One of the parameters is not valid, please try again or contact support" :@"" :@"OK" :@"" :nil :self.view :0];
                }else{
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"Your changes were saved successfully" :@"" :@"OK" :@"" :nil :self.view :0];
                    
                    //save on defaults
                    [self saveInfoOnDefault];
                }
            });
            
        });
        
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [self updateApp];
                                                         }];
        [alertController addAction:action];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                           [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                       }];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)changePassword{
    [alert removeFromSuperview];
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateUserPassword&user_id=%@&password=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], _passwordNew.text]];
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [backgroundView removeFromSuperview];
            
            if (dic) {
                _alert = [[CustomAlert alloc] init];
                [_alert alertView:nil :nil :@"Notice" :@"Your changes were saved successfully" :@"" :@"OK" :@"" :nil :self.view :0];
                
                //save on defaults
                NSString *det1 = _passwordNew.text;
                [[NSUserDefaults standardUserDefaults] setObject:det1 forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }else{
                _alert = [[CustomAlert alloc] init];
                [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
            }
            
            
        });
        
    });
    
}

-(void)closeAlert{
    [alert removeFromSuperview];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    switch (textField.tag) {
        case 1:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqFirstName.hidden = NO;
            }else{
                _reqFirstName.hidden = YES;
            }
        }
            break;
        case 2:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqLastName.hidden = NO;
            }else{
                _reqLastName.hidden = YES;
            }
        }
            break;
        case 3:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqPhone.hidden = NO;
            }else{
                _reqPhone.hidden = YES;
            }
        }
            break;
        case 4:
        {
            if ([textField.text isEqualToString:@""]) {
                _reqEmail.hidden = NO;
            }else{
                _reqEmail.hidden = YES;
            }
        }
            break;
    }
}
- (IBAction)callSupport:(id)sender {
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        NSLog(@"cannnot call");
    }
    
}

-(void)updateApp{
    NSString *iTunesLink = @"https://itunes.apple.com/il/app/truckiez/id1191308814?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

@end

//
//  dbFunctions.m
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "dbFunctions.h"
#import "FMDB.h"
#import "PushObject.h"
#import "JSON.h"
#import "InternetConnection.h"

@interface dbFunctions ()
{
    FMDatabase *database;
    JSON *_json;
    InternetConnection *_InternetConnection;
}

@end

@implementation dbFunctions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)createTables{
    [self openDB];
    [database executeUpdate:@"create table pushTBL(pushId integer primary key, messageId integer, pushTitle text, pushContent text, messageType text, isRead integer, pushDate text, userID integer)"];
    [self closeDB];
    
}

-(void)test{
    [self openDB];
    
    NSString *deleteQueryQuoteProduct = @"DROP TABLE pushTBL;";
    [database executeUpdate:deleteQueryQuoteProduct];
    
    [database executeUpdate:@"create table pushTBL(pushId integer primary key, messageId integer, pushTitle text, pushContent text, messageType text, isRead integer, pushDate text, userID integer)"];
    [database close];
}

-(void)openDB{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [paths objectAtIndex:0];
    NSString *dbPath = [docPath stringByAppendingPathComponent:@"TruckiesDB.sqlite"];
    
    database = [FMDatabase databaseWithPath:dbPath];
    [database open];
}

-(void)closeDB{
    [database close];
}

-(int)checkIfPushSend:(NSInteger)messageId :(NSString *)messageType{
    [self openDB];
    int countPush = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE messageId = %ld and messageType = '%@' and userID = %ld",(long)messageId, messageType, [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    [self closeDB];
    return countPush;
}

-(void)insertPush:(NSInteger)messageId :(NSString *)messageTitle :(NSString *)messageText :(NSString *)messageType{
    [self openDB];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd/MM/ yy HH:mm:ss"];
    NSDate *date = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO pushTBL VALUES(null, '%ld', '%@', '%@', '%@', 0, '%@', %d)", (long)messageId, messageTitle, messageText, messageType, dateString, [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]];
    
    //raise the badge
    NSInteger countPushUnRead = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE isRead = 0 and userID = %d", [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = countPushUnRead;
    NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
    [badgeDefault setInteger:countPushUnRead forKey:@"badge"];
    
    [badgeDefault synchronize];
    
    [database executeUpdate:insertQuery];
    
    
    [self closeDB];
}

-(void)updateJobPush:(NSInteger)messageId :(NSString *)messageTitle :(NSString *)messageText :(NSString *)messageType{
    [self openDB];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd/MM/ yy HH:mm:ss"];
    NSDate *date = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:date];

    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE pushTBL SET pushTitle = '%@', pushContent = '%@', pushDate = '%@' WHERE messageId = '%ld' and messageType = '%@' and userID = %ld",messageTitle, messageText, dateString, (long)messageId, messageType, [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]];
    [database executeUpdate:updateQuery];
    
    [self closeDB];
}

-(void)updatePush:(NSInteger)messageId :(NSString *)messageType{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
    [self openDB];
    NSUInteger countPush = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE messageId = %ld and isRead = 1 and messageType = '%@' and userID = %ld",(long)messageId , messageType, [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    if (countPush == 0) {
        
        
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE pushTBL  SET isRead = 1 WHERE messageId = '%ld' and messageType = '%@' and userID = %ld",(long)messageId, messageType, [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]];
        [database executeUpdate:updateQuery];
        //send is read to server
        if ([messageType isEqualToString:@"message"]) {
            
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateMessageUser&user_id=%@&message_id=%ld", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (long)messageId]];
        }
        
        
        
    }
    //reduce the badge
    NSUInteger countPushUnRead = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE isRead = 0 and userID = %ld", [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = countPushUnRead;
    NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
    [badgeDefault setInteger:countPushUnRead forKey:@"badge"];
    
    [badgeDefault synchronize];
    
    [self closeDB];
    }
}

-(int)returnBadgeNum{
    [self openDB];
    int countPushUnRead = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE isRead = 0 and userID = %ld", [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    [self closeDB];
    return countPushUnRead;
}

-(NSMutableArray*)selectPush{
    [self openDB];
    NSString *sqlSelectQuery = [NSString stringWithFormat:@"SELECT * FROM pushTBL WHERE userID = %ld ORDER BY pushId DESC", [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]];
    FMResultSet *resultsWithNameLocation = [database executeQuery:sqlSelectQuery];
    NSMutableArray *pushArr = [[NSMutableArray alloc] init];
    while ([resultsWithNameLocation next]) {
        PushObject *push = [[PushObject alloc] init];
        
        push.pushID = [[NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"pushId"]]integerValue];
        push.messageID = [[NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"messageId"]]integerValue];
        push.pushTitle = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"pushTitle"]];
        push.pushText = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"pushContent"]];
        push.messageType = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"messageType"]];
        push.isRead = [[NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"isRead"]] intValue];
        push.pushDate = [NSString stringWithFormat:@"%@",[resultsWithNameLocation stringForColumn:@"pushDate"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/ yy HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:push.pushDate];
        
        push.pushDateFormat = date;
        
        [pushArr addObject:push];
        
    }
    [self closeDB];
    return pushArr;
}

-(void)deleteAllPush{
    [self openDB];
    
    NSString *deleteQueryQuoteProduct = @"DELETE * FROM pushTBL";
    [database executeUpdate:deleteQueryQuoteProduct];
    [database close];
}
-(void)deletePush:(NSInteger)pushId{
    [self openDB];
    
    
    NSString *deleteQueryQuoteProduct = [NSString stringWithFormat: @"DELETE FROM pushTBL WHERE pushId ='%ld'",(long)pushId];
    
    NSUInteger countPushUnRead = [database intForQuery:[NSString stringWithFormat: @"SELECT COUNT(*) FROM pushTBL WHERE isRead = 0 and userID = %d", [[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"] integerValue]]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = countPushUnRead;
    NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
    [badgeDefault setInteger:countPushUnRead forKey:@"badge"];
    
    [badgeDefault synchronize];
    
    [database executeUpdate:deleteQueryQuoteProduct];
    [database close];
}

-(BOOL)clearPushTable{
    [self openDB];
    BOOL success =  [database executeUpdate:@"DELETE FROM pushTBL"];
    [self closeDB];
    
    return success;
}

@end

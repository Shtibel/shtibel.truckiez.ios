//
//  ShipmentsControllerFunctions.h
//  location
//
//  Created by Menachem Mizrachi on 06/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@import GoogleMaps;

@interface ShipmentsControllerFunctions : UIViewController<GMSMapViewDelegate>

- (UIView *)openMap:(UIView*)map :(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng;
-(NSString *)addPolyLine:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng;

-(NSString *)nextStatus:(NSInteger)status;

-(int)updateJobStatus:(NSInteger)jobID :(NSInteger)statusJobID;
-(int)updateJobStatusInLocation:(NSInteger)jobID :(NSInteger)statusJobID;

-(NSString *)addPolyLineStaticMap:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng;
@end

//
//  PushNotificationFunctions.m
//  truckies
//
//  Created by Menachem Mizrachi on 23/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "PushNotificationFunctions.h"
#import "JSON.h"
#import "PushNotification.h"
#import "Jobs.h"
#import "shipmentObject.h"
#import "Offers.h"
#import "offerObject.h"
#import "Desktop.h"

@interface PushNotificationFunctions ()
{
    JSON *_json;
    shipmentObject *_job;
}

@end

@implementation PushNotificationFunctions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSDictionary*)returnOfferAnswer:(NSInteger)offerID{
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getSpecificOffer&user_id=%@&carrier_id=%@&offer_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"],(long)offerID]];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    return dic;
    
}

-(offerObject *)returnOffer:(NSDictionary *)key{
    
    offerObject *offer = [[offerObject alloc] init];
    
    
    offer.offerId = [[key objectForKey:@"id"] integerValue];
    
    if ([key objectForKey:@"origin_special_site_instructions"]!= (id)[NSNull null]) {
        offer.origin_special_site_instructions = [key objectForKey:@"origin_special_site_instructions"];
    }else{
        offer.origin_special_site_instructions = @"";
    }
    
    if ([key objectForKey:@"destination_special_site_instructions"]!= (id)[NSNull null]) {
        offer.destination_special_site_instructions = [key objectForKey:@"destination_special_site_instructions"];
    }else{
        offer.destination_special_site_instructions = @"";
    }
    
    if ([key objectForKey:@"load_weight_type"] != (id)[NSNull null]) {
        offer.load_weight_type = [key objectForKey:@"load_weight_type"];
    }else{
        offer.load_weight_type = @"";
    }
    
    if ([key objectForKey:@"status_name"] != (id)[NSNull null]) {
        offer.status_name = [key objectForKey:@"status_name"];
    }else{
        offer.status_name = @"";
    }
    
    if ([key objectForKey:@"origin_lat"] != (id)[NSNull null]) {
        offer.origin_lat = [key objectForKey:@"origin_lat"];
    }else{
        offer.origin_lat = @"";
    }
    
    if ([key objectForKey:@"origin_lng"] != (id)[NSNull null]) {
        offer.origin_lng = [key objectForKey:@"origin_lng"];
    }else{
        offer.origin_lng = @"";
    }
    
    if ([key objectForKey:@"destination_lat"] != (id)[NSNull null]) {
        offer.destination_lat = [key objectForKey:@"destination_lat"];
    }else{
        offer.destination_lat = @"";
    }
    
    if ([key objectForKey:@"destination_lng"] != (id)[NSNull null]) {
        offer.destination_lng = [key objectForKey:@"destination_lng"];
    }else{
        offer.destination_lng = @"";
    }
    
    if ([key objectForKey:@"shipment_carrier_payout_text"] != (id)[NSNull null]) {
        offer.shipment_carrier_payout_text = [key objectForKey:@"shipment_carrier_payout_text"];
    }else{
        offer.shipment_carrier_payout_text = @"";
    }
    
    if ([key objectForKey:@"box_num"] != (id)[NSNull null]) {
        offer.box_num = [[key objectForKey:@"box_num"] integerValue];
    }else{
        offer.box_num = 0;
    }
    
    if ([key objectForKey:@"pallet_num"] != (id)[NSNull null]) {
        offer.pallet_num = [[key objectForKey:@"pallet_num"] integerValue];
    }else{
        offer.pallet_num = 0;
    }
    
    if ([key objectForKey:@"truckload_num"] != (id)[NSNull null]) {
        offer.truckload_num = [[key objectForKey:@"truckload_num"] integerValue];
    }else{
        offer.truckload_num = 0;
    }
    
    if ([key objectForKey:@"shipment_special_request"]!= (id)[NSNull null]) {
        offer.shipment_special_request = [key objectForKey:@"shipment_special_request"];
    }else{
        offer.shipment_special_request = @"";
    }
    
    if ([key objectForKey:@"pickup_date"]!= (id)[NSNull null]) {
        offer.pickup_date = [key objectForKey:@"pickup_date"];
    }else{
        offer.pickup_date = @"";
    }
    
    if ([key objectForKey:@"dropoff_date"]!= (id)[NSNull null]) {
        offer.dropoff_date = [key objectForKey:@"dropoff_date"];
    }else{
        offer.dropoff_date = @"";
    }
    
    if ([key objectForKey:@"pickup_from_time"]!= (id)[NSNull null]) {
        offer.pickup_from_time = [key objectForKey:@"pickup_from_time"];
    }else{
        offer.pickup_from_time = @"";
    }
    
    if ([key objectForKey:@"pickup_till_time"]!= (id)[NSNull null]) {
        offer.pickup_till_time = [key objectForKey:@"pickup_till_time"];
    }else{
        offer.pickup_till_time = @"";
    }
    
    if ([key objectForKey:@"dropoff_from_time"]!= (id)[NSNull null]) {
        offer.dropoff_from_time = [key objectForKey:@"dropoff_from_time"];
    }else{
        offer.dropoff_from_time = @"";
    }
    
    if ([key objectForKey:@"dropoff_till_time"]!= (id)[NSNull null]) {
        offer.dropoff_till_time = [key objectForKey:@"dropoff_till_time"];
    }else{
        offer.dropoff_till_time = @"";
    }
    
    
    if ([key objectForKey:@"total_load_weight"]!= (id)[NSNull null]) {
        offer.total_load_weight = [key objectForKey:@"total_load_weight"];
    }else{
        offer.total_load_weight = @"";
    }
    
    if ([key objectForKey:@"total_driving_distance"]!= (id)[NSNull null]) {
        offer.total_driving_distance = [[key objectForKey:@"total_driving_distance"] integerValue];
    }else{
        offer.total_driving_distance = 0;
    }
    
    if ([key objectForKey:@"pickup_in_time"]!= (id)[NSNull null]) {
        offer.pickup_in_time = [key objectForKey:@"pickup_in_time"];
    }else{
        offer.pickup_in_time = @"";
    }
    
    if ([key objectForKey:@"origin_address"]!= (id)[NSNull null]) {
        offer.origin_address= [key objectForKey:@"origin_address"];
    }else{
        offer.origin_address = @"";
    }
    
    if ([key objectForKey:@"origin_address_name"]!= (id)[NSNull null]) {
        offer.origin_address_name = [key objectForKey:@"origin_address_name"];
    }else{
        offer.origin_address_name = @"";
    }
    
    if ([key objectForKey:@"destination_address"]!= (id)[NSNull null]) {
        offer.destination_address = [key objectForKey:@"destination_address"];
    }else{
        offer.destination_address = @"";
    }
    
    if ([key objectForKey:@"destination_address_name"]!= (id)[NSNull null]) {
        offer.destination_address_name = [key objectForKey:@"destination_address_name"];
    }else{
        offer.destination_address_name = @"";
    }
    
    if ([key objectForKey:@"original_pickup_date"]!= (id)[NSNull null]) {
        offer.original_pickup_date = [key objectForKey:@"original_pickup_date"];
    }else{
        offer.original_pickup_date = @"";
    }
    
    if ([key objectForKey:@"shipment_load"]!= (id)[NSNull null]) {
        offer.shipment_load = [key objectForKey:@"shipment_load"];
    }else{
        offer.shipment_load = @"";
    }
    
    if ([key objectForKey:@"comments"]!= (id)[NSNull null]) {
        offer.comments = [key objectForKey:@"comments"];
    }else{
        offer.comments = @"";
    }
    
    if ([key objectForKey:@"shipment_google_root"]!= (id)[NSNull null]) {
        offer.shipment_google_root = [key objectForKey:@"shipment_google_root"];
    }else{
        offer.shipment_google_root = @"";
    }
    
    if ([key objectForKey:@"truck_type_name"]!= (id)[NSNull null]) {
        offer.truck_type_name = [key objectForKey:@"truck_type_name"];
    }else{
        offer.truck_type_name = @"";
    }

    return offer;
}

-(NSDictionary*)returnJobAnswer:(NSInteger)jobID{
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getSpecificJob&user_id=%@&carrier_id=%@&job_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"],(long)jobID]];
    
    _json = [[JSON alloc] init];
    NSDictionary *dic = [[NSDictionary alloc] init];
    dic = [_json FromJson:myData];
    
    return dic;
    
}

-(shipmentObject *)returnJob:(NSDictionary *)key{
    
    shipmentObject *sh;
    _job = [[shipmentObject alloc] init];
    sh = [_job returnJob:key];
    return sh;
    
}


-(void)openPush:(NSString*)type :(NSInteger)messageID{
    if ([type isEqualToString:@"message"]) {
        //open desktop
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        Desktop *desktop = [storyboard instantiateViewControllerWithIdentifier:@"Desktop"];
        
        self.window.rootViewController = desktop;
        [self.window makeKeyAndVisible];
        //open notification controller
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PushNotification *PushNotification = [storyboard instantiateViewControllerWithIdentifier:@"PushNotification"];
        
        self.window.rootViewController = PushNotification;
        [self.window makeKeyAndVisible];
        
    }else if ([type isEqualToString:@"job"]){
        //open desktop
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        Desktop *desktop = [storyboard instantiateViewControllerWithIdentifier:@"Desktop"];
        
        self.window.rootViewController = desktop;
        [self.window makeKeyAndVisible];
        //get the specific job
        shipmentObject *job = [[shipmentObject alloc] init];
        NSDictionary *dic= [[NSDictionary alloc] init];
        dic = [self returnJobAnswer:messageID];
        if ([[dic objectForKey:@"err"] intValue] == 0 && [dic objectForKey:@"job"] != (id)[NSNull null]) {
            
            NSDictionary *jobDic = [[NSDictionary alloc] init];
            jobDic = [dic objectForKey:@"job"];
            job = [self returnJob:jobDic];
            
            //open the jobs controller
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            Jobs *Jobs = [storyboard instantiateViewControllerWithIdentifier:@"Jobs"];
            Jobs.job = job;
            
            self.window.rootViewController = Jobs;
            [self.window makeKeyAndVisible];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                            message:@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }else if ([type isEqualToString:@"offer"] ){
        //open desktop
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        Desktop *desktop = [storyboard instantiateViewControllerWithIdentifier:@"Desktop"];
        
        self.window.rootViewController = desktop;
        [self.window makeKeyAndVisible];
        //get the specific offer
        offerObject *offer = [[offerObject alloc] init];
        NSDictionary *dic= [[NSDictionary alloc] init];
        dic = [self returnOfferAnswer:messageID];
        if ([[dic objectForKey:@"err"] intValue] == 0 && [dic objectForKey:@"offer"] != (id)[NSNull null]) {
            
            NSDictionary *offerDic = [[NSDictionary alloc] init];
            offerDic = [dic objectForKey:@"offer"];
            offer = [self returnOffer:offerDic];
            
            //open the offers controller
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            Offers *Offer = [storyboard instantiateViewControllerWithIdentifier:@"Offers"];
            Offer.offer = offer;
            
            self.window.rootViewController = Offer;
            [self.window makeKeyAndVisible];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                            message:@"This offer is no longer assigned to you. If you need help with this offer please contact your carrier or Truckiez"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }

}

@end

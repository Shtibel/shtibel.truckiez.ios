//
//  ShipmentsControllerFunctions.m
//  location
//
//  Created by Menachem Mizrachi on 06/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "ShipmentsControllerFunctions.h"
#import "mapsControllerFunc.h"
#import "JSON.h"
#import "StatusArray.h"
@import CocoaLumberjack;
#import "InternetConnection.h"

@interface ShipmentsControllerFunctions ()
{
    JSON *_json;
    mapsControllerFunc *_mapFunc;
    StatusArray *_status;
    InternetConnection *_InternetConnection;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation ShipmentsControllerFunctions

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIView *)openMap:(UIView*)map :(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng{
    
    GMSCameraPosition *camera;
    GMSMapView *mapView_;
    
    camera = [GMSCameraPosition cameraWithLatitude:[originLat floatValue]
                                         longitude:[originLng floatValue]
                                              zoom:6];
    
    
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, map.frame.size.width, map.frame.size.height) camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = NO;
    mapView_.settings.myLocationButton = NO;
    mapView_.trafficEnabled = NO;
    mapView_.userInteractionEnabled = NO;
    
    
    //add markers
    CLLocationCoordinate2D position = { [originLat floatValue], [originLng floatValue]};
    GMSMarker *addMarker = [GMSMarker markerWithPosition:position];
    addMarker.title = @"origin";
    addMarker.appearAnimation = YES;
    addMarker.flat = YES;
    addMarker.snippet = @"";
    addMarker.map = mapView_;
    _mapFunc = [[mapsControllerFunc alloc] init];
    addMarker.icon = [_mapFunc image:[UIImage imageNamed:@"fromIcon"] scaledToSize:CGSizeMake(40.0f, 40.0f)];
    
    CLLocationCoordinate2D position2 = { [destLat floatValue], [destLng floatValue]};
    GMSMarker *addMarker2 = [GMSMarker markerWithPosition:position2];
    addMarker2.title = @"origin";
    addMarker2.appearAnimation = YES;
    addMarker2.flat = YES;
    addMarker2.snippet = @"";
    addMarker2.map = mapView_;
    _mapFunc = [[mapsControllerFunc alloc] init];
    addMarker2.icon = [_mapFunc image:[UIImage imageNamed:@"toIcon"] scaledToSize:CGSizeMake(40.0f, 40.0f)];
    
    //zoom to show all markers
    CLLocationCoordinate2D firstLocation = addMarker.position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
    
    bounds = [bounds includingCoordinate:addMarker2.position];
    
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
    
    
    [self addRoute:originLat :originLng :destLat :destLng :mapView_];
    
    [map addSubview:mapView_];
    return map;
}

-(void)addRoute:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng :(GMSMapView *)mapView{
    _InternetConnection = [[InternetConnection alloc] init];
    
    if([_InternetConnection connected])
        
    {
        

    //get from google haking
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@,%@&destination=%@,%@&sensor=false&mode=driving&alternatives=true&key=AIzaSyAKJvVBiZFJjGTdb6CbJNqMnpBEE-ktmIo", originLat, originLng, destLat, destLng];
    
    NSData *myData = [[NSData alloc] init];
    _json = [[JSON alloc] init];
    myData = [_json getDataFrom:url];
    NSDictionary *result = [[NSDictionary alloc] init];
    _json = [[JSON alloc] init];
    result = [_json FromJson:myData];
    
    GMSMutablePath *path = [GMSMutablePath path];
    NSArray *routes = [result objectForKey:@"routes"];
    NSDictionary *firstRoute = [routes objectAtIndex:0];
    NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
    NSArray *steps = [leg objectForKey:@"steps"];
    long stepIndex = 0;
    
    CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
    
    for (NSDictionary *step in steps) {
        
        NSDictionary *start_location = [step objectForKey:@"start_location"];
        stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
        
        [path addCoordinate:[self coordinateWithLocation:start_location]];
        
        NSString *polyLinePoints = [[step objectForKey:@"polyline"] objectForKey:@"points"];
        GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:polyLinePoints];
        for (long p=0; p<polyLinePath.count; p++) {
            [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
        }
        
        if ([steps count] == stepIndex){
            NSDictionary *end_location = [step objectForKey:@"end_location"];
            stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
            [path addCoordinate:[self coordinateWithLocation:end_location]];
        }
    }
    
    GMSPolyline *polyline = nil;
    polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor redColor];
    polyline.strokeWidth = 3.f;
    polyline.map = mapView;
    }
    
}

-(NSString *)addPolyLineStaticMap:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng{
    
    NSString *pathStr = @"";
    _InternetConnection = [[InternetConnection alloc] init];
    
    if([_InternetConnection connected])
        
    {
        

    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@,%@&destination=%@,%@&sensor=false&mode=driving&alternatives=true&key=AIzaSyAKJvVBiZFJjGTdb6CbJNqMnpBEE-ktmIo", originLat, originLng, destLat, destLng];
    
    NSData *myData = [[NSData alloc] init];
    _json = [[JSON alloc] init];
    myData = [_json getDataFrom:url];
    NSDictionary *result = [[NSDictionary alloc] init];
    _json = [[JSON alloc] init];
    result = [_json FromJson:myData];
    
    //  GMSMutablePath *path = [GMSMutablePath path];
    NSArray *routes = [result objectForKey:@"routes"];
    if (routes.count != 0) {
        NSDictionary *secondRoute = [routes objectAtIndex:0];
        NSDictionary *overview = [secondRoute objectForKey:@"overview_polyline" ];
        pathStr = [overview objectForKey:@"points"];
    }
    }
    
    return pathStr;
}

-(NSString *)addPolyLine:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng{
    
    NSString *pathStr = @"";
    _InternetConnection = [[InternetConnection alloc] init];
    
    if([_InternetConnection connected])
        
    {
        

    pathStr = @"path=color:0x378EB9ff|weight:4";
    NSString *str = @"";
    //get from google haking
    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@,%@&destination=%@,%@&sensor=false&mode=driving&alternatives=true&key=AIzaSyAKJvVBiZFJjGTdb6CbJNqMnpBEE-ktmIo", originLat, originLng, destLat, destLng];
    
    NSData *myData = [[NSData alloc] init];
    _json = [[JSON alloc] init];
    myData = [_json getDataFrom:url];
    NSDictionary *result = [[NSDictionary alloc] init];
    _json = [[JSON alloc] init];
    result = [_json FromJson:myData];
    
  //  GMSMutablePath *path = [GMSMutablePath path];
    NSArray *routes = [result objectForKey:@"routes"];
    NSDictionary *firstRoute = [routes objectAtIndex:0];
    NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
    NSArray *steps = [leg objectForKey:@"steps"];
    long stepIndex = 0;
    
    CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
    
    for (NSDictionary *step in steps) {
        
        NSDictionary *start_location = [step objectForKey:@"start_location"];
        stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
        
        str = [NSString stringWithFormat:@"|%@,%@", [start_location objectForKey:@"lat"], [start_location objectForKey:@"lng"]];
        pathStr = [pathStr stringByAppendingString:str];
        
        if ([steps count] == stepIndex){
            NSDictionary *end_location = [step objectForKey:@"end_location"];
            stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
            str = [NSString stringWithFormat:@"|%@,%@", [end_location objectForKey:@"lat"], [end_location objectForKey:@"lng"]];
            pathStr = [pathStr stringByAppendingString:str];
        }
    }
//    
//    GMSPolyline *polyline = nil;
//    polyline = [GMSPolyline polylineWithPath:path];
//    polyline.strokeColor = [UIColor redColor];
//    polyline.strokeWidth = 3.f;
    }
    return pathStr;
}

- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    
    return CLLocationCoordinate2DMake(latitude, longitude);
}

-(NSString *)nextStatus:(NSInteger)status{

    StatusArray *globals = [StatusArray sharedInstance];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    arr = globals.statusArr;
    
    NSString *statusName = @"";
    
    switch (status) {
        case 5:
            statusName = [arr objectAtIndex:1];
            break;
        case 6:
            statusName = [arr objectAtIndex:1];
            break;
        case 7:
            statusName = [arr objectAtIndex:2];
            break;
        case 8:
            statusName = [arr objectAtIndex:2];
            break;
        case 9:
            statusName = [arr objectAtIndex:2];
            break;
        case 11:
            statusName = [arr objectAtIndex:3];
            break;
        case 12:
            statusName = [arr objectAtIndex:4];
            break;
        case 13:
            statusName = [arr objectAtIndex:4];
            break;
        case 14:
            statusName = [arr objectAtIndex:4];
            break;
        case 15:
            statusName = [arr objectAtIndex:5];
            break;
        case 16:
            statusName = [arr objectAtIndex:6];
            break;
        case 17:
            statusName = [arr objectAtIndex:7];
            break;
        default:[arr objectAtIndex:1];
            break;
    }

    return statusName;
}

-(int)updateJobStatus:(NSInteger)jobID :(NSInteger)statusJobID{
    int status;
    switch (statusJobID) {
        case 5:
            status = 7;
            break;
        case 7:
            status = 11;
            break;
        case 8:
            status = 11;
            break;
        case 9:
            status = 11;
            break;
        case 11:
            status = 12;
            break;
        case 12:
            status = 15;
            break;
        case 13:
            status = 15;
            break;
        case 14:
            status = 15;
            break;
        case 15:
            status = 16;
            break;
        case 16:
            status = 17;
            break;
        case 17:
            status = 18;
            break;
        case 18:
            status = 19;
            break;
        default:status = statusJobID;
            break;
    }
    
    

//    _json = [[JSON alloc] init];
//    NSDictionary *Dic = [[NSDictionary alloc] init];
//    Dic = [_json FromJson:myData];
    return status;
}

-(int)updateJobStatusInLocation:(NSInteger)jobID :(NSInteger)statusJobID{
    int status;
    switch (statusJobID) {
        case 7:
            status = 8;
            break;
        case 8:
            status = 9;
            break;
        case 12:
            status = 13;
            break;
        case 13:
            status = 14;
            break;
        default:status = statusJobID;
            break;
    }
    
    if (status != 0) {
        NSString *urlToSend = [NSString stringWithFormat:@"tr-api/?action=updateShipmentSatus&user_id=%@&carrier_id=%@&shipment_id=%ld&status_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], (long)jobID, (long)status];
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:urlToSend];
        DDLogVerbose(@"location test: %@",urlToSend);
        
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        DDLogVerbose(@"location test: %@",dic);
        
        if ([[dic objectForKey:@"err"] boolValue] == false) {
            //check if update right
            NSDictionary *jobDic = [[NSDictionary alloc] init];
            jobDic = [dic objectForKey:@"shipment"];
            
            if ([jobDic objectForKey:@"status_id"] != 0 && [jobDic objectForKey:@"status_id"] != (id)[NSNull null])
            {
                status  = [[jobDic objectForKey:@"status_id"] intValue];
            }
            
        }

    }
    return status;
}

@end

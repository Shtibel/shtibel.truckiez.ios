//
//  StaticVars.m
//  truckiez
//
//  Created by Menachem Mizrachi on 22/03/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "StaticVars.h"

NSString *serverURL = @"https://demo.truckiez.com.au/";
@implementation StaticVars

+ (NSString*) url {return serverURL;}

@end

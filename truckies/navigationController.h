//
//  navigationController.h
//  truckies
//
//  Created by Menachem Mizrachi on 04/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TPFloatRatingView.h"
#import <CoreText/CoreText.h>

@interface navigationController : UITableViewController<UINavigationControllerDelegate, TPFloatRatingViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userFullName;
@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property (strong, nonatomic) IBOutlet UIView *statusUserView;
@property (strong, nonatomic) IBOutlet UIView *accountView;


@end

//
//  DesktopOfferCell.h
//  truckies
//
//  Created by Menachem Mizrachi on 05/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesktopOfferCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UILabel *pickupDate;
@property (strong, nonatomic) IBOutlet UILabel *pickupHour;
@property (strong, nonatomic) IBOutlet UILabel *dropoffDate;
@property (strong, nonatomic) IBOutlet UILabel *dropoffHour;
@property (strong, nonatomic) IBOutlet UIView *offerContentView;
@property (strong, nonatomic) IBOutlet UIView *mapCell;
@property (strong, nonatomic) IBOutlet UIImageView *mapImage;
@property (strong, nonatomic) IBOutlet UIView *priceView;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UIView *codeView;
@property (strong, nonatomic) IBOutlet UILabel *codeLabel;
@property (strong, nonatomic) IBOutlet UILabel *weight;

@end

//
//  LoginSettings.m
//  truckiez
//
//  Created by Menachem Mizrachi on 26/01/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "LoginSettings.h"
#import "JSON.h"
#import "dbFunctions.h"
@import CocoaLumberjack;

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
@implementation LoginSettings
{
    JSON *_json;
    dbFunctions *_dbFunctions;
}

-(int)loginSetting:(NSString *)phone :(NSString *)password{
    DDLogVerbose(@"start thread backround");
        
        DDLogVerbose(@"send phone and password to server");
    UIDevice *uniqeDevice = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[uniqeDevice identifierForVendor]UUIDString];
        _json = [[JSON alloc] init];
        NSData *myData = [[NSData alloc] init];
        myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=login&phone=%@&password=%@&unique_device_number=%@",[phone stringByReplacingOccurrencesOfString:@" " withString:@""],password, currentDeviceId]];
        
        DDLogVerbose(@"convert data from server to dictionary");
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        
        DDLogVerbose(@"check if get errType from server");
        if([[dic objectForKey:@"errType"] isEqualToString:@""])
        {
            
            DDLogVerbose(@"save data on defaults");
            NSString *defaultString = [dic objectForKey:@"user_id"];
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"userID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //full name
            if ([dic objectForKey:@"user_full_name"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"user_full_name"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //first name
            if ([dic objectForKey:@"user_first_name"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"user_first_name"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_first_name"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //last name
            if ([dic objectForKey:@"user_last_name"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"user_last_name"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_last_name"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //email
            if ([dic objectForKey:@"user_email"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"user_email"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //carier id
            if ([dic objectForKey:@"carrier_id"]== (id)[NSNull null]) {
                defaultString = @"0";
            }else{
                defaultString = [dic objectForKey:@"carrier_id"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"carrier_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //image
            if ([dic objectForKey:@"user_image"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"user_image"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_image"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //can accept offer
            if ([dic objectForKey:@"can_accept_offers"]== (id)[NSNull null]) {
                defaultString = @"0";
            }else{
                defaultString = [dic objectForKey:@"can_accept_offers"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"can_accept_offers"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //can see price
            if ([dic objectForKey:@"can_see_price"]== (id)[NSNull null]) {
                defaultString = @"0";
            }else{
                defaultString = [dic objectForKey:@"can_see_price"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"can_see_price"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //can see offer
            if ([dic objectForKey:@"can_see_offers"]== (id)[NSNull null]) {
                defaultString = @"0";
            }else{
                defaultString = [dic objectForKey:@"can_see_offers"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"can_see_offers"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //user status
            if ([dic objectForKey:@"user_status"]== (id)[NSNull null]) {
                defaultString = @"0";
            }else{
                defaultString = [dic objectForKey:@"user_status"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_status"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //truck type id
            if ([dic objectForKey:@"truck_type_id"]== (id)[NSNull null]) {
                defaultString = @"1";
            }else{
                defaultString = [dic objectForKey:@"truck_type_id"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"truck_type_id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //driver status
            defaultString = @"2";
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"driverStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //phone
            if ([dic objectForKey:@"phone"]== (id)[NSNull null]) {
                defaultString = @"";
            }else{
                defaultString = [dic objectForKey:@"phone"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"user_phone"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            defaultString = password;
            [[NSUserDefaults standardUserDefaults] setObject:defaultString forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //badge
            DDLogVerbose(@"badge number");
            _dbFunctions = [[dbFunctions alloc] init];
            NSInteger badge = [_dbFunctions returnBadgeNum];
            [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
            NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
            [badgeDefault setInteger:badge forKey:@"badge"];
            
            [badgeDefault synchronize];
            
            int checkLoginFirst = 0;
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"checkLoginFirstTime"] == nil)
            {
                checkLoginFirst = 1;
                [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"checkLoginFirstTime"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            DDLogVerbose(@"go to service to get rating");
            //get rating
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            //Bary Warning: you run on main thread and launch to server
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getRating&user_id=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
            
            DDLogVerbose(@"check if get data from service about getRating");
            if (myData) {
                
                DDLogVerbose(@"convert getRating to dictionary");
                _json = [[JSON alloc] init];
                NSDictionary *dic = [[NSDictionary alloc] init];
                dic = [_json FromJson:myData];
                
                NSString *avg_rating;
                DDLogVerbose(@"check if return rating");
                if([dic objectForKey:@"avg_rating"] == (id)[NSNull null]){
                    DDLogVerbose(@"not return rating");
                    avg_rating = @"0.0";
                    [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"avg_rating"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                }else{
                    DDLogVerbose(@"return rating!!!");
                    avg_rating = [dic objectForKey:@"avg_rating"];
                    [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"avg_rating"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                NSString *number_of_voters;
                DDLogVerbose(@"check if return number of voters");
                if([dic objectForKey:@"number_of_voters"] == (id)[NSNull null]){
                    DDLogVerbose(@"not return number_of_voters");
                    number_of_voters = @"0";
                    [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"number_of_voters"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                }else{
                    DDLogVerbose(@"return number_of_voters!!!");
                    number_of_voters = [dic objectForKey:@"number_of_voters"];
                    [[NSUserDefaults standardUserDefaults] setObject:avg_rating forKey:@"number_of_voters"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
            }
            
            
            DDLogVerbose(@"save login on default checkLogin");
            //save login on default
            NSString *firstTime = @"login";
            NSUserDefaults *firstTimeDefault = [NSUserDefaults standardUserDefaults];
            [firstTimeDefault setObject:firstTime forKey:@"checkLogin"];
            
            [firstTimeDefault synchronize];
            
            DDLogVerbose(@"save login on default checkAgree");
            //save first bulid
            firstTime = @"login";
            firstTimeDefault = [NSUserDefaults standardUserDefaults];
            [firstTimeDefault setObject:firstTime forKey:@"checkAgree"];
            
            [firstTimeDefault synchronize];
            DDLogVerbose(@"save 4 on default driverStatus");
            firstTime = @"4";
            firstTimeDefault = [NSUserDefaults standardUserDefaults];
            [firstTimeDefault setObject:firstTime forKey:@"driverStatus"];
            
            [firstTimeDefault synchronize];
            
            DDLogVerbose(@"set to server updateAvailableStatus");
            //set to service
            _json = [[JSON alloc] init];
            myData = [[NSData alloc] init];
            //Bary warning: you go to the server form main thread
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=updateAvailableStatus&user_id=%@&driver_application_status_id=4",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
            
                if (checkLoginFirst == 1) {
                    return 1;
                }else{
                    return 2;
                }
        }else{
            return 3;
        }

}

-(BOOL)sendPhone:(NSString*)phoneNumber{
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=getPasswordSMS&phone=%@",[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    
    if (myData) {
        DDLogVerbose(@"convert data from server to dictionary");
        _json = [[JSON alloc] init];
        NSDictionary *dic = [[NSDictionary alloc] init];
        dic = [_json FromJson:myData];
        
        if ([[dic objectForKey:@"errType"] isEqualToString:@""]) {
            return YES;
        }else{
            return NO;
        }
        
    }else{
        return NO;
    }
}
@end

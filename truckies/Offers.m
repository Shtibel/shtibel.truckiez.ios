//
//  Offers.m
//  truckies
//
//  Created by Menachem Mizrachi on 08/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "Offers.h"
#import "mapsControllerFunc.h"
#import "JSON.h"
#import "Desktop.h"
#import "CustomAlert.h"
#import "InternetConnection.h"
#import "PushNotificationFunctions.h"
#import "shipmentObject.h"
#import "Jobs.h"
#import "LoaderImg.h"
#import "StaticVars.h"

@interface Offers ()
{
    mapsControllerFunc *_mapFunc;
    CustomAlert *_alert;
    InternetConnection *_InternetConnection;
    PushNotificationFunctions *_func;
    LoaderImg *_loaderIMG;
    
    UIView *alert;
    CGFloat widthAlert;
    
    JSON *_json;
    shipmentObject *job;
    NSLayoutConstraint *offersContentViewHeight;
    NSLayoutConstraint *openOfferHeight;
    NSLayoutConstraint *heightOriginText;
    NSLayoutConstraint *heightOriginText2;
    NSLayoutConstraint *heightOriginTop;
    NSLayoutConstraint *heightOriginContent;
    NSLayoutConstraint *heightDestinationText;
    NSLayoutConstraint *heightDestinationText2;
    NSLayoutConstraint *heightDestinationTop;
    NSLayoutConstraint *heightDestinationContent;
    NSLayoutConstraint *heightRouteView;
    NSLayoutConstraint *heightSpecialRequest;
    NSLayoutConstraint *topSpecialRequest;
    NSLayoutConstraint *heightLoadView;
    NSLayoutConstraint *heightLoadbottom;
    NSLayoutConstraint *heightCommentBottom;
    NSLayoutConstraint *heightCommentView;
    NSLayoutConstraint *heightCommentsView;
    
    NSLayoutConstraint *showDetailsTop;
    int checkOpen;
    
    GMSMarker *marker;
    GMSCameraPosition *camera;
    GMSMapView *mapView_;
    
    float heightContentScroll;
    
    int desktopTab;
}

@end

@implementation Offers

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadGesture];
    [self loadPage];
    [self loadPage2];
    
    [self.view removeConstraint: openOfferHeight];
    openOfferHeight = [NSLayoutConstraint constraintWithItem:_contentView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:heightContentScroll + 70];
    [self.view addConstraint:openOfferHeight];
    
    [self.view removeConstraint: offersContentViewHeight];
    CGFloat heightScreen = [UIScreen mainScreen].bounds.size.height;
    heightScreen = heightScreen - 124;
    if ((heightContentScroll + 136) < heightScreen) {
        heightScreen = heightContentScroll + 136;
    }
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_showDetailView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:heightScreen];
    
    [self.view addConstraint:offersContentViewHeight];
    
    showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetailView
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1
                                                   constant:-heightScreen + 176];
    [self.view addConstraint:showDetailsTop];
    NSLog(@"done!!!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)loadGesture{
    UITapGestureRecognizer *showDeatilsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDeatils)];
    showDeatilsTap.numberOfTapsRequired = 1;
    [_showDetailView setUserInteractionEnabled:YES];
    [_showDetailView addGestureRecognizer:showDeatilsTap];
    
    UITapGestureRecognizer *acceptOfferTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(acceptOfferTap)];
    acceptOfferTap.numberOfTapsRequired = 1;
    [_acceptOffer setUserInteractionEnabled:YES];
    [_acceptOffer addGestureRecognizer:acceptOfferTap];
}

-(void)loadPage{
    
    widthAlert = [UIScreen mainScreen].bounds.size.width - 60;
    
    _truckName.text = _offer.truck_type_name;
    _priceView.layer.cornerRadius = 15.0f;
    
    _priceView.frame = CGRectInset(_priceView.frame, -1.0f, -1.0f);
    _priceView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _priceView.layer.borderWidth = 1.0f;
    
    _offerCode.text = [NSString stringWithFormat:@"OFFER#%ld",_offer.offerId];
    //show details
    _pickupDate.text = [NSString stringWithFormat:@"Pickup %@ %@-%@",_offer.pickup_date, _offer.pickup_from_time, _offer.pickup_till_time];
    _dropoffDate.text = [NSString stringWithFormat:@"Dropoff %@ %@-%@",_offer.dropoff_date, _offer.dropoff_from_time, _offer.dropoff_till_time];
    _price.text = _offer.shipment_carrier_payout_text;
    
    //load content to show details
    CGFloat getCellWidth = self.view.bounds.size.width - 20;
    float widthCell = 0;
    
    if (_offer.box_num != 0) {
        
        NSString *text;
        text = [NSString stringWithFormat:@"%ld Boxes",(long)_offer.box_num];
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
        
        widthCell = widthCell + 40 + idealFrame.size.width;
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        img.image = [UIImage imageNamed:@"box2.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        label.text = [NSString stringWithFormat:@"%ld Boxes",(long)_offer.box_num];
        
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_offerContentLoad addSubview:boxes];
        [boxes addSubview:img];
        [boxes addSubview:label];
        
    }
    
    if (_offer.pallet_num != 0) {
        
        NSString *text;
        text = [NSString stringWithFormat:@"%ld Pallets",(long)_offer.pallet_num];
        
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        UIView *pallets;
        if ((widthCell + 30 + idealFrame.size.width) > getCellWidth)
        {
            pallets = [[UIView alloc]initWithFrame:CGRectMake(5,35,30 + idealFrame.size.width,30)];
            widthCell = idealFrame.size.width + 50;
        }else{
            pallets = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
            widthCell = widthCell + 40 + idealFrame.size.width;
        }
        
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        img.image = [UIImage imageNamed:@"pallet2.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        label.text = [NSString stringWithFormat:@"%ld Pallets",(long)_offer.pallet_num];
        
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_offerContentLoad addSubview:pallets];
        [pallets addSubview:img];
        [pallets addSubview:label];
        
    }
    
    if (_offer.truckload_num != 0) {
        
        NSString *text;
        //text = @"Truckload";
        text = _offer.truck_type_name;
        
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont boldSystemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        
        UIView *boxes = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
        
        widthCell = widthCell + 40 + idealFrame.size.width;
        //        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
        //        img.image = [UIImage imageNamed:@"truckload2.png"];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5,7,idealFrame.size.width + 5,20)];
        label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
        //label.text = @"Truckload";
        label.text = _offer.truck_type_name;
        [label setFont:[UIFont boldSystemFontOfSize:14]];
        
        [_offerContentLoad addSubview:boxes];
        //[boxes addSubview:img];
        [boxes addSubview:label];
        
    }
    
    //special request in jobContentView
    NSArray *arraySpecialRequest = [_offer.shipment_special_request componentsSeparatedByString:@","];
    UIView *specialReq = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,arraySpecialRequest.count * 25,40)];
    
    for (int i=0; i<arraySpecialRequest.count; i++) {
        UIImageView *imageHolder;
        imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*20 + 5, 5, 20, 20)];
        
        UIImage *image1;
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[StaticVars url],arraySpecialRequest[i]]]];
        
        image1 = [UIImage imageWithData: imageData];
        imageHolder.image = image1;
        
        [specialReq addSubview:imageHolder];
    }
    
    //weight
    int widthCellToWeight = widthCell + arraySpecialRequest.count * 25 + 20;
    if (![_offer.total_load_weight isEqualToString:@""]) {
        NSString *text;
        
        if ([_offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
            text = [NSString stringWithFormat:@"%@ %@T",_offer.truck_type_name ,_offer.total_load_weight];
        }else{
            text = [NSString stringWithFormat:@"%@ %@KG",_offer.truck_type_name ,_offer.total_load_weight];
        }
        CGSize frameSize = CGSizeMake(300, 30);
        UIFont *font = [UIFont systemFontOfSize:14];
        
        CGRect idealFrame = [text boundingRectWithSize:frameSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName:font }
                                               context:nil];
        
        if ((getCellWidth - widthCellToWeight) >= 30 + idealFrame.size.width) {
            
            UIView *weight = [[UIView alloc]initWithFrame:CGRectMake(widthCell + 10,0,30 + idealFrame.size.width,30)];
            
            widthCell = widthCell + 40 + idealFrame.size.width;
            
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(5,5,20,20)];
            
            if ([_offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
                img.image = [UIImage imageNamed:@"T2.png"];
            }else{
                img.image = [UIImage imageNamed:@"KG2.png"];
            }
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30,7,idealFrame.size.width + 5,20)];
            label.textColor = [UIColor colorWithRed:0.04 green:0.33 blue:0.47 alpha:1.0];
            label.text = text;
            [label setFont:[UIFont boldSystemFontOfSize:14]];
            
            [_offerContentLoad addSubview:weight];
            [weight addSubview:img];
            [weight addSubview:label];
            
            //req view
            CGRect btFrame;
            btFrame = specialReq.frame;
            btFrame.origin.x = widthCell;
            specialReq.frame = btFrame;
            
        }
    }
    
    [_offerContentLoad addSubview:specialReq];
    
    checkOpen = 0;
    heightContentScroll = 0;
    
    openOfferHeight = [NSLayoutConstraint constraintWithItem:_contentView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:0];
    [self.view addConstraint:openOfferHeight];
    
    offersContentViewHeight = [NSLayoutConstraint constraintWithItem:_showDetailView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:106];
    [self.view addConstraint:offersContentViewHeight];
    
    [_showDetailView.layer setShadowOffset:CGSizeMake(1, 1)];
    [_showDetailView.layer setShadowColor:[[UIColor grayColor] CGColor]];
    [_showDetailView.layer setShadowOpacity:0.5];
    
    [_acceptOffer.layer setCornerRadius:5.0f];
    //load content
    UIBezierPath *maskPath;
    CAShapeLayer *maskLayer;
    CGFloat borderWidth;
    CGRect textsize;
    CGFloat originTextWidth = [UIScreen mainScreen].bounds.size.width;
    CGSize constraint;
    NSDictionary *attributes;
    
    float originTop;
    float destinationTop;
    
    //////////////////////route
    //origin text
    _originText.text = _offer.origin_address;
    _originText.lineBreakMode = NSLineBreakByWordWrapping;
    _originText.numberOfLines = 0;
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
    textsize =[_originText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    if (textsize.size.height < 25) {
        originTop = 25;
    }else{
        originTop = textsize.size.height;
    }
    
    
    heightOriginText = [NSLayoutConstraint constraintWithItem:_originText
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:textsize.size.height];
    [self.view addConstraint:heightOriginText];
    
    heightOriginTop = [NSLayoutConstraint constraintWithItem:_originTop
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:originTop + 35];
    [self.view addConstraint:heightOriginTop];
    
    /////instruction
    int origin_ins = 0;
    if (![_offer.origin_special_site_instructions isEqualToString:@""]) {
     
    NSString *str = _offer.origin_special_site_instructions;
    CGSize constraint4 = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, 400);
    NSDictionary *attributes4 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
    CGRect textsize4 = [str boundingRectWithSize:constraint4 options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes4 context:nil];
    
    UILabel *origin_intruction;
    origin_intruction = [[UILabel alloc] initWithFrame:CGRectMake(10,originTop + 70,textsize4.size.width,textsize4.size.height)];
    
    origin_intruction.text = str;
    origin_intruction.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    [origin_intruction setFont:[UIFont systemFontOfSize:15]];
    
    origin_intruction.lineBreakMode = NSLineBreakByWordWrapping;
    origin_intruction.numberOfLines = 0;
    if (![str isEqualToString:@""]) {
        textsize4.size.height += 10;
    }else{
        textsize4.size.height = 0;
    }
    origin_ins = textsize4.size.height;
    
    [_originContentView addSubview:origin_intruction];
    
    }
    
    /////
    
    heightOriginContent = [NSLayoutConstraint constraintWithItem:_originContentView
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:originTop + 70 + origin_ins];
    [self.view addConstraint:heightOriginContent];
    
    _pickupText.text = [NSString stringWithFormat:@"%@-%@ %@",_offer.pickup_from_time, _offer.pickup_till_time, _offer.original_pickup_date];
    
    
    //destination text
    _destinationText.text = _offer.destination_address;
    _destinationText.lineBreakMode = NSLineBreakByWordWrapping;
    _destinationText.numberOfLines = 0;
    
    
    constraint = CGSizeMake(originTextWidth - 35, 400);
    attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
    textsize =[_destinationText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    if (textsize.size.height < 25) {
        destinationTop = 25;
    }else{
        destinationTop = textsize.size.height;
    }
    
    heightDestinationText = [NSLayoutConstraint constraintWithItem:_destinationText
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:textsize.size.height];
    [self.view addConstraint:heightDestinationText];
    
    heightDestinationTop = [NSLayoutConstraint constraintWithItem:_destinationTop
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0
                                                         constant:destinationTop + 35];
    [self.view addConstraint:heightDestinationTop];
    
    /////instruction
    int destination_ins = 0;
    if (![_offer.destination_special_site_instructions isEqualToString:@""]) {
        
        NSString *str = _offer.destination_special_site_instructions;
        CGSize constraint4 = CGSizeMake([UIScreen mainScreen].bounds.size.width-20, 400);
        NSDictionary *attributes4 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        CGRect textsize4 = [str boundingRectWithSize:constraint4 options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes4 context:nil];
        
        UILabel *origin_intruction;
        origin_intruction = [[UILabel alloc] initWithFrame:CGRectMake(10,originTop + 70,textsize4.size.width,textsize4.size.height)];
        
        origin_intruction.text = str;
        origin_intruction.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        [origin_intruction setFont:[UIFont systemFontOfSize:15]];
        
        origin_intruction.lineBreakMode = NSLineBreakByWordWrapping;
        origin_intruction.numberOfLines = 0;
        /*if (![str isEqualToString:@""]) {
            textsize4.size.height += 10;
        }else{
            textsize4.size.height = 0;
        }*/
        destination_ins = textsize4.size.height;
        
        [_destinationContentView addSubview:origin_intruction];
        
    }
    
    /////

    heightDestinationContent = [NSLayoutConstraint constraintWithItem:_destinationContentView
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:destinationTop + 70 + destination_ins];
    [self.view addConstraint:heightDestinationContent];
    
    heightRouteView = [NSLayoutConstraint constraintWithItem:_routeView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:originTop + destinationTop + 190 + origin_ins + destination_ins];
    [self.view addConstraint:heightRouteView];
    
    
    _dropoffText.text = [NSString stringWithFormat:@"%@-%@ %@",_offer.dropoff_from_time, _offer.dropoff_till_time, _offer.dropoff_date];
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_topRoute.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _topRoute.layer.mask = maskLayer;
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_destinationContentView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    
    _routeView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _routeView.frame = CGRectInset(_routeView.frame, -borderWidth, -borderWidth);
    _routeView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _routeView.layer.borderWidth = borderWidth;
    
    CGFloat height = CGRectGetHeight(_routeView.bounds);
    heightContentScroll = heightContentScroll + height + 5;
    ////////load
    //    NSArray *arrayShipmentLoad = [_offer.shipment_load componentsSeparatedByString:@","];
    //    for (int i=0; i<arrayShipmentLoad.count; i++) {
    //        NSArray *arr = [arrayShipmentLoad[i] componentsSeparatedByString:@":"];
    //
    //        UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(10, i*30, 20, 20)];
    //        UIImage *image = [[UIImage alloc] init];
    //        if ([arr[0] isEqualToString:@"box"]) {
    //            image = [UIImage imageNamed:@"box.png"];
    //        }else{
    //            image = [UIImage imageNamed:@"pallet.png"];
    //        }
    //        imageHolder.image = image;
    //
    //        [_loadBottom addSubview:imageHolder];
    //
    //        UILabel *loadText = [[UILabel alloc] initWithFrame:CGRectMake(35, i*30, originTextWidth - 45, 20)];
    //        loadText.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
    //        loadText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    //        loadText.text = arr[1];
    //
    //        [_loadBottom addSubview:loadText];
    //
    //
    //    }
    
    UILabel *loadText = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, originTextWidth - 45, 20)];
    loadText.font = [UIFont fontWithName:@"Helvetica Neue" size:15];
    loadText.textColor = [UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
    if ([_offer.load_weight_type caseInsensitiveCompare:@"t"] == NSOrderedSame) {
        loadText.text = [NSString stringWithFormat:@"%@ %@T",_offer.truck_type_name ,_offer.total_load_weight];
    }else{
        loadText.text = [NSString stringWithFormat:@"%@ %@KG",_offer.truck_type_name ,_offer.total_load_weight];
    }
    
    [_loadBottom addSubview:loadText];
    
    
    heightLoadbottom = [NSLayoutConstraint constraintWithItem:_loadBottom
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:30];
    [self.view addConstraint:heightLoadbottom];
    
    heightLoadView = [NSLayoutConstraint constraintWithItem:_loadContentView
                                                  attribute:NSLayoutAttributeHeight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1.0
                                                   constant:76];
    [self.view addConstraint:heightLoadView];
    
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_loadTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
    
    maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    _loadTop.layer.mask = maskLayer;
    
    _loadContentView.layer.cornerRadius = 5.0f;
    borderWidth = 1.0f;
    _loadContentView.frame = CGRectInset(_loadContentView.frame, -borderWidth, -borderWidth);
    _loadContentView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _loadContentView.layer.borderWidth = borderWidth;
    
    height = CGRectGetHeight(_loadContentView.bounds);
    heightContentScroll = heightContentScroll + height + 5;
    //////special request
    
    if (![_offer.shipment_special_request isEqualToString:@""]) {
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            
            NSArray *arraySpecialRequest = [_offer.shipment_special_request componentsSeparatedByString:@","];
            
            for (int i=0; i<arraySpecialRequest.count; i++) {
                UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(i*25, 5, 20, 20)];
                UIImage *image1;
                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[StaticVars url],arraySpecialRequest[i]]]];
                
                image1 = [UIImage imageWithData: imageData];
                imageHolder.image = image1;
                
                [_SRBottom addSubview:imageHolder];
            }
        }
        maskPath = [UIBezierPath bezierPathWithRoundedRect:_SRTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        _SRTop.layer.mask = maskLayer;
        
        _SRview.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _SRview.frame = CGRectInset(_SRview.frame, -borderWidth, -borderWidth);
        _SRview.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _SRview.layer.borderWidth = borderWidth;
        
        height = CGRectGetHeight(_SRview.bounds);
        heightContentScroll = heightContentScroll + height + 5;
        
    }else{
        heightSpecialRequest = [NSLayoutConstraint constraintWithItem:_SRview
                                                            attribute:NSLayoutAttributeHeight
                                                            relatedBy:NSLayoutRelationEqual
                                                               toItem:nil
                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                           multiplier:1.0
                                                             constant:0];
        [self.view addConstraint:heightSpecialRequest];
        
        topSpecialRequest = [NSLayoutConstraint constraintWithItem:_loadContentView
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:_SRview
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1
                                                          constant:0];
        [self.view addConstraint:topSpecialRequest];
        
        _SRview.hidden = YES;
    }
    
    
    //comments
    if (![_offer.comments isEqualToString:@""]) {
        _commentText.text = _offer.comments;
        _commentText.lineBreakMode = NSLineBreakByWordWrapping;
        _commentText.numberOfLines = 0;
        
        
        constraint = CGSizeMake(originTextWidth - 20, 400);
        attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
        textsize =[_commentText.text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        originTop = textsize.size.height;
        
        heightCommentBottom = [NSLayoutConstraint constraintWithItem:_commentsBottom
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:textsize.size.height + 10];
        [self.view addConstraint:heightCommentBottom];
        
        heightCommentView = [NSLayoutConstraint constraintWithItem:_commentsView
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:textsize.size.height + 40];
        [self.view addConstraint:heightCommentView];
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:_commentsTop.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(20.0, 20.0)];
        
        maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        _commentsTop.layer.mask = maskLayer;
        
        _commentsView.layer.cornerRadius = 5.0f;
        borderWidth = 1.0f;
        _commentsView.frame = CGRectInset(_commentsView.frame, -borderWidth, -borderWidth);
        _commentsView.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
        _commentsView.layer.borderWidth = borderWidth;
        height = CGRectGetHeight(_commentsView.bounds);
        heightContentScroll = heightContentScroll + height + 5;
    }else{
        heightCommentsView = [NSLayoutConstraint constraintWithItem:_commentsView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:0];
        [self.view addConstraint:heightCommentsView];
        _commentsView.hidden = YES;
    }
    
}

-(void)loadPage2{
    
    [self performSelector:@selector(openMap:) withObject:_map afterDelay:0.5];
    
}

-(void)locationFunc{
    
}

- (IBAction)openMap:(id)sender {
    _map.hidden = NO;
    
    camera = [GMSCameraPosition cameraWithLatitude:[_offer.origin_lat floatValue]
                                         longitude:[_offer.origin_lng floatValue]
                                              zoom:16];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, _map.frame.size.width, _map.frame.size.height) camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = NO;
    mapView_.settings.myLocationButton = NO;
    mapView_.settings.consumesGesturesInView = YES;
    mapView_.trafficEnabled = NO;
    //mapView_.mapType = kGMSTypeNormal;
    
    _mapFunc = [[mapsControllerFunc alloc] init];
    [_mapFunc addOfferMarkers:_offer :mapView_];
    
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        _mapFunc = [[mapsControllerFunc alloc] init];
        [_mapFunc addRoute:_offer.origin_lat :_offer.origin_lng :_offer.destination_lat :_offer.destination_lng :mapView_];
    }
    
    
    [_map addSubview:mapView_];
}



-(void)showDeatils{
    if (checkOpen == 0) {
        
        [self.view removeConstraint: showDetailsTop];
        showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetailView
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                       constant:64];
        [self.view addConstraint:showDetailsTop];
        
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
            checkOpen = 1;
        }];
    }else{
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        height = height - 124;
        if ((heightContentScroll + 136) < height) {
            height = heightContentScroll + 136;
        }
        [self.view removeConstraint: showDetailsTop];
        showDetailsTop = [NSLayoutConstraint constraintWithItem:_showDetailView
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.view
                                                      attribute:NSLayoutAttributeTop
                                                     multiplier:1
                                                       constant:-height + 176];
        [self.view addConstraint:showDetailsTop];
        [self.view setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
            checkOpen = 0;
        }];
    }
    
}

- (IBAction)back:(id)sender{
    //    int checkModal = [self isModal];
    //    if (checkModal == true) {
    //        [self dismissViewControllerAnimated:YES completion:^{
    //            [self.navigationController popToRootViewControllerAnimated:YES];
    //        }];
    //    }else{
//    UIView *backgroundView;
//    _loaderIMG = [[LoaderImg alloc] init];
//    backgroundView = [_loaderIMG loader:backgroundView];
//    [self.view addSubview:backgroundView];
//    
//    [self performSegueWithIdentifier:@"backToDesktop" sender:self];
    
    //    }
    [self dismissViewControllerAnimated:YES completion:^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }];
}

- (BOOL)isModal {
    if([self presentingViewController])
        return YES;
    
    return NO;
}

-(void)acceptOfferTap{
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"can_accept_offers"] intValue] != 1) {
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"This offer is no longer assigned to you. If you need help with this offer please contact your carrier or Truckiez" :@"" :@"OK" :@"" :nil :self.view :0];
    }else{
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"right_version"] == YES){
        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 75,0,70,35)];
        [doSomething addTarget:self
                        action:@selector(acceptOfferClick)
              forControlEvents:UIControlEventTouchUpInside];
        
        alert = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        
        alert = [_alert alertView:doSomething :nil :@"Accept offer" :[NSString stringWithFormat:@"Are you sure you have the truck: %@",_offer.truck_type_name] :@"Yes" :@"No" :@"" :nil :self.view :0];
        }else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"There is a new version for truckiez application, please update to complete the action"
                                                                                     message:nil
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"go to app store"
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [self updateApp];
                                                             }];
            [alertController addAction:action];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"do nothing" style:UIAlertActionStyleCancel
                                                           handler:^(UIAlertAction * action) {
                                                               [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                           }];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            
        }
    }else{
        if (buttonIndex == 0) {
            [self performSegueWithIdentifier:@"backToDesktop" sender:self];
        }
    }
}

-(void)FromDesktop:(int)offerTab{
    desktopTab = offerTab;
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([segue.identifier isEqualToString:@"backToDesktop"]) {
//        Desktop *desktop = [segue destinationViewController];
//        [desktop getSelectTab:2 :desktopTab];
//    }
//
//}

-(void)closeAlert{
    [alert removeFromSuperview];
}

-(void)acceptOfferClick{
    [alert removeFromSuperview];
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        //show loader
        UIView *backgroundView;
        _loaderIMG = [[LoaderImg alloc] init];
        backgroundView = [_loaderIMG loader:backgroundView];
        [self.view addSubview:backgroundView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=acceptOffer&user_id=%@&shipment_id=%ld&carrier_id=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], (long)_offer.offerId, [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"]]];
            
            
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([[dic objectForKey:@"accept"] isEqualToString:@"yes"]) {
                    
                    NSDictionary *dic= [[NSDictionary alloc] init];
                    _func = [[PushNotificationFunctions alloc] init];
                    dic = [_func returnJobAnswer:_offer.offerId];
                    
                    NSDictionary *jobDic = [[NSDictionary alloc] init];
                    jobDic = [dic objectForKey:@"job"];
                    
                    _func = [[PushNotificationFunctions alloc] init];
                    job = [[shipmentObject alloc] init];
                    job = [_func returnJob:jobDic];
                    
                    [self performSegueWithIdentifier:@"openJob" sender:self];
                    
                }else{
                    [backgroundView removeFromSuperview];
                    _alert = [[CustomAlert alloc] init];
                    [_alert alertView:nil :nil :@"Notice" :@"This offer is no longer assigned to you. If you need help with this offer please contact your carrier or Truckiez" :@"" :@"OK" :@"" :nil :self.view :0];
                }
                
            });
            
        });
        
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
    
}

-(void)alertBack{
    [alert removeFromSuperview];
    [self performSegueWithIdentifier:@"backToDesktop" sender:self];
}

-(void)offerAccept{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadOfferTable" object:nil];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (IBAction)handlePanShowDetails:(UIPanGestureRecognizer *)recognizer {
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    height = height - 124;
    if ((heightContentScroll + 136) < height) {
        height = heightContentScroll + 136;
    }
    
    
    CGPoint translation = [recognizer translationInView:_showDetailView];
    CGPoint velocity = [recognizer velocityInView:_showDetailView];
    
    if (velocity.y > 0) {
        checkOpen = 1;
    }else{
        checkOpen = 0;
    }
    if (recognizer.view.center.y <= (self.view.bounds.size.height-height/2 - 64)) {
        
        if (recognizer.view.center.y >= -44) {
            recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                 recognizer.view.center.y + translation.y);
            [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetailView];
        }else{
            if (velocity.y > 0) {
                recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                     -43);
                [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetailView];
            }
        }
        
        
    }else{
        if (velocity.y < 0) {
            recognizer.view.center = CGPointMake((self.view.bounds.size.width/2),
                                                 self.view.bounds.size.height-height/2 - 65);
            [recognizer setTranslation:CGPointMake(0, 0) inView:_showDetailView];
        }
    }
    
    
}

- (IBAction)callSupport:(id)sender {
    NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:st];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        NSLog(@"cannnot call");
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openJob"]) {
        Jobs *jobController = [segue destinationViewController];
        jobController.job = [[shipmentObject alloc] init];
        jobController.job = job;
        jobController.checkLastController = 2;
        
    }
}

-(void)updateApp{
    NSString *iTunesLink = @"https://itunes.apple.com/il/app/truckiez/id1191308814?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}
@end

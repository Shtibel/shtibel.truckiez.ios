//
//  Offers.h
//  truckies
//
//  Created by Menachem Mizrachi on 08/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@import GoogleMaps;
#import "offerObject.h"
#import "shipmentObject.h"

@interface Offers : UIViewController<CLLocationManagerDelegate, GMSMapViewDelegate>

@property offerObject *offer;

- (IBAction)back:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *offerCode;

@property (strong, nonatomic) IBOutlet UIView *showDetailView;
@property (strong, nonatomic) IBOutlet UIView *map;
//open offer
@property (strong, nonatomic) IBOutlet UIScrollView *scrollOffer;
@property (strong, nonatomic) IBOutlet UIView *contentView;
//route
@property (strong, nonatomic) IBOutlet UIView *routeView;
@property (strong, nonatomic) IBOutlet UIView *topRoute;

@property (strong, nonatomic) IBOutlet UIView *originContentView;
@property (strong, nonatomic) IBOutlet UIView *originTop;
@property (strong, nonatomic) IBOutlet UIView *originBottom;
@property (strong, nonatomic) IBOutlet UIImageView *originImage;
@property (strong, nonatomic) IBOutlet UILabel *originText;

@property (strong, nonatomic) IBOutlet UIImageView *pickupImage;

@property (strong, nonatomic) IBOutlet UILabel *pickupText;
@property (strong, nonatomic) IBOutlet UIView *destinationContentView;
@property (strong, nonatomic) IBOutlet UIView *destinationTop;
@property (strong, nonatomic) IBOutlet UIView *destinationBottom;

@property (strong, nonatomic) IBOutlet UIImageView *destinationImage;
@property (strong, nonatomic) IBOutlet UILabel *destinationText;

@property (strong, nonatomic) IBOutlet UIImageView *dropoffImage;
@property (strong, nonatomic) IBOutlet UILabel *dropoffText;

//load
@property (strong, nonatomic) IBOutlet UIView *loadContentView;
@property (strong, nonatomic) IBOutlet UIView *loadTop;
@property (strong, nonatomic) IBOutlet UIView *loadBottom;


//special request
@property (strong, nonatomic) IBOutlet UIView *SRview;
@property (strong, nonatomic) IBOutlet UIView *SRTop;
@property (strong, nonatomic) IBOutlet UIView *SRBottom;

//comments
@property (strong, nonatomic) IBOutlet UIView *commentsView;
@property (strong, nonatomic) IBOutlet UIView *commentsTop;
@property (strong, nonatomic) IBOutlet UIView *commentsBottom;


@property (strong, nonatomic) IBOutlet UILabel *commentText;
@property (strong, nonatomic) IBOutlet UIView *acceptOffer;

-(void)FromDesktop:(int)offerTab;

//show details
@property (strong, nonatomic) IBOutlet UILabel *pickupDate;
@property (strong, nonatomic) IBOutlet UILabel *dropoffDate;
@property (strong, nonatomic) IBOutlet UIView *offerContentLoad;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UIView *priceView;

- (IBAction)handlePanShowDetails:(UIPanGestureRecognizer *)recognizer;

- (IBAction)callSupport:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *truckName;

@end

//
//  Terms.m
//  truckies
//
//  Created by Menachem Mizrachi on 15/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "Terms.h"
#import "StrippingHTML.h"
#import "Login.h"

@interface Terms ()
{
    NSLayoutConstraint *truckiesLogoHeigh;
    NSLayoutConstraint *ContentViewHeight;
    int checkbox;
    StrippingHTML *_html;
}

@end

@implementation Terms

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadPage{
    _splashScreen.image = [UIImage imageNamed:_imgName];
    _splashScreen.clipsToBounds = YES;
    _splashScreen.contentMode = UIViewContentModeScaleAspectFill;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    truckiesLogoHeigh = [NSLayoutConstraint constraintWithItem:_truckiesLogo
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                      constant:(width - 180)/2];
    [self.view addConstraint:truckiesLogoHeigh];
    
    UITapGestureRecognizer *checkboxTerm = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkboxClick)];
    checkboxTerm.numberOfTapsRequired = 1;
    [_checkboxView setUserInteractionEnabled:YES];
    [_checkboxView addGestureRecognizer:checkboxTerm];
    checkbox = 0;
    
    _checkBox.layer.borderColor = [UIColor colorWithRed:0.82 green:0.84 blue:0.84 alpha:1.0].CGColor;
    _checkBox.layer.borderWidth = 1.0f;
    
    //text
    CGRect textsize;
    textsize.size.height = 0;
    width = [UIScreen mainScreen].bounds.size.width - 80;
    NSString *text = [[NSUserDefaults standardUserDefaults]
                      stringForKey:@"app_terms"];
    _html = [[StrippingHTML alloc] init];
    NSString *clearText = [_html stringByStrippingHTML:text];
    if (![text isEqualToString:@""]) {
        CGSize constraint = CGSizeMake(width, 2000);
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0] forKey:NSFontAttributeName];
        textsize = [clearText boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    }
    
    
    UILabel *middleText = [[UILabel alloc] initWithFrame:CGRectMake(10,40,width,textsize.size.height)];
    middleText.textColor = [UIColor whiteColor];
    middleText.text = clearText;
    [middleText setFont:[UIFont systemFontOfSize:15]];
    middleText.lineBreakMode = NSLineBreakByWordWrapping;
    middleText.numberOfLines = 0;
    
    [_contentView addSubview:middleText];
    
    ContentViewHeight = [NSLayoutConstraint constraintWithItem:_contentView
                                                           attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:textsize.size.height + 40];
    [self.view addConstraint:ContentViewHeight];

}

-(void)checkboxClick{
    if (checkbox == 0) {
        _checkBox.image = [UIImage imageNamed:@"full_checkbox.png"];
        checkbox = 1;
    }else{
        _checkBox.image = [UIImage imageNamed:@""];
        checkbox = 0;
    }
}

- (IBAction)back:(id)sender {
    
    if (checkbox == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkboxReload" object:nil];
    }
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

@end

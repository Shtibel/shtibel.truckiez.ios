//
//  ConvertColor.h
//  location
//
//  Created by Menachem Mizrachi on 10/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConvertColor : UIViewController

- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range;

@end

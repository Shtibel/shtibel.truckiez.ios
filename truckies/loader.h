//
//  loader.h
//  truckies
//
//  Created by Menachem Mizrachi on 21/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loader : UIViewController

-(UIView *)loaderImg:(UIView *)backgroundView;
@end

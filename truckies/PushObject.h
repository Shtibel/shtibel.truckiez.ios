//
//  PushObject.h
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushObject : NSObject

@property NSInteger pushID;
@property NSInteger messageID;
@property NSString *pushTitle;
@property NSString *pushText;
@property NSString *messageType;
@property int isRead;
@property NSString *pushDate;
@property NSDate *pushDateFormat;

@end

//
//  PickupConfirmation.m
//  truckies
//
//  Created by Menachem Mizrachi on 27/02/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "PickupConfirmation.h"
#import "PickupConfirmationCell.h"
#import "typeObject.h"
#import "PickupConfirmationFunc.h"
#import "AFNetworking.h"
#import "InternetConnection.h"
#import "LoaderImg.h"
#import "CustomAlert.h"
#import "ShipmentsControllerFunctions.h"
#import "JSON.h"
#import "LocationObject.h"
#import "DesktopFunc.h"
#import "Jobs.h"
@import CocoaLumberjack;
#import "typeObject.h"
#import "StaticVars.h"


@interface PickupConfirmation ()
{
    CGFloat widthScreen;
    int cellsNumbers;
    
    NSLayoutConstraint *tableHeight;
    NSLayoutConstraint *scrollHeight;
    NSLayoutConstraint *conTopHeight;
    NSLayoutConstraint *conTextfieldHeight;
    NSMutableArray *typesArr;
    PickupConfirmationFunc *_func;
    InternetConnection *_InternetConnection;
    LoaderImg *_loaderIMG;
    CustomAlert *_alert;
    JSON *_json;
    ShipmentsControllerFunctions *_SFunc;
    DesktopFunc *_desktopFunc;
    shipmentObject *_sh;
    NSMutableArray *typesArr2;
    NSMutableArray *quantityArr;
    bool mouseSwiped;
    CGPoint lastPoint;
    UIImage *imageBol;
    UIView *backgroundView;
    int nextLength;
    
    NSArray *arrId;
    NSArray *arrQua;
    UIImage *imageSig;
    NSString *conStr;
    NSString *senderNameStr;
    NSString *senderPlaceholder;
    NSString *driverNameStr;
    NSString *confirmationDateStr;
    NSString *signatureURLStr;
    NSString *numberPhoneStr;
    NSString *weightStr;
    
    NSString *sendTypeStr;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation PickupConfirmation

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_con > 2) {
        [self loadPageUneditableUI];
        [self loadPageUneditableContent];
    }else{
        [self loadPageUI];
        [self loadPageContent];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (IBAction)driverConfirmation:(id)sender {
    _func = [[PickupConfirmationFunc alloc] init];
    BOOL checkQuantity = [_func checkQuantity:typesArr2 :quantityArr];
    int checkPhone =  (int)[_senderNumber.text length];
    int checkWeight = [_weightText.text intValue];
    if (_job.truck_max_weight < checkWeight) {
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"The weight you entered exceed the maximum weight" :@"" :@"OK" :@"" :nil :self.view :0];
    }else if (checkPhone != 12 && checkPhone != 0){
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Phone text field must contain 10 characters" :@"" :@"OK" :@"" :nil :self.view :0];
    }else if (_drawImage.image == nil){
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Please ask the reciver to sign" :@"" :@"OK" :@"" :nil :self.view :0];
    }else if (checkQuantity == NO){
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Missing a quantity in one or more of the text fields" :@"" :@"OK" :@"" :nil :self.view :0];
    }else if ([_senderName.text isEqualToString:@""]){
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Please enter the reciver name" :@"" :@"OK" :@"" :nil :self.view :0];
        
    }else{
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            
            _loaderIMG = [[LoaderImg alloc] init];
            backgroundView = [_loaderIMG loader:backgroundView];
            [self.view addSubview:backgroundView];
            UIImage *mainImageView;
            mainImageView = _drawImage.image;
            NSData *imageToUpload;
            imageToUpload = UIImageJPEGRepresentation(mainImageView, 0.25);
            NSString *phoneSTR = [_senderNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            _func = [[PickupConfirmationFunc alloc] init];
            NSString *arrString = [_func arrString:typesArr2 :quantityArr :typesArr];
            NSString *registerUrl = [NSString stringWithFormat:@"%@/tr-api/?action=saveConfirmations&user_id=%@&type=%@&total_weight=%@&sender_name=%@&confirmation_phone=%@&shipment_id=%ld%@",[StaticVars url],[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], sendTypeStr ,_weightText.text ,_senderName.text , phoneSTR,(long)_job.shipmentsId,arrString];
            NSString *encodedUrl = [registerUrl stringByAddingPercentEscapesUsingEncoding:
                                    NSUTF8StringEncoding];
            
            NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:encodedUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileData:imageToUpload name:@"file" fileName:[NSString stringWithFormat:@"%@_signature_%ld.jpg",sendTypeStr ,(long)_job.shipmentsId] mimeType:@"image/jpeg"];
            } error:nil];
            
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSURLSessionUploadTask *uploadTask;
            uploadTask = [manager
                          uploadTaskWithStreamedRequest:request
                          progress:^(NSProgress * _Nonnull uploadProgress) {
                              // This is not called back on the main queue.
                              // You are responsible for dispatching to the main queue for UI updates
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  //Update the progress view
                                  //                          [progressView setProgress:uploadProgress.fractionCompleted];
                                  
                              });
                          }
                          completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                              if (response) {
                                  [self changeStatusClick];
                              }else{
                                  [backgroundView  removeFromSuperview];
                                  _alert = [[CustomAlert alloc] init];
                                  [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
                              }
                              
                              
                          }];
            
            [uploadTask resume];
        }else{
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        }
    }
    
}

- (IBAction)addType:(id)sender {
    if (cellsNumbers <4) {
        cellsNumbers +=1;
        for (int i = 0; i<typesArr.count; i++) {
            typeObject *t = [[typeObject alloc] init];
            t = [typesArr objectAtIndex:i];
            
            int flag = 0;
            for (int j = 0; j < typesArr2.count; j++) {
                typeObject *t2 = [[typeObject alloc] init];
                t2 = [typesArr2 objectAtIndex:j];
                if ([t.load_type_name isEqualToString:t2.load_type_name]) {
                    j = (int)typesArr2.count;
                }
                if (j == typesArr2.count-1) {
                    flag = 1;
                }
            }
            
            if (flag == 1) {
                [typesArr2 addObject:t];
                i = (int)typesArr.count;
                if (quantityArr.count < typesArr2.count) {
                    [quantityArr addObject:@"0"];
                }
                //
            }
            
        }
        [_table reloadData];
        
        [self.view removeConstraint:tableHeight];
        tableHeight = [NSLayoutConstraint constraintWithItem:_selectView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                      toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                    constant:50*cellsNumbers];
        
        [self.view addConstraint:tableHeight];
        
        [self.view removeConstraint:scrollHeight];
        scrollHeight = [NSLayoutConstraint constraintWithItem:_topScroll
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:50*cellsNumbers + 175];
        
        [self.view addConstraint:scrollHeight];
    }
    
}

- (IBAction)clear:(id)sender {
    _drawImage.image = nil;
}

-(void)pickup{
    conStr = @"PICKUP";
    sendTypeStr = @"pickup";
    weightStr = _job.total_load_weight;
    senderPlaceholder = @"Sender";
}

-(void)dropoff{
    conStr = @"DROPOFF";
    sendTypeStr = @"dropoff";
    weightStr = _job.total_weight_pickup;
    senderPlaceholder = @"Receiver";
}

-(void)pickupUneditable{
    arrId = [[NSArray alloc] init];
    arrId = _job.load_types_pickup_str;
    
    arrQua = [[NSArray alloc] init];
    arrQua = _job.load_types_pickup_quantity_str;
    signatureURLStr = [NSString stringWithFormat:@"%@%@",[StaticVars url],_job.signature_url_pickup];
    
    conStr = @"PICKUP";
    senderNameStr = _job.signature_receiver_name_pickup;
    senderPlaceholder = @"Sender";
    driverNameStr = _job.confirmation_user_pickup;
    confirmationDateStr = _job.confirmation_date_pickup;
    sendTypeStr = @"pickup";
    numberPhoneStr = _job.confirmation_phone_pickup;
    weightStr = _job.total_weight_pickup;
}

-(void)dropoffUneditable{
    arrId = [[NSArray alloc] init];
    arrId = _job.load_types_dropoff_str;
    
    arrQua = [[NSArray alloc] init];
    arrQua = _job.load_types_dropoff_quantity_str;
    
    signatureURLStr = [NSString stringWithFormat:@"%@%@",[StaticVars url],_job.signature_url];
    
    conStr = @"DROPOFF";
    senderNameStr = _job.signature_receiver_name;
    senderPlaceholder = @"Receiver";
    driverNameStr = _job.confirmation_user_dropoff;
    confirmationDateStr = _job.confirmation_date_dropoff;
    sendTypeStr = @"dropoff";
    numberPhoneStr = _job.confirmation_phone_dropoff;
    weightStr = _job.total_weight_dropoff;
}

-(void)loadPageUneditableUI{
    _addButton.hidden = YES;
    _addButton2.hidden = YES;
    _signatureLabel.hidden = YES;
    _clearButton.hidden = YES;
    
    
    backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0,60,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 120)];
    [self.view addSubview:backgroundView];
    
    if (_con == 3) {
        [self pickupUneditable];
    }else{
        [self dropoffUneditable];
    }
    [self loadText2];
    [self loadBorders];
}

-(void)loadPageUneditableContent{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        typesArr = [[NSMutableArray alloc] init];
        _func = [[PickupConfirmationFunc alloc] init];
        _InternetConnection = [[InternetConnection alloc] init];
        if([_InternetConnection connected])
        {
            typesArr = [_func loadTypeFromServer];
        }
        typesArr2 = [[NSMutableArray alloc] init];
        quantityArr = [[NSMutableArray alloc] init];
        
        int countArr;
        if (_con == 3) {
            countArr = (int)_job.load_types_pickup_str.count;
        }else{
            countArr = (int)_job.load_types_dropoff_str.count;
        }
        
        
        for (int i = 0; i < countArr; i++) {
            for (int j = 0; j < typesArr.count; j++) {
                typeObject *t = [[typeObject alloc] init];
                t = [typesArr objectAtIndex:j];
                NSString *strId = arrId[i];
                if ([strId intValue] == t.typeId) {
                    [typesArr2 addObject:t];
                    j = (int)typesArr.count;
                }
            }
            [quantityArr addObject:arrQua[i]];
        }
        
        
        cellsNumbers = (int)typesArr2.count;
        NSData *dataImageShipments;
        if (![signatureURLStr isEqualToString:@""]) {
            NSURL *urlForImageShipments = [NSURL URLWithString:signatureURLStr];
            dataImageShipments = [NSData dataWithContentsOfURL:urlForImageShipments];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_table reloadData];
            tableHeight = [NSLayoutConstraint constraintWithItem:_selectView
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:50*cellsNumbers];
            
            [self.view addConstraint:tableHeight];
            
            [self.view removeConstraint:scrollHeight];
            int h;
            if ([numberPhoneStr isEqualToString:@""]) {
                h = 105;
            }else{
                h = 155;
            }
            scrollHeight = [NSLayoutConstraint constraintWithItem:_topScroll
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0
                                                         constant:50*cellsNumbers + h];
            
            [self.view addConstraint:scrollHeight];
            _drawImage.image = [UIImage imageWithData:dataImageShipments];
        });
    });
}

-(void)loadText2{
    _titleLabel.text = [NSString stringWithFormat:@"%@ CONFIRMATION #%ld",conStr,(long)_job.shipmentsId];
    _senderName.text = [NSString stringWithFormat:@"%@ name: %@",senderPlaceholder,senderNameStr];
    _signatureLabel.text = [NSString stringWithFormat:@"%@ signature",senderPlaceholder];
    
    if ([numberPhoneStr isEqualToString:@""]) {
        //hiden phone
        _senderNumber.hidden = YES;
        [self.view removeConstraint:scrollHeight];
        scrollHeight = [NSLayoutConstraint constraintWithItem:_topScroll
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                     constant:155];
        
        [self.view addConstraint:scrollHeight];
    }else{
        _senderNumber.text = [NSString stringWithFormat:@"SMS phone: %@",numberPhoneStr];
    }
    _weightText.text = [NSString stringWithFormat:@"%d",[weightStr intValue]];
    _driverName.text = driverNameStr;
    _confirmationDate.text = confirmationDateStr;
}

-(void)loadPageContent{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        typesArr = [[NSMutableArray alloc] init];
        _func = [[PickupConfirmationFunc alloc] init];
        typesArr = [_func loadTypeFromServer];
        typesArr2 = [[NSMutableArray alloc] init];
        
        if (_con == 2) {
            
            arrQua = [[NSArray alloc] init];
            arrQua = _job.load_types_pickup_quantity_str;
            arrId = [[NSArray alloc] init];
            arrId = _job.load_types_pickup_str;
            quantityArr = [[NSMutableArray alloc] init];
            
            for (int i = 0; i < _job.load_types_pickup_str.count; i++) {
                for (int j = 0; j < typesArr.count; j++) {
                    typeObject *t = [[typeObject alloc] init];
                    t = [typesArr objectAtIndex:j];
                    NSString *strId = arrId[i];
                    if ([strId intValue] == t.typeId) {
                        [typesArr2 addObject:t];
                        j = (int)typesArr.count;
                    }
                }
                [quantityArr addObject:arrQua[i]];
            }
            
            cellsNumbers = (int)typesArr2.count;
            
        }else{
            
            typeObject *t = [[typeObject alloc] init];
            if (typesArr.count > 0) {
                t = [typesArr objectAtIndex:0];
            }
            [typesArr2 addObject:t];
            quantityArr = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0", nil];
            cellsNumbers = 1;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_table reloadData];
            tableHeight = [NSLayoutConstraint constraintWithItem:_selectView
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:50*cellsNumbers];
            
            [self.view addConstraint:tableHeight];
            
            [self.view removeConstraint:scrollHeight];
            scrollHeight = [NSLayoutConstraint constraintWithItem:_topScroll
                                                        attribute:NSLayoutAttributeHeight
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0
                                                         constant:50*cellsNumbers + 155];
            
            [self.view addConstraint:scrollHeight];
        });
    });
}
-(void)loadPageUI{
    widthScreen = [UIScreen mainScreen].bounds.size.width;
    cellsNumbers = 1;
    _drawImage.image = nil;
    _closeUneditable.hidden = YES;
    _confirmationTop.hidden = YES;
    conTopHeight = [NSLayoutConstraint constraintWithItem:_confirmationTop
                                                attribute:NSLayoutAttributeHeight
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:nil
                                                attribute:NSLayoutAttributeNotAnAttribute
                                               multiplier:1.0
                                                 constant:0];
    
    [self.view addConstraint:conTopHeight];
    
    if (_con == 1) {
        [self pickup];
    }else{
        [self dropoff];
    }
    
    [self loadText];
    [self loadBorders];
}

-(void)cancelNumberPad{
    [self.view endEditing:YES];
}

-(void)loadText{
    _titleLabel.text = [NSString stringWithFormat:@"%@ CONFIRMATION #%ld",conStr,(long)_job.shipmentsId];
    _senderName.placeholder = [NSString stringWithFormat:@"%@ name",senderPlaceholder];
    _senderNumber.placeholder = @"Send confirmation to this number - Optional";
    _weightText.text = [NSString stringWithFormat:@"%d",[weightStr intValue]];
    _signatureLabel.text = [NSString stringWithFormat:@"%@ signature",senderPlaceholder];
}
-(void)loadBorders{
    [_weightView.layer setCornerRadius:5.0f];
    [_weightView.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_weightView.layer setBorderWidth:1.0f];
    
    [_addButton.layer setCornerRadius:15.0f];
    [_addButton.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_addButton.layer setBorderWidth:1.0f];
    
    [_clearButton.layer setCornerRadius:15.0f];
    [_clearButton.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_clearButton.layer setBorderWidth:1.0f];
    
    [_closeButton.layer setCornerRadius:5.0f];
    [_closeButton.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_closeButton.layer setBorderWidth:1.0f];
    
    [_closeUneditable.layer setCornerRadius:5.0f];
    [_closeUneditable.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_closeUneditable.layer setBorderWidth:1.0f];
    
    [_driverConfirmationButton.layer setCornerRadius:5.0f];
    [_driverConfirmationButton.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_driverConfirmationButton.layer setBorderWidth:1.0f];
    
    [_drawImageView.layer setCornerRadius:5.0f];
    [_drawImageView.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
    [_drawImageView.layer setBorderWidth:1.0f];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellsNumbers;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PickupCell";
    
    PickupConfirmationCell *cell = (PickupConfirmationCell *)[_table dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    typeObject *t = [[typeObject alloc] init];
    t = [typesArr2 objectAtIndex:indexPath.row];
    cell.type.text =[NSString stringWithFormat:@"Load type: %@", t.load_type_name];
    cell.type.delegate = self;
    cell.type.tag = indexPath.row + 10;
    
    if (_con == 1 || _con == 2) {
        cell.quantity2.hidden = YES;
        cell.quantity.placeholder = @"Quantity";
        if (![[quantityArr objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cell.quantity.text = [quantityArr objectAtIndex:indexPath.row];
        }else{
            cell.quantity.text = @"";
        }
    }else{
        cell.quantity2.text = [quantityArr objectAtIndex:indexPath.row];
        cell.remove.hidden = YES;
        cell.arrow.hidden = YES;
    }
    
    
    cell.quantity.delegate = self;
    cell.quantity.tag = indexPath.row + 20;
    
    if (indexPath.row != 0) {
        cell.remove.hidden = NO;
        [cell.remove.layer setCornerRadius:15.0f];
        [cell.remove.layer setBorderWidth:1.0f];
        [cell.remove.layer setBorderColor:[UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor];
        
        UITapGestureRecognizer *Tap;
        Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeCell:)];
        Tap.numberOfTapsRequired = 1;
        [cell.remove setUserInteractionEnabled:YES];
        [cell.remove addGestureRecognizer:Tap];
        cell.remove.tag = indexPath.row;
    }else{
        cell.remove.hidden = YES;
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)removeCell:(UIGestureRecognizer *)gestureRecognizer{
    UIView *b = (UIView *)gestureRecognizer.view;
    int currentCell = (int)b.tag;
    
    cellsNumbers -=1;
    [typesArr2 removeObjectAtIndex:currentCell];
    [quantityArr removeObjectAtIndex:currentCell];
    //[quantityArr addObject:@"0"];
    /* PickupConfirmationCell *cell = [_table cellForRowAtIndexPath:
     [NSIndexPath indexPathForRow:cellsNumbers inSection:1]];
     cell.type.text = @"";*/
    
    [_table reloadData];
    
    [self.view removeConstraint:tableHeight];
    tableHeight = [NSLayoutConstraint constraintWithItem:_selectView
                                               attribute:NSLayoutAttributeHeight
                                               relatedBy:NSLayoutRelationEqual
                                                  toItem:nil
                                               attribute:NSLayoutAttributeNotAnAttribute
                                              multiplier:1.0
                                                constant:50*cellsNumbers];
    
    [self.view addConstraint:tableHeight];
    
    [self.view removeConstraint:scrollHeight];
    scrollHeight = [NSLayoutConstraint constraintWithItem:_topScroll
                                                attribute:NSLayoutAttributeHeight
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:nil
                                                attribute:NSLayoutAttributeNotAnAttribute
                                               multiplier:1.0
                                                 constant:50*cellsNumbers + 175];
    
    [self.view addConstraint:scrollHeight];
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textfield tag:%ld",(long)textField.tag);
    if (textField.tag >= 10 && textField.tag < 20) {
        [textField resignFirstResponder];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose type:"
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        for (int i = 0; i<typesArr.count; i++) {
            
            typeObject *typeO = [[typeObject alloc] init];
            typeO = [typesArr objectAtIndex:i];
            UIAlertAction *actionss = [UIAlertAction actionWithTitle:typeO.load_type_name
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   [self changeTextfield:typeO.load_type_name :textField :i :(int)textField.tag];
                                                               }];
            [alert addAction:actionss];
        }
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                       handler:^(UIAlertAction * action) {
                                                           [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                                                       }];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)]];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag>=20) {
        int q = textField.tag%20;
        [quantityArr replaceObjectAtIndex:q withObject:textField.text];
    }
}


-(void)changeTextfield:(NSString*)type :(UITextField*)textfield :(int)ind :(int)tag{
    int t = tag-10;
    typesArr2[t] = [typesArr objectAtIndex:ind];
    textfield.text = [NSString stringWithFormat:@"Load type: %@",type];
}

//touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    
    if (_con == 1 || _con == 2) {
        lastPoint = [touch locationInView:_drawImage];
        lastPoint.y -= 20;
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_con == 1 || _con == 2) {
        mouseSwiped = YES;
        
        UITouch *touch = [touches anyObject];
        CGPoint currentPoint = [touch locationInView:_drawImage];
        currentPoint.y -= 20;
        
        
        UIGraphicsBeginImageContext(_drawImage.frame.size);
        [_drawImage.image drawInRect:CGRectMake(0, 0, _drawImage.frame.size.width, _drawImage.frame.size.height)];
        //CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 3.0);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
        CGContextBeginPath(UIGraphicsGetCurrentContext());
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        _drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        lastPoint = currentPoint;
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_con == 1 || _con == 2) {
        if(!mouseSwiped) {
            UIGraphicsBeginImageContext(_drawImage.frame.size);
            [_drawImage.image drawInRect:CGRectMake(0, 0, _drawImage.frame.size.width, _drawImage.frame.size.height)];
            CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
            CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 3.0);
            CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
            CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
            CGContextStrokePath(UIGraphicsGetCurrentContext());
            CGContextFlush(UIGraphicsGetCurrentContext());
            _drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    int heightScroll = 50*cellsNumbers + 40;
    if(scrollView.contentOffset.y > heightScroll){
        _scroll.scrollEnabled = NO;
        _scroll.userInteractionEnabled = NO;
    }
    
}

-(void)changeStatusClick{
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            int updateStatus;
            if (_con == 1) {
                updateStatus = 11;
            }else{
                updateStatus = 16;
            }
            
            
            NSString *urlToSend = [NSString stringWithFormat:@"tr-api/?action=updateShipmentSatus&user_id=%@&carrier_id=%@&shipment_id=%ld&status_id=%ld",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"], [[NSUserDefaults standardUserDefaults] stringForKey:@"carrier_id"], (long)_job.shipmentsId, (long)updateStatus];
            _json = [[JSON alloc] init];
            NSData *myData = [[NSData alloc] init];
            myData = [_json getDataFrom:urlToSend];
            
            DDLogVerbose(@"test 5: %@",urlToSend);
            _json = [[JSON alloc] init];
            NSDictionary *dic = [[NSDictionary alloc] init];
            dic = [_json FromJson:myData];
            
            DDLogVerbose(@"pickup confirmation test: %@",dic);
            if ([[dic objectForKey:@"err"] boolValue] == false) {
                
                //check if update right
                NSDictionary *jobDic = [[NSDictionary alloc] init];
                jobDic = [dic objectForKey:@"shipment"];
                
                if ([jobDic objectForKey:@"status_id"] != 0 && [jobDic objectForKey:@"status_id"] != (id)[NSNull null]) {
                    _job.status_id = [[jobDic objectForKey:@"status_id"] intValue];
                    
                    _job = [[shipmentObject alloc] init];
                    _sh = [[shipmentObject alloc] init];
                    _job = [_sh returnJob:jobDic];
                    
                    //change on status array
                    LocationObject *location = [LocationObject sharedInstance];
                    _desktopFunc = [[DesktopFunc alloc] init];
                    location.jobsArray = [[NSMutableArray alloc] init];
                    location.jobsArray = [_desktopFunc loadJobsToChngeStatus];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [backgroundView  removeFromSuperview];
                        [self performSegueWithIdentifier:@"updateJob" sender:self];
                    });
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [backgroundView  removeFromSuperview];
                    });
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([[dic objectForKey:@"errType"] isEqualToString:@"update shipment"]) {
                        CGFloat widthAlert = widthScreen - 60;
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(outToDesktop)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertview2:doSomething :@"Notice" :@"The job has changed. Please reopen it" :@"OK" :self.view];
                    }else{
                        CGFloat widthAlert = widthScreen - 60;
                        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(widthAlert/2 - 35,0,70,35)];
                        [doSomething addTarget:self
                                        action:@selector(outToDesktop)
                              forControlEvents:UIControlEventTouchUpInside];
                        
                        
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertview2:doSomething :@"Notice" :@"This job is no longer assigned to you. If you need help with this job please contact your carrier or Truckiez" :@"OK" :self.view];
                        
                    }
                    [backgroundView  removeFromSuperview];
                });
            }
            
        });
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)outToDesktop{
    [self performSegueWithIdentifier:@"openDesktop" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"updateJob"]) {
        Jobs *jobsController = [segue destinationViewController];
        jobsController.job = [[shipmentObject alloc] init];
        jobsController.job = _job;
    }
}

//validate phone
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == _senderNumber) {
        int length = (int)[self getLength:textField.text];
        
        if(length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 4 && nextLength < length)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@ ",num];
            
        }
        else if(length == 7 && nextLength < length)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@ %@ ",[num  substringToIndex:4],[num substringFromIndex:4]];
        }
        nextLength = length;
    }
    return YES;
    
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}


- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

@end

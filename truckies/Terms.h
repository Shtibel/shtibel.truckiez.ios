//
//  Terms.h
//  truckies
//
//  Created by Menachem Mizrachi on 15/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface Terms : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *splashScreen;
@property (strong, nonatomic) IBOutlet UIImageView *truckiesLogo;
@property NSString *imgName;
@property (strong, nonatomic) IBOutlet UIView *checkboxView;
@property (strong, nonatomic) IBOutlet UIImageView *checkBox;
- (IBAction)back:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

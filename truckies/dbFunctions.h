//
//  dbFunctions.h
//  truckies
//
//  Created by Menachem Mizrachi on 22/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dbFunctions : UIViewController

-(void)createTables;
-(void)insertPush:(NSInteger)messageId :(NSString *)messageTitle :(NSString *)messageText :(NSString *)messageType;
-(void)updatePush:(NSInteger)messageId :(NSString *)messageType;
-(void)updateJobPush:(NSInteger)messageId :(NSString *)messageTitle :(NSString *)messageText :(NSString *)messageType;
-(int)checkIfPushSend:(NSInteger)messageId :(NSString *)messageType;
-(void)deletePush:(NSInteger)pushId;

-(NSMutableArray*)selectPush;

-(int)returnBadgeNum;

-(BOOL)clearPushTable;
-(void)deleteAllPush;

@end

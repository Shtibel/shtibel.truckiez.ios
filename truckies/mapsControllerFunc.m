//
//  mapsCobntrollerFunc.m
//  location
//
//  Created by Menachem Mizrachi on 03/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "mapsControllerFunc.h"
#import "JSON.h"
@import CocoaLumberjack;
#import "InternetConnection.h"
#import "StaticVars.h"


@interface mapsControllerFunc ()
{
    JSON *_json;
    InternetConnection*_InternetConnection;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation mapsControllerFunc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSString *)returnTimeLabel{
    
    NSDate *now = [NSDate date];
    NSDateComponents *currentTime = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond
                                                                    fromDate:now];
    
    NSString *timeLabel = @"";
    if(currentTime.hour >= 5 && currentTime.hour < 13){
        timeLabel = @"good morning";
    }else if (currentTime.hour >= 12 && currentTime.hour < 19){
        timeLabel = @"good afternoon";
    }else if (currentTime.hour >= 19 && currentTime.hour < 23){
        timeLabel = @"good evening";
    }
    
    return timeLabel;
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}
-(void)sendToDB:(NSString *)latitudeLabel :(NSString *)longitudeLabel :(NSString *)driverStatus{
    
    _json = [[JSON alloc] init];
    NSLog(@"crash");
    [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=locationMove&user_id=%@&lat=%@&lng=%@&driver_application_status_id=%@",[[NSUserDefaults standardUserDefaults]
                                                                                                                                           stringForKey:@"userID"], latitudeLabel, longitudeLabel, driverStatus]];
    
    NSLog(@"send from maps: %@ %@ %@",latitudeLabel, longitudeLabel, driverStatus);
    
}

- (NSData *) getDataFrom:(NSString *)url{
    
    if ([NSThread isMainThread])
    {
        NSLog(@"Run On main Thread!! check if needed");
    }
    NSString *url2 = [NSString stringWithFormat:@"%@/%@",[StaticVars url],url];
    NSString *encodedUrl = [url2 stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:encodedUrl]];
    
    NSError *error = [[NSError alloc] init];
    NSHTTPURLResponse *responseCode = nil;
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    NSTimeInterval newTimeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval duration = newTimeInSeconds - timeInSeconds;
    DDLogVerbose(@"number seconds to get data from server: %f",duration);
    
    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %li", url2, (long)[responseCode statusCode]);
        return nil;
    }
    return oResponseData;//[[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
}



-(void)addOfferMarkers:(offerObject *)offer :(GMSMapView *)mapView_{
    GMSMarker *addMarker = [[GMSMarker alloc] init];
    addMarker.position = CLLocationCoordinate2DMake([offer.origin_lat floatValue], [offer.origin_lng floatValue]);
    addMarker.title = @"from";
    //addMarker.snippet = [key objectForKey:@"shipper_name"];
    addMarker.snippet = [NSString stringWithFormat:@"id:%ld",(long)offer.offerId];
    addMarker.map = mapView_;
    addMarker.icon = [self image:[UIImage imageNamed:@"from.png"] scaledToSize:CGSizeMake(37.5f, 60.0f)];
    
    CLLocationCoordinate2D firstLocation = addMarker.position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
    
    
    addMarker = [[GMSMarker alloc] init];
    addMarker.position = CLLocationCoordinate2DMake([offer.destination_lat floatValue], [offer.destination_lng floatValue]);
    addMarker.title = @"to";
    //addMarker.snippet = [key objectForKey:@"shipper_name"];
    addMarker.snippet = [NSString stringWithFormat:@"id:%ld",(long)offer.offerId];
    addMarker.map = mapView_;
    addMarker.icon = [self image:[UIImage imageNamed:@"to.png"] scaledToSize:CGSizeMake(37.5f, 60.0f)];
    bounds = [bounds includingCoordinate:addMarker.position];
    
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
}

-(void)addJobsMarkers:(shipmentObject *)job :(GMSMapView *)mapView_ :(NSString *)myLocationLat :(NSString *)myLocationLon{
    
    
    GMSMarker *addMarker = [[GMSMarker alloc] init];
    addMarker.position = CLLocationCoordinate2DMake([job.origin_lat floatValue], [job.origin_lng floatValue]);
    addMarker.title = @"from";
    //addMarker.snippet = [key objectForKey:@"shipper_name"];
    addMarker.snippet = [NSString stringWithFormat:@"id:%ld",(long)job.shipmentsId];
    addMarker.map = mapView_;
    addMarker.icon = [self image:[UIImage imageNamed:@"from.png"] scaledToSize:CGSizeMake(37.5f, 60.0f)];
    
    CLLocationCoordinate2D firstLocation = addMarker.position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
    
    
    addMarker = [[GMSMarker alloc] init];
    addMarker.position = CLLocationCoordinate2DMake([job.destination_lat floatValue], [job.destination_lng floatValue]);
    addMarker.title = @"to";
    //addMarker.snippet = [key objectForKey:@"shipper_name"];
    addMarker.snippet = [NSString stringWithFormat:@"id:%ld",(long)job.shipmentsId];
    addMarker.map = mapView_;
    addMarker.icon = [self image:[UIImage imageNamed:@"to.png"] scaledToSize:CGSizeMake(37.5f, 60.0f)];
    bounds = [bounds includingCoordinate:addMarker.position];
    
    CLLocationCoordinate2D myLocation = CLLocationCoordinate2DMake([myLocationLat floatValue], [myLocationLon floatValue]);
    bounds = [bounds includingCoordinate:myLocation];
    
    //    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
}


//- (void)ShowAllMarkers
//{
//    CLLocationCoordinate2D firstLocation = ((GMSMarker *)markers.firstObject).position;
//    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
//
//    for (GMSMarker *marker in markers) {
//        bounds = [bounds includingCoordinate:marker.position];
//    }
//
//    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
//}

-(void)addRoute:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng :(GMSMapView *)mapView{
    _InternetConnection = [[InternetConnection alloc] init];
    
    if([_InternetConnection connected])
        
    {
        
        
        //get from google haking
        NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@,%@&destination=%@,%@&sensor=false&mode=driving&alternatives=true&key=AIzaSyAKJvVBiZFJjGTdb6CbJNqMnpBEE-ktmIo", originLat, originLng, destLat, destLng];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *myData = [[NSData alloc] init];
            _json = [[JSON alloc] init];
            myData = [_json getDataFrom2:url];
            NSDictionary *result = [[NSDictionary alloc] init];
            _json = [[JSON alloc] init];
            result = [_json FromJson:myData];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (![[result objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"] && (result != nil)) {
                    
                    
                    GMSMutablePath *path = [GMSMutablePath path];
                    NSArray *routes = [result objectForKey:@"routes"];
                    if (routes.count >0) {
                        
                        NSDictionary *firstRoute = [routes objectAtIndex:0];
                        NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
                        NSArray *steps = [leg objectForKey:@"steps"];
                        long stepIndex = 0;
                        
                        CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
                        
                        for (NSDictionary *step in steps) {
                            
                            NSDictionary *start_location = [step objectForKey:@"start_location"];
                            stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
                            
                            [path addCoordinate:[self coordinateWithLocation:start_location]];
                            
                            NSString *polyLinePoints = [[step objectForKey:@"polyline"] objectForKey:@"points"];
                            GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:polyLinePoints];
                            for (long p=0; p<polyLinePath.count; p++) {
                                [path addCoordinate:[polyLinePath coordinateAtIndex:p]];
                            }
                            
                            if ([steps count] == stepIndex){
                                NSDictionary *end_location = [step objectForKey:@"end_location"];
                                stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
                                [path addCoordinate:[self coordinateWithLocation:end_location]];
                            }
                        }
                        
                        GMSPolyline *polyline = nil;
                        polyline = [GMSPolyline polylineWithPath:path];
                        polyline.strokeColor = [UIColor colorWithRed:0.22 green:0.56 blue:0.73 alpha:1.0];
                        polyline.strokeWidth = 3.f;
                        polyline.map = mapView;
                    }
                }
            });
        });
    }
}


- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    
    return CLLocationCoordinate2DMake(latitude, longitude);
}


@end

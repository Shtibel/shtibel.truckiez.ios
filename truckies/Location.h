//
//  Location.h
//  truckiez
//
//  Created by Menachem Mizrachi on 23/01/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationManager.h"

@interface Location : NSObject<CLLocationManagerDelegate>
{
}
-(void) startLocation;

-(void)sendMyLocationAtFirstTime;


@end

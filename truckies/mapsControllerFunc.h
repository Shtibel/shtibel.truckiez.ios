//
//  mapsCobntrollerFunc.h
//  location
//
//  Created by Menachem Mizrachi on 03/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@import GoogleMaps;
#import "offerObject.h"
#import "shipmentObject.h"

@interface mapsControllerFunc : UIViewController<GMSMapViewDelegate>

-(NSString *)returnTimeLabel;
- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size;
-(void)sendToDB:(NSString *)latitudeLabel :(NSString *)longitudeLabel :(NSString *)driverStatus;

-(void)addOfferMarkers:(offerObject *)offer :(GMSMapView *)mapView_;
-(void)addRoute:(NSString *)originLat :(NSString *)originLng :(NSString *)destLat :(NSString *)destLng :(GMSMapView *)mapView;
-(void)addJobsMarkers:(shipmentObject *)job :(GMSMapView *)mapView_ :(NSString *)myLocationLat :(NSString *)myLocationLon;
@end

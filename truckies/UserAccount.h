//
//  UserAccount.h
//  truckies
//
//  Created by Menachem Mizrachi on 21/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"

@interface UserAccount : UIViewController<UIActionSheetDelegate, UITextViewDelegate, UITextFieldDelegate, TPFloatRatingViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *avilable;
@property (strong, nonatomic) IBOutlet UILabel *fullName;

- (IBAction)back:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *statusCircle;
//textfield
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UIView *phoneView;

@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UIView *emailView;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIView *passwordView;


@property (strong, nonatomic) IBOutlet UIView *updateInfoview;
@property (strong, nonatomic) IBOutlet UIView *topUpdateInfo;

@property (strong, nonatomic) IBOutlet UIView *updateImageView;
@property (strong, nonatomic) IBOutlet UIView *topUpdateImage;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UIButton *truckType;
@property (strong, nonatomic) IBOutlet UIView *truckTypeView;

- (IBAction)truckType:(id)sender;
- (IBAction)changePictureUser:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *updatePasswordView;
@property (strong, nonatomic) IBOutlet UIView *topUpdatePassword;
- (IBAction)updateInfo:(id)sender;
- (IBAction)updatePassword:(id)sender;


@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;
@property (strong, nonatomic) IBOutlet UIView *confirmPasswordView;
@property (strong, nonatomic) IBOutlet UITextField *passwordNew;
@property (strong, nonatomic) IBOutlet UIView *passwordNewView;


@property (strong, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *changePicture;
@property (strong, nonatomic) IBOutlet UIButton *saveInfoButton;
@property (strong, nonatomic) IBOutlet UIButton *savePasswordView;
//required fields
@property (strong, nonatomic) IBOutlet UILabel *reqFirstName;
@property (strong, nonatomic) IBOutlet UILabel *reqLastName;
@property (strong, nonatomic) IBOutlet UILabel *reqPhone;
@property (strong, nonatomic) IBOutlet UILabel *reqEmail;

- (IBAction)callSupport:(id)sender;  

@end

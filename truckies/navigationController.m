//
//  navigationController.m
//  truckies
//
//  Created by Menachem Mizrachi on 04/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "navigationController.h"
#import "navCustomCell.h"
#import "JSON.h"
#import "dbFunctions.h"
#import "CustomAlert.h"
#import "LocationObject.h"
@import CocoaLumberjack;
#import "InternetConnection.h"


@interface navigationController ()
{
    JSON *_json;
    dbFunctions *_dbFunctions;
    CustomAlert *_alert;
    InternetConnection *_InternetConnection;
    
    NSArray *arr;
    NSArray *arr2;
    
    NSLayoutConstraint *statusViewWidth;
    UIView *alert;
    CGFloat widthAlert;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation navigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //created by batya
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableTapped:)];
    [self.tableView addGestureRecognizer:tap];
    //created by batya
    
    arr = [[NSArray alloc] init];
    arr = @[@"Notifications", @"Account", @"Call Support", @"Sign out"];
    
    arr2 = [[NSArray alloc] init];
    arr2 = @[@"dashboard_notification.png", @"dashboard_account.png", @"dashboard_support.png", @"dashboard_signout.png"];
    [self loadPage];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSData *dataImageShipments = [[NSUserDefaults standardUserDefaults] objectForKey:@"userImageObject"];
    _userImage.image = [UIImage imageWithData:dataImageShipments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadPage{
    widthAlert = 200;
    if([[[NSUserDefaults standardUserDefaults]
         stringForKey:@"driverStatus"] intValue] == 4){
        _statusLabel.text = @"Unvailable";
        _statusView.backgroundColor = [UIColor colorWithRed:0.99 green:0.33 blue:0.23 alpha:1.0];
        
        statusViewWidth = [NSLayoutConstraint constraintWithItem:_statusUserView
                                                       attribute:NSLayoutAttributeWidth
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:103];
        [self.view addConstraint:statusViewWidth];
    }else{
        
        
        _statusLabel.text = @"Available";
        _statusView.backgroundColor = [UIColor colorWithRed:0.22 green:0.71 blue:0.29 alpha:1.0];
        
        statusViewWidth = [NSLayoutConstraint constraintWithItem:_statusUserView
                                                       attribute:NSLayoutAttributeWidth
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:83];
        [self.view addConstraint:statusViewWidth];
    }
    
    _userFullName.text = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"userName"];
    
    [_statusView.layer setCornerRadius:6.0f];
    [_userImage.layer setCornerRadius:40.0f];
    
    _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
    _userImage.clipsToBounds = YES;
    _userImage.contentMode = UIViewContentModeScaleAspectFill;
    _userImage.layer.borderColor = [UIColor colorWithRed:0.81 green:0.86 blue:0.87 alpha:1.0].CGColor;
    _userImage.layer.borderWidth = 1.0f;
    
    //rating
    
    float avg_rating = [[[NSUserDefaults standardUserDefaults]
                   stringForKey:@"avg_rating"] floatValue];
    
    self.ratingView.delegate = self;
    self.ratingView.delegate = self;
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"star_empty.png"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"star_full.png"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 1;
    self.ratingView.rating = avg_rating;
    self.ratingView.floatRatings = YES;
    
    UITapGestureRecognizer *userAccount = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userAccountClick)];
    userAccount.numberOfTapsRequired = 1;
    [_accountView setUserInteractionEnabled:YES];
    [_accountView addGestureRecognizer:userAccount];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"NavigationCell";
    navCustomCell *cell = (navCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.CellLabel.text = arr[indexPath.row];
    cell.cellIcon.image = [UIImage imageNamed:arr2[indexPath.row]];
    
    if (indexPath.row == 0) {
        _dbFunctions =[[dbFunctions alloc] init];
        int badge = [_dbFunctions returnBadgeNum];
        [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
        NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
        [badgeDefault setInteger:badge forKey:@"badge"];
        
        if (badge == 0) {
            cell.badgeView.hidden = YES;
        }else{
            [cell.badgeView.layer setCornerRadius:10.0f];
            cell.badgeTitle.text = [NSString stringWithFormat:@"%d",badge];
        }
    }else{
        cell.badgeView.hidden = YES;
    }
    
//    if (indexPath.row == 1) {
//        cell.seperatorTop.hidden = YES;
//    }
    if (indexPath.row != 3) {
        cell.seperatorBottom.hidden = YES;
    }
    if (indexPath.row == 3) {
        CGFloat selfHeight=cell.contentView.bounds.size.height;
        NSLog(@"height:%f",selfHeight);
        UILabel *version = [[UILabel alloc] initWithFrame:CGRectMake(10,selfHeight - 70,120,20)];
        version.text = [NSString stringWithFormat:@"Version: %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
        version.font = [UIFont systemFontOfSize:14];
        version.textColor = [UIColor whiteColor];
        [cell.contentView addSubview:version];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    if (indexPath.row == 3) {
        return height - 322;
    }else{
        return 44;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"NavigationCell";
    navCustomCell *cell = (navCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"openPushNotification" sender:self];
        
    }
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"openUserAccount" sender:self];
    }
    if (indexPath.row == 2) {
        
    }
    if (indexPath.row == 3) {
        
        UIButton *doSomething = [[UIButton alloc]initWithFrame:CGRectMake(20,0,widthAlert/2 - 25,35)];
        [doSomething addTarget:self
                        action:@selector(signOut)
              forControlEvents:UIControlEventTouchUpInside];
        
        alert = [[UIView alloc] init];
        _alert = [[CustomAlert alloc] init];
        
        alert = [_alert alertView:doSomething :nil :@"Notice" :@"Are you sure you want to logout?" :@"OK" :@"CANCEL" :@"" :nil :self.view :widthAlert];
    }
}
//created by batya
- (void)tableTapped:(UITapGestureRecognizer *)tap
{

    CGPoint location = [tap locationInView:self.tableView];
    printf("x %f y %f\n",location.x,location.y);
    NSIndexPath *path = [self.tableView indexPathForRowAtPoint:location];
    
    if(path)
    {
        if (path.row == 2) {
            DDLogVerbose(@"supportCall");
            NSString *st = [[NSString stringWithFormat:@"telprompt:%@",[[NSUserDefaults standardUserDefaults]stringForKey:@"app_support_phone"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSURL *phoneUrl = [NSURL URLWithString:st];
            
            if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            } else
            {
                DDLogVerbose(@"cannnot call");
            }

        }
        if (path.row==3) {
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            if (location.y>screenHeight-54) {
                [self tableView:self.tableView didSelectRowAtIndexPath:path];

            }
        }
        else{
            // tap was on existing row, so pass it to the delegate method
            [self tableView:self.tableView didSelectRowAtIndexPath:path];
        }
    }
    else
    {
        // handle tap on empty space below existing rows however you want
    }
}
//created by batya


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    static NSString *cellIdentifier = @"Cell";
//
//
//    dashboardTableCell *cell = (dashboardTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.titleCell.text = menuTitle[indexPath.row];
//    cell.imageCell.image = [UIImage imageNamed:menuImage[indexPath.row]];
//
//    return cell;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)closeAlert{
    [alert removeFromSuperview];
}

-(void)signOut{
    _InternetConnection = [[InternetConnection alloc] init];
    
    if([_InternetConnection connected])
        
    {
        

    //set to service
    _json = [[JSON alloc] init];
    NSData *myData = [[NSData alloc] init];
    myData = [_json getDataFrom:[NSString stringWithFormat:@"tr-api/?action=logOut&user_id=%@&driver_application_status_id=1",[[NSUserDefaults standardUserDefaults] stringForKey:@"userID"]]];
    if (!myData) {
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }else{
        NSDictionary *dic = [[NSDictionary alloc] init];
        _json = [[JSON alloc] init];
        dic = [_json FromJson:myData];
        if ([[dic objectForKey:@"errType"] isEqualToString:@""]) {
            //stop set location
            
            LocationObject *location = [LocationObject sharedInstance];
            [location.locationManager stopUpdatingLocation];
            //delete all notification
            _dbFunctions = [[dbFunctions alloc] init];
            [_dbFunctions deleteAllPush];
            
            //set badge 0
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            NSUserDefaults *badgeDefault = [NSUserDefaults standardUserDefaults];
            [badgeDefault setInteger:0 forKey:@"badge"];
            
            [badgeDefault synchronize];
            
            //save nil on default checkLogin
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"checkLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"driverStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self performSegueWithIdentifier:@"logOut" sender:self];
            
        }else{
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        }
    }
    }else{
        
        _alert = [[CustomAlert alloc] init];
        
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
        
        
    }
    

}

-(void)userAccountClick{
    [self performSegueWithIdentifier:@"openUserAccount" sender:self];
}

@end

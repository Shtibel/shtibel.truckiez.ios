//
//  DesktopFunc.h
//  truckies
//
//  Created by Menachem Mizrachi on 03/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@import GoogleMaps;

@interface DesktopFunc : UIViewController<GMSMapViewDelegate>

-(NSMutableArray *)loadContent;
-(NSMutableArray *)loadContentWhenScroll:(int)select :(NSString *)searchURL;

-(NSURL*)GetGoogleStaticMap:(NSString *)latitude :(NSString *)longitude :(NSString *)latitude2 :(NSString *)longitude2  :(NSString *)path;

-(void)sendToDB:(NSString *)latitudeLabel :(NSString *)longitudeLabel :(NSString *)driverStatus;

-(NSString *)dayString:(NSString *)dateObject;

-(NSMutableArray *)loadContentOffers;
-(NSMutableArray *)loadContentOffersWhenScroll:(int)select :(NSString *)searchURL;

-(NSMutableArray *)searchOffer:(NSString *)country :(NSString *)city :(NSString *)street :(NSString *)pallet :(NSString *)box :(NSString *)truckload :(NSString *)fromDate :(NSString *)toDate :(NSString *)keyword;

-(NSMutableArray *)searchJob:(NSString *)country :(NSString *)city :(NSString *)street :(NSString *)pallet :(NSString *)box :(NSString *)truckload :(NSString *)fromDate :(NSString *)toDate :(NSString *)keyword;

-(NSMutableArray *)loadJobsToChngeStatus;
-(NSMutableArray *)changeJobsStatus:(NSMutableArray *)arr;

@end

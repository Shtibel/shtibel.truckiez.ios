//
//  Location.m
//  truckiez
//
//  Created by Menachem Mizrachi on 23/01/2017.
//  Copyright © 2017 Menachem Mizrachi. All rights reserved.
//

#import "Location.h"
#import "LocationObject.h"
#import "mapsControllerFunc.h"
#import "DesktopFunc.h"
@import CocoaLumberjack;
#import "InternetConnection.h"

@interface Location ()
{
    mapsControllerFunc *_mapFunc;
    DesktopFunc *_desktopFunc;
    bool isInitialized;
    LocationObject *locationO;
    InternetConnection *_InternetConnection;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation Location
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

-(void) startLocation
{
    locationO = [LocationObject sharedInstance];
    locationO.locationManager = [[CLLocationManager alloc] init];
    locationO.locationManager.delegate = self;
    
    locationO.meters = 50;
    
    if( [self requestLocationPermission]){
        [self startLocationManager];
    }else{
        DDLogVerbose(@"does no have permission: request permission");
        
        
        
        [locationO.locationManager requestAlwaysAuthorization];
    }
}
-(bool)requestLocationPermission{
    
    DDLogVerbose(@"check permission");
    //    location.locationManager = [[CLLocationManager alloc] init];
    //    location.locationManager.delegate = self;
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways){
        
        
        return false;
        
    }else{
        DDLogVerbose(@"have already permission");
        return true;
    }
    //    return false;
}
-(void)startLocationManager{
    if (isInitialized==false){
        DDLogVerbose(@"startLocationManager");
    //locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    //locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationO.locationManager startMonitoringSignificantLocationChanges];
    [locationO.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [locationO.locationManager setDistanceFilter:50];
    [locationO.locationManager startUpdatingLocation];
    [locationO.locationManager startUpdatingHeading];
    
    isInitialized = true;
    }
}
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking granting location access!");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User denied location access request!!");
            
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            NSLog(@"got permission location so start startSetMyLocation!!");
            
            [self startLocationManager];
        } break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{

    CLLocation *currentLocation = newLocation;
//    currentLocation.speed = 20
    float testSpeed = currentLocation.speed/3.6;
    
    if (testSpeed >= 15 && locationO.meters == 50) {
        locationO.meters = 20;
//        [locationO.locationManager startMonitoringSignificantLocationChanges];
//        [locationO.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
        [locationO.locationManager setDistanceFilter:20];
        [locationO.locationManager startUpdatingLocation];
//        [locationO.locationManager startUpdatingHeading];
        DDLogVerbose(@"change state speed1:%f meters:%f",testSpeed,locationO.meters);
   }else if (testSpeed < 15 && locationO.meters == 20){
       locationO.meters = 50;
//        [locationO.locationManager startMonitoringSignificantLocationChanges];
//        [locationO.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
       [locationO.locationManager setDistanceFilter:50];
        [locationO.locationManager startUpdatingLocation];
//        [locationO.locationManager startUpdatingHeading];
       DDLogVerbose(@"change state speed2:%f meters:%f",testSpeed,locationO.meters);

    }
    
    
    if (currentLocation != nil) {
        DDLogVerbose(@"if current location != nil");
        locationO.longitude = [NSString stringWithFormat:@"%.4f", currentLocation.coordinate.longitude];
        locationO.latitude = [NSString stringWithFormat:@"%.4f", currentLocation.coordinate.latitude];
        
        [[NSUserDefaults standardUserDefaults] setObject:locationO.latitude forKey:@"latitudeLabel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:locationO.longitude forKey:@"longitudLabel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        float a1 = [[[NSUserDefaults standardUserDefaults] stringForKey:@"latitudeLabel"] floatValue];
        float a2 = currentLocation.coordinate.latitude;
        float b1 = [[[NSUserDefaults standardUserDefaults]
                     stringForKey:@"longitudLabel"] floatValue];
        float b2 = currentLocation.coordinate.longitude;
        
//        if (((a1 - a2) >0.0004) || ((a1 - a2) < -0.0004) || ((b1 - b2) >0.0004) || ((b1 - b2) < -0.0004)) {
            //go to background
        
        if (locationO.latitude != nil && locationO.longitude != nil) {
            _InternetConnection = [[InternetConnection alloc] init];
            
            if([_InternetConnection connected])
                
            {
                DDLogVerbose(@"send mylocation to server");
                _mapFunc = [[mapsControllerFunc alloc] init];
                [_mapFunc sendToDB:locationO.latitude :locationO.longitude :[[NSUserDefaults standardUserDefaults]
                                                                           stringForKey:@"driverStatus"]];
                
            }
            
        }
        
            
            [[NSUserDefaults standardUserDefaults] setObject:locationO.latitude forKey:@"latitudeLabel"];
            [[NSUserDefaults standardUserDefaults] setObject:locationO.longitude forKey:@"longitudLabel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if (locationO.jobsArray) {
                DDLogVerbose(@"changeJobsStatus function");
                _desktopFunc = [[DesktopFunc alloc] init];
                [_desktopFunc changeJobsStatus:locationO.jobsArray];
            }
//        }
        
        if (locationO.jobsArray) {
            DDLogVerbose(@"changeJobsStatus function");
            _desktopFunc = [[DesktopFunc alloc] init];
            [_desktopFunc changeJobsStatus:locationO.jobsArray];
        }
        
        
    }
    
    
    
    // Stop Location Manager
    //[locationManager stopUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
     NSLog(@"didFailWithError: %@", error);
}

-(void)sendMyLocationAtFirstTime{
    _mapFunc = [[mapsControllerFunc alloc] init];
    [_mapFunc sendToDB:[[NSUserDefaults standardUserDefaults]
                        stringForKey:@"latitudeLabel"] :[[NSUserDefaults standardUserDefaults]
                                                         stringForKey:@"longitudLabel"] :[[NSUserDefaults standardUserDefaults]
                                                                 stringForKey:@"driverStatus"]];
}

-(void)stopLocation{
    
}


@end

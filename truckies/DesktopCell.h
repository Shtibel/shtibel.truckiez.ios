//
//  DesktopCell.h
//  truckies
//
//  Created by Menachem Mizrachi on 03/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesktopCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UIView *mapCell;
@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UIView *pickupView;
@property (strong, nonatomic) IBOutlet UIView *statusColor;
@property (strong, nonatomic) IBOutlet UILabel *statusTitle;
@property (strong, nonatomic) IBOutlet UIImageView *pickupImage;
@property (strong, nonatomic) IBOutlet UILabel *pickupText;
@property (strong, nonatomic) IBOutlet UIView *dropoffView;
@property (strong, nonatomic) IBOutlet UILabel *dropoffText;
@property (strong, nonatomic) IBOutlet UIView *jobContentView;
@property (strong, nonatomic) IBOutlet UIImageView *mapImage;
@property (strong, nonatomic) IBOutlet UIView *priceView;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UIView *codeView;
@property (strong, nonatomic) IBOutlet UILabel *codeLabel;

@end

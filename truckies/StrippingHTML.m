//
//  StrippingHTML.m
//  NirApp
//
//  Created by Menachem Mizrachi on 28/01/2016.
//  Copyright © 2016 Dan Dushinsky. All rights reserved.
//

#import "StrippingHTML.h"

@interface StrippingHTML ()

@end

@implementation StrippingHTML

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSString *) stringByStrippingHTML:(NSString *)htmlStr{
    
    
    NSRange r;
    NSString *s = htmlStr;
//    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
//        s = [s stringByReplacingCharactersInRange:r withString:@""];
    
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }

    
   NSString *descString = [s stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    descString = [descString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"״"];
    descString =[descString stringByReplacingOccurrencesOfString:@"&ndash;" withString:@"-"];
    descString =[descString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];

    return descString;
}


@end

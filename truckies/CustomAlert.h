//
//  CustomAlert.h
//  truckies
//
//  Created by Menachem Mizrachi on 30/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <CoreGraphics/CoreGraphics.h>

@interface CustomAlert : UIViewController

-(UIView*)loadTextAndPhone:(NSString*)str :(NSString *)phoneNumber :(CGRect)sizeView :(NSString *)actionString;

-(UIView *)alertView:(UIButton *)doSomething :(UIButton *)moreButton :(NSString *)alertTitle :(NSString *)alertMessage :(NSString *)alertDo :(NSString *)alertClose :(NSString *)alertMoreButton :(UIView *)loadView :(UIView *)viewController :(CGFloat)widthScreen;
-(UIView *)alertview2:(UIButton *)doSomething :(NSString*)alertTitle :(NSString*)alertMessage :(NSString*)buttonTitle :(UIView *)viewController;
@end

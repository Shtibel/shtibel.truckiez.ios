//
//  Jobs.h
//  truckies
//
//  Created by Menachem Mizrachi on 07/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@import GoogleMaps;
#import "shipmentObject.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "TPFloatRatingView.h"

@interface Jobs : UIViewController<GMSMapViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, TPFloatRatingViewDelegate, UITextViewDelegate>

@property shipmentObject *job;

@property (strong, nonatomic) IBOutlet UILabel *jobCode;
- (IBAction)back:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *map;

//open details
@property (strong, nonatomic) IBOutlet UIView *showDetails;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIView *contentView;
//route
@property (strong, nonatomic) IBOutlet UIView *routeView;
@property (strong, nonatomic) IBOutlet UIView *topRoute;
@property (strong, nonatomic) IBOutlet UIView *originContentView;
@property (strong, nonatomic) IBOutlet UIView *originTop;
@property (strong, nonatomic) IBOutlet UIView *originMiddle;
@property (strong, nonatomic) IBOutlet UIView *originBottom;
@property (strong, nonatomic) IBOutlet UIImageView *originImage;
@property (strong, nonatomic) IBOutlet UILabel *originText;
@property (strong, nonatomic) IBOutlet UIImageView *pickupImage;
@property (strong, nonatomic) IBOutlet UILabel *pickupText;
@property (strong, nonatomic) IBOutlet UIImageView *pickupContactImage;
@property (strong, nonatomic) IBOutlet UILabel *pickupContactText;


@property (strong, nonatomic) IBOutlet UIView *destinationContentView;
@property (strong, nonatomic) IBOutlet UIView *destinationTop;
@property (strong, nonatomic) IBOutlet UIView *destinationMiddle;
@property (strong, nonatomic) IBOutlet UIView *destinationBottom;
@property (strong, nonatomic) IBOutlet UIImageView *destinationImage;
@property (strong, nonatomic) IBOutlet UILabel *destinationText;
@property (strong, nonatomic) IBOutlet UIImageView *dropoffImage;
@property (strong, nonatomic) IBOutlet UILabel *dropoffText;
@property (strong, nonatomic) IBOutlet UIImageView *dropoffContactImage;


//load
@property (strong, nonatomic) IBOutlet UIView *loadContentView;
@property (strong, nonatomic) IBOutlet UIView *loadTop;
@property (strong, nonatomic) IBOutlet UIView *loadBottom;

//special request
@property (strong, nonatomic) IBOutlet UIView *SRview;
@property (strong, nonatomic) IBOutlet UIView *SRTop;
@property (strong, nonatomic) IBOutlet UIView *SRBottom;

//comments
@property (strong, nonatomic) IBOutlet UIView *commentsView;
@property (strong, nonatomic) IBOutlet UIView *commentsTop;
@property (strong, nonatomic) IBOutlet UIView *commentsBottom;
@property (strong, nonatomic) IBOutlet UILabel *commentText;


@property (strong, nonatomic) IBOutlet UIView *detailsTap;

@property (strong, nonatomic) IBOutlet UIView *changeStatus;
@property (strong, nonatomic) IBOutlet UILabel *changeStatusText;

@property (strong, nonatomic) IBOutlet UIImageView *myLoactionButton;

@property (strong, nonatomic) IBOutlet UIButton *signature;
- (IBAction)signatureClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bol;
- (IBAction)bolClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *routeButton;
@property (strong, nonatomic) IBOutlet UIImageView *shareButton;

//signature
@property (strong, nonatomic) IBOutlet UIView *signatureView;
@property (strong, nonatomic) IBOutlet UIView *drawImageView;
@property (strong, nonatomic) IBOutlet UIImageView *drawImage;
- (IBAction)saveSignature:(id)sender;
- (IBAction)clearSignature:(id)sender;
- (IBAction)closeSignature:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *nameRecived;
@property (strong, nonatomic) IBOutlet UILabel *signHere;
@property (strong, nonatomic) IBOutlet UIButton *signatureSaveButton;
@property (strong, nonatomic) IBOutlet UIButton *signatureClearButton;
@property (strong, nonatomic) IBOutlet UIButton *signatureCloseButton;


//bol
@property (strong, nonatomic) IBOutlet UIView *bolView;
@property (strong, nonatomic) IBOutlet UIView *bolImageView;
@property (strong, nonatomic) IBOutlet UIImageView *bolImage;
- (IBAction)saveBOL:(id)sender;
- (IBAction)captureBOL:(id)sender;
- (IBAction)closeBOL:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contactView;
@property (strong, nonatomic) IBOutlet UIView *dropofContactView;

@property (strong, nonatomic) IBOutlet UIView *addImagesView;

@property (strong, nonatomic) IBOutlet UIButton *bolSaveButton;
@property (strong, nonatomic) IBOutlet UIButton *bolCaptureButton;
@property (strong, nonatomic) IBOutlet UIButton *bolCloseButton;

//show details
@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UILabel *statusName;
@property (strong, nonatomic) IBOutlet UIView *statusCircleColor;
@property (strong, nonatomic) IBOutlet UIView *pickupView;
@property (strong, nonatomic) IBOutlet UIImageView *pickupImg;
@property (strong, nonatomic) IBOutlet UILabel *pickupViewText;
@property (strong, nonatomic) IBOutlet UIView *jobContentLoad;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UIView *priceView;

@property (strong, nonatomic) IBOutlet UIView *shipperDetailsView;
@property (strong, nonatomic) IBOutlet UIView *shipperDetailsBottom;
@property (strong, nonatomic) IBOutlet UIView *shipperDeatilsTop;


- (IBAction)handlePanShowDetails:(UIPanGestureRecognizer *)recognizer;

@property int checkLastController;
- (IBAction)callSupport:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *truckName;

@property (strong, nonatomic) IBOutlet UIImageView *camera;
@property (strong, nonatomic) IBOutlet UIImageView *notes;
@property (strong, nonatomic) IBOutlet UIButton *dropoffSignature;

//confirmation
@property (strong, nonatomic) IBOutlet UIView *confirmation1;
@property (strong, nonatomic) IBOutlet UIView *confirmation2;
@property (strong, nonatomic) IBOutlet UIView *confirmation3;
@property (strong, nonatomic) IBOutlet UIButton *con1pickup;
@property (strong, nonatomic) IBOutlet UIButton *con2pickup;
@property (strong, nonatomic) IBOutlet UIButton *con2dropoff;
@property (strong, nonatomic) IBOutlet UIButton *con3pickup;
@property (strong, nonatomic) IBOutlet UIButton *con3dropoff;
@property (strong, nonatomic) IBOutlet UIButton *con3pod;

- (IBAction)conPickup:(id)sender;
- (IBAction)conDropoff:(id)sender;
- (IBAction)conPOD:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bolCloseButton2;

@property (strong, nonatomic) IBOutlet UIView *cancelPODView;
@property (strong, nonatomic) IBOutlet UIView *captureClickView;
@property (strong, nonatomic) IBOutlet UILabel *captureClickLabel;


@property (strong, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet TPFloatRatingView *rating;
@property (strong, nonatomic) IBOutlet UIView *navView;



@end

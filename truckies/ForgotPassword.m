//
//  ForgotPassword.m
//  truckies
//
//  Created by Menachem Mizrachi on 25/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import "ForgotPassword.h"
#import "Login.h"
#import "JSON.h"
#import "InternetConnection.h"
#import "CustomAlert.h"
#import "LoaderImg.h"
#import "LoginSettings.h"
#import "Location.h"
#import "Wellcome.h"
#import "StaticVars.h"
@import CocoaLumberjack;

@interface ForgotPassword ()
{
    JSON *_json;
    InternetConnection *_InternetConnection;
    CustomAlert *_alert;
    LoaderImg *_loaderIMG;
    LoginSettings *_ls;
    Location *_location;
    NSString *emailStr;
    
    NSLayoutConstraint *truckiesLogoHeigh;
}

@end

static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@implementation ForgotPassword

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)loadPage{
    
    _splashScreen.image = [UIImage imageNamed:_imgName];
    UITapGestureRecognizer *Tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    Tap.numberOfTapsRequired = 1;
    [_splashScreen setUserInteractionEnabled:YES];
    [_splashScreen addGestureRecognizer:Tap];
    
    [_email.layer setCornerRadius:5.0f];
    [_sendButton.layer setCornerRadius:5.0f];
    
    _splashScreen.clipsToBounds = YES;
    _splashScreen.contentMode = UIViewContentModeScaleAspectFill;
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    truckiesLogoHeigh = [NSLayoutConstraint constraintWithItem:_truckiesLogo
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                      constant:(width - 180)/2];
    [self.view addConstraint:truckiesLogoHeigh];
    _explainText.text = @"A password has been sent to you in SMS message";
    _explainText.lineBreakMode = NSLineBreakByWordWrapping;
    _explainText.numberOfLines = 0;
}

- (IBAction)backLogin:(id)sender{
    //    [self performSegueWithIdentifier:@"backToLogin" sender:self];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
- (IBAction)needSignUp:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/",[StaticVars url]]]];
}

- (IBAction)sendEmail:(id)sender {
    [self.view endEditing:YES];
    //check internet connection
    _InternetConnection = [[InternetConnection alloc] init];
    if([_InternetConnection connected])
    {
        if(![_email.text isEqualToString:@""]){
            
            //show loader
            UIView *backgroundView;
            _loaderIMG = [[LoaderImg alloc] init];
            backgroundView = [_loaderIMG loader:backgroundView];
            [self.view addSubview:backgroundView];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                _ls = [[LoginSettings alloc] init];
                int check =[_ls loginSetting:_phone :_email.text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    if (check == 1) {
                        DDLogVerbose(@"if first login go to wellcomeVC");
                        DDLogVerbose(@"start with location");
                        _location = [[Location alloc] init];
                        [_location sendMyLocationAtFirstTime];
                        _location = [[Location alloc] init];
                        [_location startLocation];
                        [self performSegueWithIdentifier:@"openWellcome" sender:self];
                    }else if(check == 2){
                        DDLogVerbose(@"if not first login go to desktopeVC");
                        _location = [[Location alloc] init];
                        [_location sendMyLocationAtFirstTime];
                        [self performSegueWithIdentifier:@"openDesktop" sender:self];
                    }else{
                        DDLogVerbose(@"if back error from server dictionary show alert: Phone or password are not valid");
                        
                        _alert = [[CustomAlert alloc] init];
                        [_alert alertView:nil :nil :@"Notice" :@"Password is not correct" :@"" :@"OK" :@"" :nil :self.view :0];
                        
                    }

                    [backgroundView removeFromSuperview];
                });
                
            });
            
        }else{
            _alert = [[CustomAlert alloc] init];
            [_alert alertView:nil :nil :@"Notice" :@"Please enter the password" :@"" :@"OK" :@"" :nil :self.view :0];
        }
    }else{
        _alert = [[CustomAlert alloc] init];
        [_alert alertView:nil :nil :@"Notice" :@"Internet connection problem, please try again" :@"" :@"OK" :@"" :nil :self.view :0];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    textField.text = @"";
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //validate
    [textField resignFirstResponder];
    return YES;
}

-(void)backToLogin{
    UIView *backgroundView;
    _loaderIMG = [[LoaderImg alloc] init];
    backgroundView = [_loaderIMG loader:backgroundView];
    [self.view addSubview:backgroundView];
    
    [self performSegueWithIdentifier:@"backToLogin" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"openWellcome"]) {
        Wellcome *Wellcome = [segue destinationViewController];
        Wellcome.imgName = _imgName;
    }
}

-(void)closeKeyboard{
    [self.view endEditing:YES];
}

@end

//
//  navCustomCell.h
//  truckies
//
//  Created by Menachem Mizrachi on 29/08/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface navCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *CellLabel;
@property (strong, nonatomic) IBOutlet UIView *viewCell;
@property (strong, nonatomic) IBOutlet UIView *selectCellView;
@property (strong, nonatomic) IBOutlet UIImageView *cellIcon;

@property (strong, nonatomic) IBOutlet UIView *badgeView;
@property (strong, nonatomic) IBOutlet UILabel *badgeTitle;
@property (strong, nonatomic) IBOutlet UIView *seperatorTop;
@property (strong, nonatomic) IBOutlet UIView *seperatorBottom;
//float tapx;

@end

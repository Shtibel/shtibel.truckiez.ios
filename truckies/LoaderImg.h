//
//  LoaderImg.h
//  truckies
//
//  Created by Menachem Mizrachi on 21/09/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface LoaderImg : UIViewController
-(UIView *)loader:(UIView *)backgroundView;
@end

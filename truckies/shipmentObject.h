//
//  shipmentObject.h
//  location
//
//  Created by Menachem Mizrachi on 04/07/2016.
//  Copyright © 2016 Menachem Mizrachi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shipmentObject : NSObject

@property NSInteger shipmentsId;
@property NSInteger status_id;
@property NSString *status_name;
@property NSString *status_name_circle_color;
@property NSString *origin_lat;
@property NSString *origin_lng;
@property NSString *destination_lat;
@property NSString *destination_lng;
@property NSString *shipment_carrier_payout_text;
@property NSInteger box_num;
@property NSInteger pallet_num;
@property NSInteger truckload_num;
@property NSString *shipment_special_request;
@property NSString *shipment_special_request_with_document;
@property NSString *pickup_date;
@property NSString *dropoff_date;
@property NSString *pickup_from_time;
@property NSString *pickup_till_time;
@property NSString *dropoff_from_time;
@property NSString *dropoff_till_time;
@property NSInteger recycle;
@property NSInteger driver_id;
@property NSString *total_load_weight;
@property NSInteger total_driving_distance;
@property NSString *pickup_in_time;
@property NSString *dropoff_in_time;
@property NSString *origin_address;
@property NSString *origin_address_name;
@property NSString *destination_address;
@property NSString *destination_address_name;
@property NSString *original_pickup_date;
@property NSString *comments;
@property NSString *shipment_load;
@property NSString *origin_contact_name;
@property NSString *origin_contact_phone;
@property NSString *destination_contact_name;
@property NSString *destination_contact_phone;
@property NSString *bol_url;
@property NSString *shipment_google_root;
@property NSString *completed_at;
@property NSString *truck_type_name;
@property NSString *shipper_name;
@property NSString *shipper_phone;
@property NSString *origin_special_site_instructions;
@property NSString *destination_special_site_instructions;
@property NSString *load_weight_type;
@property NSString *driver_notes;

@property int truck_max_weight;
@property NSString *signature_receiver_name_pickup;
@property NSString *signature_receiver_name;
@property NSString *confirmation_phone_pickup;
@property NSString *confirmation_phone_dropoff;
@property NSString *confirmation_user_pickup;
@property NSString *confirmation_user_dropoff;
@property NSArray *load_types_pickup_str;
@property NSArray *load_types_pickup_quantity_str;
@property NSArray *load_types_dropoff_str;
@property NSArray *load_types_dropoff_quantity_str;
@property NSString *confirmation_date_pickup;
@property NSString *confirmation_date_dropoff;
@property NSString *signature_url_pickup;
@property NSString *signature_url;
@property NSString *total_weight_pickup;
@property NSString *total_weight_dropoff;
@property int rating;
@property NSString *feedback;


-(shipmentObject*)returnJob:(NSDictionary *)key;
@end
